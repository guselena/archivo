﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Archivo
{
    public partial class frmresumen : Form
    {
        int paginasimpresas;
        public frmresumen()
        {
            InitializeComponent();
        }


        private void frmresumen_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'archivoDataSet.cajas' table. You can move, or remove it, as needed.
            this.cajasTableAdapter.Fill(this.archivoDataSet.cajas);
            // TODO: This line of code loads data into the 'archivoDataSet.operaciones' table. You can move, or remove it, as needed.
            this.operacionesTableAdapter.Fill(this.archivoDataSet.operaciones);
            // TODO: This line of code loads data into the 'archivoDataSet.clientes' table. You can move, or remove it, as needed.
            this.clientesTableAdapter.Fill(this.archivoDataSet.clientes);

            for (int i = 0; i <= archivoDataSet.clientes.Rows.Count - 1; i++)
            {
                if (lbNombreEmpresa.FindString(clientesDataGridView.Rows[i].Cells[1].Value.ToString()) == -1)
                    lbNombreEmpresa.Items.Add(clientesDataGridView.Rows[i].Cells[1].Value.ToString());
            }
            tbNombreClienteFiltro.AutoCompleteCustomSource.Clear();
            foreach (object o in lbNombreEmpresa.Items)
                tbNombreClienteFiltro.AutoCompleteCustomSource.Add(o.ToString());


        }

        private void btnFiltrar_Click(object sender, EventArgs e)
        {
            DataView dv = archivoDataSet.operaciones.DefaultView;
            string filter = "Nocliente > 0";
            dv.RowFilter = filter;      // Carga todas las operaciones
            operacionesBindingSource.DataSource = dv;
            if (cbNombreCliente.Checked && tbNombreClienteFiltro.Text != "")
            {
                DataView dv2 = archivoDataSet.clientes.DefaultView;
                string filter2 = "Nocliente > 0";
                dv2.RowFilter = filter2;
                clientesBindingSource.DataSource = dv2;   // Carga todos los clientes
                filter2 = "NombreEmpresa LIKE '*" + tbNombreClienteFiltro.Text + "*'";
                dv2.RowFilter = filter2;
                clientesBindingSource.DataSource = dv2;   // Deja solo la fila(s) del cliente
                string nocliente2 = clientesDataGridView.Rows[0].Cells[0].Value.ToString();   // Extrae el Nº de cliente
                filter = "Nocliente = " + nocliente2;   // filtra operaciones
            }
            if (cbNoCliente.Checked && tbNoClienteFiltro.Text != "")
            {
                DataView dv2 = archivoDataSet.clientes.DefaultView;
                string filter2 = "Nocliente > 0";
                dv2.RowFilter = filter2;
                clientesBindingSource.DataSource = dv2;   // Carga todos los clientes
                filter2 = "Nocliente = " + tbNoClienteFiltro.Text;
                dv2.RowFilter = filter2;
                clientesBindingSource.DataSource = dv2;   // Deja solo la fila(s) del cliente
                filter = "Nocliente = " + tbNoClienteFiltro.Text;
            }

            if (cbNoCliente.Checked || cbNombreCliente.Checked) // Se filtro por Nº de cliente o por nombre de cliente?
            {
                DataView dv3 = archivoDataSet.cajas.DefaultView;
                string filter3 = "Nocliente >0";
                dv3.RowFilter = filter3;
                cajasBindingSource.DataSource = dv3;
                filter3 = filter;
                dv3.RowFilter = filter3;
                cajasBindingSource.DataSource = dv3; // Se filtran cajas para calcular cantidad total;
            }




            if (cbFecha.Checked)
                filter += " AND Fecha >= '" + dtFechaDesde.Value.ToShortDateString() + " 00:00:00'  AND Fecha <= '" + dtFechaHasta.Value.ToShortDateString() + " 23:59:59'";
            if (cbTipo.Checked && cbTipo.Text!="")
                filter += " AND Tipo LIKE '*" + cmbTipo.Text + "*'";



            dv.RowFilter = filter;
            operacionesBindingSource.DataSource = dv;
            operacionesDataGridView.Sort(operacionesDataGridView.Columns[2], ListSortDirection.Ascending);




        }

            
        
        private void cbNoCliente_CheckedChanged(object sender, EventArgs e)
        {
            if (cbNoCliente.Checked)
            {
                tbNoClienteFiltro.Enabled = true;
                cbNombreCliente.Checked = false;
            }
            else
            {
                tbNoClienteFiltro.Enabled = false;
            }
        }

        private void cbNombreCliente_CheckedChanged(object sender, EventArgs e)
        {
            if (cbNombreCliente.Checked)
            {
                tbNombreClienteFiltro.Enabled = true;
                cbNoCliente.Checked = false;
            }
            else
                tbNombreClienteFiltro.Enabled = false;
        }

        private void cbFecha_CheckedChanged(object sender, EventArgs e)
        {
            if (cbFecha.Checked)
            {
                dtFechaDesde.Enabled = true;
                dtFechaHasta.Enabled = true;
            }
            else
            {
                dtFechaDesde.Enabled = false;
                dtFechaHasta.Enabled = false;
            }
        }

        private void cbTipo_CheckedChanged(object sender, EventArgs e)
        {
            if (cbTipo.Checked)
                cmbTipo.Enabled = true;
            else
                cmbTipo.Enabled = false;
        }

        private void btnimprimir_Click(object sender, EventArgs e)
        {
            btnFiltrar_Click(this, e);
            printDialog1.Document = printDocument1;
            string nores = ((int)udResNo.Value).ToString("D4");
            printDocument1.DocumentName = "R" + nores + " (" + dtfechafact.Value.Day.ToString("D2") + "-" + dtfechafact.Value.Month.ToString("D2") + "-" + dtfechafact.Value.Year.ToString()+")";
            if (printDialog1.ShowDialog() == DialogResult.OK)
            {
                paginasimpresas = 0;
                printDocument1.Print();
            }
        }

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            Pen penlinea = new Pen(Color.Black, 0.5F);
            e.Graphics.PageUnit = GraphicsUnit.Millimeter;
            Graphics Canvas = e.Graphics;
            Canvas.PageUnit = GraphicsUnit.Millimeter;
            float dpiCorrectionX = Canvas.DpiX / 100;
            float dpiCorrectionY = Canvas.DpiY / 100;
            Point[] Points = new Point[2];


            Points[0] = new Point(Convert.ToInt32(e.PageBounds.X * dpiCorrectionX), Convert.ToInt32(e.PageBounds.Y * dpiCorrectionY));
            Points[1] = new Point(Convert.ToInt32(e.PageBounds.Width * dpiCorrectionX), Convert.ToInt32(e.PageBounds.Height * dpiCorrectionY));
            Canvas.TransformPoints(System.Drawing.Drawing2D.CoordinateSpace.Page, System.Drawing.Drawing2D.CoordinateSpace.Device, Points);
            Rectangle Bounds = new Rectangle(Points[0].X, Points[0].Y, Points[1].X - 20, Points[1].Y - 20);
            float mizq = Bounds.Left;
            float msup = Bounds.Top;
            float anchopag = Bounds.Width;
            float altopag = Bounds.Height;
            Font letratitulo = new Font("Arial", 23);
            Font letracodigo = new Font("Arial", 12);
            Font letradatobold = new Font("Arial", 12, FontStyle.Bold);


            int cantoperporpag = 46;        // cantidad de operaciones que entran en una pagina
            int cantopertotal = operacionesDataGridView.RowCount;  // cantidad de operaciones (entran 35 por pagina)
            int cantpaginas = ((int)((cantopertotal-1)/ cantoperporpag))+1; // calcula la cantidad de paginas requeridas
            int primeraop = paginasimpresas * cantoperporpag;      // primera pagina: fila 0, segunda: fila 35, etc.
            int cantoperaimprimir=0;
            if (paginasimpresas + 1 == cantpaginas)   // es la ultima pagina?
                cantoperaimprimir = cantopertotal - primeraop;  // si
            else
                cantoperaimprimir = cantoperporpag;                         // no
            e.Graphics.DrawImage(pctlogo.Image, mizq, msup, 89, 28);
            e.Graphics.DrawString("Resumen de Operaciones", letratitulo, Brushes.Black, mizq + 94, msup);

            float msuptitulo = msup + 11;
            e.Graphics.DrawString("Nº: " + udResNo.Value.ToString("00000"), letradatobold, Brushes.Black, mizq + 94, msuptitulo);
            msuptitulo += 5;
            if (cbNoCliente.Checked || cbNombreCliente.Checked)
            {
                e.Graphics.DrawString("Cliente: " + clientesDataGridView.Rows[0].Cells[1].Value.ToString(), letradatobold, Brushes.Black, mizq + 94, msuptitulo);
                msuptitulo += 5;
            }
            if (cbFecha.Checked)
            {
                e.Graphics.DrawString("Fecha: del " + dtFechaDesde.Value.ToShortDateString() + " al " + dtFechaHasta.Value.ToShortDateString(), letradatobold, Brushes.Black, mizq + 94, msuptitulo);
                msuptitulo += 5;
            }
            if (cbTipo.Checked && cbTipo.Text!="")
            {
                e.Graphics.DrawString("Tipo de Operación: " + cmbTipo.Text, letradatobold, Brushes.Black, mizq + 94, msuptitulo);
            }

            Pen pentabla=new Pen(Brushes.Black,0.5F);
            Pen pentablainf=new Pen(Brushes.Red,0.5F);
            /*
            e.Graphics.DrawLine(pentabla,mizq+14,msup+32,mizq+181,msup+32);
            e.Graphics.DrawLine(pentablainf, mizq + 14, msup + 37, mizq + 181, msup + 37);
            */
            e.Graphics.DrawLine(pentabla, mizq + 6, msup + 32, mizq + 189, msup + 32);
            e.Graphics.DrawLine(pentablainf, mizq + 6, msup + 37, mizq + 189, msup + 37);

            
            e.Graphics.DrawString("Nº Cliente", letradatobold, Brushes.Black, mizq + 7, msup + 32);
            e.Graphics.DrawString("Fecha", letradatobold, Brushes.Black, mizq + 35, msup + 32);
            e.Graphics.DrawString("Tipo de Operación", letradatobold, Brushes.Black, mizq + 59, msup + 32);
            e.Graphics.DrawString("Traslado", letradatobold, Brushes.Black, mizq + 120, msup + 32);
            e.Graphics.DrawString("Código de Caja", letradatobold, Brushes.Black, mizq + 141, msup + 32);
            

            for (int i = 1; i <= cantoperaimprimir; i++)
            {
                e.Graphics.DrawString(operacionesDataGridView.Rows[i+(cantoperporpag*paginasimpresas) - 1].Cells[3].Value.ToString(), letracodigo, Brushes.Black,mizq + 7, msup + 33 + 5 * i);
                e.Graphics.DrawString(((DateTime)operacionesDataGridView.Rows[i + (cantoperporpag * paginasimpresas) - 1].Cells[2].Value).ToShortDateString(), letracodigo, Brushes.Black, mizq + 35, msup + 33 + 5 * i);
                e.Graphics.DrawString(operacionesDataGridView.Rows[i + (cantoperporpag * paginasimpresas) - 1].Cells[1].Value.ToString(), letracodigo, Brushes.Black, mizq + 59, msup + 33 + 5 * i);
                string trasladada="Sí";
                if ((bool)operacionesDataGridView.Rows[i + (cantoperporpag * paginasimpresas) - 1].Cells[5].Value == false)
                    trasladada = "No";
                e.Graphics.DrawString(trasladada, letracodigo, Brushes.Black, mizq + 120, msup + 33 + 5 * i);


                e.Graphics.DrawString(operacionesDataGridView.Rows[i + (cantoperporpag * paginasimpresas) - 1].Cells[4].Value.ToString(), letracodigo, Brushes.Black, mizq + 141, msup + 33 + 5 * i);

            }

            e.Graphics.DrawString("Página "+(paginasimpresas+1).ToString()+"/"+cantpaginas.ToString(),letradatobold,Brushes.Black,mizq+90,msup+275);

            
    



            paginasimpresas++;
            if (paginasimpresas == cantpaginas)
                e.HasMorePages = false;
            else
                e.HasMorePages=true;


        }

        private void btnimpfact_Click(object sender, EventArgs e)
        {
            btnFiltrar_Click(this, e);
            if (!cbNoCliente.Checked && !cbNombreCliente.Checked)
            {
                MessageBox.Show("Debe seleccionarse un cliente para filtrar resultados", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (!cbFecha.Checked)
            {
                MessageBox.Show("Debe seleccionarse un rango de fechas para filtrar resultados", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            printDialog1.Document = printDocument2;
            bool respinsc=false;
            if (clientesDataGridView.Rows[0].Cells[17].Value.ToString() == "Responsable Inscripto")
                respinsc = true;
            string tipofact="B";
            if(respinsc) tipofact="A";

            printDocument2.DocumentName = "F" + tipofact + dtfechafact.Value.Year.ToString() + dtfechafact.Value.Month.ToString("D2") + dtfechafact.Value.Day.ToString("D2");
            
            if (printDialog1.ShowDialog() == DialogResult.OK)
                printDocument2.Print();

        }

        private void printDocument2_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            Pen penlinea = new Pen(Color.Black, 0.5F);
            e.Graphics.PageUnit = GraphicsUnit.Millimeter;
            Graphics Canvas = e.Graphics;
            Canvas.PageUnit = GraphicsUnit.Millimeter;
            float dpiCorrectionX = Canvas.DpiX / 100;
            float dpiCorrectionY = Canvas.DpiY / 100;
            Point[] Points = new Point[2];


            Points[0] = new Point(Convert.ToInt32(e.PageBounds.X * dpiCorrectionX), Convert.ToInt32(e.PageBounds.Y * dpiCorrectionY));
            Points[1] = new Point(Convert.ToInt32(e.PageBounds.Width * dpiCorrectionX), Convert.ToInt32(e.PageBounds.Height * dpiCorrectionY));
            Canvas.TransformPoints(System.Drawing.Drawing2D.CoordinateSpace.Page, System.Drawing.Drawing2D.CoordinateSpace.Device, Points);
            Rectangle Bounds = new Rectangle(Points[0].X, Points[0].Y, Points[1].X - 20, Points[1].Y - 20);
            float mizq = Bounds.Left;
            float msup = Bounds.Top;
            float anchopag = Bounds.Width;
            float altopag = Bounds.Height;
            Font letratitulo = new Font("Arial", 23);
            Font letracodigo = new Font("Arial", 12);
            Font letradatobold = new Font("Arial", 12, FontStyle.Bold);



            msup = Bounds.Top - 7; // Corre el margen para factura nueva
            imprimeletrachica(e,"Fecha:"+dtfechafact.Value.ToShortDateString(), mizq + 130, msup + 30); // fecha de emisión
            imprimeletrachica(e,clientesDataGridView.Rows[0].Cells[1].Value.ToString(), mizq + 10, msup + 67); // Nombre del cliente
            imprimeletrachica(e,clientesDataGridView.Rows[0].Cells[2].Value.ToString(), mizq + 10, msup + 71); // Dirección
            imprimeletrachica(e,"CUIT: " + clientesDataGridView.Rows[0].Cells[16].Value.ToString(), mizq + 10, msup + 75); // CUIT
            imprimeletrachica(e,"IVA: " + clientesDataGridView.Rows[0].Cells[17].Value.ToString(), mizq + 10, msup + 79); //Condición de IVA   
            imprimeletrachica(e, "Condiciones de Pago: " + tbCondPago.Text, mizq + 10, msup + 91);
            imprimeletrachica(e, "Según resumen Nº: " +  udResNo.Value.ToString("00000"), mizq + 105, msup + 91); // resumen al que referencia
            bool respinsc=false; // Bandera que indica si es responsable inscripto
            if (clientesDataGridView.Rows[0].Cells[17].Value.ToString() == "Responsable Inscripto")
                respinsc = true;
            decimal preciounitdec,importedec;
            string preciounitstr,importestr;
            float subtotal=0;

            float comdetalley = 110;
            imprimeletrachica(e,"CANTIDAD",mizq+8,msup+comdetalley-5);
            imprimeletrachica(e, "DESCRIPCION", mizq + 32, msup + comdetalley - 5);
            imprimeletrachica(e, "PRECIO UNIT.", mizq + 135, msup + comdetalley - 5);
            imprimeletrachica(e, "IMPORTE", mizq + 170, msup + comdetalley - 5);

            for (int i = 0; i <= dgvfactura.RowCount - 2; i++)
            {
                imprimeletrachica(e, dgvfactura.Rows[i].Cells[0].Value.ToString(), mizq + 8, msup + comdetalley + i * 5);
                imprimeletrachica(e, dgvfactura.Rows[i].Cells[1].Value.ToString(), mizq + 32, msup + comdetalley + i * 5);
                preciounitstr = dgvfactura.Rows[i].Cells[2].Value.ToString();
                preciounitdec = Convert.ToDecimal(preciounitstr);
                if (!respinsc)           // si no es resp. inscr. pongo el precio con IVA
                {
                    preciounitdec *= (decimal)1.21;
                    preciounitstr = preciounitdec.ToString("N2");
                }
                imprimeletrachica(e, preciounitstr, mizq + 135, msup + comdetalley + i * 5);
                importestr = dgvfactura.Rows[i].Cells[3].Value.ToString();
                importedec = Convert.ToDecimal(importestr);
                if (!respinsc)           // si no es resp. inscr. pongo el precio con IVA
                {
                    importedec *= (decimal)1.21;
                    importestr = importedec.ToString("N2");
                }
                imprimeletrachica(e, importestr, mizq + 170, msup + comdetalley + i * 5);

                subtotal += float.Parse(importestr);
            }
            if (respinsc)
            {
                imprimeletrachica(e, "IMPORTE NETO:", mizq + 130, msup + 250);
                imprimeletrachica(e, subtotal.ToString("N2"), mizq + 170, msup + 250);
                imprimeletrachica(e, "IVA:", mizq + 130, msup + 255);
                imprimeletrachica(e, (subtotal * 0.21F).ToString("N2"), mizq + 170, msup + 255);
                imprimeletrachica(e, "TOTAL:", mizq + 130, msup + 260);
                imprimeletrachica(e, (subtotal * 1.21F).ToString("N2"), mizq + 170, msup + 260);
            }
            else
            {
                imprimeletrachica(e, "TOTAL:", mizq + 130, msup + 260);
                imprimeletrachica(e, subtotal.ToString("N2"), mizq + 170, msup + 260);
            }

            e.HasMorePages = false;

        }

        void imprimeletrachica(System.Drawing.Printing.PrintPageEventArgs e, string cadena, float x, float y)
        {
            Font letra = new Font("Arial", 12);
            e.Graphics.DrawString(cadena, letra, Brushes.Black, x, y);
        }

        private void btngenfact_Click(object sender, EventArgs e)
        {
            dgvfactura.Rows.Clear();
            decimal preciounitdec, importedec;

            int n=dgvfactura.Rows.Add();
            dgvfactura.Rows[n].Cells[0].Value = "1";
            string planstr = clientesDataGridView.Rows[0].Cells[14].Value.ToString(); // Extrae el Plan
            DateTime fechaabono = dtFechaDesde.Value.AddMonths(1);
            dgvfactura.Rows[n].Cells[1].Value="Abono mensual Plan " + planstr+" "+fechaabono.ToString("MMMM")+" "+fechaabono.Year.ToString();
            string preciounitstr = "0,00";
            string importestr = "0,00";


            if (planstr == "A")
            {
                preciounitstr = Comun.abonoplana.ToString("N2");
            }
            if (planstr == "B")
            {
                preciounitstr = Comun.abonoplanb.ToString("N2");
            }
            if (planstr == "C")
            {
                preciounitstr = Comun.abonoplanc.ToString("N2");
            }
            preciounitdec = Convert.ToDecimal(preciounitstr);
            preciounitstr = preciounitdec.ToString("N2");   // tengo el abono del plan en preciounitstr
            dgvfactura.Rows[n].Cells[2].Value = preciounitstr;
            dgvfactura.Rows[n].Cells[3].Value = preciounitstr;


            int cantcajas = cajasDataGridView.RowCount; // cantidad de cajas del cliente

            if (planstr == "A" && cantcajas > Comun.maxplana) // Se pasó del Plan A?
            {
                int cajasexcedentes = cantcajas - Comun.maxplana;
                n=dgvfactura.Rows.Add();
                dgvfactura.Rows[n].Cells[0].Value=cajasexcedentes.ToString();
                dgvfactura.Rows[n].Cells[1].Value="Cajas excedentes del plan";
                
                preciounitstr = Comun.cajaexcedenteA.ToString("N2");
                preciounitdec = Convert.ToDecimal(preciounitstr);
                importedec = Convert.ToDecimal(cajasexcedentes) * preciounitdec;
                importestr = importedec.ToString("N2");
                dgvfactura.Rows[n].Cells[2].Value=preciounitstr;
                dgvfactura.Rows[n].Cells[3].Value=importestr;
            }

            if (planstr == "B" && cantcajas > Comun.maxplanb) // Se pasó del Plan B?
            {
                int cajasexcedentes = cantcajas - Comun.maxplanb;
                n = dgvfactura.Rows.Add();
                dgvfactura.Rows[n].Cells[0].Value = cajasexcedentes.ToString();
                dgvfactura.Rows[n].Cells[1].Value = "Cajas excedentes del plan";

                preciounitstr = Comun.cajaexcedenteB.ToString("N2");
                preciounitdec = Convert.ToDecimal(preciounitstr);
                importedec = Convert.ToDecimal(cajasexcedentes) * preciounitdec;
                importestr = importedec.ToString("N2");
                dgvfactura.Rows[n].Cells[2].Value = preciounitstr;
                dgvfactura.Rows[n].Cells[3].Value = importestr;
            }

            if (planstr == "C" && cantcajas > Comun.maxplanc) // Se pasó del Plan C?
            {
                int cajasexcedentes = cantcajas - Comun.maxplanc;
                n = dgvfactura.Rows.Add();
                dgvfactura.Rows[n].Cells[0].Value = cajasexcedentes.ToString();
                dgvfactura.Rows[n].Cells[1].Value = "Cajas excedentes del plan";

                preciounitstr = Comun.cajaexcedenteC.ToString("N2");
                preciounitdec = Convert.ToDecimal(preciounitstr);
                importedec = Convert.ToDecimal(cajasexcedentes) * preciounitdec;
                importestr = importedec.ToString("N2");
                dgvfactura.Rows[n].Cells[2].Value = preciounitstr;
                dgvfactura.Rows[n].Cells[3].Value = importestr;

            }

            if((bool)clientesDataGridView.Rows[0].Cells["hosting"].Value) // Tiene plan de hosting?
            {                
                n=dgvfactura.Rows.Add(); // Si
                dgvfactura.Rows[n].Cells[0].Value="1";
                dgvfactura.Rows[n].Cells[1].Value="Abono hosting " + fechaabono.ToString("MMMM")+ " "+ fechaabono.Year.ToString();
                
                preciounitstr = (Comun.abonohosting.ToString("N2"));
                preciounitdec = Convert.ToDecimal(preciounitstr);
                importedec = preciounitdec;
                importestr = importedec.ToString("N2");
                dgvfactura.Rows[n].Cells[2].Value=preciounitstr;
                dgvfactura.Rows[n].Cells[3].Value=importestr;
            }






            // Calcula el total de búsquedas de documentos internos no urgente
            int totalbusquedasdoc = 0;
            for (int i = 0; i <= operacionesDataGridView.RowCount - 1; i++)
            {
                if (operacionesDataGridView.Rows[i].Cells[1].Value.ToString() == "Búsqueda de documento" &&
                    (bool)operacionesDataGridView.Rows[i].Cells[6].Value == false)
                    totalbusquedasdoc++;
            }

            if (totalbusquedasdoc != 0)
            {
                // Total de búsquedas internas
                n = dgvfactura.Rows.Add();
                dgvfactura.Rows[n].Cells[0].Value = totalbusquedasdoc.ToString();
                dgvfactura.Rows[n].Cells[1].Value = "Búsquedas de documento";
                preciounitstr = Comun.tarifadoc.ToString("N2");
                preciounitdec = Convert.ToDecimal(preciounitstr);
                dgvfactura.Rows[n].Cells[2].Value = preciounitstr;
                importestr = (totalbusquedasdoc * float.Parse(preciounitstr)).ToString("N2");
                dgvfactura.Rows[n].Cells[3].Value = importestr;
            }

            // Calcula el total de búsquedas de documentos internos urgentes
            int totalbusquedasdocurgentes = 0;
            for (int i = 0; i <= operacionesDataGridView.RowCount - 1; i++)
            {
                if (operacionesDataGridView.Rows[i].Cells[1].Value.ToString() == "Búsqueda de documento" &&
                    (bool)operacionesDataGridView.Rows[i].Cells[6].Value == true)
                    totalbusquedasdocurgentes++;
            }

            if (totalbusquedasdocurgentes != 0)
            {
                // Total de búsquedas internas
                n = dgvfactura.Rows.Add();
                dgvfactura.Rows[n].Cells[0].Value = totalbusquedasdocurgentes.ToString();
                dgvfactura.Rows[n].Cells[1].Value = "Búsquedas de documento urgentes";
                preciounitstr = (Comun.tarifadoc*2).ToString("N2");
                preciounitdec = Convert.ToDecimal(preciounitstr);
                dgvfactura.Rows[n].Cells[2].Value = preciounitstr;
                importestr = (totalbusquedasdocurgentes * float.Parse(preciounitstr)).ToString("N2");
                dgvfactura.Rows[n].Cells[3].Value = importestr;
            }



            // Calcula el total de búsquedas no urgentes
            int totalbusquedas = 0;

            // Suma los retiros para consulta de cajas enteras
            for (int i = 0; i <= operacionesDataGridView.RowCount - 1; i++)
            {
                if (operacionesDataGridView.Rows[i].Cells[1].Value.ToString() == "Retiro para Consulta" &&
                    (bool)operacionesDataGridView.Rows[i].Cells[6].Value == false)
                    totalbusquedas++;
            }

            // Suma las búsquedas de caja para extraer documento interno
            for (int i = 0; i <= operacionesDataGridView.RowCount - 1; i++)
            {
                if (operacionesDataGridView.Rows[i].Cells[1].Value.ToString() == "Búsqueda de caja en depósito" &&
                    (bool)operacionesDataGridView.Rows[i].Cells[6].Value==false)
                    totalbusquedas++;
            }

            // Suma las búsquedas de caja para dar de baja.
            for (int i = 0; i <= operacionesDataGridView.RowCount - 1; i++)
            {
                if (operacionesDataGridView.Rows[i].Cells[1].Value.ToString() == "Baja") 
                    totalbusquedas++;
            }



            if (totalbusquedas != 0)
            {
                n=dgvfactura.Rows.Add();
                dgvfactura.Rows[n].Cells[0].Value=totalbusquedas.ToString();
                dgvfactura.Rows[n].Cells[1].Value="Busquedas de Caja en Depósito";
                preciounitstr = Comun.tarifabusqueda.ToString("N2");
                preciounitdec = Convert.ToDecimal(preciounitstr);
                dgvfactura.Rows[n].Cells[2].Value=preciounitstr;
                importestr = (totalbusquedas * float.Parse(preciounitstr)).ToString("N2");
                dgvfactura.Rows[n].Cells[3].Value=importestr;
            }

            // Calcula el total de búsquedas urgentes
            int totalbusquedasurgentes = 0;

            // Suma los retiros para consulta de cajas enteras
            for (int i = 0; i <= operacionesDataGridView.RowCount - 1; i++)
            {
                if (operacionesDataGridView.Rows[i].Cells[1].Value.ToString() == "Retiro para Consulta" &&
                    (bool)operacionesDataGridView.Rows[i].Cells[6].Value == true)
                    totalbusquedasurgentes++;
            }

            // Suma las búsquedas de caja para extraer documento interno
            for (int i = 0; i <= operacionesDataGridView.RowCount - 1; i++)
            {
                if (operacionesDataGridView.Rows[i].Cells[1].Value.ToString() == "Búsqueda de caja en depósito" &&
                    (bool)operacionesDataGridView.Rows[i].Cells[6].Value == true)
                    totalbusquedasurgentes++;
            }


            if (totalbusquedasurgentes != 0)
            {
                n = dgvfactura.Rows.Add();
                dgvfactura.Rows[n].Cells[0].Value = totalbusquedasurgentes.ToString();
                dgvfactura.Rows[n].Cells[1].Value = "Busquedas de Caja en Depósito Urgentes";
                preciounitstr = (Comun.tarifabusqueda*2).ToString("N2");
                preciounitdec = Convert.ToDecimal(preciounitstr);
                dgvfactura.Rows[n].Cells[2].Value = preciounitstr;
                importestr = (totalbusquedasurgentes * float.Parse(preciounitstr)).ToString("N2");
                dgvfactura.Rows[n].Cells[3].Value = importestr;
            }


            // Calcula los traslados de ingresos del mes hasta 5 cajas tarifa fija y otra tarifa por cada caja excedente
            int totaltraslados = 0;                           // cuenta cuantos traslados hay en el mes
            for (int i = 0; i <= operacionesDataGridView.RowCount - 1; i++)
            {
                if ((bool)operacionesDataGridView.Rows[i].Cells[5].Value == true &&
                    operacionesDataGridView.Rows[i].Cells[1].Value.ToString()=="Ingreso")
                    totaltraslados++;                                                       // un traslado en la primerfecha
            }
            if (totaltraslados != 0)
            {
                n = dgvfactura.Rows.Add();                              // agrega la ultima fila
                dgvfactura.Rows[n].Cells[0].Value = "1";
                dgvfactura.Rows[n].Cells[1].Value = "Traslado de cajas ingresos (total " + totaltraslados.ToString() + " cajas).";

                preciounitstr = Comun.tarifatraslado.ToString("N2");    // precio de la primer caja
                preciounitdec = Convert.ToDecimal(preciounitstr);
                if (totaltraslados > 5)
                {
                    preciounitstr = Comun.tarifatrasladoadicional.ToString("N2");     // precio de la caja adicional
                    preciounitdec += (totaltraslados - 5) * (Convert.ToDecimal(preciounitstr));   // suma el precio de las cajas adicionales
                }
                preciounitstr = preciounitdec.ToString("N2");
                dgvfactura.Rows[n].Cells[2].Value = preciounitstr;
                dgvfactura.Rows[n].Cells[3].Value = preciounitstr;
            }
                            
            
            
            /* Calcula los traslados no de ingresos de la sig. manera:
             * Para cada fecha cobra la primer caja un precio y cada caja adicional otro
             */ 
            DateTime fechaprimercaja=DateTime.Now;           // fecha de la primer caja con determinada fecha
            DateTime fechafilaactual=DateTime.Now;           // fecha de la operacion de la fila actual
            string fechaprimercajastr=fechaprimercaja.ToShortDateString();
            string fechafilaactualstr = fechafilaactual.ToShortDateString();        
            bool primerfilatraslado=true;                      // bandera que indica si es la primera operacion con traslado del resumen
            int trasladosdelafecha=0;                           // cuenta cuantos traslados hay en determinada fecha

            
            for (int i = 0; i <= operacionesDataGridView.RowCount - 1; i++)
            {
                if ((bool)operacionesDataGridView.Rows[i].Cells[5].Value == true &&
                    operacionesDataGridView.Rows[i].Cells[1].Value.ToString()!="Ingreso")
                {
                    if (primerfilatraslado)                                                          // si es la primera operacion con traslado del resumen
                    {
                        fechaprimercaja = (DateTime)operacionesDataGridView.Rows[i].Cells[2].Value;
                        fechaprimercajastr = fechaprimercaja.ToShortDateString();
                        primerfilatraslado = false;                                                   // borra la bandera
                        trasladosdelafecha++;                                                       // un traslado en la primerfecha
                        fechafilaactualstr = fechaprimercajastr;
                    }
                    else
                    {
                        fechafilaactual = (DateTime)operacionesDataGridView.Rows[i].Cells[2].Value;
                        fechafilaactualstr = fechafilaactual.ToShortDateString();
                        if (fechafilaactualstr == fechaprimercajastr)                                  // es un traslado adicional dentro de la misma fecha?
                            trasladosdelafecha++;                                                   // si, incrementa la cuenta para esta fecha
                        else
                        {
                            n = dgvfactura.Rows.Add();
                            dgvfactura.Rows[n].Cells[0].Value = "1";
                            dgvfactura.Rows[n].Cells[1].Value = "Traslado " + fechaprimercajastr + " (total " + trasladosdelafecha.ToString() + " traslados).";

                            preciounitstr = Comun.tarifatraslado.ToString("N2");    // precio de la primer caja
                            preciounitdec = Convert.ToDecimal(preciounitstr);
                            if (trasladosdelafecha > 1)
                            {
                                preciounitstr = Comun.tarifatrasladoadicional.ToString("N2");     // precio de la caja adicional
                                preciounitdec += (trasladosdelafecha - 1) * (Convert.ToDecimal(preciounitstr));   // suma el precio de las cajas adicionales
                            }
                            preciounitstr = preciounitdec.ToString("N2");
                            dgvfactura.Rows[n].Cells[2].Value = preciounitstr;
                            dgvfactura.Rows[n].Cells[3].Value = preciounitstr;
                            trasladosdelafecha=1;
                            fechaprimercaja = (DateTime)operacionesDataGridView.Rows[i].Cells[2].Value; // no, cambia la fecha de la primer caja
                            fechaprimercajastr = fechaprimercaja.ToShortDateString();
                        }
                    }
                }
            }
            if (trasladosdelafecha != 0)
            {
                n = dgvfactura.Rows.Add();                              // agrega la ultima fila
                dgvfactura.Rows[n].Cells[0].Value = "1";
                dgvfactura.Rows[n].Cells[1].Value = "Traslado " + fechafilaactualstr + "(total " + trasladosdelafecha.ToString() + " traslados).";

                preciounitstr = Comun.tarifatraslado.ToString("N2");    // precio de la primer caja
                preciounitdec = Convert.ToDecimal(preciounitstr);
                if (trasladosdelafecha > 5)
                {
                    preciounitstr = Comun.tarifatrasladoadicional.ToString("N2");     // precio de la caja adicional
                    preciounitdec += (trasladosdelafecha - 5) * (Convert.ToDecimal(preciounitstr));   // suma el precio de las cajas adicionales
                }

                
                
                
/*                
                if (trasladosdelafecha > 1)
                {
                    preciounitstr = Comun.tarifatrasladoadicional.ToString("N2");     // precio de la caja adicional
                    preciounitdec += (trasladosdelafecha - 1) * (Convert.ToDecimal(preciounitstr));   // suma el precio de las cajas adicionales
                }*/
                preciounitstr = preciounitdec.ToString("N2");
                dgvfactura.Rows[n].Cells[2].Value = preciounitstr;
                dgvfactura.Rows[n].Cells[3].Value = preciounitstr;
                trasladosdelafecha = 1;
            }
            


            /*// Calcula los traslados del mes hasta 5 cajas tarifa fija y otra tarifa por cada caja excedente
            int totaltraslados = 0;                           // cuenta cuantos traslados hay en el mes
            for (int i = 0; i <= operacionesDataGridView.RowCount - 1; i++)
            {
                if ((bool)operacionesDataGridView.Rows[i].Cells[5].Value == true)
                    totaltraslados++;                                                       // un traslado en la primerfecha
            }
            if (totaltraslados != 0)
            {
                n = dgvfactura.Rows.Add();                              // agrega la ultima fila
                dgvfactura.Rows[n].Cells[0].Value = "1";
                dgvfactura.Rows[n].Cells[1].Value = "Traslado de cajas (total " + totaltraslados.ToString() + " cajas).";

                preciounitstr = Comun.tarifatraslado.ToString("N2");    // precio de la primer caja
                preciounitdec = Convert.ToDecimal(preciounitstr);
                if (totaltraslados > 5)
                {
                    preciounitstr = Comun.tarifatrasladoadicional.ToString("N2");     // precio de la caja adicional
                    preciounitdec += (totaltraslados - 5) * (Convert.ToDecimal(preciounitstr));   // suma el precio de las cajas adicionales
                }
                preciounitstr = preciounitdec.ToString("N2");
                dgvfactura.Rows[n].Cells[2].Value = preciounitstr;
                dgvfactura.Rows[n].Cells[3].Value = preciounitstr;
            }
            */                



            //Calcula el total de cajas vacías entregadas
            int totalcajas = 0;
            for (int i = 0; i <= operacionesDataGridView.RowCount - 1; i++)
            {
                string opstr = operacionesDataGridView.Rows[i].Cells[1].Value.ToString();
                if (opstr.Length >= 5 && opstr.Substring(5) == "Entrega de Cajas")
                {
                    int cantcajasv = Convert.ToInt32(opstr.Substring(0, 4));
                    totalcajas += cantcajasv;
                }
            }

            if (totalcajas != 0)
            {
                n=dgvfactura.Rows.Add();
                dgvfactura.Rows[n].Cells[0].Value=totalcajas.ToString();
                dgvfactura.Rows[n].Cells[1].Value="Cajas Vacías";
                preciounitstr = Comun.preciocaja.ToString("N2");
                preciounitdec = Convert.ToDecimal(preciounitstr);
                dgvfactura.Rows[n].Cells[2].Value=preciounitstr;
                importestr = (totalcajas * float.Parse(preciounitstr)).ToString("N2");
                dgvfactura.Rows[n].Cells[3].Value=importestr;
            }

            // Calcula el total de páginas excedentes escaneadas
            int totalpaginas = 0;
            for (int i = 0; i <= operacionesDataGridView.RowCount - 1; i++)
            {
                int paginasfila = 0;
                if (operacionesDataGridView.Rows[i].Cells[7].Value != DBNull.Value &&
                    (int)operacionesDataGridView.Rows[i].Cells[7].Value > Comun.maxpaginas)
                {
                    paginasfila = (int)operacionesDataGridView.Rows[i].Cells[7].Value;
                    paginasfila -= Comun.maxpaginas;  // resta la cantidad de páginas sin costo
                    totalpaginas += paginasfila;
                }
            }
            if (totalpaginas > 0)
            {
                n = dgvfactura.Rows.Add();
                dgvfactura.Rows[n].Cells[0].Value = totalpaginas.ToString();
                dgvfactura.Rows[n].Cells[1].Value = "Páginas excedentes escaneadas";
                preciounitstr = Comun.tarifascan.ToString("N2");
                preciounitdec = Convert.ToDecimal(preciounitstr);
                dgvfactura.Rows[n].Cells[2].Value = preciounitstr;
                importestr = (totalpaginas * float.Parse(preciounitstr)).ToString("N2");
                dgvfactura.Rows[n].Cells[3].Value = importestr;
            }
        }



    }
}
