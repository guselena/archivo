﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Archivo
{
    public partial class frmimprime_individual : Form
    {
        frmImprimeEtiquetas frmetiq;

        public frmimprime_individual()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (tbCodigo.Text.Length != 19)
            {
                MessageBox.Show("El código debe tener 19 caracteres de longitud", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            frmetiq = new frmImprimeEtiquetas(tbCodigo.Text);
            frmetiq.ShowDialog(this);
        }
    }
}
