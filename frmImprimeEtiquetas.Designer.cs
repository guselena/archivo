﻿namespace Archivo
{
    partial class frmImprimeEtiquetas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.pctbarras = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pctbarras)).BeginInit();
            this.SuspendLayout();
            // 
            // printDialog1
            // 
            this.printDialog1.Document = this.printDocument1;
            this.printDialog1.UseEXDialog = true;
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);
            // 
            // pctbarras
            // 
            this.pctbarras.Location = new System.Drawing.Point(12, 12);
            this.pctbarras.Name = "pctbarras";
            this.pctbarras.Size = new System.Drawing.Size(294, 84);
            this.pctbarras.TabIndex = 1;
            this.pctbarras.TabStop = false;
            this.pctbarras.Visible = false;
            // 
            // frmImprimeEtiquetas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(240, 63);
            this.ControlBox = false;
            this.Controls.Add(this.pctbarras);
            this.Name = "frmImprimeEtiquetas";
            this.Text = "Impresión de Etiquetas";
            this.Load += new System.EventHandler(this.frmImprimeEtiquetas_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pctbarras)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PrintDialog printDialog1;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.PictureBox pctbarras;
    }
}