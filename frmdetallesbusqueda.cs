﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Archivo
{
    public partial class frmdetallesbusqueda : Form
    {
        int paginasimpresas;

        public frmdetallesbusqueda()
        {
            InitializeComponent();
        }

        private void operacionesBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.operacionesBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.archivoDataSet);

        }

        private void frmdetallesbusqueda_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'archivoDataSet.cajas' table. You can move, or remove it, as needed.
            this.cajasTableAdapter.Fill(this.archivoDataSet.cajas);
            // TODO: This line of code loads data into the 'archivoDataSet.clientes' table. You can move, or remove it, as needed.
            this.clientesTableAdapter.Fill(this.archivoDataSet.clientes);
            // TODO: This line of code loads data into the 'archivoDataSet.operaciones' table. You can move, or remove it, as needed.
            this.operacionesTableAdapter.Fill(this.archivoDataSet.operaciones);
            DataView dv = archivoDataSet.operaciones.DefaultView;
            string filter = "Tipo='Búsqueda de Documento'";
            dv.RowFilter = filter;      // Sólo las busquedas de documento
            operacionesBindingSource.DataSource = dv;

        }

        private void btnFiltrar_Click(object sender, EventArgs e)
        {
            DataView dv = archivoDataSet.operaciones.DefaultView;
            string filter = "Tipo='Búsqueda de Documento'";
            dv.RowFilter = filter;      // Carga todas las operaciones
            operacionesBindingSource.DataSource = dv;
            if (cbNombreCliente.Checked && tbNombreClienteFiltro.Text != "")
            {
                DataView dv2 = archivoDataSet.clientes.DefaultView;
                string filter2 = "Nocliente > 0";
                dv2.RowFilter = filter2;
                clientesBindingSource.DataSource = dv2;   // Carga todos los clientes
                filter2 = "NombreEmpresa LIKE '*" + tbNombreClienteFiltro.Text + "*'";
                dv2.RowFilter = filter2;
                clientesBindingSource.DataSource = dv2;   // Deja solo la fila(s) del cliente
                string nocliente2 = clientesDataGridView.Rows[0].Cells[0].Value.ToString();   // Extrae el Nº de cliente
                filter += " AND Nocliente = " + nocliente2;   // filtra operaciones
            }
            if (cbNoCliente.Checked && tbNoClienteFiltro.Text != "")
            {
                DataView dv2 = archivoDataSet.clientes.DefaultView;
                string filter2 = "Nocliente > 0";
                dv2.RowFilter = filter2;
                clientesBindingSource.DataSource = dv2;   // Carga todos los clientes
                filter2 = "Nocliente = " + tbNoClienteFiltro.Text;
                dv2.RowFilter = filter2;
                clientesBindingSource.DataSource = dv2;   // Deja solo la fila(s) del cliente
                filter += " AND Nocliente = " + tbNoClienteFiltro.Text;
            }

            if (cbFecha.Checked)
                filter += " AND Fecha >= '" + dtFechaDesde.Value.ToShortDateString() + " 00:00:00'  AND Fecha <= '" + dtFechaHasta.Value.ToShortDateString() + " 23:59:59'";

            dv.RowFilter = filter;
            operacionesBindingSource.DataSource = dv;
            operacionesDataGridView.Sort(operacionesDataGridView.Columns[0], ListSortDirection.Ascending);


        }

        private void cbNoCliente_CheckedChanged(object sender, EventArgs e)
        {
            if (cbNoCliente.Checked)
            {
                tbNoClienteFiltro.Enabled = true;
                cbNombreCliente.Checked = false;
            }
            else
            {
                tbNoClienteFiltro.Enabled = false;
            }

        }


        private void cbNombreCliente_CheckedChanged(object sender, EventArgs e)
        {
            if (cbNombreCliente.Checked)
            {
                tbNombreClienteFiltro.Enabled = true;
                cbNoCliente.Checked = false;
            }
            else
                tbNombreClienteFiltro.Enabled = false;
        }

        private void cbFecha_CheckedChanged(object sender, EventArgs e)
        {
            if (cbFecha.Checked)
            {
                dtFechaDesde.Enabled = true;
                dtFechaHasta.Enabled = true;
            }
            else
            {
                dtFechaDesde.Enabled = false;
                dtFechaHasta.Enabled = false;
            }
        }


        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            Pen penlinea = new Pen(Color.Black, 0.5F);
            e.Graphics.PageUnit = GraphicsUnit.Millimeter;
            Graphics Canvas = e.Graphics;
            Canvas.PageUnit = GraphicsUnit.Millimeter;
            float dpiCorrectionX = Canvas.DpiX / 100;
            float dpiCorrectionY = Canvas.DpiY / 100;
            Point[] Points = new Point[2];


            Points[0] = new Point(Convert.ToInt32(e.PageBounds.X * dpiCorrectionX), Convert.ToInt32(e.PageBounds.Y * dpiCorrectionY));
            Points[1] = new Point(Convert.ToInt32(e.PageBounds.Width * dpiCorrectionX), Convert.ToInt32(e.PageBounds.Height * dpiCorrectionY));
            Canvas.TransformPoints(System.Drawing.Drawing2D.CoordinateSpace.Page, System.Drawing.Drawing2D.CoordinateSpace.Device, Points);
            Rectangle Bounds = new Rectangle(Points[0].X, Points[0].Y, Points[1].X - 20, Points[1].Y - 20);
            float mizq = Bounds.Left;
            float msup = Bounds.Top;
            float anchopag = Bounds.Width;
            float altopag = Bounds.Height;
            Font letratitulo = new Font("Arial", 23);
            Font letracodigo = new Font("Arial", 12);
            Font letradatobold = new Font("Arial", 12, FontStyle.Bold);


            int cantoperporpag = 46;        // cantidad de operaciones que entran en una pagina
            int cantopertotal = operacionesDataGridView.RowCount;  // cantidad de operaciones (entran 35 por pagina)
            int cantpaginas = ((int)((cantopertotal - 1) / cantoperporpag)) + 1; // calcula la cantidad de paginas requeridas
            int primeraop = paginasimpresas * cantoperporpag;      // primera pagina: fila 0, segunda: fila 35, etc.
            int cantoperaimprimir = 0;
            if (paginasimpresas + 1 == cantpaginas)   // es la ultima pagina?
                cantoperaimprimir = cantopertotal - primeraop;  // si
            else
                cantoperaimprimir = cantoperporpag;                         // no
            e.Graphics.DrawImage(pctlogo.Image, mizq, msup, 89, 28);
            e.Graphics.DrawString("Resumen de Búsqueda de", letratitulo, Brushes.Black, mizq + 94, msup);
            e.Graphics.DrawString("Documentos", letratitulo, Brushes.Black, mizq + 94, msup+7);

            float msuptitulo = msup + 11;
            msuptitulo += 5;
            if (cbNoCliente.Checked || cbNombreCliente.Checked)
            {
                e.Graphics.DrawString("Cliente: " + clientesDataGridView.Rows[0].Cells[1].Value.ToString(), letradatobold, Brushes.Black, mizq + 94, msuptitulo);
                msuptitulo += 5;
            }
            if (cbFecha.Checked)
            {
                e.Graphics.DrawString("Fecha: del " + dtFechaDesde.Value.ToShortDateString() + " al " + dtFechaHasta.Value.ToShortDateString(), letradatobold, Brushes.Black, mizq + 94, msuptitulo);
                msuptitulo += 5;
            }
            Pen pentabla = new Pen(Brushes.Black, 0.5F);
            Pen pentablainf = new Pen(Brushes.Red, 0.5F);
            /*
            e.Graphics.DrawLine(pentabla,mizq+14,msup+32,mizq+181,msup+32);
            e.Graphics.DrawLine(pentablainf, mizq + 14, msup + 37, mizq + 181, msup + 37);
            */
            e.Graphics.DrawLine(pentabla, mizq + 6, msup + 32, mizq + 189, msup + 32);
            e.Graphics.DrawLine(pentablainf, mizq + 6, msup + 37, mizq + 189, msup + 37);


            e.Graphics.DrawString("Fecha", letradatobold, Brushes.Black, mizq + 7, msup + 32);
            e.Graphics.DrawString("Descripción", letradatobold, Brushes.Black, mizq + 59, msup + 32);
            e.Graphics.DrawString("Urgente", letradatobold, Brushes.Black, mizq + 120, msup + 32);
            e.Graphics.DrawString("Traslado", letradatobold, Brushes.Black, mizq + 141, msup + 32);
            e.Graphics.DrawString("Páginas", letradatobold, Brushes.Black, mizq + 162, msup + 32);

            for (int i = 1; i <= cantoperaimprimir; i++)
            {
                e.Graphics.DrawString(((DateTime)operacionesDataGridView.Rows[i + (cantoperporpag * paginasimpresas) - 1].Cells[2].Value).ToShortDateString(), letracodigo, Brushes.Black, mizq + 7, msup + 33 + 5 * i);
                e.Graphics.DrawString(operacionesDataGridView.Rows[i + (cantoperporpag * paginasimpresas) - 1].Cells[4].Value.ToString(), letracodigo, Brushes.Black, mizq + 59, msup + 33 + 5 * i);
                string urgente = "Sí";
                if (operacionesDataGridView.Rows[i + (cantoperporpag * paginasimpresas) - 1].Cells[7].Value != DBNull.Value)
                {
                    if ((bool)operacionesDataGridView.Rows[i + (cantoperporpag * paginasimpresas) - 1].Cells[7].Value == false)
                        urgente = "No";
                }
                else
                    urgente = "No";
                e.Graphics.DrawString(urgente, letracodigo, Brushes.Black, mizq + 120, msup + 33 + 5 * i);

                string traslado = "Sí";
                if (operacionesDataGridView.Rows[i + (cantoperporpag * paginasimpresas) - 1].Cells[6].Value != DBNull.Value)
                {
                    if ((bool)operacionesDataGridView.Rows[i + (cantoperporpag * paginasimpresas) - 1].Cells[6].Value == false)
                        traslado = "No";
                }
                else
                    traslado = "No";
                e.Graphics.DrawString(traslado, letracodigo, Brushes.Black, mizq + 141, msup + 33 + 5 * i);

                e.Graphics.DrawString(operacionesDataGridView.Rows[i + (cantoperporpag * paginasimpresas) - 1].Cells[5].Value.ToString(), letracodigo, Brushes.Black, mizq + 162, msup + 33 + 5 * i);

            }

            e.Graphics.DrawString("Página " + (paginasimpresas + 1).ToString() + "/" + cantpaginas.ToString(), letradatobold, Brushes.Black, mizq + 90, msup + 275);






            paginasimpresas++;
            if (paginasimpresas == cantpaginas)
                e.HasMorePages = false;
            else
                e.HasMorePages = true;


        }


        private void btnimprimir_Click_1(object sender, EventArgs e)
        {
            printDialog1.Document = printDocument1;
            printDocument1.DocumentName = "RD" + DateTime.Today.Year.ToString() + DateTime.Today.Month.ToString("D2") + DateTime.Today.Day.ToString("D2");
            if (printDialog1.ShowDialog() == DialogResult.OK)
            {
                paginasimpresas = 0;
                printDocument1.DocumentName = "RD" + DateTime.Today.Year.ToString() + DateTime.Today.Month.ToString("D2") + DateTime.Today.Day.ToString("D2");
                printDocument1.Print();
            }

        }

    }
}
