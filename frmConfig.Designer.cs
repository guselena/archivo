﻿namespace Archivo
{
    partial class frmConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.udRacksFila10 = new System.Windows.Forms.NumericUpDown();
            this.lblRacksFila10 = new System.Windows.Forms.Label();
            this.udRacksFila9 = new System.Windows.Forms.NumericUpDown();
            this.lblRacksFila9 = new System.Windows.Forms.Label();
            this.udRacksFila8 = new System.Windows.Forms.NumericUpDown();
            this.lblRacksFila8 = new System.Windows.Forms.Label();
            this.udRacksFila7 = new System.Windows.Forms.NumericUpDown();
            this.lblRacksFila7 = new System.Windows.Forms.Label();
            this.udRacksFila6 = new System.Windows.Forms.NumericUpDown();
            this.lblRacksFila6 = new System.Windows.Forms.Label();
            this.udRacksFila5 = new System.Windows.Forms.NumericUpDown();
            this.lblRacksFila5 = new System.Windows.Forms.Label();
            this.udRacksFila4 = new System.Windows.Forms.NumericUpDown();
            this.lblRacksFila4 = new System.Windows.Forms.Label();
            this.udRacksFila3 = new System.Windows.Forms.NumericUpDown();
            this.lblRacksFila3 = new System.Windows.Forms.Label();
            this.udRacksFila2 = new System.Windows.Forms.NumericUpDown();
            this.lblRacksFila2 = new System.Windows.Forms.Label();
            this.udRacksFila1 = new System.Windows.Forms.NumericUpDown();
            this.lblRacksFila1 = new System.Windows.Forms.Label();
            this.udCajasxPiso = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.udCantPisos = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.udCantfilas = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.udTarifaScan = new System.Windows.Forms.NumericUpDown();
            this.label21 = new System.Windows.Forms.Label();
            this.udMaxpaginas = new System.Windows.Forms.NumericUpDown();
            this.label20 = new System.Windows.Forms.Label();
            this.udDoc = new System.Windows.Forms.NumericUpDown();
            this.label19 = new System.Windows.Forms.Label();
            this.udAbonoHosting = new System.Windows.Forms.NumericUpDown();
            this.label18 = new System.Windows.Forms.Label();
            this.udTrasladoAdicional = new System.Windows.Forms.NumericUpDown();
            this.label14 = new System.Windows.Forms.Label();
            this.udPrecioCaja = new System.Windows.Forms.NumericUpDown();
            this.label13 = new System.Windows.Forms.Label();
            this.udTarifaTraslado = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.udTarifaConsulta = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.udCajaExcedenteC = new System.Windows.Forms.NumericUpDown();
            this.label17 = new System.Windows.Forms.Label();
            this.udAbonoPlanC = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.udMaxPlanC = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.udCajaExcedenteB = new System.Windows.Forms.NumericUpDown();
            this.label16 = new System.Windows.Forms.Label();
            this.udAbonoPlanB = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.udMaxPlanB = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.udCajaExcedenteA = new System.Windows.Forms.NumericUpDown();
            this.udAbonoPlanA = new System.Windows.Forms.NumericUpDown();
            this.label15 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.udMaxPlanA = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.btnAceptar = new System.Windows.Forms.Button();
            this.btncancelar = new System.Windows.Forms.Button();
            this.btnaplicar = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udRacksFila10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udRacksFila9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udRacksFila8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udRacksFila7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udRacksFila6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udRacksFila5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udRacksFila4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udRacksFila3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udRacksFila2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udRacksFila1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udCajasxPiso)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udCantPisos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udCantfilas)).BeginInit();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udTarifaScan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udMaxpaginas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udDoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udAbonoHosting)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udTrasladoAdicional)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udPrecioCaja)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udTarifaTraslado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udTarifaConsulta)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udCajaExcedenteC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udAbonoPlanC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udMaxPlanC)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udCajaExcedenteB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udAbonoPlanB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udMaxPlanB)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udCajaExcedenteA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udAbonoPlanA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udMaxPlanA)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Location = new System.Drawing.Point(13, 13);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(699, 334);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox1);
            this.tabPage2.Controls.Add(this.udCajasxPiso);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Controls.Add(this.udCantPisos);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Controls.Add(this.udCantfilas);
            this.tabPage2.Controls.Add(this.label1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(691, 308);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Layout del Depósito";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.udRacksFila10);
            this.groupBox1.Controls.Add(this.lblRacksFila10);
            this.groupBox1.Controls.Add(this.udRacksFila9);
            this.groupBox1.Controls.Add(this.lblRacksFila9);
            this.groupBox1.Controls.Add(this.udRacksFila8);
            this.groupBox1.Controls.Add(this.lblRacksFila8);
            this.groupBox1.Controls.Add(this.udRacksFila7);
            this.groupBox1.Controls.Add(this.lblRacksFila7);
            this.groupBox1.Controls.Add(this.udRacksFila6);
            this.groupBox1.Controls.Add(this.lblRacksFila6);
            this.groupBox1.Controls.Add(this.udRacksFila5);
            this.groupBox1.Controls.Add(this.lblRacksFila5);
            this.groupBox1.Controls.Add(this.udRacksFila4);
            this.groupBox1.Controls.Add(this.lblRacksFila4);
            this.groupBox1.Controls.Add(this.udRacksFila3);
            this.groupBox1.Controls.Add(this.lblRacksFila3);
            this.groupBox1.Controls.Add(this.udRacksFila2);
            this.groupBox1.Controls.Add(this.lblRacksFila2);
            this.groupBox1.Controls.Add(this.udRacksFila1);
            this.groupBox1.Controls.Add(this.lblRacksFila1);
            this.groupBox1.Location = new System.Drawing.Point(200, 8);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(280, 290);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Cantidad de Racks x Fila";
            // 
            // udRacksFila10
            // 
            this.udRacksFila10.Location = new System.Drawing.Point(48, 252);
            this.udRacksFila10.Name = "udRacksFila10";
            this.udRacksFila10.Size = new System.Drawing.Size(51, 20);
            this.udRacksFila10.TabIndex = 19;
            // 
            // lblRacksFila10
            // 
            this.lblRacksFila10.AutoSize = true;
            this.lblRacksFila10.Location = new System.Drawing.Point(1, 254);
            this.lblRacksFila10.Name = "lblRacksFila10";
            this.lblRacksFila10.Size = new System.Drawing.Size(41, 13);
            this.lblRacksFila10.TabIndex = 18;
            this.lblRacksFila10.Text = "Fila 10:";
            // 
            // udRacksFila9
            // 
            this.udRacksFila9.Location = new System.Drawing.Point(48, 226);
            this.udRacksFila9.Name = "udRacksFila9";
            this.udRacksFila9.Size = new System.Drawing.Size(51, 20);
            this.udRacksFila9.TabIndex = 17;
            // 
            // lblRacksFila9
            // 
            this.lblRacksFila9.AutoSize = true;
            this.lblRacksFila9.Location = new System.Drawing.Point(7, 228);
            this.lblRacksFila9.Name = "lblRacksFila9";
            this.lblRacksFila9.Size = new System.Drawing.Size(35, 13);
            this.lblRacksFila9.TabIndex = 16;
            this.lblRacksFila9.Text = "Fila 9:";
            // 
            // udRacksFila8
            // 
            this.udRacksFila8.Location = new System.Drawing.Point(48, 200);
            this.udRacksFila8.Name = "udRacksFila8";
            this.udRacksFila8.Size = new System.Drawing.Size(51, 20);
            this.udRacksFila8.TabIndex = 15;
            // 
            // lblRacksFila8
            // 
            this.lblRacksFila8.AutoSize = true;
            this.lblRacksFila8.Location = new System.Drawing.Point(7, 202);
            this.lblRacksFila8.Name = "lblRacksFila8";
            this.lblRacksFila8.Size = new System.Drawing.Size(35, 13);
            this.lblRacksFila8.TabIndex = 14;
            this.lblRacksFila8.Text = "Fila 8:";
            // 
            // udRacksFila7
            // 
            this.udRacksFila7.Location = new System.Drawing.Point(48, 174);
            this.udRacksFila7.Name = "udRacksFila7";
            this.udRacksFila7.Size = new System.Drawing.Size(51, 20);
            this.udRacksFila7.TabIndex = 13;
            // 
            // lblRacksFila7
            // 
            this.lblRacksFila7.AutoSize = true;
            this.lblRacksFila7.Location = new System.Drawing.Point(7, 176);
            this.lblRacksFila7.Name = "lblRacksFila7";
            this.lblRacksFila7.Size = new System.Drawing.Size(35, 13);
            this.lblRacksFila7.TabIndex = 12;
            this.lblRacksFila7.Text = "Fila 7:";
            // 
            // udRacksFila6
            // 
            this.udRacksFila6.Location = new System.Drawing.Point(48, 148);
            this.udRacksFila6.Name = "udRacksFila6";
            this.udRacksFila6.Size = new System.Drawing.Size(51, 20);
            this.udRacksFila6.TabIndex = 11;
            // 
            // lblRacksFila6
            // 
            this.lblRacksFila6.AutoSize = true;
            this.lblRacksFila6.Location = new System.Drawing.Point(7, 150);
            this.lblRacksFila6.Name = "lblRacksFila6";
            this.lblRacksFila6.Size = new System.Drawing.Size(35, 13);
            this.lblRacksFila6.TabIndex = 10;
            this.lblRacksFila6.Text = "Fila 6:";
            // 
            // udRacksFila5
            // 
            this.udRacksFila5.Location = new System.Drawing.Point(48, 122);
            this.udRacksFila5.Name = "udRacksFila5";
            this.udRacksFila5.Size = new System.Drawing.Size(51, 20);
            this.udRacksFila5.TabIndex = 9;
            // 
            // lblRacksFila5
            // 
            this.lblRacksFila5.AutoSize = true;
            this.lblRacksFila5.Location = new System.Drawing.Point(7, 124);
            this.lblRacksFila5.Name = "lblRacksFila5";
            this.lblRacksFila5.Size = new System.Drawing.Size(35, 13);
            this.lblRacksFila5.TabIndex = 8;
            this.lblRacksFila5.Text = "Fila 5:";
            // 
            // udRacksFila4
            // 
            this.udRacksFila4.Location = new System.Drawing.Point(48, 96);
            this.udRacksFila4.Name = "udRacksFila4";
            this.udRacksFila4.Size = new System.Drawing.Size(51, 20);
            this.udRacksFila4.TabIndex = 7;
            // 
            // lblRacksFila4
            // 
            this.lblRacksFila4.AutoSize = true;
            this.lblRacksFila4.Location = new System.Drawing.Point(7, 98);
            this.lblRacksFila4.Name = "lblRacksFila4";
            this.lblRacksFila4.Size = new System.Drawing.Size(35, 13);
            this.lblRacksFila4.TabIndex = 6;
            this.lblRacksFila4.Text = "Fila 4:";
            // 
            // udRacksFila3
            // 
            this.udRacksFila3.Location = new System.Drawing.Point(48, 70);
            this.udRacksFila3.Name = "udRacksFila3";
            this.udRacksFila3.Size = new System.Drawing.Size(51, 20);
            this.udRacksFila3.TabIndex = 5;
            // 
            // lblRacksFila3
            // 
            this.lblRacksFila3.AutoSize = true;
            this.lblRacksFila3.Location = new System.Drawing.Point(7, 72);
            this.lblRacksFila3.Name = "lblRacksFila3";
            this.lblRacksFila3.Size = new System.Drawing.Size(35, 13);
            this.lblRacksFila3.TabIndex = 4;
            this.lblRacksFila3.Text = "Fila 3:";
            // 
            // udRacksFila2
            // 
            this.udRacksFila2.Location = new System.Drawing.Point(48, 44);
            this.udRacksFila2.Name = "udRacksFila2";
            this.udRacksFila2.Size = new System.Drawing.Size(51, 20);
            this.udRacksFila2.TabIndex = 3;
            // 
            // lblRacksFila2
            // 
            this.lblRacksFila2.AutoSize = true;
            this.lblRacksFila2.Location = new System.Drawing.Point(7, 46);
            this.lblRacksFila2.Name = "lblRacksFila2";
            this.lblRacksFila2.Size = new System.Drawing.Size(35, 13);
            this.lblRacksFila2.TabIndex = 2;
            this.lblRacksFila2.Text = "Fila 2:";
            // 
            // udRacksFila1
            // 
            this.udRacksFila1.Location = new System.Drawing.Point(48, 18);
            this.udRacksFila1.Name = "udRacksFila1";
            this.udRacksFila1.Size = new System.Drawing.Size(51, 20);
            this.udRacksFila1.TabIndex = 1;
            // 
            // lblRacksFila1
            // 
            this.lblRacksFila1.AutoSize = true;
            this.lblRacksFila1.Location = new System.Drawing.Point(7, 20);
            this.lblRacksFila1.Name = "lblRacksFila1";
            this.lblRacksFila1.Size = new System.Drawing.Size(35, 13);
            this.lblRacksFila1.TabIndex = 0;
            this.lblRacksFila1.Text = "Fila 1:";
            // 
            // udCajasxPiso
            // 
            this.udCajasxPiso.Location = new System.Drawing.Point(134, 58);
            this.udCajasxPiso.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.udCajasxPiso.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udCajasxPiso.Name = "udCajasxPiso";
            this.udCajasxPiso.Size = new System.Drawing.Size(51, 20);
            this.udCajasxPiso.TabIndex = 5;
            this.udCajasxPiso.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 60);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(125, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Cantidad de cajas x piso:";
            // 
            // udCantPisos
            // 
            this.udCantPisos.Location = new System.Drawing.Point(134, 32);
            this.udCantPisos.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.udCantPisos.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udCantPisos.Name = "udCantPisos";
            this.udCantPisos.Size = new System.Drawing.Size(51, 20);
            this.udCantPisos.TabIndex = 3;
            this.udCantPisos.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(34, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Cantidad de pisos:";
            // 
            // udCantfilas
            // 
            this.udCantfilas.Location = new System.Drawing.Point(134, 6);
            this.udCantfilas.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.udCantfilas.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udCantfilas.Name = "udCantfilas";
            this.udCantfilas.Size = new System.Drawing.Size(51, 20);
            this.udCantfilas.TabIndex = 1;
            this.udCantfilas.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udCantfilas.ValueChanged += new System.EventHandler(this.udCantfilas_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(40, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Cantidad de filas:";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.udTarifaScan);
            this.tabPage1.Controls.Add(this.label21);
            this.tabPage1.Controls.Add(this.udMaxpaginas);
            this.tabPage1.Controls.Add(this.label20);
            this.tabPage1.Controls.Add(this.udDoc);
            this.tabPage1.Controls.Add(this.label19);
            this.tabPage1.Controls.Add(this.udAbonoHosting);
            this.tabPage1.Controls.Add(this.label18);
            this.tabPage1.Controls.Add(this.udTrasladoAdicional);
            this.tabPage1.Controls.Add(this.label14);
            this.tabPage1.Controls.Add(this.udPrecioCaja);
            this.tabPage1.Controls.Add(this.label13);
            this.tabPage1.Controls.Add(this.udTarifaTraslado);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.udTarifaConsulta);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.groupBox4);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(691, 308);
            this.tabPage1.TabIndex = 2;
            this.tabPage1.Text = "Tarifas";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // udTarifaScan
            // 
            this.udTarifaScan.DecimalPlaces = 2;
            this.udTarifaScan.Location = new System.Drawing.Point(449, 257);
            this.udTarifaScan.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.udTarifaScan.Name = "udTarifaScan";
            this.udTarifaScan.Size = new System.Drawing.Size(58, 20);
            this.udTarifaScan.TabIndex = 25;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(297, 259);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(146, 13);
            this.label21.TabIndex = 24;
            this.label21.Text = "Costo por página escaneada:";
            // 
            // udMaxpaginas
            // 
            this.udMaxpaginas.Location = new System.Drawing.Point(232, 257);
            this.udMaxpaginas.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.udMaxpaginas.Name = "udMaxpaginas";
            this.udMaxpaginas.Size = new System.Drawing.Size(59, 20);
            this.udMaxpaginas.TabIndex = 21;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(18, 259);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(208, 13);
            this.label20.TabIndex = 23;
            this.label20.Text = "Cantidad de páginas a escanear sin costo:";
            // 
            // udDoc
            // 
            this.udDoc.DecimalPlaces = 2;
            this.udDoc.Location = new System.Drawing.Point(165, 199);
            this.udDoc.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.udDoc.Name = "udDoc";
            this.udDoc.Size = new System.Drawing.Size(58, 20);
            this.udDoc.TabIndex = 22;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(18, 201);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(129, 13);
            this.label19.TabIndex = 21;
            this.label19.Text = "Búsqueda de documento:";
            // 
            // udAbonoHosting
            // 
            this.udAbonoHosting.DecimalPlaces = 2;
            this.udAbonoHosting.Location = new System.Drawing.Point(232, 232);
            this.udAbonoHosting.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.udAbonoHosting.Name = "udAbonoHosting";
            this.udAbonoHosting.Size = new System.Drawing.Size(58, 20);
            this.udAbonoHosting.TabIndex = 20;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(18, 234);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(193, 13);
            this.label18.TabIndex = 19;
            this.label18.Text = "Abono mensual de hosting de archivos:";
            // 
            // udTrasladoAdicional
            // 
            this.udTrasladoAdicional.DecimalPlaces = 2;
            this.udTrasladoAdicional.Location = new System.Drawing.Point(371, 147);
            this.udTrasladoAdicional.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.udTrasladoAdicional.Name = "udTrasladoAdicional";
            this.udTrasladoAdicional.Size = new System.Drawing.Size(58, 20);
            this.udTrasladoAdicional.TabIndex = 18;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(238, 149);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(127, 13);
            this.label14.TabIndex = 17;
            this.label14.Text = "Traslado x caja adicional:";
            // 
            // udPrecioCaja
            // 
            this.udPrecioCaja.DecimalPlaces = 2;
            this.udPrecioCaja.Location = new System.Drawing.Point(165, 173);
            this.udPrecioCaja.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.udPrecioCaja.Name = "udPrecioCaja";
            this.udPrecioCaja.Size = new System.Drawing.Size(58, 20);
            this.udPrecioCaja.TabIndex = 16;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(17, 175);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(99, 13);
            this.label13.TabIndex = 15;
            this.label13.Text = "Valor de cada caja:";
            // 
            // udTarifaTraslado
            // 
            this.udTarifaTraslado.DecimalPlaces = 2;
            this.udTarifaTraslado.Location = new System.Drawing.Point(166, 147);
            this.udTarifaTraslado.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.udTarifaTraslado.Name = "udTarifaTraslado";
            this.udTarifaTraslado.Size = new System.Drawing.Size(58, 20);
            this.udTarifaTraslado.TabIndex = 14;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(18, 149);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(82, 13);
            this.label12.TabIndex = 13;
            this.label12.Text = "Traslado x caja:";
            // 
            // udTarifaConsulta
            // 
            this.udTarifaConsulta.DecimalPlaces = 2;
            this.udTarifaConsulta.Location = new System.Drawing.Point(166, 121);
            this.udTarifaConsulta.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.udTarifaConsulta.Name = "udTarifaConsulta";
            this.udTarifaConsulta.Size = new System.Drawing.Size(58, 20);
            this.udTarifaConsulta.TabIndex = 12;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(18, 123);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(149, 13);
            this.label11.TabIndex = 11;
            this.label11.Text = "Búsqueda en Depósito x caja:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(122, 119);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(0, 13);
            this.label4.TabIndex = 10;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.udCajaExcedenteC);
            this.groupBox4.Controls.Add(this.label17);
            this.groupBox4.Controls.Add(this.udAbonoPlanC);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.udMaxPlanC);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Location = new System.Drawing.Point(458, 6);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(220, 105);
            this.groupBox4.TabIndex = 4;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Plan C";
            // 
            // udCajaExcedenteC
            // 
            this.udCajaExcedenteC.DecimalPlaces = 2;
            this.udCajaExcedenteC.Location = new System.Drawing.Point(103, 68);
            this.udCajaExcedenteC.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.udCajaExcedenteC.Name = "udCajaExcedenteC";
            this.udCajaExcedenteC.Size = new System.Drawing.Size(58, 20);
            this.udCajaExcedenteC.TabIndex = 22;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(10, 70);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(84, 13);
            this.label17.TabIndex = 21;
            this.label17.Text = "Caja excedente:";
            // 
            // udAbonoPlanC
            // 
            this.udAbonoPlanC.DecimalPlaces = 2;
            this.udAbonoPlanC.Location = new System.Drawing.Point(102, 40);
            this.udAbonoPlanC.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.udAbonoPlanC.Name = "udAbonoPlanC";
            this.udAbonoPlanC.Size = new System.Drawing.Size(58, 20);
            this.udAbonoPlanC.TabIndex = 3;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 42);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(84, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "Abono Mensual:";
            // 
            // udMaxPlanC
            // 
            this.udMaxPlanC.Location = new System.Drawing.Point(102, 14);
            this.udMaxPlanC.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.udMaxPlanC.Name = "udMaxPlanC";
            this.udMaxPlanC.Size = new System.Drawing.Size(59, 20);
            this.udMaxPlanC.TabIndex = 1;
            this.udMaxPlanC.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 16);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(90, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "Máximo de Cajas:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.udCajaExcedenteB);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.udAbonoPlanB);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.udMaxPlanB);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Location = new System.Drawing.Point(232, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(220, 105);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Plan B";
            // 
            // udCajaExcedenteB
            // 
            this.udCajaExcedenteB.DecimalPlaces = 2;
            this.udCajaExcedenteB.Location = new System.Drawing.Point(102, 66);
            this.udCajaExcedenteB.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.udCajaExcedenteB.Name = "udCajaExcedenteB";
            this.udCajaExcedenteB.Size = new System.Drawing.Size(58, 20);
            this.udCajaExcedenteB.TabIndex = 22;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(9, 68);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(84, 13);
            this.label16.TabIndex = 21;
            this.label16.Text = "Caja excedente:";
            // 
            // udAbonoPlanB
            // 
            this.udAbonoPlanB.DecimalPlaces = 2;
            this.udAbonoPlanB.Location = new System.Drawing.Point(102, 40);
            this.udAbonoPlanB.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.udAbonoPlanB.Name = "udAbonoPlanB";
            this.udAbonoPlanB.Size = new System.Drawing.Size(58, 20);
            this.udAbonoPlanB.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 42);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(84, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "Abono Mensual:";
            // 
            // udMaxPlanB
            // 
            this.udMaxPlanB.Location = new System.Drawing.Point(102, 14);
            this.udMaxPlanB.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.udMaxPlanB.Name = "udMaxPlanB";
            this.udMaxPlanB.Size = new System.Drawing.Size(59, 20);
            this.udMaxPlanB.TabIndex = 1;
            this.udMaxPlanB.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(90, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Máximo de Cajas:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.udCajaExcedenteA);
            this.groupBox2.Controls.Add(this.udAbonoPlanA);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.udMaxPlanA);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Location = new System.Drawing.Point(6, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(220, 105);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Plan A";
            // 
            // udCajaExcedenteA
            // 
            this.udCajaExcedenteA.DecimalPlaces = 2;
            this.udCajaExcedenteA.Location = new System.Drawing.Point(103, 66);
            this.udCajaExcedenteA.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.udCajaExcedenteA.Name = "udCajaExcedenteA";
            this.udCajaExcedenteA.Size = new System.Drawing.Size(58, 20);
            this.udCajaExcedenteA.TabIndex = 20;
            // 
            // udAbonoPlanA
            // 
            this.udAbonoPlanA.DecimalPlaces = 2;
            this.udAbonoPlanA.Location = new System.Drawing.Point(102, 40);
            this.udAbonoPlanA.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.udAbonoPlanA.Name = "udAbonoPlanA";
            this.udAbonoPlanA.Size = new System.Drawing.Size(58, 20);
            this.udAbonoPlanA.TabIndex = 3;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(10, 68);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(84, 13);
            this.label15.TabIndex = 19;
            this.label15.Text = "Caja excedente:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 42);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(84, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Abono Mensual:";
            // 
            // udMaxPlanA
            // 
            this.udMaxPlanA.Location = new System.Drawing.Point(102, 14);
            this.udMaxPlanA.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.udMaxPlanA.Name = "udMaxPlanA";
            this.udMaxPlanA.Size = new System.Drawing.Size(59, 20);
            this.udMaxPlanA.TabIndex = 1;
            this.udMaxPlanA.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(90, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Máximo de Cajas:";
            // 
            // btnAceptar
            // 
            this.btnAceptar.Location = new System.Drawing.Point(471, 353);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(75, 23);
            this.btnAceptar.TabIndex = 1;
            this.btnAceptar.Text = "Aceptar";
            this.btnAceptar.UseVisualStyleBackColor = true;
            this.btnAceptar.Click += new System.EventHandler(this.btnAceptar_Click);
            // 
            // btncancelar
            // 
            this.btncancelar.Location = new System.Drawing.Point(552, 353);
            this.btncancelar.Name = "btncancelar";
            this.btncancelar.Size = new System.Drawing.Size(75, 23);
            this.btncancelar.TabIndex = 2;
            this.btncancelar.Text = "Cancelar";
            this.btncancelar.UseVisualStyleBackColor = true;
            this.btncancelar.Click += new System.EventHandler(this.btncancelar_Click);
            // 
            // btnaplicar
            // 
            this.btnaplicar.Location = new System.Drawing.Point(633, 353);
            this.btnaplicar.Name = "btnaplicar";
            this.btnaplicar.Size = new System.Drawing.Size(75, 23);
            this.btnaplicar.TabIndex = 3;
            this.btnaplicar.Text = "Aplicar";
            this.btnaplicar.UseVisualStyleBackColor = true;
            this.btnaplicar.Click += new System.EventHandler(this.btnaplicar_Click);
            // 
            // frmConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(719, 387);
            this.Controls.Add(this.btnaplicar);
            this.Controls.Add(this.btncancelar);
            this.Controls.Add(this.btnAceptar);
            this.Controls.Add(this.tabControl1);
            this.Name = "frmConfig";
            this.Text = "Configuración";
            this.Load += new System.EventHandler(this.Config_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udRacksFila10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udRacksFila9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udRacksFila8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udRacksFila7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udRacksFila6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udRacksFila5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udRacksFila4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udRacksFila3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udRacksFila2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udRacksFila1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udCajasxPiso)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udCantPisos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udCantfilas)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udTarifaScan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udMaxpaginas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udDoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udAbonoHosting)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udTrasladoAdicional)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udPrecioCaja)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udTarifaTraslado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udTarifaConsulta)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udCajaExcedenteC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udAbonoPlanC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udMaxPlanC)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udCajaExcedenteB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udAbonoPlanB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udMaxPlanB)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udCajaExcedenteA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udAbonoPlanA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udMaxPlanA)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.NumericUpDown udCantfilas;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown udCantPisos;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown udCajasxPiso;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown udRacksFila1;
        private System.Windows.Forms.Label lblRacksFila1;
        private System.Windows.Forms.NumericUpDown udRacksFila10;
        private System.Windows.Forms.Label lblRacksFila10;
        private System.Windows.Forms.NumericUpDown udRacksFila9;
        private System.Windows.Forms.Label lblRacksFila9;
        private System.Windows.Forms.NumericUpDown udRacksFila8;
        private System.Windows.Forms.Label lblRacksFila8;
        private System.Windows.Forms.NumericUpDown udRacksFila7;
        private System.Windows.Forms.Label lblRacksFila7;
        private System.Windows.Forms.NumericUpDown udRacksFila6;
        private System.Windows.Forms.Label lblRacksFila6;
        private System.Windows.Forms.NumericUpDown udRacksFila5;
        private System.Windows.Forms.Label lblRacksFila5;
        private System.Windows.Forms.NumericUpDown udRacksFila4;
        private System.Windows.Forms.Label lblRacksFila4;
        private System.Windows.Forms.NumericUpDown udRacksFila3;
        private System.Windows.Forms.Label lblRacksFila3;
        private System.Windows.Forms.NumericUpDown udRacksFila2;
        private System.Windows.Forms.Label lblRacksFila2;
        private System.Windows.Forms.Button btnAceptar;
        private System.Windows.Forms.Button btncancelar;
        private System.Windows.Forms.Button btnaplicar;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.NumericUpDown udPrecioCaja;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.NumericUpDown udTarifaTraslado;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown udTarifaConsulta;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.NumericUpDown udAbonoPlanC;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown udMaxPlanC;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.NumericUpDown udAbonoPlanB;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown udMaxPlanB;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.NumericUpDown udAbonoPlanA;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown udMaxPlanA;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown udCajaExcedenteA;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.NumericUpDown udTrasladoAdicional;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.NumericUpDown udCajaExcedenteC;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.NumericUpDown udCajaExcedenteB;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.NumericUpDown udAbonoHosting;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.NumericUpDown udDoc;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.NumericUpDown udTarifaScan;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.NumericUpDown udMaxpaginas;
    }
}