﻿namespace Archivo
{
    partial class frmImprimePlanilla
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.pctlogo = new System.Windows.Forms.PictureBox();
            this.pctbarras = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pctlogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctbarras)).BeginInit();
            this.SuspendLayout();
            // 
            // printDialog1
            // 
            this.printDialog1.Document = this.printDocument1;
            this.printDialog1.UseEXDialog = true;
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);
            // 
            // pctlogo
            // 
            this.pctlogo.Image = global::Archivo.Properties.Resources.logo_para_soft;
            this.pctlogo.Location = new System.Drawing.Point(44, 77);
            this.pctlogo.Name = "pctlogo";
            this.pctlogo.Size = new System.Drawing.Size(460, 121);
            this.pctlogo.TabIndex = 56;
            this.pctlogo.TabStop = false;
            // 
            // pctbarras
            // 
            this.pctbarras.Location = new System.Drawing.Point(124, 213);
            this.pctbarras.Name = "pctbarras";
            this.pctbarras.Size = new System.Drawing.Size(117, 50);
            this.pctbarras.TabIndex = 57;
            this.pctbarras.TabStop = false;
            // 
            // frmImprimePlanilla
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(421, 318);
            this.Controls.Add(this.pctbarras);
            this.Controls.Add(this.pctlogo);
            this.Name = "frmImprimePlanilla";
            this.Text = "Impresión de Planillas";
            this.Load += new System.EventHandler(this.frmImprimePlanilla_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pctlogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctbarras)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PrintDialog printDialog1;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.PictureBox pctlogo;
        private System.Windows.Forms.PictureBox pctbarras;
    }
}