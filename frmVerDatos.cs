﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Archivo
{
    public partial class frmVerDatos : Form
    {
        public frmVerDatos()
        {
            InitializeComponent();
        }


        private void frmVerDatos_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'archivoDataSet.clientes' table. You can move, or remove it, as needed.
            this.clientesTableAdapter.Fill(this.archivoDataSet.clientes);
            // TODO: This line of code loads data into the 'archivoDataSet.cajas' table. You can move, or remove it, as needed.
//            this.cajasTableAdapter.Fill(this.archivoDataSet.cajas);

            for (int i = 0; i <= archivoDataSet.clientes.Rows.Count - 1; i++)
            {
                if (lbNombreEmpresa.FindString(clientesDataGridView.Rows[i].Cells[1].Value.ToString()) == -1)
                    lbNombreEmpresa.Items.Add(clientesDataGridView.Rows[i].Cells[1].Value.ToString());
            }
            txtNombreEmpresa.AutoCompleteCustomSource.Clear();
            foreach (object o in lbNombreEmpresa.Items)
                txtNombreEmpresa.AutoCompleteCustomSource.Add(o.ToString());


        }

        private void txtNombreEmpresa_TextChanged(object sender, EventArgs e)
        {
        }

        private void frmVerDatos_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return && tbNocliente.Focused)
            {
                DataView dv = archivoDataSet.clientes.DefaultView;
                string filter = "Nocliente > '0'";      // Carga todos los registros
                dv.RowFilter = filter;
                clientesBindingSource.DataSource = dv;
                filter = "Nocliente = " + tbNocliente.Text; // Busca el Nº de cliente
                if (tbNocliente.Text != "")
                {
                    dv.RowFilter = filter;
                    if (clientesDataGridView.RowCount != 0)
                        txtNombreEmpresa.Text = clientesDataGridView.CurrentRow.Cells[1].Value.ToString(); // carga el nombre de la empresa
                }
            }

        }

        private void cbLote_CheckedChanged(object sender, EventArgs e)
        {
            if (cbLote.Checked)
                udNolote.Enabled = true;
            else
                udNolote.Enabled = false;
        }

        private void cbFecha_CheckedChanged(object sender, EventArgs e)
        {
            if (cbFecha.Checked)
            {
                dtFechaDesde.Enabled = true;
                dtFechaHasta.Enabled = true;
            }
            else
            {
                dtFechaDesde.Enabled = false;
                dtFechaHasta.Enabled = false;
            }
        }

        private void cbContenido_CheckedChanged(object sender, EventArgs e)
        {
            if (cbContenido.Checked)
                tbContenido.Enabled = true;
            else
                tbContenido.Enabled = false;
        }

        private void cbConsulta_CheckedChanged(object sender, EventArgs e)
        {
            if (cbConsulta.Checked)
            {
                rbConsultaNo.Enabled = true;
                rbConsultaSi.Enabled = true;
            }
            else
            {
                rbConsultaNo.Enabled = false;
                rbConsultaSi.Enabled = false;
            }
        }

        private void btnFiltroParam_Click(object sender, EventArgs e)
        {
            string filter="";
            string noclientestr="0";
            if (tbNocliente.Text != "")    // Si se filtra por cliente
                noclientestr = tbNocliente.Text;
            int lote = 0;
            if (cbLote.Checked)
                lote = Convert.ToInt32(udNolote.Value);
            Nullable<DateTime> fechaingdesde = null;
            Nullable<DateTime> fechainghasta = null;
            if (cbFecha.Checked)
            {
                fechaingdesde = dtFechaDesde.Value;
                fechainghasta = dtFechaHasta.Value;
                fechaingdesde =fechaingdesde.Value.Date+ new TimeSpan(0, 0, 0);
                fechainghasta =fechainghasta.Value.Date + new TimeSpan(23, 59, 59);
            }
            string contenido = "";
            if (cbContenido.Checked)
                contenido = "%" + tbContenido.Text + "%";
            string contenido2 = "";
            if (cbContenido2.Checked)
                contenido2 = "%" + tbContenido2.Text + "%";
            string contenido3 = "";
            if (cbContenido3.Checked)
                contenido3 = "%" + tbContenido3.Text + "%";
            Nullable<bool> consulta = null;
            if (cbConsulta.Checked)
            {
                consulta = rbConsultaSi.Checked;
            }
            Nullable<int> numero = null;
            if (cbNumero.Checked)
                numero = Convert.ToInt32(tbnumero.Text);
            Nullable<DateTime> fechacont = null;
            if (cbFechaCont.Checked)
            {
                fechacont = dtFechaCont.Value;
            }
            cajasTableAdapter.Fillfiltro1(archivoDataSet.cajas, noclientestr, lote, fechaingdesde, fechainghasta, contenido,contenido2,contenido3,consulta,numero,fechacont);
        }

        private void btnBuscaCodigo_Click(object sender, EventArgs e)
        {
            if (tbCodigo.Text.Length <= 19)
            {
                cajasTableAdapter.Fillfiltro2(archivoDataSet.cajas,tbCodigo.Text);
            }
            else
            {
                MessageBox.Show("El código no puede tener más de 19 caracteres.");
                return;
            }

        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.cajasBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.archivoDataSet);

        }

        private void txtNombreEmpresa_Leave(object sender, EventArgs e)
        {
            if (txtNombreEmpresa.Text == "")
            {
                tbNocliente.Text = "";
                return;
            }
            DataView dv = archivoDataSet.clientes.DefaultView;
            string filter = "Nocliente > '0'";      // Carga todos los registros
            dv.RowFilter = filter;
            clientesBindingSource.DataSource = dv;
            filter = "Nombreempresa LIKE '*" + txtNombreEmpresa.Text + "*'"; // Busca el nombre de la empresa
            dv.RowFilter = filter;
            if (clientesDataGridView.RowCount != 0)
                tbNocliente.Text = clientesDataGridView.CurrentRow.Cells[0].Value.ToString();   // Carga el No de cliente en tbNocliente
        }

        private void cbNumero_CheckedChanged(object sender, EventArgs e)
        {
            if (cbNumero.Checked) tbnumero.Enabled = true;
            else tbnumero.Enabled = false;
        }

        private void cbFechaCont_CheckedChanged(object sender, EventArgs e)
        {
            if (cbFechaCont.Checked) dtFechaCont.Enabled = true;
            else dtFechaCont.Enabled = false;
        }

        private void cbContenido2_CheckedChanged(object sender, EventArgs e)
        {
            if (cbContenido2.Checked)
                tbContenido2.Enabled = true;
            else
                tbContenido2.Enabled = false;
        }

        private void cbContenido3_CheckedChanged(object sender, EventArgs e)
        {
            if (cbContenido3.Checked)
                tbContenido3.Enabled = true;
            else
                tbContenido3.Enabled = false;
        }




    }
}
