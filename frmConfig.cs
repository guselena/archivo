﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Archivo
{
    public partial class frmConfig : Form
    {
        public frmConfig()
        {
            InitializeComponent();
        }

        private void actualizafilas()
        {
            lblRacksFila1.Visible=false;
            lblRacksFila2.Visible=false;
            lblRacksFila3.Visible=false;
            lblRacksFila4.Visible=false;
            lblRacksFila5.Visible=false;
            lblRacksFila6.Visible=false;
            lblRacksFila7.Visible=false;
            lblRacksFila8.Visible=false;
            lblRacksFila9.Visible=false;
            lblRacksFila10.Visible=false;
            udRacksFila1.Visible=false;
            udRacksFila2.Visible=false;
            udRacksFila3.Visible=false;
            udRacksFila4.Visible=false;
            udRacksFila5.Visible=false;
            udRacksFila6.Visible=false;
            udRacksFila7.Visible=false;
            udRacksFila8.Visible=false;
            udRacksFila9.Visible=false;
            udRacksFila10.Visible=false;
            Array.Resize(ref Comun.cantracks, Convert.ToInt32(udCantfilas.Value));
            if(udCantfilas.Value>=1)
            {
                lblRacksFila1.Visible=true;
                udRacksFila1.Visible=true;
                if (Comun.cantracks[0] == 0) Comun.cantracks[0] = 1;
                udRacksFila1.Value=Comun.cantracks[0];
            }
            if(udCantfilas.Value>=2)
            {
                lblRacksFila2.Visible=true;
                udRacksFila2.Visible=true;
                if (Comun.cantracks[1] == 0) Comun.cantracks[1] = 1;
                udRacksFila2.Value = Comun.cantracks[1];
            }
            if(udCantfilas.Value>=3)
            {
                lblRacksFila3.Visible=true;
                udRacksFila3.Visible=true;
                if (Comun.cantracks[2] == 0) Comun.cantracks[2] = 1;
                udRacksFila3.Value = Comun.cantracks[2];
            }
            if(udCantfilas.Value>=4)
            {
                lblRacksFila4.Visible=true;
                udRacksFila4.Visible=true;
                if (Comun.cantracks[3] == 0) Comun.cantracks[3] = 1;
                udRacksFila4.Value = Comun.cantracks[3];
            }
            if(udCantfilas.Value>=5)
            {
                lblRacksFila5.Visible=true;
                udRacksFila5.Visible=true;
                if (Comun.cantracks[4] == 0) Comun.cantracks[4] = 1;
                udRacksFila5.Value = Comun.cantracks[4];
            }
            if(udCantfilas.Value>=6)
            {
                lblRacksFila6.Visible=true;
                udRacksFila6.Visible=true;
                if (Comun.cantracks[5] == 0) Comun.cantracks[5] = 1;
                udRacksFila6.Value = Comun.cantracks[5];
            }
            if(udCantfilas.Value>=7)
            {
                lblRacksFila7.Visible=true;
                udRacksFila7.Visible=true;
                if (Comun.cantracks[6] == 0) Comun.cantracks[6] = 1;
                udRacksFila7.Value = Comun.cantracks[6];
            }
            if(udCantfilas.Value>=8)
            {
                lblRacksFila8.Visible=true;
                udRacksFila8.Visible=true;
                if (Comun.cantracks[7] == 0) Comun.cantracks[7] = 1;
                udRacksFila8.Value = Comun.cantracks[7];
            }
            if(udCantfilas.Value>=9)
            {
                lblRacksFila9.Visible=true;
                udRacksFila9.Visible=true;
                if (Comun.cantracks[8] == 0) Comun.cantracks[8] = 1;
                udRacksFila9.Value = Comun.cantracks[8];
            }
            if(udCantfilas.Value>=10)
            {
                lblRacksFila10.Visible=true;
                udRacksFila10.Visible=true;
                if (Comun.cantracks[9] == 0) Comun.cantracks[9] = 1;
                udRacksFila10.Value = Comun.cantracks[9];
            }



        }


        private void btnaplicar_Click(object sender, EventArgs e)
        {
            guardaregistro();
        }

        public void guardaregistro()
        {
            Comun.abonoplana = Convert.ToInt32(udAbonoPlanA.Value);
            Archivo.Properties.Settings.Default.Abonoplana = Comun.abonoplana;
            Comun.abonoplanb = Convert.ToInt32(udAbonoPlanB.Value);
            Archivo.Properties.Settings.Default.Abonoplanb = Comun.abonoplanb;
            Comun.abonoplanc = Convert.ToInt32(udAbonoPlanC.Value);
            Archivo.Properties.Settings.Default.Abonoplanc=Comun.abonoplanc;
            Comun.maxplana = Convert.ToInt32(udMaxPlanA.Value);
            Archivo.Properties.Settings.Default.Maxplana=Comun.maxplana;
            Comun.maxplanb = Convert.ToInt32(udMaxPlanB.Value);
            Archivo.Properties.Settings.Default.Maxplanb=Comun.maxplanb;
            Comun.maxplanc = Convert.ToInt32(udMaxPlanC.Value);
            Archivo.Properties.Settings.Default.Maxplanc=Comun.maxplanc;
            Comun.tarifabusqueda = udTarifaConsulta.Value;
            Archivo.Properties.Settings.Default.Tarifaconsulta=Comun.tarifabusqueda;
            Comun.tarifatraslado = udTarifaTraslado.Value;
            Archivo.Properties.Settings.Default.Tarifatraslado=Comun.tarifatraslado;
            Comun.preciocaja = udPrecioCaja.Value;
            Archivo.Properties.Settings.Default.Preciocaja=Comun.preciocaja;
            Comun.cajaexcedenteA = udCajaExcedenteA.Value;
            Archivo.Properties.Settings.Default.Cajaexcedentea=Comun.cajaexcedenteA;
            Comun.cajaexcedenteB = udCajaExcedenteB.Value;
            Archivo.Properties.Settings.Default.Cajaexcedenteb=Comun.cajaexcedenteB;
            Comun.cajaexcedenteC = udCajaExcedenteC.Value;
            Archivo.Properties.Settings.Default.Cajaexcedentec=Comun.cajaexcedenteC;
            Comun.tarifatrasladoadicional=udTrasladoAdicional.Value;
            Archivo.Properties.Settings.Default.Trasladoadicional=Comun.tarifatrasladoadicional;
            Comun.tarifatraslado = udTarifaTraslado.Value;
            Archivo.Properties.Settings.Default.Tarifatraslado = Comun.tarifatraslado;
            Comun.abonohosting = Convert.ToInt32(udAbonoHosting.Value);
            Archivo.Properties.Settings.Default.Abonohosting = Comun.abonohosting;
            Comun.cantfilas=Convert.ToInt32(udCantfilas.Value);
            Archivo.Properties.Settings.Default.Cantfilas=Comun.cantfilas;
            Comun.cantpisos=Convert.ToInt32(udCantPisos.Value);
            Archivo.Properties.Settings.Default.Cantpisos=Comun.cantpisos;
            Comun.cantcajasxpiso=Convert.ToInt32(udCajasxPiso.Value);
            Archivo.Properties.Settings.Default.Cantcajasxpiso=Comun.cantcajasxpiso;
            Comun.tarifadoc = udDoc.Value;
            Archivo.Properties.Settings.Default.Tarifadocumento = Comun.tarifadoc;
            Comun.maxpaginas = Convert.ToInt32(udMaxpaginas.Value);
            Archivo.Properties.Settings.Default.Maxpaginas = Comun.maxpaginas;
            Comun.tarifascan = udTarifaScan.Value;
            Archivo.Properties.Settings.Default.Tarifascan = Comun.tarifascan;

            if(Comun.cantfilas>=1)
            {
                Comun.cantracks[0]=Convert.ToInt32(udRacksFila1.Value);
                Archivo.Properties.Settings.Default.Cantracks1=Comun.cantracks[0];
            }
            if(Comun.cantfilas>=2)
            {
                Comun.cantracks[1]=Convert.ToInt32(udRacksFila2.Value);
                Archivo.Properties.Settings.Default.Cantracks2 = Comun.cantracks[1];
            }
            if(Comun.cantfilas>=3)
            {
                Comun.cantracks[2]=Convert.ToInt32(udRacksFila3.Value);
                Archivo.Properties.Settings.Default.Cantracks3 = Comun.cantracks[2];
            }
            if(Comun.cantfilas>=4)
            {
                Comun.cantracks[3]=Convert.ToInt32(udRacksFila4.Value);
                Archivo.Properties.Settings.Default.Cantracks4 = Comun.cantracks[3];
            }
            if(Comun.cantfilas>=5)
            {
                Comun.cantracks[4]=Convert.ToInt32(udRacksFila5.Value);
                Archivo.Properties.Settings.Default.Cantracks5 = Comun.cantracks[4];
            }
            if(Comun.cantfilas>=6)
            {
                Comun.cantracks[5]=Convert.ToInt32(udRacksFila6.Value);
                Archivo.Properties.Settings.Default.Cantracks6 = Comun.cantracks[5];
            }
            if(Comun.cantfilas>=7)
            {
                Comun.cantracks[6]=Convert.ToInt32(udRacksFila7.Value);
                Archivo.Properties.Settings.Default.Cantracks7 = Comun.cantracks[6];
            }
            if(Comun.cantfilas>=8)
            {
                Comun.cantracks[7]=Convert.ToInt32(udRacksFila8.Value);
                Archivo.Properties.Settings.Default.Cantracks8 = Comun.cantracks[7];
            }
            if(Comun.cantfilas>=9)
            {
                Comun.cantracks[8]=Convert.ToInt32(udRacksFila9.Value);
                Archivo.Properties.Settings.Default.Cantracks9 = Comun.cantracks[8];
            }
            if(Comun.cantfilas>=10)
            {
                Comun.cantracks[9]=Convert.ToInt32(udRacksFila10.Value);
                Archivo.Properties.Settings.Default.Cantracks10 = Comun.cantracks[9];
            }
            Archivo.Properties.Settings.Default.Save();
        }

        private void Config_Load(object sender, EventArgs e)
        {
            udCantfilas.Value=Convert.ToDecimal(Comun.cantfilas);
            udCantPisos.Value=Convert.ToDecimal(Comun.cantpisos);
            udCajasxPiso.Value=Convert.ToDecimal(Comun.cantcajasxpiso);
            actualizafilas();

            udAbonoPlanA.Value = Convert.ToDecimal(Comun.abonoplana);
            udAbonoPlanB.Value = Convert.ToDecimal(Comun.abonoplanb);
            udAbonoPlanC.Value = Convert.ToDecimal(Comun.abonoplanc);
            udMaxPlanA.Value = Convert.ToDecimal(Comun.maxplana);
            udMaxPlanB.Value = Convert.ToDecimal(Comun.maxplanb);
            udMaxPlanC.Value = Convert.ToDecimal(Comun.maxplanc);
            udCajaExcedenteA.Value = Comun.cajaexcedenteA;
            udCajaExcedenteB.Value = Comun.cajaexcedenteB;
            udCajaExcedenteC.Value = Comun.cajaexcedenteC;


            udTrasladoAdicional.Value = Comun.tarifatrasladoadicional;
            udTarifaConsulta.Value = Comun.tarifabusqueda;
            udTarifaTraslado.Value = Comun.tarifatraslado;
            udPrecioCaja.Value = Comun.preciocaja;
            udAbonoHosting.Value = Convert.ToDecimal(Comun.abonohosting);
            udDoc.Value = Comun.tarifadoc;
            udMaxpaginas.Value = Convert.ToDecimal(Comun.maxpaginas);
            udTarifaScan.Value = Comun.tarifascan;
        }

        private void btncancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            guardaregistro();
            this.Close();
        }

        private void udCantfilas_ValueChanged(object sender, EventArgs e)
        {
            actualizafilas();
        }
    }
}
