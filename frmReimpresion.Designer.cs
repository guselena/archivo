﻿namespace Archivo
{
    partial class frmReimpresion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnFiltrar = new System.Windows.Forms.Button();
            this.cbTipo = new System.Windows.Forms.CheckBox();
            this.cmbTipo = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.dtFechaHasta = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.dtFechaDesde = new System.Windows.Forms.DateTimePicker();
            this.cbFecha = new System.Windows.Forms.CheckBox();
            this.tbNombreClienteFiltro = new System.Windows.Forms.TextBox();
            this.cbNombreCliente = new System.Windows.Forms.CheckBox();
            this.tbNoClienteFiltro = new System.Windows.Forms.TextBox();
            this.cbNoCliente = new System.Windows.Forms.CheckBox();
            this.tbCodigoFiltro = new System.Windows.Forms.TextBox();
            this.cbCodigo = new System.Windows.Forms.CheckBox();
            this.cbNoOp = new System.Windows.Forms.CheckBox();
            this.tbNoOpFiltro = new System.Windows.Forms.TextBox();
            this.lbNombreEmpresa = new System.Windows.Forms.ListBox();
            this.btnimprimir = new System.Windows.Forms.Button();
            this.pctbarras = new System.Windows.Forms.PictureBox();
            this.pctlogo = new System.Windows.Forms.PictureBox();
            this.archivoDataSet = new Archivo.archivoDataSet();
            this.cajasBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cajasTableAdapter = new Archivo.archivoDataSetTableAdapters.cajasTableAdapter();
            this.tableAdapterManager = new Archivo.archivoDataSetTableAdapters.TableAdapterManager();
            this.clientesTableAdapter = new Archivo.archivoDataSetTableAdapters.clientesTableAdapter();
            this.operacionesTableAdapter = new Archivo.archivoDataSetTableAdapters.operacionesTableAdapter();
            this.cajasDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clientesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.clientesDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.operacionesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.operacionesDataGridView = new System.Windows.Forms.DataGridView();
            this.button1 = new System.Windows.Forms.Button();
            this.detallesoperacionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.detallesoperacionTableAdapter = new Archivo.archivoDataSetTableAdapters.detallesoperacionTableAdapter();
            this.NoOp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn31 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn32 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn33 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn34 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Descdoc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pctbarras)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctlogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.archivoDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cajasBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cajasDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientesDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.operacionesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.operacionesDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detallesoperacionBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnFiltrar);
            this.groupBox1.Controls.Add(this.cbTipo);
            this.groupBox1.Controls.Add(this.cmbTipo);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.dtFechaHasta);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.dtFechaDesde);
            this.groupBox1.Controls.Add(this.cbFecha);
            this.groupBox1.Controls.Add(this.tbNombreClienteFiltro);
            this.groupBox1.Controls.Add(this.cbNombreCliente);
            this.groupBox1.Controls.Add(this.tbNoClienteFiltro);
            this.groupBox1.Controls.Add(this.cbNoCliente);
            this.groupBox1.Controls.Add(this.tbCodigoFiltro);
            this.groupBox1.Controls.Add(this.cbCodigo);
            this.groupBox1.Controls.Add(this.cbNoOp);
            this.groupBox1.Controls.Add(this.tbNoOpFiltro);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox1.Location = new System.Drawing.Point(15, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1055, 173);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Filtrado";
            // 
            // btnFiltrar
            // 
            this.btnFiltrar.Location = new System.Drawing.Point(954, 144);
            this.btnFiltrar.Name = "btnFiltrar";
            this.btnFiltrar.Size = new System.Drawing.Size(87, 23);
            this.btnFiltrar.TabIndex = 21;
            this.btnFiltrar.Text = "Filtrar";
            this.btnFiltrar.UseVisualStyleBackColor = true;
            this.btnFiltrar.Click += new System.EventHandler(this.btnFiltrar_Click);
            // 
            // cbTipo
            // 
            this.cbTipo.AutoSize = true;
            this.cbTipo.Location = new System.Drawing.Point(19, 108);
            this.cbTipo.Name = "cbTipo";
            this.cbTipo.Size = new System.Drawing.Size(135, 17);
            this.cbTipo.TabIndex = 20;
            this.cbTipo.Text = "Tipo de Operación:";
            this.cbTipo.UseVisualStyleBackColor = true;
            this.cbTipo.CheckedChanged += new System.EventHandler(this.cbTipo_CheckedChanged);
            // 
            // cmbTipo
            // 
            this.cmbTipo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTipo.Enabled = false;
            this.cmbTipo.ForeColor = System.Drawing.Color.Black;
            this.cmbTipo.FormattingEnabled = true;
            this.cmbTipo.Items.AddRange(new object[] {
            "Baja",
            "Búsqueda de caja en depósito",
            "Búsqueda de documento",
            "Entrega de Cajas",
            "Ingreso",
            "Reingreso de Caja Consultada",
            "Reingreso de Documento",
            "Retiro para Consulta"});
            this.cmbTipo.Location = new System.Drawing.Point(162, 106);
            this.cmbTipo.Name = "cmbTipo";
            this.cmbTipo.Size = new System.Drawing.Size(216, 21);
            this.cmbTipo.Sorted = true;
            this.cmbTipo.TabIndex = 19;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(469, 84);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 13);
            this.label3.TabIndex = 18;
            this.label3.Text = "Hasta:";
            // 
            // dtFechaHasta
            // 
            this.dtFechaHasta.Enabled = false;
            this.dtFechaHasta.Location = new System.Drawing.Point(524, 80);
            this.dtFechaHasta.Name = "dtFechaHasta";
            this.dtFechaHasta.Size = new System.Drawing.Size(233, 21);
            this.dtFechaHasta.TabIndex = 17;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(160, 84);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 16;
            this.label2.Text = "Desde:";
            // 
            // dtFechaDesde
            // 
            this.dtFechaDesde.Enabled = false;
            this.dtFechaDesde.Location = new System.Drawing.Point(215, 80);
            this.dtFechaDesde.Name = "dtFechaDesde";
            this.dtFechaDesde.Size = new System.Drawing.Size(233, 21);
            this.dtFechaDesde.TabIndex = 15;
            // 
            // cbFecha
            // 
            this.cbFecha.AutoSize = true;
            this.cbFecha.Location = new System.Drawing.Point(21, 80);
            this.cbFecha.Name = "cbFecha";
            this.cbFecha.Size = new System.Drawing.Size(59, 17);
            this.cbFecha.TabIndex = 14;
            this.cbFecha.Text = "Fecha";
            this.cbFecha.UseVisualStyleBackColor = true;
            this.cbFecha.CheckedChanged += new System.EventHandler(this.cbFecha_CheckedChanged);
            // 
            // tbNombreClienteFiltro
            // 
            this.tbNombreClienteFiltro.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.tbNombreClienteFiltro.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.tbNombreClienteFiltro.BackColor = System.Drawing.Color.Wheat;
            this.tbNombreClienteFiltro.Enabled = false;
            this.tbNombreClienteFiltro.Location = new System.Drawing.Point(437, 54);
            this.tbNombreClienteFiltro.Name = "tbNombreClienteFiltro";
            this.tbNombreClienteFiltro.Size = new System.Drawing.Size(349, 21);
            this.tbNombreClienteFiltro.TabIndex = 13;
            // 
            // cbNombreCliente
            // 
            this.cbNombreCliente.AutoSize = true;
            this.cbNombreCliente.Location = new System.Drawing.Point(296, 57);
            this.cbNombreCliente.Name = "cbNombreCliente";
            this.cbNombreCliente.Size = new System.Drawing.Size(136, 17);
            this.cbNombreCliente.TabIndex = 12;
            this.cbNombreCliente.Text = "Nombre del Cliente";
            this.cbNombreCliente.UseVisualStyleBackColor = true;
            this.cbNombreCliente.CheckedChanged += new System.EventHandler(this.cbNombreCliente_CheckedChanged);
            // 
            // tbNoClienteFiltro
            // 
            this.tbNoClienteFiltro.BackColor = System.Drawing.Color.Wheat;
            this.tbNoClienteFiltro.Enabled = false;
            this.tbNoClienteFiltro.Location = new System.Drawing.Point(162, 54);
            this.tbNoClienteFiltro.Name = "tbNoClienteFiltro";
            this.tbNoClienteFiltro.Size = new System.Drawing.Size(45, 21);
            this.tbNoClienteFiltro.TabIndex = 11;
            // 
            // cbNoCliente
            // 
            this.cbNoCliente.AutoSize = true;
            this.cbNoCliente.Location = new System.Drawing.Point(21, 57);
            this.cbNoCliente.Name = "cbNoCliente";
            this.cbNoCliente.Size = new System.Drawing.Size(102, 17);
            this.cbNoCliente.TabIndex = 10;
            this.cbNoCliente.Text = "Nº de Cliente";
            this.cbNoCliente.UseVisualStyleBackColor = true;
            this.cbNoCliente.CheckedChanged += new System.EventHandler(this.cbNoCliente_CheckedChanged);
            // 
            // tbCodigoFiltro
            // 
            this.tbCodigoFiltro.BackColor = System.Drawing.Color.Wheat;
            this.tbCodigoFiltro.Enabled = false;
            this.tbCodigoFiltro.Location = new System.Drawing.Point(437, 29);
            this.tbCodigoFiltro.Name = "tbCodigoFiltro";
            this.tbCodigoFiltro.Size = new System.Drawing.Size(205, 21);
            this.tbCodigoFiltro.TabIndex = 9;
            // 
            // cbCodigo
            // 
            this.cbCodigo.AutoSize = true;
            this.cbCodigo.Location = new System.Drawing.Point(296, 33);
            this.cbCodigo.Name = "cbCodigo";
            this.cbCodigo.Size = new System.Drawing.Size(115, 17);
            this.cbCodigo.TabIndex = 8;
            this.cbCodigo.Text = "Código de Caja";
            this.cbCodigo.UseVisualStyleBackColor = true;
            this.cbCodigo.CheckedChanged += new System.EventHandler(this.cbCodigo_CheckedChanged);
            // 
            // cbNoOp
            // 
            this.cbNoOp.AutoSize = true;
            this.cbNoOp.Location = new System.Drawing.Point(21, 31);
            this.cbNoOp.Name = "cbNoOp";
            this.cbNoOp.Size = new System.Drawing.Size(120, 17);
            this.cbNoOp.TabIndex = 7;
            this.cbNoOp.Text = "Nº de Operación";
            this.cbNoOp.UseVisualStyleBackColor = true;
            this.cbNoOp.CheckedChanged += new System.EventHandler(this.cbNoOp_CheckedChanged);
            // 
            // tbNoOpFiltro
            // 
            this.tbNoOpFiltro.BackColor = System.Drawing.Color.Wheat;
            this.tbNoOpFiltro.Enabled = false;
            this.tbNoOpFiltro.Location = new System.Drawing.Point(162, 28);
            this.tbNoOpFiltro.Name = "tbNoOpFiltro";
            this.tbNoOpFiltro.Size = new System.Drawing.Size(116, 21);
            this.tbNoOpFiltro.TabIndex = 6;
            // 
            // lbNombreEmpresa
            // 
            this.lbNombreEmpresa.FormattingEnabled = true;
            this.lbNombreEmpresa.Location = new System.Drawing.Point(177, 399);
            this.lbNombreEmpresa.Name = "lbNombreEmpresa";
            this.lbNombreEmpresa.Size = new System.Drawing.Size(139, 95);
            this.lbNombreEmpresa.TabIndex = 38;
            // 
            // btnimprimir
            // 
            this.btnimprimir.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnimprimir.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnimprimir.Location = new System.Drawing.Point(15, 305);
            this.btnimprimir.Name = "btnimprimir";
            this.btnimprimir.Size = new System.Drawing.Size(569, 62);
            this.btnimprimir.TabIndex = 39;
            this.btnimprimir.Text = "Imprimir Planilla de Operación Seleccionada";
            this.btnimprimir.UseVisualStyleBackColor = false;
            this.btnimprimir.Click += new System.EventHandler(this.btnimprimir_Click);
            // 
            // pctbarras
            // 
            this.pctbarras.Location = new System.Drawing.Point(329, 496);
            this.pctbarras.Name = "pctbarras";
            this.pctbarras.Size = new System.Drawing.Size(117, 50);
            this.pctbarras.TabIndex = 54;
            this.pctbarras.TabStop = false;
            // 
            // pctlogo
            // 
            this.pctlogo.Image = global::Archivo.Properties.Resources.logo_para_soft;
            this.pctlogo.Location = new System.Drawing.Point(752, 452);
            this.pctlogo.Name = "pctlogo";
            this.pctlogo.Size = new System.Drawing.Size(460, 121);
            this.pctlogo.TabIndex = 55;
            this.pctlogo.TabStop = false;
            // 
            // archivoDataSet
            // 
            this.archivoDataSet.DataSetName = "archivoDataSet";
            this.archivoDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // cajasBindingSource
            // 
            this.cajasBindingSource.DataMember = "cajas";
            this.cajasBindingSource.DataSource = this.archivoDataSet;
            // 
            // cajasTableAdapter
            // 
            this.cajasTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.cajasTableAdapter = this.cajasTableAdapter;
            this.tableAdapterManager.clientesTableAdapter = this.clientesTableAdapter;
            this.tableAdapterManager.detallesoperacionTableAdapter = null;
            this.tableAdapterManager.operacionesTableAdapter = this.operacionesTableAdapter;
            this.tableAdapterManager.UpdateOrder = Archivo.archivoDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // clientesTableAdapter
            // 
            this.clientesTableAdapter.ClearBeforeFill = true;
            // 
            // operacionesTableAdapter
            // 
            this.operacionesTableAdapter.ClearBeforeFill = true;
            // 
            // cajasDataGridView
            // 
            this.cajasDataGridView.AllowUserToAddRows = false;
            this.cajasDataGridView.AllowUserToDeleteRows = false;
            this.cajasDataGridView.AutoGenerateColumns = false;
            this.cajasDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.cajasDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewCheckBoxColumn1,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9});
            this.cajasDataGridView.DataSource = this.cajasBindingSource;
            this.cajasDataGridView.Location = new System.Drawing.Point(87, 407);
            this.cajasDataGridView.Name = "cajasDataGridView";
            this.cajasDataGridView.ReadOnly = true;
            this.cajasDataGridView.Size = new System.Drawing.Size(350, 220);
            this.cajasDataGridView.TabIndex = 56;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Codigo";
            this.dataGridViewTextBoxColumn1.HeaderText = "Codigo";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Nocliente";
            this.dataGridViewTextBoxColumn2.HeaderText = "Nocliente";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Lote";
            this.dataGridViewTextBoxColumn3.HeaderText = "Lote";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "Fecha ingreso";
            this.dataGridViewTextBoxColumn4.HeaderText = "Fecha ingreso";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewCheckBoxColumn1
            // 
            this.dataGridViewCheckBoxColumn1.DataPropertyName = "Enconsulta";
            this.dataGridViewCheckBoxColumn1.HeaderText = "Enconsulta";
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            this.dataGridViewCheckBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "Contenido";
            this.dataGridViewTextBoxColumn5.HeaderText = "Contenido";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "Ubicacion";
            this.dataGridViewTextBoxColumn6.HeaderText = "Ubicacion";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "Nocaja";
            this.dataGridViewTextBoxColumn7.HeaderText = "Nocaja";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "Fechaconsulta";
            this.dataGridViewTextBoxColumn8.HeaderText = "Fechaconsulta";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "Codigoint";
            this.dataGridViewTextBoxColumn9.HeaderText = "Codigoint";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            // 
            // clientesBindingSource
            // 
            this.clientesBindingSource.DataMember = "clientes";
            this.clientesBindingSource.DataSource = this.archivoDataSet;
            // 
            // clientesDataGridView
            // 
            this.clientesDataGridView.AllowUserToAddRows = false;
            this.clientesDataGridView.AllowUserToDeleteRows = false;
            this.clientesDataGridView.AutoGenerateColumns = false;
            this.clientesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.clientesDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn13,
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewTextBoxColumn15,
            this.dataGridViewTextBoxColumn16,
            this.dataGridViewTextBoxColumn17,
            this.dataGridViewTextBoxColumn18,
            this.dataGridViewTextBoxColumn19,
            this.dataGridViewTextBoxColumn20,
            this.dataGridViewTextBoxColumn21,
            this.dataGridViewTextBoxColumn22,
            this.dataGridViewTextBoxColumn23,
            this.dataGridViewTextBoxColumn24,
            this.dataGridViewTextBoxColumn25,
            this.dataGridViewTextBoxColumn26,
            this.dataGridViewTextBoxColumn27,
            this.dataGridViewTextBoxColumn28,
            this.dataGridViewTextBoxColumn29});
            this.clientesDataGridView.DataSource = this.clientesBindingSource;
            this.clientesDataGridView.Location = new System.Drawing.Point(370, 407);
            this.clientesDataGridView.Name = "clientesDataGridView";
            this.clientesDataGridView.ReadOnly = true;
            this.clientesDataGridView.Size = new System.Drawing.Size(350, 220);
            this.clientesDataGridView.TabIndex = 56;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "Nocliente";
            this.dataGridViewTextBoxColumn10.HeaderText = "Nocliente";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "NombreEmpresa";
            this.dataGridViewTextBoxColumn11.HeaderText = "NombreEmpresa";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.DataPropertyName = "Direccion";
            this.dataGridViewTextBoxColumn12.HeaderText = "Direccion";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.DataPropertyName = "Localidad";
            this.dataGridViewTextBoxColumn13.HeaderText = "Localidad";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.DataPropertyName = "CP";
            this.dataGridViewTextBoxColumn14.HeaderText = "CP";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.DataPropertyName = "Telefono";
            this.dataGridViewTextBoxColumn15.HeaderText = "Telefono";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.DataPropertyName = "Nombrecont1";
            this.dataGridViewTextBoxColumn16.HeaderText = "Nombrecont1";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.DataPropertyName = "Telcont1";
            this.dataGridViewTextBoxColumn17.HeaderText = "Telcont1";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.DataPropertyName = "Nombrecont2";
            this.dataGridViewTextBoxColumn18.HeaderText = "Nombrecont2";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn19
            // 
            this.dataGridViewTextBoxColumn19.DataPropertyName = "Telcont2";
            this.dataGridViewTextBoxColumn19.HeaderText = "Telcont2";
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn20
            // 
            this.dataGridViewTextBoxColumn20.DataPropertyName = "Nombrecont3";
            this.dataGridViewTextBoxColumn20.HeaderText = "Nombrecont3";
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            this.dataGridViewTextBoxColumn20.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn21
            // 
            this.dataGridViewTextBoxColumn21.DataPropertyName = "Telcont3";
            this.dataGridViewTextBoxColumn21.HeaderText = "Telcont3";
            this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            this.dataGridViewTextBoxColumn21.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn22
            // 
            this.dataGridViewTextBoxColumn22.DataPropertyName = "Nombrecont4";
            this.dataGridViewTextBoxColumn22.HeaderText = "Nombrecont4";
            this.dataGridViewTextBoxColumn22.Name = "dataGridViewTextBoxColumn22";
            this.dataGridViewTextBoxColumn22.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn23
            // 
            this.dataGridViewTextBoxColumn23.DataPropertyName = "Telcont4";
            this.dataGridViewTextBoxColumn23.HeaderText = "Telcont4";
            this.dataGridViewTextBoxColumn23.Name = "dataGridViewTextBoxColumn23";
            this.dataGridViewTextBoxColumn23.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn24
            // 
            this.dataGridViewTextBoxColumn24.DataPropertyName = "Plan";
            this.dataGridViewTextBoxColumn24.HeaderText = "Plan";
            this.dataGridViewTextBoxColumn24.Name = "dataGridViewTextBoxColumn24";
            this.dataGridViewTextBoxColumn24.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn25
            // 
            this.dataGridViewTextBoxColumn25.DataPropertyName = "DireccionEntrega";
            this.dataGridViewTextBoxColumn25.HeaderText = "DireccionEntrega";
            this.dataGridViewTextBoxColumn25.Name = "dataGridViewTextBoxColumn25";
            this.dataGridViewTextBoxColumn25.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn26
            // 
            this.dataGridViewTextBoxColumn26.DataPropertyName = "CUIT";
            this.dataGridViewTextBoxColumn26.HeaderText = "CUIT";
            this.dataGridViewTextBoxColumn26.Name = "dataGridViewTextBoxColumn26";
            this.dataGridViewTextBoxColumn26.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn27
            // 
            this.dataGridViewTextBoxColumn27.DataPropertyName = "IVA";
            this.dataGridViewTextBoxColumn27.HeaderText = "IVA";
            this.dataGridViewTextBoxColumn27.Name = "dataGridViewTextBoxColumn27";
            this.dataGridViewTextBoxColumn27.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn28
            // 
            this.dataGridViewTextBoxColumn28.DataPropertyName = "usuario";
            this.dataGridViewTextBoxColumn28.HeaderText = "usuario";
            this.dataGridViewTextBoxColumn28.Name = "dataGridViewTextBoxColumn28";
            this.dataGridViewTextBoxColumn28.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn29
            // 
            this.dataGridViewTextBoxColumn29.DataPropertyName = "contrasena";
            this.dataGridViewTextBoxColumn29.HeaderText = "contrasena";
            this.dataGridViewTextBoxColumn29.Name = "dataGridViewTextBoxColumn29";
            this.dataGridViewTextBoxColumn29.ReadOnly = true;
            // 
            // operacionesBindingSource
            // 
            this.operacionesBindingSource.DataMember = "operaciones";
            this.operacionesBindingSource.DataSource = this.archivoDataSet;
            // 
            // operacionesDataGridView
            // 
            this.operacionesDataGridView.AllowUserToAddRows = false;
            this.operacionesDataGridView.AllowUserToDeleteRows = false;
            this.operacionesDataGridView.AutoGenerateColumns = false;
            this.operacionesDataGridView.BackgroundColor = System.Drawing.Color.Wheat;
            this.operacionesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.operacionesDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NoOp,
            this.dataGridViewTextBoxColumn31,
            this.dataGridViewTextBoxColumn32,
            this.dataGridViewTextBoxColumn33,
            this.dataGridViewTextBoxColumn34,
            this.dataGridViewCheckBoxColumn2,
            this.Descdoc});
            this.operacionesDataGridView.DataSource = this.operacionesBindingSource;
            this.operacionesDataGridView.Location = new System.Drawing.Point(15, 191);
            this.operacionesDataGridView.Name = "operacionesDataGridView";
            this.operacionesDataGridView.ReadOnly = true;
            this.operacionesDataGridView.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.Wheat;
            this.operacionesDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.operacionesDataGridView.Size = new System.Drawing.Size(1056, 108);
            this.operacionesDataGridView.TabIndex = 56;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(591, 305);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(479, 62);
            this.button1.TabIndex = 57;
            this.button1.Text = "Imprimir Etiqueta de Operación Seleccionada";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // detallesoperacionBindingSource
            // 
            this.detallesoperacionBindingSource.DataMember = "detallesoperacion";
            this.detallesoperacionBindingSource.DataSource = this.archivoDataSet;
            // 
            // detallesoperacionTableAdapter
            // 
            this.detallesoperacionTableAdapter.ClearBeforeFill = true;
            // 
            // NoOp
            // 
            this.NoOp.DataPropertyName = "NoOp";
            this.NoOp.HeaderText = "Nº de Operación";
            this.NoOp.Name = "NoOp";
            this.NoOp.ReadOnly = true;
            this.NoOp.Width = 65;
            // 
            // dataGridViewTextBoxColumn31
            // 
            this.dataGridViewTextBoxColumn31.DataPropertyName = "Tipo";
            this.dataGridViewTextBoxColumn31.HeaderText = "Tipo";
            this.dataGridViewTextBoxColumn31.Name = "dataGridViewTextBoxColumn31";
            this.dataGridViewTextBoxColumn31.ReadOnly = true;
            this.dataGridViewTextBoxColumn31.Width = 150;
            // 
            // dataGridViewTextBoxColumn32
            // 
            this.dataGridViewTextBoxColumn32.DataPropertyName = "Fecha";
            this.dataGridViewTextBoxColumn32.HeaderText = "Fecha";
            this.dataGridViewTextBoxColumn32.Name = "dataGridViewTextBoxColumn32";
            this.dataGridViewTextBoxColumn32.ReadOnly = true;
            this.dataGridViewTextBoxColumn32.Width = 200;
            // 
            // dataGridViewTextBoxColumn33
            // 
            this.dataGridViewTextBoxColumn33.DataPropertyName = "NoCliente";
            this.dataGridViewTextBoxColumn33.HeaderText = "Nº de Cliente";
            this.dataGridViewTextBoxColumn33.Name = "dataGridViewTextBoxColumn33";
            this.dataGridViewTextBoxColumn33.ReadOnly = true;
            this.dataGridViewTextBoxColumn33.Width = 65;
            // 
            // dataGridViewTextBoxColumn34
            // 
            this.dataGridViewTextBoxColumn34.DataPropertyName = "Codigo";
            this.dataGridViewTextBoxColumn34.HeaderText = "Código de Caja";
            this.dataGridViewTextBoxColumn34.Name = "dataGridViewTextBoxColumn34";
            this.dataGridViewTextBoxColumn34.ReadOnly = true;
            this.dataGridViewTextBoxColumn34.Width = 250;
            // 
            // dataGridViewCheckBoxColumn2
            // 
            this.dataGridViewCheckBoxColumn2.DataPropertyName = "Traslado";
            this.dataGridViewCheckBoxColumn2.HeaderText = "Traslado";
            this.dataGridViewCheckBoxColumn2.Name = "dataGridViewCheckBoxColumn2";
            this.dataGridViewCheckBoxColumn2.ReadOnly = true;
            // 
            // Descdoc
            // 
            this.Descdoc.DataPropertyName = "Descdoc";
            this.Descdoc.HeaderText = "Descdoc";
            this.Descdoc.Name = "Descdoc";
            this.Descdoc.ReadOnly = true;
            this.Descdoc.Visible = false;
            // 
            // frmReimpresion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SandyBrown;
            this.ClientSize = new System.Drawing.Size(1102, 379);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.operacionesDataGridView);
            this.Controls.Add(this.clientesDataGridView);
            this.Controls.Add(this.cajasDataGridView);
            this.Controls.Add(this.pctlogo);
            this.Controls.Add(this.pctbarras);
            this.Controls.Add(this.btnimprimir);
            this.Controls.Add(this.lbNombreEmpresa);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "frmReimpresion";
            this.Text = "Reimpresión de Planillas";
            this.Load += new System.EventHandler(this.frmReimpresion_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pctbarras)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctlogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.archivoDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cajasBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cajasDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientesDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.operacionesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.operacionesDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detallesoperacionBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox cbTipo;
        private System.Windows.Forms.ComboBox cmbTipo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dtFechaHasta;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtFechaDesde;
        private System.Windows.Forms.CheckBox cbFecha;
        private System.Windows.Forms.TextBox tbNombreClienteFiltro;
        private System.Windows.Forms.CheckBox cbNombreCliente;
        private System.Windows.Forms.TextBox tbNoClienteFiltro;
        private System.Windows.Forms.CheckBox cbNoCliente;
        private System.Windows.Forms.TextBox tbCodigoFiltro;
        private System.Windows.Forms.CheckBox cbCodigo;
        private System.Windows.Forms.CheckBox cbNoOp;
        private System.Windows.Forms.TextBox tbNoOpFiltro;
        private System.Windows.Forms.ListBox lbNombreEmpresa;
        private System.Windows.Forms.Button btnFiltrar;
        private System.Windows.Forms.Button btnimprimir;
        private System.Windows.Forms.PictureBox pctbarras;
        private System.Windows.Forms.PictureBox pctlogo;
        private archivoDataSet archivoDataSet;
        private System.Windows.Forms.BindingSource cajasBindingSource;
        private archivoDataSetTableAdapters.cajasTableAdapter cajasTableAdapter;
        private archivoDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private archivoDataSetTableAdapters.clientesTableAdapter clientesTableAdapter;
        private System.Windows.Forms.DataGridView cajasDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.BindingSource clientesBindingSource;
        private archivoDataSetTableAdapters.operacionesTableAdapter operacionesTableAdapter;
        private System.Windows.Forms.DataGridView clientesDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn23;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn24;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn25;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn26;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn27;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn28;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn29;
        private System.Windows.Forms.BindingSource operacionesBindingSource;
        private System.Windows.Forms.DataGridView operacionesDataGridView;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.BindingSource detallesoperacionBindingSource;
        private archivoDataSetTableAdapters.detallesoperacionTableAdapter detallesoperacionTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn NoOp;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn31;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn32;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn33;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn34;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Descdoc;
    }
}