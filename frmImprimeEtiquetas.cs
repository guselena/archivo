﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Archivo
{
    public partial class frmImprimeEtiquetas : Form
    {
        string codigo;
        public frmImprimeEtiquetas(string codigoaimprimir)
        {
            codigo = codigoaimprimir;
            InitializeComponent();
        }

        private void frmImprimeEtiquetas_Load(object sender, EventArgs e)
        {
            printDialog1.Document.DefaultPageSettings.Landscape = true;
            if (printDialog1.ShowDialog() == DialogResult.OK)
                printDocument1.Print();
            this.Close();
        }

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            string comando = Application.StartupPath + "\\Zint.exe ";
            string argumentos = "--notext -o barras.png -d " + codigo;
            System.Diagnostics.Process proc = new System.Diagnostics.Process();
            proc.StartInfo.FileName = comando;
            proc.StartInfo.Arguments = argumentos;
            proc.Start();
            proc.WaitForExit();
            System.IO.FileStream fs;
            fs = new System.IO.FileStream(Application.StartupPath + "\\barras.png", System.IO.FileMode.Open, System.IO.FileAccess.Read);
            pctbarras.Image = Image.FromStream(fs);
            fs.Close();

            Pen penlinea = new Pen(Color.Black, 1F);
            e.Graphics.PageUnit = GraphicsUnit.Millimeter;
            Graphics Canvas = e.Graphics;
            Canvas.PageUnit = GraphicsUnit.Millimeter;
            float dpiCorrectionX = Canvas.DpiX / 100;
            float dpiCorrectionY = Canvas.DpiY / 100;
            Point[] Points = new Point[2];

            
            Points[0] = new Point(Convert.ToInt32(e.PageBounds.X * dpiCorrectionX), Convert.ToInt32(e.PageBounds.Y * dpiCorrectionY));
            Points[1] = new Point(Convert.ToInt32(e.PageBounds.Width * dpiCorrectionX), Convert.ToInt32(e.PageBounds.Height * dpiCorrectionY));
            Canvas.TransformPoints(System.Drawing.Drawing2D.CoordinateSpace.Page, System.Drawing.Drawing2D.CoordinateSpace.Device, Points);
            Rectangle Bounds = new Rectangle(Points[0].X, Points[0].Y, Points[1].X - 20, Points[1].Y - 20);
            float mizq = Bounds.Left;
            float msup = Bounds.Top;
            float anchopag = Bounds.Width;
            float altopag = Bounds.Height;
            Font letranumeros=new Font("Arial",20);

            //e.Graphics.DrawRectangle(penlinea, mizq, msup, 62, 29);
            e.Graphics.DrawImage(pctbarras.Image, 1,0, 82, 17);
            e.Graphics.DrawString(codigo, letranumeros, Brushes.Black, 3, 18);

            
            
            e.HasMorePages = false;

            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            printDialog1.Document = printDocument1;
            if (printDialog1.ShowDialog() == DialogResult.OK)
                printDocument1.Print();
        }
    }
}
