﻿namespace Archivo
{
    partial class frmresumen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmresumen));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnFiltrar = new System.Windows.Forms.Button();
            this.cbTipo = new System.Windows.Forms.CheckBox();
            this.cmbTipo = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.dtFechaHasta = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.dtFechaDesde = new System.Windows.Forms.DateTimePicker();
            this.cbFecha = new System.Windows.Forms.CheckBox();
            this.tbNombreClienteFiltro = new System.Windows.Forms.TextBox();
            this.cbNombreCliente = new System.Windows.Forms.CheckBox();
            this.tbNoClienteFiltro = new System.Windows.Forms.TextBox();
            this.cbNoCliente = new System.Windows.Forms.CheckBox();
            this.lbNombreEmpresa = new System.Windows.Forms.ListBox();
            this.btnimprimir = new System.Windows.Forms.Button();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.pctlogo = new System.Windows.Forms.PictureBox();
            this.btnimpfact = new System.Windows.Forms.Button();
            this.printDocument2 = new System.Drawing.Printing.PrintDocument();
            this.label1 = new System.Windows.Forms.Label();
            this.dtfechafact = new System.Windows.Forms.DateTimePicker();
            this.archivoDataSet = new Archivo.archivoDataSet();
            this.clientesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.clientesTableAdapter = new Archivo.archivoDataSetTableAdapters.clientesTableAdapter();
            this.tableAdapterManager = new Archivo.archivoDataSetTableAdapters.TableAdapterManager();
            this.operacionesTableAdapter = new Archivo.archivoDataSetTableAdapters.operacionesTableAdapter();
            this.clientesDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hosting = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.operacionesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.operacionesDataGridView = new System.Windows.Forms.DataGridView();
            this.cajasBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cajasTableAdapter = new Archivo.archivoDataSetTableAdapters.cajasTableAdapter();
            this.cajasDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn31 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn32 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn33 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn34 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvfactura = new System.Windows.Forms.DataGridView();
            this.cantidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descripcion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.preciounit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.precio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btngenfact = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.udResNo = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.tbCondPago = new System.Windows.Forms.TextBox();
            this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Urgente = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Paginas = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pctlogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.archivoDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientesDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.operacionesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.operacionesDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cajasBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cajasDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvfactura)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udResNo)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnFiltrar);
            this.groupBox1.Controls.Add(this.cbTipo);
            this.groupBox1.Controls.Add(this.cmbTipo);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.dtFechaHasta);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.dtFechaDesde);
            this.groupBox1.Controls.Add(this.cbFecha);
            this.groupBox1.Controls.Add(this.tbNombreClienteFiltro);
            this.groupBox1.Controls.Add(this.cbNombreCliente);
            this.groupBox1.Controls.Add(this.tbNoClienteFiltro);
            this.groupBox1.Controls.Add(this.cbNoCliente);
            this.groupBox1.Location = new System.Drawing.Point(15, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(850, 137);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Criterio de Selección";
            // 
            // btnFiltrar
            // 
            this.btnFiltrar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnFiltrar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFiltrar.Location = new System.Drawing.Point(554, 70);
            this.btnFiltrar.Name = "btnFiltrar";
            this.btnFiltrar.Size = new System.Drawing.Size(231, 50);
            this.btnFiltrar.TabIndex = 36;
            this.btnFiltrar.Text = "Filtrar";
            this.btnFiltrar.UseVisualStyleBackColor = false;
            this.btnFiltrar.Click += new System.EventHandler(this.btnFiltrar_Click);
            // 
            // cbTipo
            // 
            this.cbTipo.AutoSize = true;
            this.cbTipo.Location = new System.Drawing.Point(16, 70);
            this.cbTipo.Name = "cbTipo";
            this.cbTipo.Size = new System.Drawing.Size(135, 17);
            this.cbTipo.TabIndex = 35;
            this.cbTipo.Text = "Tipo de Operación:";
            this.cbTipo.UseVisualStyleBackColor = true;
            this.cbTipo.CheckedChanged += new System.EventHandler(this.cbTipo_CheckedChanged);
            // 
            // cmbTipo
            // 
            this.cmbTipo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTipo.Enabled = false;
            this.cmbTipo.FormattingEnabled = true;
            this.cmbTipo.Items.AddRange(new object[] {
            "Baja",
            "Búsqueda de Caja en Depósito",
            "Búsqueda de Documento",
            "Entrega de Cajas",
            "Ingreso",
            "Reingreso de Caja Consultada",
            "Reingreso de Documento",
            "Retiro para Consulta"});
            this.cmbTipo.Location = new System.Drawing.Point(160, 68);
            this.cmbTipo.Name = "cmbTipo";
            this.cmbTipo.Size = new System.Drawing.Size(216, 21);
            this.cmbTipo.Sorted = true;
            this.cmbTipo.TabIndex = 34;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(467, 46);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 13);
            this.label3.TabIndex = 33;
            this.label3.Text = "Hasta:";
            // 
            // dtFechaHasta
            // 
            this.dtFechaHasta.Enabled = false;
            this.dtFechaHasta.Location = new System.Drawing.Point(521, 42);
            this.dtFechaHasta.Name = "dtFechaHasta";
            this.dtFechaHasta.Size = new System.Drawing.Size(233, 21);
            this.dtFechaHasta.TabIndex = 32;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(157, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 31;
            this.label2.Text = "Desde:";
            // 
            // dtFechaDesde
            // 
            this.dtFechaDesde.Enabled = false;
            this.dtFechaDesde.Location = new System.Drawing.Point(212, 42);
            this.dtFechaDesde.Name = "dtFechaDesde";
            this.dtFechaDesde.Size = new System.Drawing.Size(233, 21);
            this.dtFechaDesde.TabIndex = 30;
            // 
            // cbFecha
            // 
            this.cbFecha.AutoSize = true;
            this.cbFecha.Location = new System.Drawing.Point(19, 42);
            this.cbFecha.Name = "cbFecha";
            this.cbFecha.Size = new System.Drawing.Size(59, 17);
            this.cbFecha.TabIndex = 29;
            this.cbFecha.Text = "Fecha";
            this.cbFecha.UseVisualStyleBackColor = true;
            this.cbFecha.CheckedChanged += new System.EventHandler(this.cbFecha_CheckedChanged);
            // 
            // tbNombreClienteFiltro
            // 
            this.tbNombreClienteFiltro.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.tbNombreClienteFiltro.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.tbNombreClienteFiltro.BackColor = System.Drawing.Color.Wheat;
            this.tbNombreClienteFiltro.Enabled = false;
            this.tbNombreClienteFiltro.Location = new System.Drawing.Point(435, 16);
            this.tbNombreClienteFiltro.Name = "tbNombreClienteFiltro";
            this.tbNombreClienteFiltro.Size = new System.Drawing.Size(349, 21);
            this.tbNombreClienteFiltro.TabIndex = 28;
            // 
            // cbNombreCliente
            // 
            this.cbNombreCliente.AutoSize = true;
            this.cbNombreCliente.Location = new System.Drawing.Point(294, 19);
            this.cbNombreCliente.Name = "cbNombreCliente";
            this.cbNombreCliente.Size = new System.Drawing.Size(136, 17);
            this.cbNombreCliente.TabIndex = 27;
            this.cbNombreCliente.Text = "Nombre del Cliente";
            this.cbNombreCliente.UseVisualStyleBackColor = true;
            this.cbNombreCliente.CheckedChanged += new System.EventHandler(this.cbNombreCliente_CheckedChanged);
            // 
            // tbNoClienteFiltro
            // 
            this.tbNoClienteFiltro.BackColor = System.Drawing.Color.Wheat;
            this.tbNoClienteFiltro.Enabled = false;
            this.tbNoClienteFiltro.Location = new System.Drawing.Point(163, 17);
            this.tbNoClienteFiltro.Name = "tbNoClienteFiltro";
            this.tbNoClienteFiltro.Size = new System.Drawing.Size(45, 21);
            this.tbNoClienteFiltro.TabIndex = 26;
            // 
            // cbNoCliente
            // 
            this.cbNoCliente.AutoSize = true;
            this.cbNoCliente.Location = new System.Drawing.Point(19, 19);
            this.cbNoCliente.Name = "cbNoCliente";
            this.cbNoCliente.Size = new System.Drawing.Size(102, 17);
            this.cbNoCliente.TabIndex = 25;
            this.cbNoCliente.Text = "Nº de Cliente";
            this.cbNoCliente.UseVisualStyleBackColor = true;
            this.cbNoCliente.CheckedChanged += new System.EventHandler(this.cbNoCliente_CheckedChanged);
            // 
            // lbNombreEmpresa
            // 
            this.lbNombreEmpresa.FormattingEnabled = true;
            this.lbNombreEmpresa.Location = new System.Drawing.Point(980, 355);
            this.lbNombreEmpresa.Name = "lbNombreEmpresa";
            this.lbNombreEmpresa.Size = new System.Drawing.Size(139, 95);
            this.lbNombreEmpresa.TabIndex = 39;
            // 
            // btnimprimir
            // 
            this.btnimprimir.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnimprimir.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnimprimir.Location = new System.Drawing.Point(143, 434);
            this.btnimprimir.Name = "btnimprimir";
            this.btnimprimir.Size = new System.Drawing.Size(569, 50);
            this.btnimprimir.TabIndex = 41;
            this.btnimprimir.Text = "Imprimir Resumen con Operaciones Filtradas";
            this.btnimprimir.UseVisualStyleBackColor = false;
            this.btnimprimir.Click += new System.EventHandler(this.btnimprimir_Click);
            // 
            // printDialog1
            // 
            this.printDialog1.UseEXDialog = true;
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);
            // 
            // pctlogo
            // 
            this.pctlogo.Image = ((System.Drawing.Image)(resources.GetObject("pctlogo.Image")));
            this.pctlogo.Location = new System.Drawing.Point(897, 251);
            this.pctlogo.Name = "pctlogo";
            this.pctlogo.Size = new System.Drawing.Size(460, 121);
            this.pctlogo.TabIndex = 56;
            this.pctlogo.TabStop = false;
            // 
            // btnimpfact
            // 
            this.btnimpfact.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnimpfact.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnimpfact.Location = new System.Drawing.Point(296, 744);
            this.btnimpfact.Name = "btnimpfact";
            this.btnimpfact.Size = new System.Drawing.Size(569, 47);
            this.btnimpfact.TabIndex = 57;
            this.btnimpfact.Text = "Imprimir Factura";
            this.btnimpfact.UseVisualStyleBackColor = false;
            this.btnimpfact.Click += new System.EventHandler(this.btnimpfact_Click);
            // 
            // printDocument2
            // 
            this.printDocument2.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument2_PrintPage);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(50, 718);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(132, 13);
            this.label1.TabIndex = 58;
            this.label1.Text = "Fecha de Facturación:";
            // 
            // dtfechafact
            // 
            this.dtfechafact.Location = new System.Drawing.Point(12, 734);
            this.dtfechafact.Name = "dtfechafact";
            this.dtfechafact.Size = new System.Drawing.Size(233, 21);
            this.dtfechafact.TabIndex = 59;
            // 
            // archivoDataSet
            // 
            this.archivoDataSet.DataSetName = "archivoDataSet";
            this.archivoDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // clientesBindingSource
            // 
            this.clientesBindingSource.DataMember = "clientes";
            this.clientesBindingSource.DataSource = this.archivoDataSet;
            // 
            // clientesTableAdapter
            // 
            this.clientesTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.cajasTableAdapter = null;
            this.tableAdapterManager.clientesTableAdapter = this.clientesTableAdapter;
            this.tableAdapterManager.detallesoperacionTableAdapter = null;
            this.tableAdapterManager.operacionesTableAdapter = this.operacionesTableAdapter;
            this.tableAdapterManager.UpdateOrder = Archivo.archivoDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // operacionesTableAdapter
            // 
            this.operacionesTableAdapter.ClearBeforeFill = true;
            // 
            // clientesDataGridView
            // 
            this.clientesDataGridView.AllowUserToAddRows = false;
            this.clientesDataGridView.AutoGenerateColumns = false;
            this.clientesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.clientesDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn13,
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewTextBoxColumn15,
            this.dataGridViewTextBoxColumn16,
            this.dataGridViewTextBoxColumn17,
            this.dataGridViewTextBoxColumn18,
            this.dataGridViewTextBoxColumn19,
            this.dataGridViewTextBoxColumn20,
            this.hosting});
            this.clientesDataGridView.DataSource = this.clientesBindingSource;
            this.clientesDataGridView.Location = new System.Drawing.Point(882, 179);
            this.clientesDataGridView.Name = "clientesDataGridView";
            this.clientesDataGridView.Size = new System.Drawing.Size(350, 220);
            this.clientesDataGridView.TabIndex = 60;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Nocliente";
            this.dataGridViewTextBoxColumn1.HeaderText = "Nocliente";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "NombreEmpresa";
            this.dataGridViewTextBoxColumn2.HeaderText = "NombreEmpresa";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Direccion";
            this.dataGridViewTextBoxColumn3.HeaderText = "Direccion";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "Localidad";
            this.dataGridViewTextBoxColumn4.HeaderText = "Localidad";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "CP";
            this.dataGridViewTextBoxColumn5.HeaderText = "CP";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "Telefono";
            this.dataGridViewTextBoxColumn6.HeaderText = "Telefono";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "Nombrecont1";
            this.dataGridViewTextBoxColumn7.HeaderText = "Nombrecont1";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "Telcont1";
            this.dataGridViewTextBoxColumn8.HeaderText = "Telcont1";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "Nombrecont2";
            this.dataGridViewTextBoxColumn9.HeaderText = "Nombrecont2";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "Telcont2";
            this.dataGridViewTextBoxColumn10.HeaderText = "Telcont2";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "Nombrecont3";
            this.dataGridViewTextBoxColumn11.HeaderText = "Nombrecont3";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.DataPropertyName = "Telcont3";
            this.dataGridViewTextBoxColumn12.HeaderText = "Telcont3";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.DataPropertyName = "Nombrecont4";
            this.dataGridViewTextBoxColumn13.HeaderText = "Nombrecont4";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.DataPropertyName = "Telcont4";
            this.dataGridViewTextBoxColumn14.HeaderText = "Telcont4";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.DataPropertyName = "Plan";
            this.dataGridViewTextBoxColumn15.HeaderText = "Plan";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.DataPropertyName = "DireccionEntrega";
            this.dataGridViewTextBoxColumn16.HeaderText = "DireccionEntrega";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.DataPropertyName = "CUIT";
            this.dataGridViewTextBoxColumn17.HeaderText = "CUIT";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.DataPropertyName = "IVA";
            this.dataGridViewTextBoxColumn18.HeaderText = "IVA";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            // 
            // dataGridViewTextBoxColumn19
            // 
            this.dataGridViewTextBoxColumn19.DataPropertyName = "usuario";
            this.dataGridViewTextBoxColumn19.HeaderText = "usuario";
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            // 
            // dataGridViewTextBoxColumn20
            // 
            this.dataGridViewTextBoxColumn20.DataPropertyName = "contrasena";
            this.dataGridViewTextBoxColumn20.HeaderText = "contrasena";
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            // 
            // hosting
            // 
            this.hosting.DataPropertyName = "hosting";
            this.hosting.HeaderText = "hosting";
            this.hosting.Name = "hosting";
            // 
            // operacionesBindingSource
            // 
            this.operacionesBindingSource.DataMember = "operaciones";
            this.operacionesBindingSource.DataSource = this.archivoDataSet;
            // 
            // operacionesDataGridView
            // 
            this.operacionesDataGridView.AllowUserToAddRows = false;
            this.operacionesDataGridView.AllowUserToDeleteRows = false;
            this.operacionesDataGridView.AutoGenerateColumns = false;
            this.operacionesDataGridView.BackgroundColor = System.Drawing.Color.Wheat;
            this.operacionesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.operacionesDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn21,
            this.dataGridViewTextBoxColumn22,
            this.dataGridViewTextBoxColumn23,
            this.dataGridViewTextBoxColumn24,
            this.dataGridViewTextBoxColumn25,
            this.dataGridViewCheckBoxColumn1,
            this.Urgente,
            this.Paginas});
            this.operacionesDataGridView.DataSource = this.operacionesBindingSource;
            this.operacionesDataGridView.Location = new System.Drawing.Point(15, 156);
            this.operacionesDataGridView.Name = "operacionesDataGridView";
            this.operacionesDataGridView.ReadOnly = true;
            this.operacionesDataGridView.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.Wheat;
            this.operacionesDataGridView.Size = new System.Drawing.Size(850, 272);
            this.operacionesDataGridView.TabIndex = 60;
            // 
            // cajasBindingSource
            // 
            this.cajasBindingSource.DataMember = "cajas";
            this.cajasBindingSource.DataSource = this.archivoDataSet;
            // 
            // cajasTableAdapter
            // 
            this.cajasTableAdapter.ClearBeforeFill = true;
            // 
            // cajasDataGridView
            // 
            this.cajasDataGridView.AllowUserToAddRows = false;
            this.cajasDataGridView.AutoGenerateColumns = false;
            this.cajasDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.cajasDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn26,
            this.dataGridViewTextBoxColumn27,
            this.dataGridViewTextBoxColumn28,
            this.dataGridViewTextBoxColumn29,
            this.dataGridViewCheckBoxColumn2,
            this.dataGridViewTextBoxColumn30,
            this.dataGridViewTextBoxColumn31,
            this.dataGridViewTextBoxColumn32,
            this.dataGridViewTextBoxColumn33,
            this.dataGridViewTextBoxColumn34});
            this.cajasDataGridView.DataSource = this.cajasBindingSource;
            this.cajasDataGridView.Location = new System.Drawing.Point(909, 443);
            this.cajasDataGridView.Name = "cajasDataGridView";
            this.cajasDataGridView.Size = new System.Drawing.Size(448, 220);
            this.cajasDataGridView.TabIndex = 60;
            // 
            // dataGridViewTextBoxColumn26
            // 
            this.dataGridViewTextBoxColumn26.DataPropertyName = "Codigo";
            this.dataGridViewTextBoxColumn26.HeaderText = "Codigo";
            this.dataGridViewTextBoxColumn26.Name = "dataGridViewTextBoxColumn26";
            // 
            // dataGridViewTextBoxColumn27
            // 
            this.dataGridViewTextBoxColumn27.DataPropertyName = "Nocliente";
            this.dataGridViewTextBoxColumn27.HeaderText = "Nocliente";
            this.dataGridViewTextBoxColumn27.Name = "dataGridViewTextBoxColumn27";
            // 
            // dataGridViewTextBoxColumn28
            // 
            this.dataGridViewTextBoxColumn28.DataPropertyName = "Lote";
            this.dataGridViewTextBoxColumn28.HeaderText = "Lote";
            this.dataGridViewTextBoxColumn28.Name = "dataGridViewTextBoxColumn28";
            // 
            // dataGridViewTextBoxColumn29
            // 
            this.dataGridViewTextBoxColumn29.DataPropertyName = "Fecha ingreso";
            this.dataGridViewTextBoxColumn29.HeaderText = "Fecha ingreso";
            this.dataGridViewTextBoxColumn29.Name = "dataGridViewTextBoxColumn29";
            // 
            // dataGridViewCheckBoxColumn2
            // 
            this.dataGridViewCheckBoxColumn2.DataPropertyName = "Enconsulta";
            this.dataGridViewCheckBoxColumn2.HeaderText = "Enconsulta";
            this.dataGridViewCheckBoxColumn2.Name = "dataGridViewCheckBoxColumn2";
            // 
            // dataGridViewTextBoxColumn30
            // 
            this.dataGridViewTextBoxColumn30.DataPropertyName = "Contenido";
            this.dataGridViewTextBoxColumn30.HeaderText = "Contenido";
            this.dataGridViewTextBoxColumn30.Name = "dataGridViewTextBoxColumn30";
            // 
            // dataGridViewTextBoxColumn31
            // 
            this.dataGridViewTextBoxColumn31.DataPropertyName = "Ubicacion";
            this.dataGridViewTextBoxColumn31.HeaderText = "Ubicacion";
            this.dataGridViewTextBoxColumn31.Name = "dataGridViewTextBoxColumn31";
            // 
            // dataGridViewTextBoxColumn32
            // 
            this.dataGridViewTextBoxColumn32.DataPropertyName = "Nocaja";
            this.dataGridViewTextBoxColumn32.HeaderText = "Nocaja";
            this.dataGridViewTextBoxColumn32.Name = "dataGridViewTextBoxColumn32";
            // 
            // dataGridViewTextBoxColumn33
            // 
            this.dataGridViewTextBoxColumn33.DataPropertyName = "Fechaconsulta";
            this.dataGridViewTextBoxColumn33.HeaderText = "Fechaconsulta";
            this.dataGridViewTextBoxColumn33.Name = "dataGridViewTextBoxColumn33";
            // 
            // dataGridViewTextBoxColumn34
            // 
            this.dataGridViewTextBoxColumn34.DataPropertyName = "Codigoint";
            this.dataGridViewTextBoxColumn34.HeaderText = "Codigoint";
            this.dataGridViewTextBoxColumn34.Name = "dataGridViewTextBoxColumn34";
            // 
            // dgvfactura
            // 
            this.dgvfactura.AllowUserToOrderColumns = true;
            this.dgvfactura.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvfactura.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cantidad,
            this.descripcion,
            this.preciounit,
            this.precio});
            this.dgvfactura.Location = new System.Drawing.Point(88, 546);
            this.dgvfactura.Name = "dgvfactura";
            this.dgvfactura.Size = new System.Drawing.Size(711, 163);
            this.dgvfactura.TabIndex = 61;
            // 
            // cantidad
            // 
            this.cantidad.HeaderText = "Cantidad";
            this.cantidad.Name = "cantidad";
            this.cantidad.Width = 60;
            // 
            // descripcion
            // 
            this.descripcion.HeaderText = "Descripcion";
            this.descripcion.Name = "descripcion";
            this.descripcion.Width = 400;
            // 
            // preciounit
            // 
            this.preciounit.HeaderText = "Precio Unitario";
            this.preciounit.Name = "preciounit";
            // 
            // precio
            // 
            this.precio.HeaderText = "Precio";
            this.precio.Name = "precio";
            // 
            // btngenfact
            // 
            this.btngenfact.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btngenfact.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btngenfact.Location = new System.Drawing.Point(143, 490);
            this.btngenfact.Name = "btngenfact";
            this.btngenfact.Size = new System.Drawing.Size(569, 50);
            this.btngenfact.TabIndex = 62;
            this.btngenfact.Text = "Generar Factura con datos filtrados";
            this.btngenfact.UseVisualStyleBackColor = false;
            this.btngenfact.Click += new System.EventHandler(this.btngenfact_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 767);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(120, 13);
            this.label4.TabIndex = 63;
            this.label4.Text = "Según resumen Nº:";
            // 
            // udResNo
            // 
            this.udResNo.Location = new System.Drawing.Point(138, 765);
            this.udResNo.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.udResNo.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udResNo.Name = "udResNo";
            this.udResNo.Size = new System.Drawing.Size(82, 21);
            this.udResNo.TabIndex = 64;
            this.udResNo.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(293, 718);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(131, 13);
            this.label5.TabIndex = 65;
            this.label5.Text = "Condiciones de pago:";
            // 
            // tbCondPago
            // 
            this.tbCondPago.Location = new System.Drawing.Point(430, 715);
            this.tbCondPago.Name = "tbCondPago";
            this.tbCondPago.Size = new System.Drawing.Size(195, 21);
            this.tbCondPago.TabIndex = 66;
            this.tbCondPago.Text = "Contado";
            // 
            // dataGridViewTextBoxColumn21
            // 
            this.dataGridViewTextBoxColumn21.DataPropertyName = "NoOp";
            this.dataGridViewTextBoxColumn21.HeaderText = "Nº de Operación";
            this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            this.dataGridViewTextBoxColumn21.ReadOnly = true;
            this.dataGridViewTextBoxColumn21.Width = 60;
            // 
            // dataGridViewTextBoxColumn22
            // 
            this.dataGridViewTextBoxColumn22.DataPropertyName = "Tipo";
            this.dataGridViewTextBoxColumn22.HeaderText = "Tipo";
            this.dataGridViewTextBoxColumn22.Name = "dataGridViewTextBoxColumn22";
            this.dataGridViewTextBoxColumn22.ReadOnly = true;
            this.dataGridViewTextBoxColumn22.Width = 130;
            // 
            // dataGridViewTextBoxColumn23
            // 
            this.dataGridViewTextBoxColumn23.DataPropertyName = "Fecha";
            this.dataGridViewTextBoxColumn23.HeaderText = "Fecha";
            this.dataGridViewTextBoxColumn23.Name = "dataGridViewTextBoxColumn23";
            this.dataGridViewTextBoxColumn23.ReadOnly = true;
            this.dataGridViewTextBoxColumn23.Width = 120;
            // 
            // dataGridViewTextBoxColumn24
            // 
            this.dataGridViewTextBoxColumn24.DataPropertyName = "NoCliente";
            this.dataGridViewTextBoxColumn24.HeaderText = "Nº de Cliente";
            this.dataGridViewTextBoxColumn24.Name = "dataGridViewTextBoxColumn24";
            this.dataGridViewTextBoxColumn24.ReadOnly = true;
            this.dataGridViewTextBoxColumn24.Width = 50;
            // 
            // dataGridViewTextBoxColumn25
            // 
            this.dataGridViewTextBoxColumn25.DataPropertyName = "Codigo";
            this.dataGridViewTextBoxColumn25.HeaderText = "Código de Caja";
            this.dataGridViewTextBoxColumn25.Name = "dataGridViewTextBoxColumn25";
            this.dataGridViewTextBoxColumn25.ReadOnly = true;
            this.dataGridViewTextBoxColumn25.Width = 230;
            // 
            // dataGridViewCheckBoxColumn1
            // 
            this.dataGridViewCheckBoxColumn1.DataPropertyName = "Traslado";
            this.dataGridViewCheckBoxColumn1.HeaderText = "Traslado";
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            this.dataGridViewCheckBoxColumn1.ReadOnly = true;
            this.dataGridViewCheckBoxColumn1.Width = 60;
            // 
            // Urgente
            // 
            this.Urgente.DataPropertyName = "Urgente";
            this.Urgente.HeaderText = "Urgente";
            this.Urgente.Name = "Urgente";
            this.Urgente.ReadOnly = true;
            // 
            // Paginas
            // 
            this.Paginas.DataPropertyName = "Paginas";
            this.Paginas.HeaderText = "Paginas";
            this.Paginas.Name = "Paginas";
            this.Paginas.ReadOnly = true;
            // 
            // frmresumen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.SandyBrown;
            this.ClientSize = new System.Drawing.Size(881, 835);
            this.Controls.Add(this.tbCondPago);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.udResNo);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btngenfact);
            this.Controls.Add(this.dgvfactura);
            this.Controls.Add(this.cajasDataGridView);
            this.Controls.Add(this.operacionesDataGridView);
            this.Controls.Add(this.clientesDataGridView);
            this.Controls.Add(this.dtfechafact);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnimpfact);
            this.Controls.Add(this.pctlogo);
            this.Controls.Add(this.btnimprimir);
            this.Controls.Add(this.lbNombreEmpresa);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "frmresumen";
            this.Text = "Resumen de Operaciones";
            this.Load += new System.EventHandler(this.frmresumen_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pctlogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.archivoDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientesDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.operacionesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.operacionesDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cajasBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cajasDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvfactura)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udResNo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListBox lbNombreEmpresa;
        private System.Windows.Forms.CheckBox cbTipo;
        private System.Windows.Forms.ComboBox cmbTipo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dtFechaHasta;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtFechaDesde;
        private System.Windows.Forms.CheckBox cbFecha;
        private System.Windows.Forms.TextBox tbNombreClienteFiltro;
        private System.Windows.Forms.CheckBox cbNombreCliente;
        private System.Windows.Forms.TextBox tbNoClienteFiltro;
        private System.Windows.Forms.CheckBox cbNoCliente;
        private System.Windows.Forms.Button btnFiltrar;
        private System.Windows.Forms.Button btnimprimir;
        private System.Windows.Forms.PrintDialog printDialog1;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.PictureBox pctlogo;
        private System.Windows.Forms.Button btnimpfact;
        private System.Drawing.Printing.PrintDocument printDocument2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtfechafact;
        private archivoDataSet archivoDataSet;
        private System.Windows.Forms.BindingSource clientesBindingSource;
        private archivoDataSetTableAdapters.clientesTableAdapter clientesTableAdapter;
        private archivoDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private archivoDataSetTableAdapters.operacionesTableAdapter operacionesTableAdapter;
        private System.Windows.Forms.DataGridView clientesDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private System.Windows.Forms.BindingSource operacionesBindingSource;
        private System.Windows.Forms.DataGridView operacionesDataGridView;
        private System.Windows.Forms.BindingSource cajasBindingSource;
        private archivoDataSetTableAdapters.cajasTableAdapter cajasTableAdapter;
        private System.Windows.Forms.DataGridView cajasDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn26;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn27;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn28;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn29;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn30;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn31;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn32;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn33;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn34;
        private System.Windows.Forms.DataGridView dgvfactura;
        private System.Windows.Forms.DataGridViewTextBoxColumn cantidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn descripcion;
        private System.Windows.Forms.DataGridViewTextBoxColumn preciounit;
        private System.Windows.Forms.DataGridViewTextBoxColumn precio;
        private System.Windows.Forms.Button btngenfact;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown udResNo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbCondPago;
        private System.Windows.Forms.DataGridViewCheckBoxColumn hosting;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn23;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn24;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn25;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Urgente;
        private System.Windows.Forms.DataGridViewTextBoxColumn Paginas;
    }
}