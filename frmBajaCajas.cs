﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Archivo
{
    public partial class frmBajaCajas : Form
    {
        frmImprimePlanilla frmplan;

        public frmBajaCajas()
        {
            InitializeComponent();
        }
        private void Bajacajas_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'archivoDataSet.operaciones' table. You can move, or remove it, as needed.
            this.operacionesTableAdapter.Fill(this.archivoDataSet.operaciones);
            // TODO: This line of code loads data into the 'archivoDataSet.clientes' table. You can move, or remove it, as needed.
            this.clientesTableAdapter.Fill(this.archivoDataSet.clientes);
            // TODO: This line of code loads data into the 'archivoDataSet.cajas' table. You can move, or remove it, as needed.
            this.cajasTableAdapter.Fill(this.archivoDataSet.cajas);

        }

        private void btnBaja_Click(object sender, EventArgs e)
        {
/*            int maximo;
            if (archivoDataSet.operaciones.Rows.Count != 0)
                maximo = (int)archivoDataSet.operaciones.Compute("Max(NoOp)", "");
            else
                maximo = 0;*/
            archivoDataSet.operacionesRow filanuevaop = archivoDataSet.operaciones.NewoperacionesRow();
//            filanuevaop.NoOp = maximo + 1;*/
            filanuevaop.Tipo = "Baja";
            filanuevaop.Fecha = dtFechaBaja.Value;
            filanuevaop.NoCliente = Convert.ToInt32(tbNoCliente.Text);
            filanuevaop.Codigo = tbCodigo.Text;
            filanuevaop.Traslado = cbTraslado.Checked;
            archivoDataSet.operaciones.Rows.Add(filanuevaop);
            operacionesBindingSource.MoveLast();


            if (cajasDataGridView.RowCount == 1)
                foreach (DataGridViewRow dr in cajasDataGridView.Rows)
                    cajasDataGridView.Rows.Remove(dr);
            cajasBindingSource.EndEdit();
            operacionesBindingSource.EndEdit();
            tableAdapterManager.UpdateAll(archivoDataSet);


            if (MessageBox.Show("Caja Nº " + tbCodigo.Text + " dada de baja. Imprimir planilla de baja?", "Baja de caja",
    MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Imprimeplanilla();
            }

        }

        private void Imprimeplanilla()
        {
            DateTime fecha = DateTime.Now.Date;

            frmplan = new frmImprimePlanilla("baja", tbCodigo.Text, tbNombreCiente.Text, tbContenido.Text, fecha);
            frmplan.ShowDialog(this);
        }


        private void frmBajaCajas_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return && tbCodigo.Focused)
            {
                btnBaja.Enabled = false;
                if (tbCodigo.Text.Length == 19)
                {
                    DataView dv = archivoDataSet.cajas.DefaultView;
                    string filter = "Nocliente > 0";
                    dv.RowFilter = filter;
                    cajasBindingSource.DataSource = dv;
                    filter = "Codigo = " + tbCodigo.Text;
                    dv.RowFilter = filter;
                    cajasBindingSource.DataSource = dv;
                    if (cajasDataGridView.RowCount == 0) // Si no encuentra el codigo
                    {
                        MessageBox.Show("La caja con el código " + tbCodigo.Text + " no se encuentra en la base de datos. ");
                        return;
                    }
                    DataView dv2 = archivoDataSet.clientes.DefaultView;
                    int nocliente = Convert.ToInt32(tbCodigo.Text.Substring(0, 4));
                    filter = "Nocliente > 0";     // Carga todos los registros
                    dv2.RowFilter = filter;
                    filter = "Nocliente = " + nocliente.ToString();      // Busca el cliente
                    dv2.RowFilter = filter;
                    clientesBindingSource.DataSource = dv2;
                    if (clientesDataGridView.RowCount == 0)
                    {
                        MessageBox.Show("El cliente Nº " + nocliente.ToString() + " no existe en la base de datos.");
                        return;
                    }
                    else
                    {
                        tbNoCliente.Text = nocliente.ToString();
                        tbNombreCiente.Text = clientesDataGridView.Rows[0].Cells[1].Value.ToString();
                        tbLote.Text = Convert.ToInt32(tbCodigo.Text.Substring(4, 4)).ToString();
                        tbNocaja.Text = Convert.ToInt32(tbCodigo.Text.Substring(8, 4)).ToString();
                        tbFila.Text = Convert.ToInt32(tbCodigo.Text.Substring(12, 2)).ToString();
                        tbRack.Text = Convert.ToInt32(tbCodigo.Text.Substring(14, 2)).ToString();
                        tbPiso.Text = Convert.ToInt32(tbCodigo.Text.Substring(16, 1)).ToString();
                        tbPosicion.Text = Convert.ToInt32(tbCodigo.Text.Substring(17, 2)).ToString();
                        tbFechaIngreso.Text = cajasDataGridView.Rows[0].Cells[3].Value.ToString();
                        btnBaja.Enabled = true;
                        tbContenido.Text = contenidoTextBox.Text;
                    }



                }
                else
                {
                    tbNoCliente.Text = "";
                    tbNombreCiente.Text = "";
                    tbLote.Text = "";
                    tbNocaja.Text = "";
                    tbFila.Text = "";
                    tbRack.Text = "";
                    tbPiso.Text = "";
                    tbPosicion.Text = "";
                    tbFechaIngreso.Text = "";
                    tbContenido.Text = "";
                }
            }

        }

    }
}
