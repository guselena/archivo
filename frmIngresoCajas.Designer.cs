﻿namespace Archivo
{
    partial class frmIngresoCajas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label noclienteLabel;
            System.Windows.Forms.Label codigoLabel;
            System.Windows.Forms.Label loteLabel;
            System.Windows.Forms.Label fecha_ingresoLabel;
            System.Windows.Forms.Label nombre_EmpresaLabel;
            System.Windows.Forms.Label contenidoLabel;
            this.btnSigLote = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.udNolote = new System.Windows.Forms.NumericUpDown();
            this.udNoCaja = new System.Windows.Forms.NumericUpDown();
            this.btnsigcaja = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.udNoFila = new System.Windows.Forms.NumericUpDown();
            this.udNoRack = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.udNoPiso = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.udPos = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.btnSigPos = new System.Windows.Forms.Button();
            this.btnIngresar = new System.Windows.Forms.Button();
            this.lbNombreEmpresa = new System.Windows.Forms.ListBox();
            this.txtNombreEmpresa = new System.Windows.Forms.TextBox();
            this.tbNocliente = new System.Windows.Forms.TextBox();
            this.dtFechaingreso = new System.Windows.Forms.DateTimePicker();
            this.tbcontenido = new System.Windows.Forms.TextBox();
            this.tbcodigo = new System.Windows.Forms.TextBox();
            this.btnultimapos = new System.Windows.Forms.Button();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.pctlogo = new System.Windows.Forms.PictureBox();
            this.pctbarras = new System.Windows.Forms.PictureBox();
            this.cbTraslado = new System.Windows.Forms.CheckBox();
            this.cajasDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cajasBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.archivoDataSet = new Archivo.archivoDataSet();
            this.clientesDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clientesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.operacionesDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn31 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn32 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn33 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn34 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.operacionesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cajasTableAdapter = new Archivo.archivoDataSetTableAdapters.cajasTableAdapter();
            this.tableAdapterManager = new Archivo.archivoDataSetTableAdapters.TableAdapterManager();
            this.clientesTableAdapter = new Archivo.archivoDataSetTableAdapters.clientesTableAdapter();
            this.operacionesTableAdapter = new Archivo.archivoDataSetTableAdapters.operacionesTableAdapter();
            this.label6 = new System.Windows.Forms.Label();
            this.tbNoDesde = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tbNoHasta = new System.Windows.Forms.TextBox();
            this.dtFechaDesde = new System.Windows.Forms.DateTimePicker();
            this.dtFechaHasta = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.cbFecha = new System.Windows.Forms.CheckBox();
            noclienteLabel = new System.Windows.Forms.Label();
            codigoLabel = new System.Windows.Forms.Label();
            loteLabel = new System.Windows.Forms.Label();
            fecha_ingresoLabel = new System.Windows.Forms.Label();
            nombre_EmpresaLabel = new System.Windows.Forms.Label();
            contenidoLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.udNolote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udNoCaja)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udNoFila)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udNoRack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udNoPiso)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udPos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctlogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctbarras)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cajasDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cajasBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.archivoDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientesDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.operacionesDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.operacionesBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // noclienteLabel
            // 
            noclienteLabel.AutoSize = true;
            noclienteLabel.Location = new System.Drawing.Point(19, 9);
            noclienteLabel.Name = "noclienteLabel";
            noclienteLabel.Size = new System.Drawing.Size(88, 13);
            noclienteLabel.TabIndex = 3;
            noclienteLabel.Text = "Nº de Cliente:";
            // 
            // codigoLabel
            // 
            codigoLabel.AutoSize = true;
            codigoLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            codigoLabel.Location = new System.Drawing.Point(24, 486);
            codigoLabel.Name = "codigoLabel";
            codigoLabel.Size = new System.Drawing.Size(86, 25);
            codigoLabel.TabIndex = 5;
            codigoLabel.Text = "Codigo:";
            // 
            // loteLabel
            // 
            loteLabel.AutoSize = true;
            loteLabel.Location = new System.Drawing.Point(89, 336);
            loteLabel.Name = "loteLabel";
            loteLabel.Size = new System.Drawing.Size(36, 13);
            loteLabel.TabIndex = 7;
            loteLabel.Text = "Lote:";
            // 
            // fecha_ingresoLabel
            // 
            fecha_ingresoLabel.AutoSize = true;
            fecha_ingresoLabel.Location = new System.Drawing.Point(0, 38);
            fecha_ingresoLabel.Name = "fecha_ingresoLabel";
            fecha_ingresoLabel.Size = new System.Drawing.Size(109, 13);
            fecha_ingresoLabel.TabIndex = 9;
            fecha_ingresoLabel.Text = "Fecha de ingreso:";
            // 
            // nombre_EmpresaLabel
            // 
            nombre_EmpresaLabel.AutoSize = true;
            nombre_EmpresaLabel.Location = new System.Drawing.Point(247, 9);
            nombre_EmpresaLabel.Name = "nombre_EmpresaLabel";
            nombre_EmpresaLabel.Size = new System.Drawing.Size(129, 13);
            nombre_EmpresaLabel.TabIndex = 15;
            nombre_EmpresaLabel.Text = "Nombre de Empresa:";
            // 
            // contenidoLabel
            // 
            contenidoLabel.AutoSize = true;
            contenidoLabel.Location = new System.Drawing.Point(37, 72);
            contenidoLabel.Name = "contenidoLabel";
            contenidoLabel.Size = new System.Drawing.Size(70, 13);
            contenidoLabel.TabIndex = 16;
            contenidoLabel.Text = "Contenido:";
            // 
            // btnSigLote
            // 
            this.btnSigLote.Location = new System.Drawing.Point(216, 331);
            this.btnSigLote.Name = "btnSigLote";
            this.btnSigLote.Size = new System.Drawing.Size(288, 23);
            this.btnSigLote.TabIndex = 19;
            this.btnSigLote.Text = "Buscar último lote de este cliente";
            this.btnSigLote.UseVisualStyleBackColor = true;
            this.btnSigLote.Click += new System.EventHandler(this.btnSigLote_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(54, 362);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 13);
            this.label1.TabIndex = 20;
            this.label1.Text = "Nº de Caja:";
            // 
            // udNolote
            // 
            this.udNolote.BackColor = System.Drawing.Color.Wheat;
            this.udNolote.Location = new System.Drawing.Point(132, 334);
            this.udNolote.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.udNolote.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udNolote.Name = "udNolote";
            this.udNolote.Size = new System.Drawing.Size(78, 21);
            this.udNolote.TabIndex = 23;
            this.udNolote.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udNolote.ValueChanged += new System.EventHandler(this.udNolote_ValueChanged);
            // 
            // udNoCaja
            // 
            this.udNoCaja.BackColor = System.Drawing.Color.Wheat;
            this.udNoCaja.Location = new System.Drawing.Point(132, 360);
            this.udNoCaja.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.udNoCaja.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udNoCaja.Name = "udNoCaja";
            this.udNoCaja.Size = new System.Drawing.Size(78, 21);
            this.udNoCaja.TabIndex = 24;
            this.udNoCaja.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udNoCaja.ValueChanged += new System.EventHandler(this.udNoCaja_ValueChanged);
            // 
            // btnsigcaja
            // 
            this.btnsigcaja.Location = new System.Drawing.Point(216, 357);
            this.btnsigcaja.Name = "btnsigcaja";
            this.btnsigcaja.Size = new System.Drawing.Size(288, 23);
            this.btnsigcaja.TabIndex = 25;
            this.btnsigcaja.Text = "Buscar Siguiente Caja Disponible en Este Lote";
            this.btnsigcaja.UseVisualStyleBackColor = true;
            this.btnsigcaja.Click += new System.EventHandler(this.btnsigcaja_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(59, 390);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 26;
            this.label2.Text = "Nº de Fila:";
            // 
            // udNoFila
            // 
            this.udNoFila.BackColor = System.Drawing.Color.Wheat;
            this.udNoFila.Location = new System.Drawing.Point(132, 388);
            this.udNoFila.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.udNoFila.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udNoFila.Name = "udNoFila";
            this.udNoFila.Size = new System.Drawing.Size(78, 21);
            this.udNoFila.TabIndex = 27;
            this.udNoFila.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udNoFila.ValueChanged += new System.EventHandler(this.udNoFila_ValueChanged);
            // 
            // udNoRack
            // 
            this.udNoRack.BackColor = System.Drawing.Color.Wheat;
            this.udNoRack.Location = new System.Drawing.Point(132, 414);
            this.udNoRack.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.udNoRack.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udNoRack.Name = "udNoRack";
            this.udNoRack.Size = new System.Drawing.Size(78, 21);
            this.udNoRack.TabIndex = 29;
            this.udNoRack.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udNoRack.ValueChanged += new System.EventHandler(this.udNoRack_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(48, 416);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 13);
            this.label3.TabIndex = 28;
            this.label3.Text = "Nº de Rack:";
            // 
            // udNoPiso
            // 
            this.udNoPiso.BackColor = System.Drawing.Color.Wheat;
            this.udNoPiso.Location = new System.Drawing.Point(132, 438);
            this.udNoPiso.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.udNoPiso.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udNoPiso.Name = "udNoPiso";
            this.udNoPiso.Size = new System.Drawing.Size(78, 21);
            this.udNoPiso.TabIndex = 31;
            this.udNoPiso.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udNoPiso.ValueChanged += new System.EventHandler(this.udNoPiso_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(55, 440);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 13);
            this.label4.TabIndex = 30;
            this.label4.Text = "Nº de Piso:";
            // 
            // udPos
            // 
            this.udPos.BackColor = System.Drawing.Color.Wheat;
            this.udPos.Location = new System.Drawing.Point(132, 464);
            this.udPos.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.udPos.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udPos.Name = "udPos";
            this.udPos.Size = new System.Drawing.Size(78, 21);
            this.udPos.TabIndex = 33;
            this.udPos.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udPos.ValueChanged += new System.EventHandler(this.udPos_ValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 466);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(117, 13);
            this.label5.TabIndex = 32;
            this.label5.Text = "Posición en el piso:";
            // 
            // btnSigPos
            // 
            this.btnSigPos.Location = new System.Drawing.Point(217, 386);
            this.btnSigPos.Name = "btnSigPos";
            this.btnSigPos.Size = new System.Drawing.Size(283, 98);
            this.btnSigPos.TabIndex = 34;
            this.btnSigPos.Text = "Buscar Siguiente Posición Absoluta Disponible (Hueco)";
            this.btnSigPos.UseVisualStyleBackColor = true;
            this.btnSigPos.Click += new System.EventHandler(this.btnSigPos_Click);
            // 
            // btnIngresar
            // 
            this.btnIngresar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnIngresar.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIngresar.Location = new System.Drawing.Point(237, 538);
            this.btnIngresar.Name = "btnIngresar";
            this.btnIngresar.Size = new System.Drawing.Size(371, 71);
            this.btnIngresar.TabIndex = 36;
            this.btnIngresar.Text = "INGRESAR CAJA";
            this.btnIngresar.UseVisualStyleBackColor = false;
            this.btnIngresar.Click += new System.EventHandler(this.btnIngresar_Click);
            // 
            // lbNombreEmpresa
            // 
            this.lbNombreEmpresa.FormattingEnabled = true;
            this.lbNombreEmpresa.Location = new System.Drawing.Point(953, 404);
            this.lbNombreEmpresa.Name = "lbNombreEmpresa";
            this.lbNombreEmpresa.Size = new System.Drawing.Size(139, 95);
            this.lbNombreEmpresa.TabIndex = 37;
            // 
            // txtNombreEmpresa
            // 
            this.txtNombreEmpresa.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtNombreEmpresa.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtNombreEmpresa.BackColor = System.Drawing.Color.Wheat;
            this.txtNombreEmpresa.Location = new System.Drawing.Point(383, 6);
            this.txtNombreEmpresa.Name = "txtNombreEmpresa";
            this.txtNombreEmpresa.Size = new System.Drawing.Size(486, 21);
            this.txtNombreEmpresa.TabIndex = 38;
            this.txtNombreEmpresa.Leave += new System.EventHandler(this.txtNombreEmpresa_Leave);
            // 
            // tbNocliente
            // 
            this.tbNocliente.BackColor = System.Drawing.Color.Wheat;
            this.tbNocliente.Location = new System.Drawing.Point(124, 6);
            this.tbNocliente.Name = "tbNocliente";
            this.tbNocliente.Size = new System.Drawing.Size(116, 21);
            this.tbNocliente.TabIndex = 47;
            this.tbNocliente.TextChanged += new System.EventHandler(this.tbNocliente_TextChanged);
            this.tbNocliente.Leave += new System.EventHandler(this.tbNocliente_Leave);
            // 
            // dtFechaingreso
            // 
            this.dtFechaingreso.Location = new System.Drawing.Point(124, 32);
            this.dtFechaingreso.Name = "dtFechaingreso";
            this.dtFechaingreso.Size = new System.Drawing.Size(233, 21);
            this.dtFechaingreso.TabIndex = 48;
            // 
            // tbcontenido
            // 
            this.tbcontenido.BackColor = System.Drawing.Color.Wheat;
            this.tbcontenido.Location = new System.Drawing.Point(124, 69);
            this.tbcontenido.Multiline = true;
            this.tbcontenido.Name = "tbcontenido";
            this.tbcontenido.Size = new System.Drawing.Size(745, 179);
            this.tbcontenido.TabIndex = 49;
            // 
            // tbcodigo
            // 
            this.tbcodigo.BackColor = System.Drawing.Color.Wheat;
            this.tbcodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbcodigo.Location = new System.Drawing.Point(132, 492);
            this.tbcodigo.Name = "tbcodigo";
            this.tbcodigo.Size = new System.Drawing.Size(737, 40);
            this.tbcodigo.TabIndex = 50;
            // 
            // btnultimapos
            // 
            this.btnultimapos.Location = new System.Drawing.Point(507, 385);
            this.btnultimapos.Name = "btnultimapos";
            this.btnultimapos.Size = new System.Drawing.Size(299, 99);
            this.btnultimapos.TabIndex = 52;
            this.btnultimapos.Text = "Buscar última posición disponible (Final)";
            this.btnultimapos.UseVisualStyleBackColor = true;
            this.btnultimapos.Click += new System.EventHandler(this.btnultimapos_Click);
            // 
            // printDialog1
            // 
            this.printDialog1.UseEXDialog = true;
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);
            // 
            // pctlogo
            // 
            this.pctlogo.Image = global::Archivo.Properties.Resources.logo_para_soft;
            this.pctlogo.Location = new System.Drawing.Point(918, 501);
            this.pctlogo.Name = "pctlogo";
            this.pctlogo.Size = new System.Drawing.Size(460, 121);
            this.pctlogo.TabIndex = 54;
            this.pctlogo.TabStop = false;
            // 
            // pctbarras
            // 
            this.pctbarras.Location = new System.Drawing.Point(918, 340);
            this.pctbarras.Name = "pctbarras";
            this.pctbarras.Size = new System.Drawing.Size(117, 50);
            this.pctbarras.TabIndex = 53;
            this.pctbarras.TabStop = false;
            // 
            // cbTraslado
            // 
            this.cbTraslado.AutoSize = true;
            this.cbTraslado.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cbTraslado.Location = new System.Drawing.Point(2, 314);
            this.cbTraslado.Name = "cbTraslado";
            this.cbTraslado.Size = new System.Drawing.Size(147, 17);
            this.cbTraslado.TabIndex = 56;
            this.cbTraslado.Text = "Traslado a Domicilio:";
            this.cbTraslado.UseVisualStyleBackColor = true;
            // 
            // cajasDataGridView
            // 
            this.cajasDataGridView.AllowUserToAddRows = false;
            this.cajasDataGridView.AllowUserToDeleteRows = false;
            this.cajasDataGridView.AutoGenerateColumns = false;
            this.cajasDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.cajasDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewCheckBoxColumn1,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9});
            this.cajasDataGridView.DataSource = this.cajasBindingSource;
            this.cajasDataGridView.Location = new System.Drawing.Point(1095, 32);
            this.cajasDataGridView.Name = "cajasDataGridView";
            this.cajasDataGridView.ReadOnly = true;
            this.cajasDataGridView.Size = new System.Drawing.Size(350, 84);
            this.cajasDataGridView.TabIndex = 57;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Codigo";
            this.dataGridViewTextBoxColumn1.HeaderText = "Codigo";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Nocliente";
            this.dataGridViewTextBoxColumn2.HeaderText = "Nocliente";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Lote";
            this.dataGridViewTextBoxColumn3.HeaderText = "Lote";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "Fecha ingreso";
            this.dataGridViewTextBoxColumn4.HeaderText = "Fecha ingreso";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewCheckBoxColumn1
            // 
            this.dataGridViewCheckBoxColumn1.DataPropertyName = "Enconsulta";
            this.dataGridViewCheckBoxColumn1.HeaderText = "Enconsulta";
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            this.dataGridViewCheckBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "Contenido";
            this.dataGridViewTextBoxColumn5.HeaderText = "Contenido";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "Ubicacion";
            this.dataGridViewTextBoxColumn6.HeaderText = "Ubicacion";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "Nocaja";
            this.dataGridViewTextBoxColumn7.HeaderText = "Nocaja";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "Fechaconsulta";
            this.dataGridViewTextBoxColumn8.HeaderText = "Fechaconsulta";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "Codigoint";
            this.dataGridViewTextBoxColumn9.HeaderText = "Codigoint";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            // 
            // cajasBindingSource
            // 
            this.cajasBindingSource.DataMember = "cajas";
            this.cajasBindingSource.DataSource = this.archivoDataSet;
            // 
            // archivoDataSet
            // 
            this.archivoDataSet.DataSetName = "archivoDataSet";
            this.archivoDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // clientesDataGridView
            // 
            this.clientesDataGridView.AllowUserToAddRows = false;
            this.clientesDataGridView.AllowUserToDeleteRows = false;
            this.clientesDataGridView.AutoGenerateColumns = false;
            this.clientesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.clientesDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn13,
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewTextBoxColumn15,
            this.dataGridViewTextBoxColumn16,
            this.dataGridViewTextBoxColumn17,
            this.dataGridViewTextBoxColumn18,
            this.dataGridViewTextBoxColumn19,
            this.dataGridViewTextBoxColumn20,
            this.dataGridViewTextBoxColumn21,
            this.dataGridViewTextBoxColumn22,
            this.dataGridViewTextBoxColumn23,
            this.dataGridViewTextBoxColumn24,
            this.dataGridViewTextBoxColumn25,
            this.dataGridViewTextBoxColumn26,
            this.dataGridViewTextBoxColumn27,
            this.dataGridViewTextBoxColumn28,
            this.dataGridViewTextBoxColumn29});
            this.clientesDataGridView.DataSource = this.clientesBindingSource;
            this.clientesDataGridView.Location = new System.Drawing.Point(898, 176);
            this.clientesDataGridView.Name = "clientesDataGridView";
            this.clientesDataGridView.ReadOnly = true;
            this.clientesDataGridView.Size = new System.Drawing.Size(350, 105);
            this.clientesDataGridView.TabIndex = 57;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "Nocliente";
            this.dataGridViewTextBoxColumn10.HeaderText = "Nocliente";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "NombreEmpresa";
            this.dataGridViewTextBoxColumn11.HeaderText = "NombreEmpresa";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.DataPropertyName = "Direccion";
            this.dataGridViewTextBoxColumn12.HeaderText = "Direccion";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.DataPropertyName = "Localidad";
            this.dataGridViewTextBoxColumn13.HeaderText = "Localidad";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.DataPropertyName = "CP";
            this.dataGridViewTextBoxColumn14.HeaderText = "CP";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.DataPropertyName = "Telefono";
            this.dataGridViewTextBoxColumn15.HeaderText = "Telefono";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.DataPropertyName = "Nombrecont1";
            this.dataGridViewTextBoxColumn16.HeaderText = "Nombrecont1";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.DataPropertyName = "Telcont1";
            this.dataGridViewTextBoxColumn17.HeaderText = "Telcont1";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.DataPropertyName = "Nombrecont2";
            this.dataGridViewTextBoxColumn18.HeaderText = "Nombrecont2";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn19
            // 
            this.dataGridViewTextBoxColumn19.DataPropertyName = "Telcont2";
            this.dataGridViewTextBoxColumn19.HeaderText = "Telcont2";
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn20
            // 
            this.dataGridViewTextBoxColumn20.DataPropertyName = "Nombrecont3";
            this.dataGridViewTextBoxColumn20.HeaderText = "Nombrecont3";
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            this.dataGridViewTextBoxColumn20.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn21
            // 
            this.dataGridViewTextBoxColumn21.DataPropertyName = "Telcont3";
            this.dataGridViewTextBoxColumn21.HeaderText = "Telcont3";
            this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            this.dataGridViewTextBoxColumn21.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn22
            // 
            this.dataGridViewTextBoxColumn22.DataPropertyName = "Nombrecont4";
            this.dataGridViewTextBoxColumn22.HeaderText = "Nombrecont4";
            this.dataGridViewTextBoxColumn22.Name = "dataGridViewTextBoxColumn22";
            this.dataGridViewTextBoxColumn22.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn23
            // 
            this.dataGridViewTextBoxColumn23.DataPropertyName = "Telcont4";
            this.dataGridViewTextBoxColumn23.HeaderText = "Telcont4";
            this.dataGridViewTextBoxColumn23.Name = "dataGridViewTextBoxColumn23";
            this.dataGridViewTextBoxColumn23.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn24
            // 
            this.dataGridViewTextBoxColumn24.DataPropertyName = "Plan";
            this.dataGridViewTextBoxColumn24.HeaderText = "Plan";
            this.dataGridViewTextBoxColumn24.Name = "dataGridViewTextBoxColumn24";
            this.dataGridViewTextBoxColumn24.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn25
            // 
            this.dataGridViewTextBoxColumn25.DataPropertyName = "DireccionEntrega";
            this.dataGridViewTextBoxColumn25.HeaderText = "DireccionEntrega";
            this.dataGridViewTextBoxColumn25.Name = "dataGridViewTextBoxColumn25";
            this.dataGridViewTextBoxColumn25.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn26
            // 
            this.dataGridViewTextBoxColumn26.DataPropertyName = "CUIT";
            this.dataGridViewTextBoxColumn26.HeaderText = "CUIT";
            this.dataGridViewTextBoxColumn26.Name = "dataGridViewTextBoxColumn26";
            this.dataGridViewTextBoxColumn26.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn27
            // 
            this.dataGridViewTextBoxColumn27.DataPropertyName = "IVA";
            this.dataGridViewTextBoxColumn27.HeaderText = "IVA";
            this.dataGridViewTextBoxColumn27.Name = "dataGridViewTextBoxColumn27";
            this.dataGridViewTextBoxColumn27.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn28
            // 
            this.dataGridViewTextBoxColumn28.DataPropertyName = "usuario";
            this.dataGridViewTextBoxColumn28.HeaderText = "usuario";
            this.dataGridViewTextBoxColumn28.Name = "dataGridViewTextBoxColumn28";
            this.dataGridViewTextBoxColumn28.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn29
            // 
            this.dataGridViewTextBoxColumn29.DataPropertyName = "contrasena";
            this.dataGridViewTextBoxColumn29.HeaderText = "contrasena";
            this.dataGridViewTextBoxColumn29.Name = "dataGridViewTextBoxColumn29";
            this.dataGridViewTextBoxColumn29.ReadOnly = true;
            // 
            // clientesBindingSource
            // 
            this.clientesBindingSource.DataMember = "clientes";
            this.clientesBindingSource.DataSource = this.archivoDataSet;
            // 
            // operacionesDataGridView
            // 
            this.operacionesDataGridView.AllowUserToAddRows = false;
            this.operacionesDataGridView.AllowUserToDeleteRows = false;
            this.operacionesDataGridView.AutoGenerateColumns = false;
            this.operacionesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.operacionesDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn30,
            this.dataGridViewTextBoxColumn31,
            this.dataGridViewTextBoxColumn32,
            this.dataGridViewTextBoxColumn33,
            this.dataGridViewTextBoxColumn34,
            this.dataGridViewCheckBoxColumn2});
            this.operacionesDataGridView.DataSource = this.operacionesBindingSource;
            this.operacionesDataGridView.Location = new System.Drawing.Point(1095, 238);
            this.operacionesDataGridView.Name = "operacionesDataGridView";
            this.operacionesDataGridView.ReadOnly = true;
            this.operacionesDataGridView.Size = new System.Drawing.Size(350, 92);
            this.operacionesDataGridView.TabIndex = 57;
            // 
            // dataGridViewTextBoxColumn30
            // 
            this.dataGridViewTextBoxColumn30.DataPropertyName = "NoOp";
            this.dataGridViewTextBoxColumn30.HeaderText = "NoOp";
            this.dataGridViewTextBoxColumn30.Name = "dataGridViewTextBoxColumn30";
            this.dataGridViewTextBoxColumn30.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn31
            // 
            this.dataGridViewTextBoxColumn31.DataPropertyName = "Tipo";
            this.dataGridViewTextBoxColumn31.HeaderText = "Tipo";
            this.dataGridViewTextBoxColumn31.Name = "dataGridViewTextBoxColumn31";
            this.dataGridViewTextBoxColumn31.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn32
            // 
            this.dataGridViewTextBoxColumn32.DataPropertyName = "Fecha";
            this.dataGridViewTextBoxColumn32.HeaderText = "Fecha";
            this.dataGridViewTextBoxColumn32.Name = "dataGridViewTextBoxColumn32";
            this.dataGridViewTextBoxColumn32.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn33
            // 
            this.dataGridViewTextBoxColumn33.DataPropertyName = "NoCliente";
            this.dataGridViewTextBoxColumn33.HeaderText = "NoCliente";
            this.dataGridViewTextBoxColumn33.Name = "dataGridViewTextBoxColumn33";
            this.dataGridViewTextBoxColumn33.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn34
            // 
            this.dataGridViewTextBoxColumn34.DataPropertyName = "Codigo";
            this.dataGridViewTextBoxColumn34.HeaderText = "Codigo";
            this.dataGridViewTextBoxColumn34.Name = "dataGridViewTextBoxColumn34";
            this.dataGridViewTextBoxColumn34.ReadOnly = true;
            // 
            // dataGridViewCheckBoxColumn2
            // 
            this.dataGridViewCheckBoxColumn2.DataPropertyName = "Traslado";
            this.dataGridViewCheckBoxColumn2.HeaderText = "Traslado";
            this.dataGridViewCheckBoxColumn2.Name = "dataGridViewCheckBoxColumn2";
            this.dataGridViewCheckBoxColumn2.ReadOnly = true;
            // 
            // operacionesBindingSource
            // 
            this.operacionesBindingSource.DataMember = "operaciones";
            this.operacionesBindingSource.DataSource = this.archivoDataSet;
            // 
            // cajasTableAdapter
            // 
            this.cajasTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.cajasTableAdapter = this.cajasTableAdapter;
            this.tableAdapterManager.clientesTableAdapter = this.clientesTableAdapter;
            this.tableAdapterManager.detallesoperacionTableAdapter = null;
            this.tableAdapterManager.operacionesTableAdapter = this.operacionesTableAdapter;
            this.tableAdapterManager.UpdateOrder = Archivo.archivoDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // clientesTableAdapter
            // 
            this.clientesTableAdapter.ClearBeforeFill = true;
            // 
            // operacionesTableAdapter
            // 
            this.operacionesTableAdapter.ClearBeforeFill = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(48, 257);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(66, 13);
            this.label6.TabIndex = 58;
            this.label6.Text = "Desde Nº:";
            // 
            // tbNoDesde
            // 
            this.tbNoDesde.BackColor = System.Drawing.Color.Wheat;
            this.tbNoDesde.Location = new System.Drawing.Point(124, 254);
            this.tbNoDesde.Name = "tbNoDesde";
            this.tbNoDesde.Size = new System.Drawing.Size(130, 21);
            this.tbNoDesde.TabIndex = 59;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(260, 257);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 13);
            this.label7.TabIndex = 60;
            this.label7.Text = "Hasta Nº:";
            // 
            // tbNoHasta
            // 
            this.tbNoHasta.BackColor = System.Drawing.Color.Wheat;
            this.tbNoHasta.Location = new System.Drawing.Point(328, 254);
            this.tbNoHasta.Name = "tbNoHasta";
            this.tbNoHasta.Size = new System.Drawing.Size(130, 21);
            this.tbNoHasta.TabIndex = 61;
            // 
            // dtFechaDesde
            // 
            this.dtFechaDesde.Enabled = false;
            this.dtFechaDesde.Location = new System.Drawing.Point(124, 281);
            this.dtFechaDesde.Name = "dtFechaDesde";
            this.dtFechaDesde.Size = new System.Drawing.Size(233, 21);
            this.dtFechaDesde.TabIndex = 72;
            // 
            // dtFechaHasta
            // 
            this.dtFechaHasta.Enabled = false;
            this.dtFechaHasta.Location = new System.Drawing.Point(413, 281);
            this.dtFechaHasta.Name = "dtFechaHasta";
            this.dtFechaHasta.Size = new System.Drawing.Size(233, 21);
            this.dtFechaHasta.TabIndex = 73;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(363, 287);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(44, 13);
            this.label9.TabIndex = 74;
            this.label9.Text = "Hasta:";
            // 
            // cbFecha
            // 
            this.cbFecha.AutoSize = true;
            this.cbFecha.Location = new System.Drawing.Point(16, 283);
            this.cbFecha.Name = "cbFecha";
            this.cbFecha.Size = new System.Drawing.Size(102, 17);
            this.cbFecha.TabIndex = 75;
            this.cbFecha.Text = "Fecha desde:";
            this.cbFecha.UseVisualStyleBackColor = true;
            this.cbFecha.CheckedChanged += new System.EventHandler(this.cbFecha_CheckedChanged);
            // 
            // frmIngresoCajas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SandyBrown;
            this.ClientSize = new System.Drawing.Size(893, 611);
            this.Controls.Add(this.cbFecha);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.dtFechaHasta);
            this.Controls.Add(this.dtFechaDesde);
            this.Controls.Add(this.tbNoHasta);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.tbNoDesde);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.operacionesDataGridView);
            this.Controls.Add(this.clientesDataGridView);
            this.Controls.Add(this.cajasDataGridView);
            this.Controls.Add(this.cbTraslado);
            this.Controls.Add(this.pctbarras);
            this.Controls.Add(this.btnultimapos);
            this.Controls.Add(this.tbcodigo);
            this.Controls.Add(this.tbcontenido);
            this.Controls.Add(this.dtFechaingreso);
            this.Controls.Add(this.tbNocliente);
            this.Controls.Add(this.txtNombreEmpresa);
            this.Controls.Add(this.lbNombreEmpresa);
            this.Controls.Add(this.btnIngresar);
            this.Controls.Add(this.btnSigPos);
            this.Controls.Add(this.udPos);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.udNoPiso);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.udNoRack);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.udNoFila);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnsigcaja);
            this.Controls.Add(this.udNoCaja);
            this.Controls.Add(this.udNolote);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnSigLote);
            this.Controls.Add(contenidoLabel);
            this.Controls.Add(nombre_EmpresaLabel);
            this.Controls.Add(fecha_ingresoLabel);
            this.Controls.Add(loteLabel);
            this.Controls.Add(codigoLabel);
            this.Controls.Add(noclienteLabel);
            this.Controls.Add(this.pctlogo);
            this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.KeyPreview = true;
            this.Name = "frmIngresoCajas";
            this.Text = "Ingreso de Cajas";
            this.Load += new System.EventHandler(this.IngresoCajas_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmIngresoCajas_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.udNolote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udNoCaja)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udNoFila)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udNoRack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udNoPiso)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udPos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctlogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctbarras)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cajasDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cajasBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.archivoDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientesDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.operacionesDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.operacionesBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSigLote;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown udNolote;
        private System.Windows.Forms.NumericUpDown udNoCaja;
        private System.Windows.Forms.Button btnsigcaja;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown udNoFila;
        private System.Windows.Forms.NumericUpDown udNoRack;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown udNoPiso;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown udPos;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnSigPos;
        private System.Windows.Forms.Button btnIngresar;
        private System.Windows.Forms.ListBox lbNombreEmpresa;
        private System.Windows.Forms.TextBox txtNombreEmpresa;
        private System.Windows.Forms.TextBox tbNocliente;
        private System.Windows.Forms.DateTimePicker dtFechaingreso;
        private System.Windows.Forms.TextBox tbcontenido;
        private System.Windows.Forms.TextBox tbcodigo;
        private System.Windows.Forms.Button btnultimapos;
        private System.Windows.Forms.PrintDialog printDialog1;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.PictureBox pctlogo;
        private System.Windows.Forms.PictureBox pctbarras;
        private System.Windows.Forms.CheckBox cbTraslado;
        private archivoDataSet archivoDataSet;
        private System.Windows.Forms.BindingSource cajasBindingSource;
        private archivoDataSetTableAdapters.cajasTableAdapter cajasTableAdapter;
        private archivoDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private archivoDataSetTableAdapters.clientesTableAdapter clientesTableAdapter;
        private System.Windows.Forms.DataGridView cajasDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.BindingSource clientesBindingSource;
        private archivoDataSetTableAdapters.operacionesTableAdapter operacionesTableAdapter;
        private System.Windows.Forms.DataGridView clientesDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn23;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn24;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn25;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn26;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn27;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn28;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn29;
        private System.Windows.Forms.BindingSource operacionesBindingSource;
        private System.Windows.Forms.DataGridView operacionesDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn30;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn31;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn32;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn33;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn34;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbNoDesde;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbNoHasta;
        private System.Windows.Forms.DateTimePicker dtFechaDesde;
        private System.Windows.Forms.DateTimePicker dtFechaHasta;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckBox cbFecha;
    }
}