﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Archivo
{
    public partial class frmIngresoCajasSinFecha : Form
    {
        public frmIngresoCajasSinFecha()
        {
            InitializeComponent();
        }

        private void cajasBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.cajasBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.archivoDataSet);

        }

        private void frmIngresoCajasSinFecha_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'archivoDataSet.operaciones' table. You can move, or remove it, as needed.
            this.operacionesTableAdapter.Fill(this.archivoDataSet.operaciones);
            // TODO: This line of code loads data into the 'archivoDataSet.cajas' table. You can move, or remove it, as needed.
            this.cajasTableAdapter.Fill(this.archivoDataSet.cajas);

        }

        private void btnBuscaCodigo_Click(object sender, EventArgs e)
        {
            if (tbCodigo.Text.Length == 19)
            {
                DataView dv = archivoDataSet.cajas.DefaultView;
                string filter = "Nocliente > 0";
                dv.RowFilter = filter;
                cajasBindingSource.DataSource = dv;
                filter = "Codigo = " + tbCodigo.Text;
                dv.RowFilter = filter;
                cajasBindingSource.DataSource = dv;
                if (cajasDataGridView.RowCount == 0)
                {
                    MessageBox.Show("El código ingresado no se encuentra en la base de datos", "Error");
                }
                else
                {
                    tbcontenido.Text = cajasDataGridView[5, 0].Value.ToString();
                    if (cajasDataGridView[3, 0].Value.ToString() != "")
                    {
                        MessageBox.Show("Esta caja ya tiene una fecha de ingreso asignada", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        dtFechaingreso.Value = (DateTime)cajasDataGridView[3, 0].Value;
                    }
/*                    else
                    {
                        dtFechaingreso.Value = DateTime.Today;
                    }*/
                }
            }
            else
            {
                MessageBox.Show("El código debe tener 19 caracteres.");
                return;
            }

        }

        private void btnIngresar_Click(object sender, EventArgs e)
        {
            archivoDataSet.operacionesRow filanuevaop = archivoDataSet.operaciones.NewoperacionesRow();
            //            filanuevaop.NoOp = maximo + 1;
            filanuevaop.Tipo = "Ingreso";
            filanuevaop.Fecha = dtFechaingreso.Value;
            filanuevaop.NoCliente = Convert.ToInt32(cajasDataGridView[1,0].Value);
            filanuevaop.Codigo = tbCodigo.Text;
            filanuevaop.Traslado = cbTraslado.Checked;

            archivoDataSet.operaciones.Rows.Add(filanuevaop);
            operacionesBindingSource.MoveLast();

            cajasDataGridView[3, 0].Value = dtFechaingreso.Value;
            cajasDataGridView[5, 0].Value = tbcontenido.Text;

            if (tbNoDesde.Text != "" && tbNoHasta.Text != "")
            {
                if (Convert.ToInt32(tbNoDesde.Text) > Convert.ToInt32(tbNoHasta.Text))
                {
                    MessageBox.Show("El número desde debe ser <= al número hasta");
                    return;
                }
                else
                {
                    cajasDataGridView[10,0].Value=Convert.ToInt32(tbNoDesde.Text);
                    cajasDataGridView[11, 0].Value = Convert.ToInt32(tbNoHasta.Text);
                }
            }
            if (cbFecha.Checked)
            {
                if (dtFechaDesde.Value > dtFechaHasta.Value)
                {
                    MessageBox.Show("La fecha desde debe ser <= a la fecha hasta");
                    return;
                }
                else
                {
                    cajasDataGridView[12, 0].Value = dtFechaDesde.Value;
                    cajasDataGridView[13, 0].Value = dtFechaHasta.Value;
                }
            }



            this.Validate();
            this.cajasBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.archivoDataSet);

            MessageBox.Show("Ingreso realizado. Código " + tbCodigo.Text + ".");

        }

        private void cbFecha_CheckedChanged(object sender, EventArgs e)
        {
            if (cbFecha.Checked)
            {
                dtFechaDesde.Enabled = true;
                dtFechaHasta.Enabled = true;
            }
            else
            {
                dtFechaDesde.Enabled = false;
                dtFechaHasta.Enabled = false;
            }
        }

        private void tbCodigo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                btnBuscaCodigo_Click(this, e);
            }
        }

    }
}
