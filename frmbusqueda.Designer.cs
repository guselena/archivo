﻿namespace Archivo
{
    partial class frmbusqueda
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label contenidoLabel;
            this.label1 = new System.Windows.Forms.Label();
            this.tbCodigo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbNombreCiente = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbNoCliente = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbLote = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbNocaja = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbFila = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tbRack = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tbPiso = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tbPosicion = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tbFechaIngreso = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.dtFechaconsulta = new System.Windows.Forms.DateTimePicker();
            this.btnRetirar = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.tbContenido = new System.Windows.Forms.TextBox();
            this.cbTraslado = new System.Windows.Forms.CheckBox();
            this.cajasDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cajasBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.archivoDataSet = new Archivo.archivoDataSet();
            this.clientesDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clientesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.operacionesDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn31 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn32 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn33 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn34 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.operacionesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.contenidoTextBox = new System.Windows.Forms.TextBox();
            this.rbdocint = new System.Windows.Forms.RadioButton();
            this.rbbusqcaja = new System.Windows.Forms.RadioButton();
            this.rbreingreso = new System.Windows.Forms.RadioButton();
            this.detallesoperacionDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn35 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn36 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn37 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.detallesoperacionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tbDescripcion = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.udPaginas = new System.Windows.Forms.NumericUpDown();
            this.cbUrgente = new System.Windows.Forms.CheckBox();
            this.cajasTableAdapter = new Archivo.archivoDataSetTableAdapters.cajasTableAdapter();
            this.tableAdapterManager = new Archivo.archivoDataSetTableAdapters.TableAdapterManager();
            this.clientesTableAdapter = new Archivo.archivoDataSetTableAdapters.clientesTableAdapter();
            this.detallesoperacionTableAdapter = new Archivo.archivoDataSetTableAdapters.detallesoperacionTableAdapter();
            this.operacionesTableAdapter = new Archivo.archivoDataSetTableAdapters.operacionesTableAdapter();
            this.pctbarras = new System.Windows.Forms.PictureBox();
            this.pctlogo = new System.Windows.Forms.PictureBox();
            contenidoLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.cajasDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cajasBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.archivoDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientesDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.operacionesDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.operacionesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detallesoperacionDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detallesoperacionBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udPaginas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctbarras)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctlogo)).BeginInit();
            this.SuspendLayout();
            // 
            // contenidoLabel
            // 
            contenidoLabel.AutoSize = true;
            contenidoLabel.Location = new System.Drawing.Point(688, 178);
            contenidoLabel.Name = "contenidoLabel";
            contenidoLabel.Size = new System.Drawing.Size(70, 13);
            contenidoLabel.TabIndex = 44;
            contenidoLabel.Text = "Contenido:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(56, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Código de caja:";
            // 
            // tbCodigo
            // 
            this.tbCodigo.BackColor = System.Drawing.Color.Wheat;
            this.tbCodigo.Location = new System.Drawing.Point(157, 12);
            this.tbCodigo.Name = "tbCodigo";
            this.tbCodigo.Size = new System.Drawing.Size(401, 21);
            this.tbCodigo.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(27, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(129, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "Nombre de Empresa:";
            // 
            // tbNombreCiente
            // 
            this.tbNombreCiente.BackColor = System.Drawing.Color.Wheat;
            this.tbNombreCiente.Location = new System.Drawing.Point(157, 63);
            this.tbNombreCiente.Name = "tbNombreCiente";
            this.tbNombreCiente.ReadOnly = true;
            this.tbNombreCiente.Size = new System.Drawing.Size(401, 21);
            this.tbNombreCiente.TabIndex = 16;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(68, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 13);
            this.label3.TabIndex = 17;
            this.label3.Text = "Nº de cliente:";
            // 
            // tbNoCliente
            // 
            this.tbNoCliente.BackColor = System.Drawing.Color.Wheat;
            this.tbNoCliente.Location = new System.Drawing.Point(157, 37);
            this.tbNoCliente.Name = "tbNoCliente";
            this.tbNoCliente.ReadOnly = true;
            this.tbNoCliente.Size = new System.Drawing.Size(76, 21);
            this.tbNoCliente.TabIndex = 18;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(114, 92);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 13);
            this.label4.TabIndex = 19;
            this.label4.Text = "Lote:";
            // 
            // tbLote
            // 
            this.tbLote.BackColor = System.Drawing.Color.Wheat;
            this.tbLote.Location = new System.Drawing.Point(157, 89);
            this.tbLote.Name = "tbLote";
            this.tbLote.ReadOnly = true;
            this.tbLote.Size = new System.Drawing.Size(76, 21);
            this.tbLote.TabIndex = 20;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(241, 92);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 13);
            this.label5.TabIndex = 21;
            this.label5.Text = "Nº de caja:";
            // 
            // tbNocaja
            // 
            this.tbNocaja.BackColor = System.Drawing.Color.Wheat;
            this.tbNocaja.Location = new System.Drawing.Point(318, 89);
            this.tbNocaja.Name = "tbNocaja";
            this.tbNocaja.ReadOnly = true;
            this.tbNocaja.Size = new System.Drawing.Size(73, 21);
            this.tbNocaja.TabIndex = 22;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(427, 92);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(31, 13);
            this.label6.TabIndex = 23;
            this.label6.Text = "Fila:";
            // 
            // tbFila
            // 
            this.tbFila.BackColor = System.Drawing.Color.Wheat;
            this.tbFila.Location = new System.Drawing.Point(464, 89);
            this.tbFila.Name = "tbFila";
            this.tbFila.ReadOnly = true;
            this.tbFila.Size = new System.Drawing.Size(73, 21);
            this.tbFila.TabIndex = 24;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(110, 118);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(40, 13);
            this.label7.TabIndex = 25;
            this.label7.Text = "Rack:";
            // 
            // tbRack
            // 
            this.tbRack.BackColor = System.Drawing.Color.Wheat;
            this.tbRack.Location = new System.Drawing.Point(157, 115);
            this.tbRack.Name = "tbRack";
            this.tbRack.ReadOnly = true;
            this.tbRack.Size = new System.Drawing.Size(76, 21);
            this.tbRack.TabIndex = 26;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(276, 118);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 13);
            this.label8.TabIndex = 27;
            this.label8.Text = "Piso:";
            // 
            // tbPiso
            // 
            this.tbPiso.BackColor = System.Drawing.Color.Wheat;
            this.tbPiso.Location = new System.Drawing.Point(318, 115);
            this.tbPiso.Name = "tbPiso";
            this.tbPiso.ReadOnly = true;
            this.tbPiso.Size = new System.Drawing.Size(73, 21);
            this.tbPiso.TabIndex = 28;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(399, 118);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(58, 13);
            this.label9.TabIndex = 29;
            this.label9.Text = "Posición:";
            // 
            // tbPosicion
            // 
            this.tbPosicion.BackColor = System.Drawing.Color.Wheat;
            this.tbPosicion.Location = new System.Drawing.Point(464, 115);
            this.tbPosicion.Name = "tbPosicion";
            this.tbPosicion.ReadOnly = true;
            this.tbPosicion.Size = new System.Drawing.Size(73, 21);
            this.tbPosicion.TabIndex = 30;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(43, 144);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(111, 13);
            this.label10.TabIndex = 31;
            this.label10.Text = "Fecha de Ingreso:";
            // 
            // tbFechaIngreso
            // 
            this.tbFechaIngreso.BackColor = System.Drawing.Color.Wheat;
            this.tbFechaIngreso.Location = new System.Drawing.Point(157, 141);
            this.tbFechaIngreso.Name = "tbFechaIngreso";
            this.tbFechaIngreso.ReadOnly = true;
            this.tbFechaIngreso.Size = new System.Drawing.Size(234, 21);
            this.tbFechaIngreso.TabIndex = 32;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(108, 411);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(45, 13);
            this.label11.TabIndex = 33;
            this.label11.Text = "Fecha:";
            // 
            // dtFechaconsulta
            // 
            this.dtFechaconsulta.Location = new System.Drawing.Point(159, 405);
            this.dtFechaconsulta.Name = "dtFechaconsulta";
            this.dtFechaconsulta.Size = new System.Drawing.Size(233, 21);
            this.dtFechaconsulta.TabIndex = 34;
            // 
            // btnRetirar
            // 
            this.btnRetirar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnRetirar.Enabled = false;
            this.btnRetirar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRetirar.Location = new System.Drawing.Point(159, 542);
            this.btnRetirar.Name = "btnRetirar";
            this.btnRetirar.Size = new System.Drawing.Size(266, 64);
            this.btnRetirar.TabIndex = 35;
            this.btnRetirar.Text = "Ingresar búsqueda";
            this.btnRetirar.UseVisualStyleBackColor = false;
            this.btnRetirar.Click += new System.EventHandler(this.btnRetirar_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(84, 171);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(70, 13);
            this.label12.TabIndex = 38;
            this.label12.Text = "Contenido:";
            // 
            // tbContenido
            // 
            this.tbContenido.BackColor = System.Drawing.Color.Wheat;
            this.tbContenido.Location = new System.Drawing.Point(157, 168);
            this.tbContenido.Multiline = true;
            this.tbContenido.Name = "tbContenido";
            this.tbContenido.ReadOnly = true;
            this.tbContenido.Size = new System.Drawing.Size(401, 191);
            this.tbContenido.TabIndex = 39;
            // 
            // cbTraslado
            // 
            this.cbTraslado.AutoSize = true;
            this.cbTraslado.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cbTraslado.Location = new System.Drawing.Point(402, 407);
            this.cbTraslado.Name = "cbTraslado";
            this.cbTraslado.Size = new System.Drawing.Size(80, 17);
            this.cbTraslado.TabIndex = 41;
            this.cbTraslado.Text = "Traslado:";
            this.cbTraslado.UseVisualStyleBackColor = true;
            // 
            // cajasDataGridView
            // 
            this.cajasDataGridView.AllowUserToAddRows = false;
            this.cajasDataGridView.AllowUserToDeleteRows = false;
            this.cajasDataGridView.AutoGenerateColumns = false;
            this.cajasDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.cajasDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewCheckBoxColumn1,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9});
            this.cajasDataGridView.DataSource = this.cajasBindingSource;
            this.cajasDataGridView.Location = new System.Drawing.Point(596, 15);
            this.cajasDataGridView.Name = "cajasDataGridView";
            this.cajasDataGridView.ReadOnly = true;
            this.cajasDataGridView.Size = new System.Drawing.Size(350, 105);
            this.cajasDataGridView.TabIndex = 42;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Codigo";
            this.dataGridViewTextBoxColumn1.HeaderText = "Codigo";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Nocliente";
            this.dataGridViewTextBoxColumn2.HeaderText = "Nocliente";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Lote";
            this.dataGridViewTextBoxColumn3.HeaderText = "Lote";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "Fecha ingreso";
            this.dataGridViewTextBoxColumn4.HeaderText = "Fecha ingreso";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewCheckBoxColumn1
            // 
            this.dataGridViewCheckBoxColumn1.DataPropertyName = "Enconsulta";
            this.dataGridViewCheckBoxColumn1.HeaderText = "Enconsulta";
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            this.dataGridViewCheckBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "Contenido";
            this.dataGridViewTextBoxColumn5.HeaderText = "Contenido";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "Ubicacion";
            this.dataGridViewTextBoxColumn6.HeaderText = "Ubicacion";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "Nocaja";
            this.dataGridViewTextBoxColumn7.HeaderText = "Nocaja";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "Fechaconsulta";
            this.dataGridViewTextBoxColumn8.HeaderText = "Fechaconsulta";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "Codigoint";
            this.dataGridViewTextBoxColumn9.HeaderText = "Codigoint";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            // 
            // cajasBindingSource
            // 
            this.cajasBindingSource.DataMember = "cajas";
            this.cajasBindingSource.DataSource = this.archivoDataSet;
            // 
            // archivoDataSet
            // 
            this.archivoDataSet.DataSetName = "archivoDataSet";
            this.archivoDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // clientesDataGridView
            // 
            this.clientesDataGridView.AllowUserToAddRows = false;
            this.clientesDataGridView.AllowUserToDeleteRows = false;
            this.clientesDataGridView.AutoGenerateColumns = false;
            this.clientesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.clientesDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn13,
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewTextBoxColumn15,
            this.dataGridViewTextBoxColumn16,
            this.dataGridViewTextBoxColumn17,
            this.dataGridViewTextBoxColumn18,
            this.dataGridViewTextBoxColumn19,
            this.dataGridViewTextBoxColumn20,
            this.dataGridViewTextBoxColumn21,
            this.dataGridViewTextBoxColumn22,
            this.dataGridViewTextBoxColumn23,
            this.dataGridViewTextBoxColumn24,
            this.dataGridViewTextBoxColumn25,
            this.dataGridViewTextBoxColumn26,
            this.dataGridViewTextBoxColumn27,
            this.dataGridViewTextBoxColumn28,
            this.dataGridViewTextBoxColumn29});
            this.clientesDataGridView.DataSource = this.clientesBindingSource;
            this.clientesDataGridView.Location = new System.Drawing.Point(964, 15);
            this.clientesDataGridView.Name = "clientesDataGridView";
            this.clientesDataGridView.ReadOnly = true;
            this.clientesDataGridView.Size = new System.Drawing.Size(350, 105);
            this.clientesDataGridView.TabIndex = 42;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "Nocliente";
            this.dataGridViewTextBoxColumn10.HeaderText = "Nocliente";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "NombreEmpresa";
            this.dataGridViewTextBoxColumn11.HeaderText = "NombreEmpresa";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.DataPropertyName = "Direccion";
            this.dataGridViewTextBoxColumn12.HeaderText = "Direccion";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.DataPropertyName = "Localidad";
            this.dataGridViewTextBoxColumn13.HeaderText = "Localidad";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.DataPropertyName = "CP";
            this.dataGridViewTextBoxColumn14.HeaderText = "CP";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.DataPropertyName = "Telefono";
            this.dataGridViewTextBoxColumn15.HeaderText = "Telefono";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.DataPropertyName = "Nombrecont1";
            this.dataGridViewTextBoxColumn16.HeaderText = "Nombrecont1";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.DataPropertyName = "Telcont1";
            this.dataGridViewTextBoxColumn17.HeaderText = "Telcont1";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.DataPropertyName = "Nombrecont2";
            this.dataGridViewTextBoxColumn18.HeaderText = "Nombrecont2";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn19
            // 
            this.dataGridViewTextBoxColumn19.DataPropertyName = "Telcont2";
            this.dataGridViewTextBoxColumn19.HeaderText = "Telcont2";
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn20
            // 
            this.dataGridViewTextBoxColumn20.DataPropertyName = "Nombrecont3";
            this.dataGridViewTextBoxColumn20.HeaderText = "Nombrecont3";
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            this.dataGridViewTextBoxColumn20.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn21
            // 
            this.dataGridViewTextBoxColumn21.DataPropertyName = "Telcont3";
            this.dataGridViewTextBoxColumn21.HeaderText = "Telcont3";
            this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            this.dataGridViewTextBoxColumn21.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn22
            // 
            this.dataGridViewTextBoxColumn22.DataPropertyName = "Nombrecont4";
            this.dataGridViewTextBoxColumn22.HeaderText = "Nombrecont4";
            this.dataGridViewTextBoxColumn22.Name = "dataGridViewTextBoxColumn22";
            this.dataGridViewTextBoxColumn22.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn23
            // 
            this.dataGridViewTextBoxColumn23.DataPropertyName = "Telcont4";
            this.dataGridViewTextBoxColumn23.HeaderText = "Telcont4";
            this.dataGridViewTextBoxColumn23.Name = "dataGridViewTextBoxColumn23";
            this.dataGridViewTextBoxColumn23.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn24
            // 
            this.dataGridViewTextBoxColumn24.DataPropertyName = "Plan";
            this.dataGridViewTextBoxColumn24.HeaderText = "Plan";
            this.dataGridViewTextBoxColumn24.Name = "dataGridViewTextBoxColumn24";
            this.dataGridViewTextBoxColumn24.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn25
            // 
            this.dataGridViewTextBoxColumn25.DataPropertyName = "DireccionEntrega";
            this.dataGridViewTextBoxColumn25.HeaderText = "DireccionEntrega";
            this.dataGridViewTextBoxColumn25.Name = "dataGridViewTextBoxColumn25";
            this.dataGridViewTextBoxColumn25.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn26
            // 
            this.dataGridViewTextBoxColumn26.DataPropertyName = "CUIT";
            this.dataGridViewTextBoxColumn26.HeaderText = "CUIT";
            this.dataGridViewTextBoxColumn26.Name = "dataGridViewTextBoxColumn26";
            this.dataGridViewTextBoxColumn26.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn27
            // 
            this.dataGridViewTextBoxColumn27.DataPropertyName = "IVA";
            this.dataGridViewTextBoxColumn27.HeaderText = "IVA";
            this.dataGridViewTextBoxColumn27.Name = "dataGridViewTextBoxColumn27";
            this.dataGridViewTextBoxColumn27.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn28
            // 
            this.dataGridViewTextBoxColumn28.DataPropertyName = "usuario";
            this.dataGridViewTextBoxColumn28.HeaderText = "usuario";
            this.dataGridViewTextBoxColumn28.Name = "dataGridViewTextBoxColumn28";
            this.dataGridViewTextBoxColumn28.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn29
            // 
            this.dataGridViewTextBoxColumn29.DataPropertyName = "contrasena";
            this.dataGridViewTextBoxColumn29.HeaderText = "contrasena";
            this.dataGridViewTextBoxColumn29.Name = "dataGridViewTextBoxColumn29";
            this.dataGridViewTextBoxColumn29.ReadOnly = true;
            // 
            // clientesBindingSource
            // 
            this.clientesBindingSource.DataMember = "clientes";
            this.clientesBindingSource.DataSource = this.archivoDataSet;
            // 
            // operacionesDataGridView
            // 
            this.operacionesDataGridView.AllowUserToAddRows = false;
            this.operacionesDataGridView.AllowUserToDeleteRows = false;
            this.operacionesDataGridView.AutoGenerateColumns = false;
            this.operacionesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.operacionesDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn30,
            this.dataGridViewTextBoxColumn31,
            this.dataGridViewTextBoxColumn32,
            this.dataGridViewTextBoxColumn33,
            this.dataGridViewTextBoxColumn34,
            this.dataGridViewCheckBoxColumn2});
            this.operacionesDataGridView.DataSource = this.operacionesBindingSource;
            this.operacionesDataGridView.Location = new System.Drawing.Point(964, 126);
            this.operacionesDataGridView.Name = "operacionesDataGridView";
            this.operacionesDataGridView.ReadOnly = true;
            this.operacionesDataGridView.Size = new System.Drawing.Size(350, 115);
            this.operacionesDataGridView.TabIndex = 42;
            // 
            // dataGridViewTextBoxColumn30
            // 
            this.dataGridViewTextBoxColumn30.DataPropertyName = "NoOp";
            this.dataGridViewTextBoxColumn30.HeaderText = "NoOp";
            this.dataGridViewTextBoxColumn30.Name = "dataGridViewTextBoxColumn30";
            this.dataGridViewTextBoxColumn30.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn31
            // 
            this.dataGridViewTextBoxColumn31.DataPropertyName = "Tipo";
            this.dataGridViewTextBoxColumn31.HeaderText = "Tipo";
            this.dataGridViewTextBoxColumn31.Name = "dataGridViewTextBoxColumn31";
            this.dataGridViewTextBoxColumn31.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn32
            // 
            this.dataGridViewTextBoxColumn32.DataPropertyName = "Fecha";
            this.dataGridViewTextBoxColumn32.HeaderText = "Fecha";
            this.dataGridViewTextBoxColumn32.Name = "dataGridViewTextBoxColumn32";
            this.dataGridViewTextBoxColumn32.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn33
            // 
            this.dataGridViewTextBoxColumn33.DataPropertyName = "NoCliente";
            this.dataGridViewTextBoxColumn33.HeaderText = "NoCliente";
            this.dataGridViewTextBoxColumn33.Name = "dataGridViewTextBoxColumn33";
            this.dataGridViewTextBoxColumn33.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn34
            // 
            this.dataGridViewTextBoxColumn34.DataPropertyName = "Codigo";
            this.dataGridViewTextBoxColumn34.HeaderText = "Codigo";
            this.dataGridViewTextBoxColumn34.Name = "dataGridViewTextBoxColumn34";
            this.dataGridViewTextBoxColumn34.ReadOnly = true;
            // 
            // dataGridViewCheckBoxColumn2
            // 
            this.dataGridViewCheckBoxColumn2.DataPropertyName = "Traslado";
            this.dataGridViewCheckBoxColumn2.HeaderText = "Traslado";
            this.dataGridViewCheckBoxColumn2.Name = "dataGridViewCheckBoxColumn2";
            this.dataGridViewCheckBoxColumn2.ReadOnly = true;
            // 
            // operacionesBindingSource
            // 
            this.operacionesBindingSource.DataMember = "operaciones";
            this.operacionesBindingSource.DataSource = this.archivoDataSet;
            // 
            // contenidoTextBox
            // 
            this.contenidoTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.cajasBindingSource, "Contenido", true));
            this.contenidoTextBox.Location = new System.Drawing.Point(763, 175);
            this.contenidoTextBox.Name = "contenidoTextBox";
            this.contenidoTextBox.Size = new System.Drawing.Size(116, 21);
            this.contenidoTextBox.TabIndex = 45;
            // 
            // rbdocint
            // 
            this.rbdocint.Checked = true;
            this.rbdocint.Location = new System.Drawing.Point(159, 366);
            this.rbdocint.Name = "rbdocint";
            this.rbdocint.Size = new System.Drawing.Size(134, 33);
            this.rbdocint.TabIndex = 46;
            this.rbdocint.TabStop = true;
            this.rbdocint.Text = "Búsqueda de documento interno";
            this.rbdocint.UseVisualStyleBackColor = true;
            // 
            // rbbusqcaja
            // 
            this.rbbusqcaja.Location = new System.Drawing.Point(289, 365);
            this.rbbusqcaja.Name = "rbbusqcaja";
            this.rbbusqcaja.Size = new System.Drawing.Size(146, 34);
            this.rbbusqcaja.TabIndex = 47;
            this.rbbusqcaja.Text = "Búsqueda de caja en depósito";
            this.rbbusqcaja.UseVisualStyleBackColor = true;
            // 
            // rbreingreso
            // 
            this.rbreingreso.Location = new System.Drawing.Point(432, 365);
            this.rbreingreso.Name = "rbreingreso";
            this.rbreingreso.Size = new System.Drawing.Size(126, 34);
            this.rbreingreso.TabIndex = 48;
            this.rbreingreso.Text = "Reingreso de documento";
            this.rbreingreso.UseVisualStyleBackColor = true;
            // 
            // detallesoperacionDataGridView
            // 
            this.detallesoperacionDataGridView.AutoGenerateColumns = false;
            this.detallesoperacionDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.detallesoperacionDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn35,
            this.dataGridViewTextBoxColumn36,
            this.dataGridViewTextBoxColumn37});
            this.detallesoperacionDataGridView.DataSource = this.detallesoperacionBindingSource;
            this.detallesoperacionDataGridView.Location = new System.Drawing.Point(964, 247);
            this.detallesoperacionDataGridView.Name = "detallesoperacionDataGridView";
            this.detallesoperacionDataGridView.Size = new System.Drawing.Size(300, 220);
            this.detallesoperacionDataGridView.TabIndex = 48;
            // 
            // dataGridViewTextBoxColumn35
            // 
            this.dataGridViewTextBoxColumn35.DataPropertyName = "NoOp";
            this.dataGridViewTextBoxColumn35.HeaderText = "NoOp";
            this.dataGridViewTextBoxColumn35.Name = "dataGridViewTextBoxColumn35";
            // 
            // dataGridViewTextBoxColumn36
            // 
            this.dataGridViewTextBoxColumn36.DataPropertyName = "Descripcion";
            this.dataGridViewTextBoxColumn36.HeaderText = "Descripcion";
            this.dataGridViewTextBoxColumn36.Name = "dataGridViewTextBoxColumn36";
            // 
            // dataGridViewTextBoxColumn37
            // 
            this.dataGridViewTextBoxColumn37.DataPropertyName = "En consulta";
            this.dataGridViewTextBoxColumn37.HeaderText = "En consulta";
            this.dataGridViewTextBoxColumn37.Name = "dataGridViewTextBoxColumn37";
            // 
            // detallesoperacionBindingSource
            // 
            this.detallesoperacionBindingSource.DataMember = "detallesoperacion";
            this.detallesoperacionBindingSource.DataSource = this.archivoDataSet;
            // 
            // tbDescripcion
            // 
            this.tbDescripcion.BackColor = System.Drawing.Color.Wheat;
            this.tbDescripcion.Location = new System.Drawing.Point(160, 455);
            this.tbDescripcion.Multiline = true;
            this.tbDescripcion.Name = "tbDescripcion";
            this.tbDescripcion.Size = new System.Drawing.Size(401, 54);
            this.tbDescripcion.TabIndex = 49;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(157, 439);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(215, 13);
            this.label13.TabIndex = 50;
            this.label13.Text = "Descripción del documento retirado:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(164, 520);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(129, 13);
            this.label14.TabIndex = 51;
            this.label14.Text = "Cantidad de páginas:";
            // 
            // udPaginas
            // 
            this.udPaginas.Location = new System.Drawing.Point(299, 518);
            this.udPaginas.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.udPaginas.Name = "udPaginas";
            this.udPaginas.Size = new System.Drawing.Size(48, 21);
            this.udPaginas.TabIndex = 52;
            // 
            // cbUrgente
            // 
            this.cbUrgente.AutoSize = true;
            this.cbUrgente.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cbUrgente.Location = new System.Drawing.Point(355, 520);
            this.cbUrgente.Name = "cbUrgente";
            this.cbUrgente.Size = new System.Drawing.Size(76, 17);
            this.cbUrgente.TabIndex = 53;
            this.cbUrgente.Text = "Urgente:";
            this.cbUrgente.UseVisualStyleBackColor = true;
            // 
            // cajasTableAdapter
            // 
            this.cajasTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.cajasTableAdapter = this.cajasTableAdapter;
            this.tableAdapterManager.clientesTableAdapter = this.clientesTableAdapter;
            this.tableAdapterManager.detallesoperacionTableAdapter = this.detallesoperacionTableAdapter;
            this.tableAdapterManager.operacionesTableAdapter = this.operacionesTableAdapter;
            this.tableAdapterManager.UpdateOrder = Archivo.archivoDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // clientesTableAdapter
            // 
            this.clientesTableAdapter.ClearBeforeFill = true;
            // 
            // detallesoperacionTableAdapter
            // 
            this.detallesoperacionTableAdapter.ClearBeforeFill = true;
            // 
            // operacionesTableAdapter
            // 
            this.operacionesTableAdapter.ClearBeforeFill = true;
            // 
            // pctbarras
            // 
            this.pctbarras.Location = new System.Drawing.Point(901, 466);
            this.pctbarras.Name = "pctbarras";
            this.pctbarras.Size = new System.Drawing.Size(117, 50);
            this.pctbarras.TabIndex = 37;
            this.pctbarras.TabStop = false;
            // 
            // pctlogo
            // 
            this.pctlogo.Image = global::Archivo.Properties.Resources.logo_para_soft;
            this.pctlogo.Location = new System.Drawing.Point(652, 312);
            this.pctlogo.Name = "pctlogo";
            this.pctlogo.Size = new System.Drawing.Size(234, 94);
            this.pctlogo.TabIndex = 36;
            this.pctlogo.TabStop = false;
            // 
            // frmbusqueda
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SandyBrown;
            this.ClientSize = new System.Drawing.Size(576, 618);
            this.Controls.Add(this.cbUrgente);
            this.Controls.Add(this.udPaginas);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.tbDescripcion);
            this.Controls.Add(this.detallesoperacionDataGridView);
            this.Controls.Add(this.rbreingreso);
            this.Controls.Add(this.rbbusqcaja);
            this.Controls.Add(this.rbdocint);
            this.Controls.Add(contenidoLabel);
            this.Controls.Add(this.contenidoTextBox);
            this.Controls.Add(this.operacionesDataGridView);
            this.Controls.Add(this.clientesDataGridView);
            this.Controls.Add(this.cajasDataGridView);
            this.Controls.Add(this.cbTraslado);
            this.Controls.Add(this.tbContenido);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.pctbarras);
            this.Controls.Add(this.pctlogo);
            this.Controls.Add(this.btnRetirar);
            this.Controls.Add(this.dtFechaconsulta);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.tbFechaIngreso);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.tbPosicion);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.tbPiso);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.tbRack);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.tbFila);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.tbNocaja);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tbLote);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tbNoCliente);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbNombreCiente);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbCodigo);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.KeyPreview = true;
            this.Name = "frmbusqueda";
            this.Text = "Búsqueda de documento";
            this.Load += new System.EventHandler(this.frmbusqueda_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmretconsulta_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.cajasDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cajasBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.archivoDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientesDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.operacionesDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.operacionesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detallesoperacionDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detallesoperacionBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udPaginas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctbarras)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctlogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbCodigo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbNombreCiente;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbNoCliente;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbLote;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbNocaja;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbFila;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbRack;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbPiso;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tbPosicion;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbFechaIngreso;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DateTimePicker dtFechaconsulta;
        private System.Windows.Forms.Button btnRetirar;
        private System.Windows.Forms.PictureBox pctlogo;
        private System.Windows.Forms.PictureBox pctbarras;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tbContenido;
        private System.Windows.Forms.CheckBox cbTraslado;
        private archivoDataSet archivoDataSet;
        private System.Windows.Forms.BindingSource cajasBindingSource;
        private archivoDataSetTableAdapters.cajasTableAdapter cajasTableAdapter;
        private archivoDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private archivoDataSetTableAdapters.clientesTableAdapter clientesTableAdapter;
        private System.Windows.Forms.DataGridView cajasDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.BindingSource clientesBindingSource;
        private archivoDataSetTableAdapters.operacionesTableAdapter operacionesTableAdapter;
        private System.Windows.Forms.DataGridView clientesDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn23;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn24;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn25;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn26;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn27;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn28;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn29;
        private System.Windows.Forms.BindingSource operacionesBindingSource;
        private System.Windows.Forms.DataGridView operacionesDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn30;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn31;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn32;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn33;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn34;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn2;
        private System.Windows.Forms.TextBox contenidoTextBox;
        private System.Windows.Forms.RadioButton rbdocint;
        private System.Windows.Forms.RadioButton rbbusqcaja;
        private System.Windows.Forms.RadioButton rbreingreso;
        private System.Windows.Forms.BindingSource detallesoperacionBindingSource;
        private archivoDataSetTableAdapters.detallesoperacionTableAdapter detallesoperacionTableAdapter;
        private System.Windows.Forms.DataGridView detallesoperacionDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn35;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn36;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn37;
        private System.Windows.Forms.TextBox tbDescripcion;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.NumericUpDown udPaginas;
        private System.Windows.Forms.CheckBox cbUrgente;
    }
}