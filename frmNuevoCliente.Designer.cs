﻿namespace Archivo
{
    partial class frmNuevoCliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label noclienteLabel;
            System.Windows.Forms.Label nombre_EmpresaLabel;
            System.Windows.Forms.Label direcciónLabel;
            System.Windows.Forms.Label localidadLabel;
            System.Windows.Forms.Label cPLabel;
            System.Windows.Forms.Label telefonoLabel;
            System.Windows.Forms.Label nombrecont1Label;
            System.Windows.Forms.Label telcont1Label;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label label4;
            System.Windows.Forms.Label label5;
            System.Windows.Forms.Label label6;
            System.Windows.Forms.Label planLabel;
            System.Windows.Forms.Label direcciónEntregaLabel;
            System.Windows.Forms.Label cUITLabel;
            System.Windows.Forms.Label iVALabel;
            System.Windows.Forms.Label emailprincipalLabel;
            System.Windows.Forms.Label emailcont1Label;
            System.Windows.Forms.Label emailcont2Label;
            System.Windows.Forms.Label emailcont3Label;
            System.Windows.Forms.Label emailcont4Label;
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.emailcont1TextBox = new System.Windows.Forms.TextBox();
            this.clientesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.archivoDataSet = new Archivo.archivoDataSet();
            this.telcont1TextBox = new System.Windows.Forms.TextBox();
            this.nombrecont1TextBox = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.emailcont2TextBox = new System.Windows.Forms.TextBox();
            this.telcont2TextBox = new System.Windows.Forms.TextBox();
            this.nombrecont2TextBox = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.emailcont3TextBox = new System.Windows.Forms.TextBox();
            this.telcont3TextBox = new System.Windows.Forms.TextBox();
            this.nombrecont3TextBox = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.emailcont4TextBox = new System.Windows.Forms.TextBox();
            this.telcont4TextBox = new System.Windows.Forms.TextBox();
            this.nombrecont4TextBox = new System.Windows.Forms.TextBox();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.nombre_EmpresaTextBox = new System.Windows.Forms.TextBox();
            this.direcciónTextBox = new System.Windows.Forms.TextBox();
            this.localidadTextBox = new System.Windows.Forms.TextBox();
            this.cPTextBox = new System.Windows.Forms.TextBox();
            this.telefonoTextBox = new System.Windows.Forms.TextBox();
            this.lbLocalidad = new System.Windows.Forms.ListBox();
            this.lbCP = new System.Windows.Forms.ListBox();
            this.planTextBox = new System.Windows.Forms.TextBox();
            this.direcciónEntregaTextBox = new System.Windows.Forms.TextBox();
            this.cUITTextBox = new System.Windows.Forms.TextBox();
            this.iVATextBox = new System.Windows.Forms.TextBox();
            this.cmbIva = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.clientesTableAdapter = new Archivo.archivoDataSetTableAdapters.clientesTableAdapter();
            this.tableAdapterManager = new Archivo.archivoDataSetTableAdapters.TableAdapterManager();
            this.clientesDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.emailprincipalTextBox = new System.Windows.Forms.TextBox();
            this.noclienteTextBox = new System.Windows.Forms.TextBox();
            this.hostingCheckBox = new System.Windows.Forms.CheckBox();
            noclienteLabel = new System.Windows.Forms.Label();
            nombre_EmpresaLabel = new System.Windows.Forms.Label();
            direcciónLabel = new System.Windows.Forms.Label();
            localidadLabel = new System.Windows.Forms.Label();
            cPLabel = new System.Windows.Forms.Label();
            telefonoLabel = new System.Windows.Forms.Label();
            nombrecont1Label = new System.Windows.Forms.Label();
            telcont1Label = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            label5 = new System.Windows.Forms.Label();
            label6 = new System.Windows.Forms.Label();
            planLabel = new System.Windows.Forms.Label();
            direcciónEntregaLabel = new System.Windows.Forms.Label();
            cUITLabel = new System.Windows.Forms.Label();
            iVALabel = new System.Windows.Forms.Label();
            emailprincipalLabel = new System.Windows.Forms.Label();
            emailcont1Label = new System.Windows.Forms.Label();
            emailcont2Label = new System.Windows.Forms.Label();
            emailcont3Label = new System.Windows.Forms.Label();
            emailcont4Label = new System.Windows.Forms.Label();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.clientesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.archivoDataSet)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.clientesDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // noclienteLabel
            // 
            noclienteLabel.AutoSize = true;
            noclienteLabel.Location = new System.Drawing.Point(66, 9);
            noclienteLabel.Name = "noclienteLabel";
            noclienteLabel.Size = new System.Drawing.Size(88, 13);
            noclienteLabel.TabIndex = 1;
            noclienteLabel.Text = "Nº de Cliente:";
            // 
            // nombre_EmpresaLabel
            // 
            nombre_EmpresaLabel.AutoSize = true;
            nombre_EmpresaLabel.Location = new System.Drawing.Point(14, 35);
            nombre_EmpresaLabel.Name = "nombre_EmpresaLabel";
            nombre_EmpresaLabel.Size = new System.Drawing.Size(143, 13);
            nombre_EmpresaLabel.TabIndex = 3;
            nombre_EmpresaLabel.Text = "Nombre de la Empresa:";
            // 
            // direcciónLabel
            // 
            direcciónLabel.AutoSize = true;
            direcciónLabel.Location = new System.Drawing.Point(86, 66);
            direcciónLabel.Name = "direcciónLabel";
            direcciónLabel.Size = new System.Drawing.Size(65, 13);
            direcciónLabel.TabIndex = 5;
            direcciónLabel.Text = "Dirección:";
            // 
            // localidadLabel
            // 
            localidadLabel.AutoSize = true;
            localidadLabel.Location = new System.Drawing.Point(85, 87);
            localidadLabel.Name = "localidadLabel";
            localidadLabel.Size = new System.Drawing.Size(65, 13);
            localidadLabel.TabIndex = 7;
            localidadLabel.Text = "Localidad:";
            // 
            // cPLabel
            // 
            cPLabel.AutoSize = true;
            cPLabel.Location = new System.Drawing.Point(506, 87);
            cPLabel.Name = "cPLabel";
            cPLabel.Size = new System.Drawing.Size(28, 13);
            cPLabel.TabIndex = 9;
            cPLabel.Text = "CP:";
            // 
            // telefonoLabel
            // 
            telefonoLabel.AutoSize = true;
            telefonoLabel.Location = new System.Drawing.Point(90, 113);
            telefonoLabel.Name = "telefonoLabel";
            telefonoLabel.Size = new System.Drawing.Size(61, 13);
            telefonoLabel.TabIndex = 11;
            telefonoLabel.Text = "Telefono:";
            // 
            // nombrecont1Label
            // 
            nombrecont1Label.AutoSize = true;
            nombrecont1Label.Location = new System.Drawing.Point(14, 23);
            nombrecont1Label.Name = "nombrecont1Label";
            nombrecont1Label.Size = new System.Drawing.Size(57, 13);
            nombrecont1Label.TabIndex = 13;
            nombrecont1Label.Text = "Nombre:";
            // 
            // telcont1Label
            // 
            telcont1Label.AutoSize = true;
            telcont1Label.Location = new System.Drawing.Point(8, 49);
            telcont1Label.Name = "telcont1Label";
            telcont1Label.Size = new System.Drawing.Size(61, 13);
            telcont1Label.TabIndex = 15;
            telcont1Label.Text = "Teléfono:";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(14, 23);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(57, 13);
            label1.TabIndex = 13;
            label1.Text = "Nombre:";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(8, 49);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(61, 13);
            label2.TabIndex = 15;
            label2.Text = "Teléfono:";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(14, 23);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(57, 13);
            label3.TabIndex = 13;
            label3.Text = "Nombre:";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new System.Drawing.Point(8, 49);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(61, 13);
            label4.TabIndex = 15;
            label4.Text = "Teléfono:";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Location = new System.Drawing.Point(14, 23);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(57, 13);
            label5.TabIndex = 13;
            label5.Text = "Nombre:";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Location = new System.Drawing.Point(8, 49);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(61, 13);
            label6.TabIndex = 15;
            label6.Text = "Teléfono:";
            // 
            // planLabel
            // 
            planLabel.AutoSize = true;
            planLabel.Location = new System.Drawing.Point(434, 169);
            planLabel.Name = "planLabel";
            planLabel.Size = new System.Drawing.Size(104, 13);
            planLabel.TabIndex = 37;
            planLabel.Text = "Plan Contratado:";
            // 
            // direcciónEntregaLabel
            // 
            direcciónEntregaLabel.AutoSize = true;
            direcciónEntregaLabel.Location = new System.Drawing.Point(22, 139);
            direcciónEntregaLabel.Name = "direcciónEntregaLabel";
            direcciónEntregaLabel.Size = new System.Drawing.Size(131, 13);
            direcciónEntregaLabel.TabIndex = 38;
            direcciónEntregaLabel.Text = "Dirección de Entrega:";
            // 
            // cUITLabel
            // 
            cUITLabel.AutoSize = true;
            cUITLabel.Location = new System.Drawing.Point(110, 165);
            cUITLabel.Name = "cUITLabel";
            cUITLabel.Size = new System.Drawing.Size(41, 13);
            cUITLabel.TabIndex = 39;
            cUITLabel.Text = "CUIT:";
            // 
            // iVALabel
            // 
            iVALabel.AutoSize = true;
            iVALabel.Location = new System.Drawing.Point(954, 251);
            iVALabel.Name = "iVALabel";
            iVALabel.Size = new System.Drawing.Size(33, 13);
            iVALabel.TabIndex = 39;
            iVALabel.Text = "IVA:";
            // 
            // emailprincipalLabel
            // 
            emailprincipalLabel.AutoSize = true;
            emailprincipalLabel.Location = new System.Drawing.Point(376, 113);
            emailprincipalLabel.Name = "emailprincipalLabel";
            emailprincipalLabel.Size = new System.Drawing.Size(43, 13);
            emailprincipalLabel.TabIndex = 43;
            emailprincipalLabel.Text = "Email:";
            // 
            // emailcont1Label
            // 
            emailcont1Label.AutoSize = true;
            emailcont1Label.Location = new System.Drawing.Point(281, 49);
            emailcont1Label.Name = "emailcont1Label";
            emailcont1Label.Size = new System.Drawing.Size(43, 13);
            emailcont1Label.TabIndex = 15;
            emailcont1Label.Text = "Email:";
            // 
            // emailcont2Label
            // 
            emailcont2Label.AutoSize = true;
            emailcont2Label.Location = new System.Drawing.Point(281, 49);
            emailcont2Label.Name = "emailcont2Label";
            emailcont2Label.Size = new System.Drawing.Size(43, 13);
            emailcont2Label.TabIndex = 15;
            emailcont2Label.Text = "Email:";
            // 
            // emailcont3Label
            // 
            emailcont3Label.AutoSize = true;
            emailcont3Label.Location = new System.Drawing.Point(281, 49);
            emailcont3Label.Name = "emailcont3Label";
            emailcont3Label.Size = new System.Drawing.Size(43, 13);
            emailcont3Label.TabIndex = 15;
            emailcont3Label.Text = "Email:";
            // 
            // emailcont4Label
            // 
            emailcont4Label.AutoSize = true;
            emailcont4Label.Location = new System.Drawing.Point(285, 49);
            emailcont4Label.Name = "emailcont4Label";
            emailcont4Label.Size = new System.Drawing.Size(43, 13);
            emailcont4Label.TabIndex = 15;
            emailcont4Label.Text = "Email:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(emailcont1Label);
            this.groupBox3.Controls.Add(this.emailcont1TextBox);
            this.groupBox3.Controls.Add(nombrecont1Label);
            this.groupBox3.Controls.Add(this.telcont1TextBox);
            this.groupBox3.Controls.Add(telcont1Label);
            this.groupBox3.Controls.Add(this.nombrecont1TextBox);
            this.groupBox3.Location = new System.Drawing.Point(14, 223);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(643, 100);
            this.groupBox3.TabIndex = 10;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Contacto 1";
            // 
            // emailcont1TextBox
            // 
            this.emailcont1TextBox.BackColor = System.Drawing.Color.Wheat;
            this.emailcont1TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clientesBindingSource, "emailcont1", true));
            this.emailcont1TextBox.Location = new System.Drawing.Point(329, 46);
            this.emailcont1TextBox.Name = "emailcont1TextBox";
            this.emailcont1TextBox.Size = new System.Drawing.Size(290, 21);
            this.emailcont1TextBox.TabIndex = 16;
            // 
            // clientesBindingSource
            // 
            this.clientesBindingSource.DataMember = "clientes";
            this.clientesBindingSource.DataSource = this.archivoDataSet;
            // 
            // archivoDataSet
            // 
            this.archivoDataSet.DataSetName = "archivoDataSet";
            this.archivoDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // telcont1TextBox
            // 
            this.telcont1TextBox.BackColor = System.Drawing.Color.Wheat;
            this.telcont1TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clientesBindingSource, "Telcont1", true));
            this.telcont1TextBox.Location = new System.Drawing.Point(76, 46);
            this.telcont1TextBox.Name = "telcont1TextBox";
            this.telcont1TextBox.Size = new System.Drawing.Size(198, 21);
            this.telcont1TextBox.TabIndex = 1;
            // 
            // nombrecont1TextBox
            // 
            this.nombrecont1TextBox.BackColor = System.Drawing.Color.Wheat;
            this.nombrecont1TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clientesBindingSource, "Nombrecont1", true));
            this.nombrecont1TextBox.Location = new System.Drawing.Point(76, 20);
            this.nombrecont1TextBox.Name = "nombrecont1TextBox";
            this.nombrecont1TextBox.Size = new System.Drawing.Size(543, 21);
            this.nombrecont1TextBox.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(emailcont2Label);
            this.groupBox1.Controls.Add(this.emailcont2TextBox);
            this.groupBox1.Controls.Add(label1);
            this.groupBox1.Controls.Add(this.telcont2TextBox);
            this.groupBox1.Controls.Add(label2);
            this.groupBox1.Controls.Add(this.nombrecont2TextBox);
            this.groupBox1.Location = new System.Drawing.Point(14, 329);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(643, 100);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Contacto 2";
            // 
            // emailcont2TextBox
            // 
            this.emailcont2TextBox.BackColor = System.Drawing.Color.Wheat;
            this.emailcont2TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clientesBindingSource, "emailcont2", true));
            this.emailcont2TextBox.Location = new System.Drawing.Point(329, 46);
            this.emailcont2TextBox.Name = "emailcont2TextBox";
            this.emailcont2TextBox.Size = new System.Drawing.Size(290, 21);
            this.emailcont2TextBox.TabIndex = 16;
            // 
            // telcont2TextBox
            // 
            this.telcont2TextBox.BackColor = System.Drawing.Color.Wheat;
            this.telcont2TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clientesBindingSource, "Telcont2", true));
            this.telcont2TextBox.Location = new System.Drawing.Point(76, 46);
            this.telcont2TextBox.Name = "telcont2TextBox";
            this.telcont2TextBox.Size = new System.Drawing.Size(198, 21);
            this.telcont2TextBox.TabIndex = 1;
            // 
            // nombrecont2TextBox
            // 
            this.nombrecont2TextBox.BackColor = System.Drawing.Color.Wheat;
            this.nombrecont2TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clientesBindingSource, "Nombrecont2", true));
            this.nombrecont2TextBox.Location = new System.Drawing.Point(76, 20);
            this.nombrecont2TextBox.Name = "nombrecont2TextBox";
            this.nombrecont2TextBox.Size = new System.Drawing.Size(543, 21);
            this.nombrecont2TextBox.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(emailcont3Label);
            this.groupBox2.Controls.Add(this.emailcont3TextBox);
            this.groupBox2.Controls.Add(label3);
            this.groupBox2.Controls.Add(this.telcont3TextBox);
            this.groupBox2.Controls.Add(label4);
            this.groupBox2.Controls.Add(this.nombrecont3TextBox);
            this.groupBox2.Location = new System.Drawing.Point(14, 435);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(643, 100);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Contacto 3";
            // 
            // emailcont3TextBox
            // 
            this.emailcont3TextBox.BackColor = System.Drawing.Color.Wheat;
            this.emailcont3TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clientesBindingSource, "emailcont3", true));
            this.emailcont3TextBox.Location = new System.Drawing.Point(329, 46);
            this.emailcont3TextBox.Name = "emailcont3TextBox";
            this.emailcont3TextBox.Size = new System.Drawing.Size(290, 21);
            this.emailcont3TextBox.TabIndex = 16;
            // 
            // telcont3TextBox
            // 
            this.telcont3TextBox.BackColor = System.Drawing.Color.Wheat;
            this.telcont3TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clientesBindingSource, "Telcont3", true));
            this.telcont3TextBox.Location = new System.Drawing.Point(76, 46);
            this.telcont3TextBox.Name = "telcont3TextBox";
            this.telcont3TextBox.Size = new System.Drawing.Size(198, 21);
            this.telcont3TextBox.TabIndex = 1;
            // 
            // nombrecont3TextBox
            // 
            this.nombrecont3TextBox.BackColor = System.Drawing.Color.Wheat;
            this.nombrecont3TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clientesBindingSource, "Nombrecont3", true));
            this.nombrecont3TextBox.Location = new System.Drawing.Point(76, 20);
            this.nombrecont3TextBox.Name = "nombrecont3TextBox";
            this.nombrecont3TextBox.Size = new System.Drawing.Size(543, 21);
            this.nombrecont3TextBox.TabIndex = 0;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(emailcont4Label);
            this.groupBox4.Controls.Add(this.emailcont4TextBox);
            this.groupBox4.Controls.Add(label5);
            this.groupBox4.Controls.Add(this.telcont4TextBox);
            this.groupBox4.Controls.Add(label6);
            this.groupBox4.Controls.Add(this.nombrecont4TextBox);
            this.groupBox4.Location = new System.Drawing.Point(14, 541);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(646, 100);
            this.groupBox4.TabIndex = 13;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Contacto 4";
            // 
            // emailcont4TextBox
            // 
            this.emailcont4TextBox.BackColor = System.Drawing.Color.Wheat;
            this.emailcont4TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clientesBindingSource, "emailcont4", true));
            this.emailcont4TextBox.Location = new System.Drawing.Point(332, 46);
            this.emailcont4TextBox.Name = "emailcont4TextBox";
            this.emailcont4TextBox.Size = new System.Drawing.Size(290, 21);
            this.emailcont4TextBox.TabIndex = 16;
            // 
            // telcont4TextBox
            // 
            this.telcont4TextBox.BackColor = System.Drawing.Color.Wheat;
            this.telcont4TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clientesBindingSource, "Telcont4", true));
            this.telcont4TextBox.Location = new System.Drawing.Point(79, 46);
            this.telcont4TextBox.Name = "telcont4TextBox";
            this.telcont4TextBox.Size = new System.Drawing.Size(198, 21);
            this.telcont4TextBox.TabIndex = 1;
            // 
            // nombrecont4TextBox
            // 
            this.nombrecont4TextBox.BackColor = System.Drawing.Color.Wheat;
            this.nombrecont4TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clientesBindingSource, "Nombrecont4", true));
            this.nombrecont4TextBox.Location = new System.Drawing.Point(79, 20);
            this.nombrecont4TextBox.Name = "nombrecont4TextBox";
            this.nombrecont4TextBox.Size = new System.Drawing.Size(543, 21);
            this.nombrecont4TextBox.TabIndex = 0;
            // 
            // btnGuardar
            // 
            this.btnGuardar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnGuardar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardar.Location = new System.Drawing.Point(239, 647);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(204, 54);
            this.btnGuardar.TabIndex = 14;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.UseVisualStyleBackColor = false;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // nombre_EmpresaTextBox
            // 
            this.nombre_EmpresaTextBox.BackColor = System.Drawing.Color.Wheat;
            this.nombre_EmpresaTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clientesBindingSource, "NombreEmpresa", true));
            this.nombre_EmpresaTextBox.Location = new System.Drawing.Point(157, 32);
            this.nombre_EmpresaTextBox.Name = "nombre_EmpresaTextBox";
            this.nombre_EmpresaTextBox.Size = new System.Drawing.Size(496, 21);
            this.nombre_EmpresaTextBox.TabIndex = 0;
            // 
            // direcciónTextBox
            // 
            this.direcciónTextBox.BackColor = System.Drawing.Color.Wheat;
            this.direcciónTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clientesBindingSource, "Direccion", true));
            this.direcciónTextBox.Location = new System.Drawing.Point(157, 58);
            this.direcciónTextBox.Name = "direcciónTextBox";
            this.direcciónTextBox.Size = new System.Drawing.Size(496, 21);
            this.direcciónTextBox.TabIndex = 1;
            // 
            // localidadTextBox
            // 
            this.localidadTextBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.localidadTextBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.localidadTextBox.BackColor = System.Drawing.Color.Wheat;
            this.localidadTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clientesBindingSource, "Localidad", true));
            this.localidadTextBox.Location = new System.Drawing.Point(157, 84);
            this.localidadTextBox.Name = "localidadTextBox";
            this.localidadTextBox.Size = new System.Drawing.Size(341, 21);
            this.localidadTextBox.TabIndex = 2;
            // 
            // cPTextBox
            // 
            this.cPTextBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cPTextBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.cPTextBox.BackColor = System.Drawing.Color.Wheat;
            this.cPTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clientesBindingSource, "CP", true));
            this.cPTextBox.Location = new System.Drawing.Point(541, 84);
            this.cPTextBox.Name = "cPTextBox";
            this.cPTextBox.Size = new System.Drawing.Size(112, 21);
            this.cPTextBox.TabIndex = 3;
            // 
            // telefonoTextBox
            // 
            this.telefonoTextBox.BackColor = System.Drawing.Color.Wheat;
            this.telefonoTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clientesBindingSource, "Telefono", true));
            this.telefonoTextBox.Location = new System.Drawing.Point(157, 110);
            this.telefonoTextBox.Name = "telefonoTextBox";
            this.telefonoTextBox.Size = new System.Drawing.Size(206, 21);
            this.telefonoTextBox.TabIndex = 4;
            // 
            // lbLocalidad
            // 
            this.lbLocalidad.FormattingEnabled = true;
            this.lbLocalidad.Location = new System.Drawing.Point(772, 121);
            this.lbLocalidad.Name = "lbLocalidad";
            this.lbLocalidad.Size = new System.Drawing.Size(139, 95);
            this.lbLocalidad.Sorted = true;
            this.lbLocalidad.TabIndex = 36;
            this.lbLocalidad.Visible = false;
            // 
            // lbCP
            // 
            this.lbCP.FormattingEnabled = true;
            this.lbCP.Location = new System.Drawing.Point(772, 238);
            this.lbCP.Name = "lbCP";
            this.lbCP.Size = new System.Drawing.Size(139, 95);
            this.lbCP.Sorted = true;
            this.lbCP.TabIndex = 37;
            this.lbCP.Visible = false;
            // 
            // planTextBox
            // 
            this.planTextBox.BackColor = System.Drawing.Color.Wheat;
            this.planTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clientesBindingSource, "Plan", true));
            this.planTextBox.Location = new System.Drawing.Point(538, 166);
            this.planTextBox.Name = "planTextBox";
            this.planTextBox.Size = new System.Drawing.Size(116, 21);
            this.planTextBox.TabIndex = 8;
            // 
            // direcciónEntregaTextBox
            // 
            this.direcciónEntregaTextBox.BackColor = System.Drawing.Color.Wheat;
            this.direcciónEntregaTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clientesBindingSource, "DireccionEntrega", true));
            this.direcciónEntregaTextBox.Location = new System.Drawing.Point(157, 136);
            this.direcciónEntregaTextBox.Name = "direcciónEntregaTextBox";
            this.direcciónEntregaTextBox.Size = new System.Drawing.Size(496, 21);
            this.direcciónEntregaTextBox.TabIndex = 6;
            // 
            // cUITTextBox
            // 
            this.cUITTextBox.BackColor = System.Drawing.Color.Wheat;
            this.cUITTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clientesBindingSource, "CUIT", true));
            this.cUITTextBox.Location = new System.Drawing.Point(157, 162);
            this.cUITTextBox.Name = "cUITTextBox";
            this.cUITTextBox.Size = new System.Drawing.Size(116, 21);
            this.cUITTextBox.TabIndex = 7;
            // 
            // iVATextBox
            // 
            this.iVATextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clientesBindingSource, "IVA", true));
            this.iVATextBox.Location = new System.Drawing.Point(993, 248);
            this.iVATextBox.Name = "iVATextBox";
            this.iVATextBox.Size = new System.Drawing.Size(116, 21);
            this.iVATextBox.TabIndex = 40;
            // 
            // cmbIva
            // 
            this.cmbIva.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbIva.FormattingEnabled = true;
            this.cmbIva.Items.AddRange(new object[] {
            "Responsable Inscripto",
            "Monotributo",
            "Exento",
            "No Responsable",
            "Responsable no Inscripto",
            "Consumidor Final"});
            this.cmbIva.Location = new System.Drawing.Point(157, 188);
            this.cmbIva.Name = "cmbIva";
            this.cmbIva.Size = new System.Drawing.Size(206, 21);
            this.cmbIva.TabIndex = 9;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(119, 191);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(33, 13);
            this.label7.TabIndex = 42;
            this.label7.Text = "IVA:";
            // 
            // clientesTableAdapter
            // 
            this.clientesTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.cajasTableAdapter = null;
            this.tableAdapterManager.clientesTableAdapter = this.clientesTableAdapter;
            this.tableAdapterManager.operacionesTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = Archivo.archivoDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // clientesDataGridView
            // 
            this.clientesDataGridView.AutoGenerateColumns = false;
            this.clientesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.clientesDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn13,
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewTextBoxColumn15,
            this.dataGridViewTextBoxColumn16,
            this.dataGridViewTextBoxColumn17,
            this.dataGridViewTextBoxColumn18,
            this.dataGridViewTextBoxColumn19,
            this.dataGridViewTextBoxColumn20,
            this.dataGridViewTextBoxColumn21,
            this.dataGridViewTextBoxColumn22,
            this.dataGridViewTextBoxColumn23,
            this.dataGridViewTextBoxColumn24,
            this.dataGridViewTextBoxColumn25});
            this.clientesDataGridView.DataSource = this.clientesBindingSource;
            this.clientesDataGridView.Location = new System.Drawing.Point(731, 375);
            this.clientesDataGridView.Name = "clientesDataGridView";
            this.clientesDataGridView.Size = new System.Drawing.Size(350, 220);
            this.clientesDataGridView.TabIndex = 43;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Nocliente";
            this.dataGridViewTextBoxColumn1.HeaderText = "Nocliente";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "NombreEmpresa";
            this.dataGridViewTextBoxColumn2.HeaderText = "NombreEmpresa";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Direccion";
            this.dataGridViewTextBoxColumn3.HeaderText = "Direccion";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "Localidad";
            this.dataGridViewTextBoxColumn4.HeaderText = "Localidad";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "CP";
            this.dataGridViewTextBoxColumn5.HeaderText = "CP";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "Telefono";
            this.dataGridViewTextBoxColumn6.HeaderText = "Telefono";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "Nombrecont1";
            this.dataGridViewTextBoxColumn7.HeaderText = "Nombrecont1";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "Telcont1";
            this.dataGridViewTextBoxColumn8.HeaderText = "Telcont1";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "Nombrecont2";
            this.dataGridViewTextBoxColumn9.HeaderText = "Nombrecont2";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "Telcont2";
            this.dataGridViewTextBoxColumn10.HeaderText = "Telcont2";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "Nombrecont3";
            this.dataGridViewTextBoxColumn11.HeaderText = "Nombrecont3";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.DataPropertyName = "Telcont3";
            this.dataGridViewTextBoxColumn12.HeaderText = "Telcont3";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.DataPropertyName = "Nombrecont4";
            this.dataGridViewTextBoxColumn13.HeaderText = "Nombrecont4";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.DataPropertyName = "Telcont4";
            this.dataGridViewTextBoxColumn14.HeaderText = "Telcont4";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.DataPropertyName = "Plan";
            this.dataGridViewTextBoxColumn15.HeaderText = "Plan";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.DataPropertyName = "DireccionEntrega";
            this.dataGridViewTextBoxColumn16.HeaderText = "DireccionEntrega";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.DataPropertyName = "CUIT";
            this.dataGridViewTextBoxColumn17.HeaderText = "CUIT";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.DataPropertyName = "IVA";
            this.dataGridViewTextBoxColumn18.HeaderText = "IVA";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            // 
            // dataGridViewTextBoxColumn19
            // 
            this.dataGridViewTextBoxColumn19.DataPropertyName = "usuario";
            this.dataGridViewTextBoxColumn19.HeaderText = "usuario";
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            // 
            // dataGridViewTextBoxColumn20
            // 
            this.dataGridViewTextBoxColumn20.DataPropertyName = "contrasena";
            this.dataGridViewTextBoxColumn20.HeaderText = "contrasena";
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            // 
            // dataGridViewTextBoxColumn21
            // 
            this.dataGridViewTextBoxColumn21.DataPropertyName = "emailprincipal";
            this.dataGridViewTextBoxColumn21.HeaderText = "emailprincipal";
            this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            // 
            // dataGridViewTextBoxColumn22
            // 
            this.dataGridViewTextBoxColumn22.DataPropertyName = "emailcont1";
            this.dataGridViewTextBoxColumn22.HeaderText = "emailcont1";
            this.dataGridViewTextBoxColumn22.Name = "dataGridViewTextBoxColumn22";
            // 
            // dataGridViewTextBoxColumn23
            // 
            this.dataGridViewTextBoxColumn23.DataPropertyName = "emailcont2";
            this.dataGridViewTextBoxColumn23.HeaderText = "emailcont2";
            this.dataGridViewTextBoxColumn23.Name = "dataGridViewTextBoxColumn23";
            // 
            // dataGridViewTextBoxColumn24
            // 
            this.dataGridViewTextBoxColumn24.DataPropertyName = "emailcont3";
            this.dataGridViewTextBoxColumn24.HeaderText = "emailcont3";
            this.dataGridViewTextBoxColumn24.Name = "dataGridViewTextBoxColumn24";
            // 
            // dataGridViewTextBoxColumn25
            // 
            this.dataGridViewTextBoxColumn25.DataPropertyName = "emailcont4";
            this.dataGridViewTextBoxColumn25.HeaderText = "emailcont4";
            this.dataGridViewTextBoxColumn25.Name = "dataGridViewTextBoxColumn25";
            // 
            // emailprincipalTextBox
            // 
            this.emailprincipalTextBox.BackColor = System.Drawing.Color.Wheat;
            this.emailprincipalTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clientesBindingSource, "emailprincipal", true));
            this.emailprincipalTextBox.Location = new System.Drawing.Point(423, 110);
            this.emailprincipalTextBox.Name = "emailprincipalTextBox";
            this.emailprincipalTextBox.Size = new System.Drawing.Size(230, 21);
            this.emailprincipalTextBox.TabIndex = 5;
            // 
            // noclienteTextBox
            // 
            this.noclienteTextBox.BackColor = System.Drawing.Color.Wheat;
            this.noclienteTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clientesBindingSource, "Nocliente", true));
            this.noclienteTextBox.Location = new System.Drawing.Point(157, 6);
            this.noclienteTextBox.Name = "noclienteTextBox";
            this.noclienteTextBox.Size = new System.Drawing.Size(116, 21);
            this.noclienteTextBox.TabIndex = 44;
            // 
            // hostingCheckBox
            // 
            this.hostingCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.clientesBindingSource, "hosting", true));
            this.hostingCheckBox.Location = new System.Drawing.Point(515, 192);
            this.hostingCheckBox.Name = "hostingCheckBox";
            this.hostingCheckBox.Size = new System.Drawing.Size(142, 24);
            this.hostingCheckBox.TabIndex = 45;
            this.hostingCheckBox.Text = "Servicio de hosting";
            this.hostingCheckBox.UseVisualStyleBackColor = true;
            // 
            // frmNuevoCliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SandyBrown;
            this.ClientSize = new System.Drawing.Size(1129, 721);
            this.Controls.Add(this.hostingCheckBox);
            this.Controls.Add(this.noclienteTextBox);
            this.Controls.Add(emailprincipalLabel);
            this.Controls.Add(this.emailprincipalTextBox);
            this.Controls.Add(this.clientesDataGridView);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.cmbIva);
            this.Controls.Add(iVALabel);
            this.Controls.Add(this.iVATextBox);
            this.Controls.Add(cUITLabel);
            this.Controls.Add(this.cUITTextBox);
            this.Controls.Add(direcciónEntregaLabel);
            this.Controls.Add(this.direcciónEntregaTextBox);
            this.Controls.Add(planLabel);
            this.Controls.Add(this.planTextBox);
            this.Controls.Add(this.lbCP);
            this.Controls.Add(this.lbLocalidad);
            this.Controls.Add(this.telefonoTextBox);
            this.Controls.Add(this.cPTextBox);
            this.Controls.Add(this.localidadTextBox);
            this.Controls.Add(this.direcciónTextBox);
            this.Controls.Add(this.nombre_EmpresaTextBox);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(telefonoLabel);
            this.Controls.Add(cPLabel);
            this.Controls.Add(localidadLabel);
            this.Controls.Add(direcciónLabel);
            this.Controls.Add(nombre_EmpresaLabel);
            this.Controls.Add(noclienteLabel);
            this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "frmNuevoCliente";
            this.Text = "Nuevo Cliente";
            this.Load += new System.EventHandler(this.frmNuevoCliente_Load);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.clientesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.archivoDataSet)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.clientesDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.TextBox telcont1TextBox;
        private System.Windows.Forms.TextBox nombrecont1TextBox;
        private System.Windows.Forms.TextBox telcont2TextBox;
        private System.Windows.Forms.TextBox nombrecont2TextBox;
        private System.Windows.Forms.TextBox telcont3TextBox;
        private System.Windows.Forms.TextBox nombrecont3TextBox;
        private System.Windows.Forms.TextBox telcont4TextBox;
        private System.Windows.Forms.TextBox nombrecont4TextBox;
        private System.Windows.Forms.TextBox nombre_EmpresaTextBox;
        private System.Windows.Forms.TextBox direcciónTextBox;
        private System.Windows.Forms.TextBox localidadTextBox;
        private System.Windows.Forms.TextBox cPTextBox;
        private System.Windows.Forms.TextBox telefonoTextBox;
        private System.Windows.Forms.ListBox lbLocalidad;
        private System.Windows.Forms.ListBox lbCP;
        private System.Windows.Forms.TextBox planTextBox;
        private System.Windows.Forms.TextBox direcciónEntregaTextBox;
        private System.Windows.Forms.TextBox cUITTextBox;
        private System.Windows.Forms.TextBox iVATextBox;
        private System.Windows.Forms.ComboBox cmbIva;
        private System.Windows.Forms.Label label7;
        private archivoDataSet archivoDataSet;
        private System.Windows.Forms.BindingSource clientesBindingSource;
        private archivoDataSetTableAdapters.clientesTableAdapter clientesTableAdapter;
        private archivoDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.TextBox emailcont1TextBox;
        private System.Windows.Forms.TextBox emailcont2TextBox;
        private System.Windows.Forms.TextBox emailcont3TextBox;
        private System.Windows.Forms.TextBox emailcont4TextBox;
        private System.Windows.Forms.DataGridView clientesDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn23;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn24;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn25;
        private System.Windows.Forms.TextBox emailprincipalTextBox;
        private System.Windows.Forms.TextBox noclienteTextBox;
        private System.Windows.Forms.CheckBox hostingCheckBox;
    }
}