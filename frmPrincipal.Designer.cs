﻿namespace Archivo
{
    partial class frmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPrincipal));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.clientesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nuevoClienteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultaYModificacionDeClientesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cajasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ingresoDeCajasADepósitoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generaciónDeCódigoSinFechaDeIngresoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ingresoDeCajasGeneradasSinFechaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.búsquedaDeDocumentoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.retiroDeCajasParaConsultaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reingresoDeCajasConsultadasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bajaDeCajasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultaDeDatosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.impresiónDeEtiquetasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.impresiónDePlanillaDeCajaDepositadaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resumenDeOperacionesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listadoDeCajasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imprimirDetallesDeBúsquedaDeDocumentosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configuraciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pctlogo = new System.Windows.Forms.PictureBox();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pctlogo)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clientesToolStripMenuItem,
            this.cajasToolStripMenuItem,
            this.configuraciónToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(983, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // clientesToolStripMenuItem
            // 
            this.clientesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nuevoClienteToolStripMenuItem,
            this.consultaYModificacionDeClientesToolStripMenuItem});
            this.clientesToolStripMenuItem.Name = "clientesToolStripMenuItem";
            this.clientesToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.clientesToolStripMenuItem.Text = "Clientes";
            // 
            // nuevoClienteToolStripMenuItem
            // 
            this.nuevoClienteToolStripMenuItem.Name = "nuevoClienteToolStripMenuItem";
            this.nuevoClienteToolStripMenuItem.Size = new System.Drawing.Size(264, 22);
            this.nuevoClienteToolStripMenuItem.Text = "Nuevo Cliente";
            this.nuevoClienteToolStripMenuItem.Click += new System.EventHandler(this.nuevoClienteToolStripMenuItem_Click);
            // 
            // consultaYModificacionDeClientesToolStripMenuItem
            // 
            this.consultaYModificacionDeClientesToolStripMenuItem.Name = "consultaYModificacionDeClientesToolStripMenuItem";
            this.consultaYModificacionDeClientesToolStripMenuItem.Size = new System.Drawing.Size(264, 22);
            this.consultaYModificacionDeClientesToolStripMenuItem.Text = "Consulta y modificación de Clientes";
            this.consultaYModificacionDeClientesToolStripMenuItem.Click += new System.EventHandler(this.consultaYModificacionDeClientesToolStripMenuItem_Click);
            // 
            // cajasToolStripMenuItem
            // 
            this.cajasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ingresoDeCajasADepósitoToolStripMenuItem,
            this.generaciónDeCódigoSinFechaDeIngresoToolStripMenuItem,
            this.ingresoDeCajasGeneradasSinFechaToolStripMenuItem,
            this.toolStripMenuItem1,
            this.búsquedaDeDocumentoToolStripMenuItem,
            this.retiroDeCajasParaConsultaToolStripMenuItem,
            this.reingresoDeCajasConsultadasToolStripMenuItem,
            this.bajaDeCajasToolStripMenuItem,
            this.consultaDeDatosToolStripMenuItem,
            this.impresiónDeEtiquetasToolStripMenuItem,
            this.impresiónDePlanillaDeCajaDepositadaToolStripMenuItem,
            this.resumenDeOperacionesToolStripMenuItem,
            this.listadoDeCajasToolStripMenuItem,
            this.imprimirDetallesDeBúsquedaDeDocumentosToolStripMenuItem});
            this.cajasToolStripMenuItem.Name = "cajasToolStripMenuItem";
            this.cajasToolStripMenuItem.Size = new System.Drawing.Size(47, 20);
            this.cajasToolStripMenuItem.Text = "Cajas";
            // 
            // ingresoDeCajasADepósitoToolStripMenuItem
            // 
            this.ingresoDeCajasADepósitoToolStripMenuItem.Name = "ingresoDeCajasADepósitoToolStripMenuItem";
            this.ingresoDeCajasADepósitoToolStripMenuItem.Size = new System.Drawing.Size(330, 22);
            this.ingresoDeCajasADepósitoToolStripMenuItem.Text = "Ingreso de Cajas a Depósito con Fecha";
            this.ingresoDeCajasADepósitoToolStripMenuItem.Click += new System.EventHandler(this.ingresoDeCajasADepósitoToolStripMenuItem_Click);
            // 
            // generaciónDeCódigoSinFechaDeIngresoToolStripMenuItem
            // 
            this.generaciónDeCódigoSinFechaDeIngresoToolStripMenuItem.Name = "generaciónDeCódigoSinFechaDeIngresoToolStripMenuItem";
            this.generaciónDeCódigoSinFechaDeIngresoToolStripMenuItem.Size = new System.Drawing.Size(330, 22);
            this.generaciónDeCódigoSinFechaDeIngresoToolStripMenuItem.Text = "Generación de Código Sin Fecha de Ingreso";
            this.generaciónDeCódigoSinFechaDeIngresoToolStripMenuItem.Click += new System.EventHandler(this.generaciónDeCódigoSinFechaDeIngresoToolStripMenuItem_Click);
            // 
            // ingresoDeCajasGeneradasSinFechaToolStripMenuItem
            // 
            this.ingresoDeCajasGeneradasSinFechaToolStripMenuItem.Name = "ingresoDeCajasGeneradasSinFechaToolStripMenuItem";
            this.ingresoDeCajasGeneradasSinFechaToolStripMenuItem.Size = new System.Drawing.Size(330, 22);
            this.ingresoDeCajasGeneradasSinFechaToolStripMenuItem.Text = "Ingreso de Cajas Generadas Sin Fecha a Depósito";
            this.ingresoDeCajasGeneradasSinFechaToolStripMenuItem.Click += new System.EventHandler(this.ingresoDeCajasGeneradasSinFechaToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(330, 22);
            this.toolStripMenuItem1.Text = "Entrega de Cajas Vacías";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // búsquedaDeDocumentoToolStripMenuItem
            // 
            this.búsquedaDeDocumentoToolStripMenuItem.Name = "búsquedaDeDocumentoToolStripMenuItem";
            this.búsquedaDeDocumentoToolStripMenuItem.Size = new System.Drawing.Size(330, 22);
            this.búsquedaDeDocumentoToolStripMenuItem.Text = "Búsqueda de documento";
            this.búsquedaDeDocumentoToolStripMenuItem.Click += new System.EventHandler(this.búsquedaDeDocumentoToolStripMenuItem_Click);
            // 
            // retiroDeCajasParaConsultaToolStripMenuItem
            // 
            this.retiroDeCajasParaConsultaToolStripMenuItem.Name = "retiroDeCajasParaConsultaToolStripMenuItem";
            this.retiroDeCajasParaConsultaToolStripMenuItem.Size = new System.Drawing.Size(330, 22);
            this.retiroDeCajasParaConsultaToolStripMenuItem.Text = "Retiro de Cajas para Consulta";
            this.retiroDeCajasParaConsultaToolStripMenuItem.Click += new System.EventHandler(this.retiroDeCajasParaConsultaToolStripMenuItem_Click);
            // 
            // reingresoDeCajasConsultadasToolStripMenuItem
            // 
            this.reingresoDeCajasConsultadasToolStripMenuItem.Name = "reingresoDeCajasConsultadasToolStripMenuItem";
            this.reingresoDeCajasConsultadasToolStripMenuItem.Size = new System.Drawing.Size(330, 22);
            this.reingresoDeCajasConsultadasToolStripMenuItem.Text = "Reingreso de Cajas Consultadas ";
            this.reingresoDeCajasConsultadasToolStripMenuItem.Click += new System.EventHandler(this.reingresoDeCajasConsultadasToolStripMenuItem_Click);
            // 
            // bajaDeCajasToolStripMenuItem
            // 
            this.bajaDeCajasToolStripMenuItem.Name = "bajaDeCajasToolStripMenuItem";
            this.bajaDeCajasToolStripMenuItem.Size = new System.Drawing.Size(330, 22);
            this.bajaDeCajasToolStripMenuItem.Text = "Baja de Cajas";
            this.bajaDeCajasToolStripMenuItem.Click += new System.EventHandler(this.bajaDeCajasToolStripMenuItem_Click);
            // 
            // consultaDeDatosToolStripMenuItem
            // 
            this.consultaDeDatosToolStripMenuItem.Name = "consultaDeDatosToolStripMenuItem";
            this.consultaDeDatosToolStripMenuItem.Size = new System.Drawing.Size(330, 22);
            this.consultaDeDatosToolStripMenuItem.Text = "Ver Datos";
            this.consultaDeDatosToolStripMenuItem.Click += new System.EventHandler(this.consultaDeDatosToolStripMenuItem_Click);
            // 
            // impresiónDeEtiquetasToolStripMenuItem
            // 
            this.impresiónDeEtiquetasToolStripMenuItem.Name = "impresiónDeEtiquetasToolStripMenuItem";
            this.impresiónDeEtiquetasToolStripMenuItem.Size = new System.Drawing.Size(330, 22);
            this.impresiónDeEtiquetasToolStripMenuItem.Text = "Impresión de Etiquetas";
            this.impresiónDeEtiquetasToolStripMenuItem.Click += new System.EventHandler(this.impresiónDeEtiquetasToolStripMenuItem_Click);
            // 
            // impresiónDePlanillaDeCajaDepositadaToolStripMenuItem
            // 
            this.impresiónDePlanillaDeCajaDepositadaToolStripMenuItem.Name = "impresiónDePlanillaDeCajaDepositadaToolStripMenuItem";
            this.impresiónDePlanillaDeCajaDepositadaToolStripMenuItem.Size = new System.Drawing.Size(330, 22);
            this.impresiónDePlanillaDeCajaDepositadaToolStripMenuItem.Text = "Reimpresión de Planillas";
            this.impresiónDePlanillaDeCajaDepositadaToolStripMenuItem.Click += new System.EventHandler(this.impresiónDePlanillaDeCajaDepositadaToolStripMenuItem_Click);
            // 
            // resumenDeOperacionesToolStripMenuItem
            // 
            this.resumenDeOperacionesToolStripMenuItem.Name = "resumenDeOperacionesToolStripMenuItem";
            this.resumenDeOperacionesToolStripMenuItem.Size = new System.Drawing.Size(330, 22);
            this.resumenDeOperacionesToolStripMenuItem.Text = "Resumen de Operaciones";
            this.resumenDeOperacionesToolStripMenuItem.Click += new System.EventHandler(this.resumenDeOperacionesToolStripMenuItem_Click);
            // 
            // listadoDeCajasToolStripMenuItem
            // 
            this.listadoDeCajasToolStripMenuItem.Name = "listadoDeCajasToolStripMenuItem";
            this.listadoDeCajasToolStripMenuItem.Size = new System.Drawing.Size(330, 22);
            this.listadoDeCajasToolStripMenuItem.Text = "Listado de cajas";
            this.listadoDeCajasToolStripMenuItem.Click += new System.EventHandler(this.listadoDeCajasToolStripMenuItem_Click);
            // 
            // imprimirDetallesDeBúsquedaDeDocumentosToolStripMenuItem
            // 
            this.imprimirDetallesDeBúsquedaDeDocumentosToolStripMenuItem.Name = "imprimirDetallesDeBúsquedaDeDocumentosToolStripMenuItem";
            this.imprimirDetallesDeBúsquedaDeDocumentosToolStripMenuItem.Size = new System.Drawing.Size(330, 22);
            this.imprimirDetallesDeBúsquedaDeDocumentosToolStripMenuItem.Text = "Imprimir detalles de búsqueda de documentos";
            this.imprimirDetallesDeBúsquedaDeDocumentosToolStripMenuItem.Click += new System.EventHandler(this.imprimirDetallesDeBúsquedaDeDocumentosToolStripMenuItem_Click);
            // 
            // configuraciónToolStripMenuItem
            // 
            this.configuraciónToolStripMenuItem.Name = "configuraciónToolStripMenuItem";
            this.configuraciónToolStripMenuItem.Size = new System.Drawing.Size(95, 20);
            this.configuraciónToolStripMenuItem.Text = "Configuración";
            this.configuraciónToolStripMenuItem.Click += new System.EventHandler(this.configuraciónToolStripMenuItem_Click);
            // 
            // pctlogo
            // 
            this.pctlogo.BackColor = System.Drawing.Color.White;
            this.pctlogo.Image = ((System.Drawing.Image)(resources.GetObject("pctlogo.Image")));
            this.pctlogo.Location = new System.Drawing.Point(12, 121);
            this.pctlogo.Margin = new System.Windows.Forms.Padding(10);
            this.pctlogo.Name = "pctlogo";
            this.pctlogo.Padding = new System.Windows.Forms.Padding(10);
            this.pctlogo.Size = new System.Drawing.Size(959, 334);
            this.pctlogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pctlogo.TabIndex = 1;
            this.pctlogo.TabStop = false;
            // 
            // frmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SandyBrown;
            this.ClientSize = new System.Drawing.Size(983, 555);
            this.Controls.Add(this.pctlogo);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmPrincipal";
            this.Text = "Archivo de Cajas";
            this.Load += new System.EventHandler(this.frmPrincipal_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pctlogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem clientesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nuevoClienteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultaYModificacionDeClientesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cajasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ingresoDeCajasADepósitoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configuraciónToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem retiroDeCajasParaConsultaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reingresoDeCajasConsultadasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bajaDeCajasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultaDeDatosToolStripMenuItem;
        public System.Windows.Forms.PictureBox pctlogo;
        private System.Windows.Forms.ToolStripMenuItem impresiónDeEtiquetasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem impresiónDePlanillaDeCajaDepositadaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resumenDeOperacionesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem generaciónDeCódigoSinFechaDeIngresoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ingresoDeCajasGeneradasSinFechaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listadoDeCajasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem búsquedaDeDocumentoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem imprimirDetallesDeBúsquedaDeDocumentosToolStripMenuItem;
    }
}

