﻿namespace Archivo
{
    partial class frmIngresoCajasSinFecha
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label contenidoLabel;
            System.Windows.Forms.Label fecha_ingresoLabel;
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnBuscaCodigo = new System.Windows.Forms.Button();
            this.tbCodigo = new System.Windows.Forms.TextBox();
            this.archivoDataSet = new Archivo.archivoDataSet();
            this.cajasBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cajasTableAdapter = new Archivo.archivoDataSetTableAdapters.cajasTableAdapter();
            this.tableAdapterManager = new Archivo.archivoDataSetTableAdapters.TableAdapterManager();
            this.operacionesTableAdapter = new Archivo.archivoDataSetTableAdapters.operacionesTableAdapter();
            this.cajasDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Numdesde = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Numhasta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Fechadesde = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Fechahasta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tbcontenido = new System.Windows.Forms.TextBox();
            this.dtFechaingreso = new System.Windows.Forms.DateTimePicker();
            this.cbTraslado = new System.Windows.Forms.CheckBox();
            this.btnIngresar = new System.Windows.Forms.Button();
            this.operacionesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.operacionesDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.cbFecha = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.dtFechaHasta = new System.Windows.Forms.DateTimePicker();
            this.dtFechaDesde = new System.Windows.Forms.DateTimePicker();
            this.tbNoHasta = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tbNoDesde = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            contenidoLabel = new System.Windows.Forms.Label();
            fecha_ingresoLabel = new System.Windows.Forms.Label();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.archivoDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cajasBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cajasDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.operacionesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.operacionesDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // contenidoLabel
            // 
            contenidoLabel.AutoSize = true;
            contenidoLabel.Location = new System.Drawing.Point(57, 110);
            contenidoLabel.Name = "contenidoLabel";
            contenidoLabel.Size = new System.Drawing.Size(70, 13);
            contenidoLabel.TabIndex = 41;
            contenidoLabel.Text = "Contenido:";
            // 
            // fecha_ingresoLabel
            // 
            fecha_ingresoLabel.AutoSize = true;
            fecha_ingresoLabel.Location = new System.Drawing.Point(17, 357);
            fecha_ingresoLabel.Name = "fecha_ingresoLabel";
            fecha_ingresoLabel.Size = new System.Drawing.Size(109, 13);
            fecha_ingresoLabel.TabIndex = 51;
            fecha_ingresoLabel.Text = "Fecha de ingreso:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnBuscaCodigo);
            this.groupBox2.Controls.Add(this.tbCodigo);
            this.groupBox2.Location = new System.Drawing.Point(14, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(978, 80);
            this.groupBox2.TabIndex = 40;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Buscar Código:";
            // 
            // btnBuscaCodigo
            // 
            this.btnBuscaCodigo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnBuscaCodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBuscaCodigo.Location = new System.Drawing.Point(694, 16);
            this.btnBuscaCodigo.Name = "btnBuscaCodigo";
            this.btnBuscaCodigo.Size = new System.Drawing.Size(254, 41);
            this.btnBuscaCodigo.TabIndex = 63;
            this.btnBuscaCodigo.Text = "Buscar";
            this.btnBuscaCodigo.UseVisualStyleBackColor = false;
            this.btnBuscaCodigo.Click += new System.EventHandler(this.btnBuscaCodigo_Click);
            // 
            // tbCodigo
            // 
            this.tbCodigo.BackColor = System.Drawing.Color.Wheat;
            this.tbCodigo.Location = new System.Drawing.Point(20, 28);
            this.tbCodigo.Name = "tbCodigo";
            this.tbCodigo.Size = new System.Drawing.Size(349, 21);
            this.tbCodigo.TabIndex = 0;
            this.tbCodigo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbCodigo_KeyDown);
            // 
            // archivoDataSet
            // 
            this.archivoDataSet.DataSetName = "archivoDataSet";
            this.archivoDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // cajasBindingSource
            // 
            this.cajasBindingSource.DataMember = "cajas";
            this.cajasBindingSource.DataSource = this.archivoDataSet;
            // 
            // cajasTableAdapter
            // 
            this.cajasTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.cajasTableAdapter = this.cajasTableAdapter;
            this.tableAdapterManager.clientesTableAdapter = null;
            this.tableAdapterManager.detallesoperacionTableAdapter = null;
            this.tableAdapterManager.operacionesTableAdapter = this.operacionesTableAdapter;
            this.tableAdapterManager.UpdateOrder = Archivo.archivoDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // operacionesTableAdapter
            // 
            this.operacionesTableAdapter.ClearBeforeFill = true;
            // 
            // cajasDataGridView
            // 
            this.cajasDataGridView.AllowUserToAddRows = false;
            this.cajasDataGridView.AutoGenerateColumns = false;
            this.cajasDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.cajasDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewCheckBoxColumn1,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.Numdesde,
            this.Numhasta,
            this.Fechadesde,
            this.Fechahasta});
            this.cajasDataGridView.DataSource = this.cajasBindingSource;
            this.cajasDataGridView.Location = new System.Drawing.Point(1050, 232);
            this.cajasDataGridView.Name = "cajasDataGridView";
            this.cajasDataGridView.Size = new System.Drawing.Size(350, 220);
            this.cajasDataGridView.TabIndex = 41;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Codigo";
            this.dataGridViewTextBoxColumn1.HeaderText = "Codigo";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Nocliente";
            this.dataGridViewTextBoxColumn2.HeaderText = "Nocliente";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Lote";
            this.dataGridViewTextBoxColumn3.HeaderText = "Lote";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "Fecha ingreso";
            this.dataGridViewTextBoxColumn4.HeaderText = "Fecha ingreso";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewCheckBoxColumn1
            // 
            this.dataGridViewCheckBoxColumn1.DataPropertyName = "Enconsulta";
            this.dataGridViewCheckBoxColumn1.HeaderText = "Enconsulta";
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "Contenido";
            this.dataGridViewTextBoxColumn5.HeaderText = "Contenido";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "Ubicacion";
            this.dataGridViewTextBoxColumn6.HeaderText = "Ubicacion";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "Nocaja";
            this.dataGridViewTextBoxColumn7.HeaderText = "Nocaja";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "Fechaconsulta";
            this.dataGridViewTextBoxColumn8.HeaderText = "Fechaconsulta";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "Codigoint";
            this.dataGridViewTextBoxColumn9.HeaderText = "Codigoint";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            // 
            // Numdesde
            // 
            this.Numdesde.DataPropertyName = "Numdesde";
            this.Numdesde.HeaderText = "Numdesde";
            this.Numdesde.Name = "Numdesde";
            // 
            // Numhasta
            // 
            this.Numhasta.DataPropertyName = "Numhasta";
            this.Numhasta.HeaderText = "Numhasta";
            this.Numhasta.Name = "Numhasta";
            // 
            // Fechadesde
            // 
            this.Fechadesde.DataPropertyName = "Fechadesde";
            this.Fechadesde.HeaderText = "Fechadesde";
            this.Fechadesde.Name = "Fechadesde";
            // 
            // Fechahasta
            // 
            this.Fechahasta.DataPropertyName = "Fechahasta";
            this.Fechahasta.HeaderText = "Fechahasta";
            this.Fechahasta.Name = "Fechahasta";
            // 
            // tbcontenido
            // 
            this.tbcontenido.BackColor = System.Drawing.Color.Wheat;
            this.tbcontenido.Location = new System.Drawing.Point(132, 107);
            this.tbcontenido.Multiline = true;
            this.tbcontenido.Name = "tbcontenido";
            this.tbcontenido.Size = new System.Drawing.Size(868, 179);
            this.tbcontenido.TabIndex = 50;
            // 
            // dtFechaingreso
            // 
            this.dtFechaingreso.Location = new System.Drawing.Point(132, 351);
            this.dtFechaingreso.Name = "dtFechaingreso";
            this.dtFechaingreso.Size = new System.Drawing.Size(271, 21);
            this.dtFechaingreso.TabIndex = 52;
            // 
            // cbTraslado
            // 
            this.cbTraslado.AutoSize = true;
            this.cbTraslado.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cbTraslado.Location = new System.Drawing.Point(409, 356);
            this.cbTraslado.Name = "cbTraslado";
            this.cbTraslado.Size = new System.Drawing.Size(147, 17);
            this.cbTraslado.TabIndex = 57;
            this.cbTraslado.Text = "Traslado a Domicilio:";
            this.cbTraslado.UseVisualStyleBackColor = true;
            // 
            // btnIngresar
            // 
            this.btnIngresar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnIngresar.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIngresar.Location = new System.Drawing.Point(330, 379);
            this.btnIngresar.Name = "btnIngresar";
            this.btnIngresar.Size = new System.Drawing.Size(371, 71);
            this.btnIngresar.TabIndex = 58;
            this.btnIngresar.Text = "GENERAR INGRESO";
            this.btnIngresar.UseVisualStyleBackColor = false;
            this.btnIngresar.Click += new System.EventHandler(this.btnIngresar_Click);
            // 
            // operacionesBindingSource
            // 
            this.operacionesBindingSource.DataMember = "operaciones";
            this.operacionesBindingSource.DataSource = this.archivoDataSet;
            // 
            // operacionesDataGridView
            // 
            this.operacionesDataGridView.AutoGenerateColumns = false;
            this.operacionesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.operacionesDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn13,
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewCheckBoxColumn2});
            this.operacionesDataGridView.DataSource = this.operacionesBindingSource;
            this.operacionesDataGridView.Location = new System.Drawing.Point(1050, 6);
            this.operacionesDataGridView.Name = "operacionesDataGridView";
            this.operacionesDataGridView.Size = new System.Drawing.Size(300, 220);
            this.operacionesDataGridView.TabIndex = 58;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "NoOp";
            this.dataGridViewTextBoxColumn10.HeaderText = "NoOp";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "Tipo";
            this.dataGridViewTextBoxColumn11.HeaderText = "Tipo";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.DataPropertyName = "Fecha";
            this.dataGridViewTextBoxColumn12.HeaderText = "Fecha";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.DataPropertyName = "NoCliente";
            this.dataGridViewTextBoxColumn13.HeaderText = "NoCliente";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.DataPropertyName = "Codigo";
            this.dataGridViewTextBoxColumn14.HeaderText = "Codigo";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            // 
            // dataGridViewCheckBoxColumn2
            // 
            this.dataGridViewCheckBoxColumn2.DataPropertyName = "Traslado";
            this.dataGridViewCheckBoxColumn2.HeaderText = "Traslado";
            this.dataGridViewCheckBoxColumn2.Name = "dataGridViewCheckBoxColumn2";
            // 
            // cbFecha
            // 
            this.cbFecha.AutoSize = true;
            this.cbFecha.Location = new System.Drawing.Point(25, 322);
            this.cbFecha.Name = "cbFecha";
            this.cbFecha.Size = new System.Drawing.Size(102, 17);
            this.cbFecha.TabIndex = 83;
            this.cbFecha.Text = "Fecha desde:";
            this.cbFecha.UseVisualStyleBackColor = true;
            this.cbFecha.CheckedChanged += new System.EventHandler(this.cbFecha_CheckedChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(372, 326);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(44, 13);
            this.label9.TabIndex = 82;
            this.label9.Text = "Hasta:";
            // 
            // dtFechaHasta
            // 
            this.dtFechaHasta.Enabled = false;
            this.dtFechaHasta.Location = new System.Drawing.Point(422, 320);
            this.dtFechaHasta.Name = "dtFechaHasta";
            this.dtFechaHasta.Size = new System.Drawing.Size(233, 21);
            this.dtFechaHasta.TabIndex = 81;
            // 
            // dtFechaDesde
            // 
            this.dtFechaDesde.Enabled = false;
            this.dtFechaDesde.Location = new System.Drawing.Point(133, 320);
            this.dtFechaDesde.Name = "dtFechaDesde";
            this.dtFechaDesde.Size = new System.Drawing.Size(233, 21);
            this.dtFechaDesde.TabIndex = 80;
            // 
            // tbNoHasta
            // 
            this.tbNoHasta.BackColor = System.Drawing.Color.Wheat;
            this.tbNoHasta.Location = new System.Drawing.Point(337, 293);
            this.tbNoHasta.Name = "tbNoHasta";
            this.tbNoHasta.Size = new System.Drawing.Size(130, 21);
            this.tbNoHasta.TabIndex = 79;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(269, 296);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 13);
            this.label7.TabIndex = 78;
            this.label7.Text = "Hasta Nº:";
            // 
            // tbNoDesde
            // 
            this.tbNoDesde.BackColor = System.Drawing.Color.Wheat;
            this.tbNoDesde.Location = new System.Drawing.Point(133, 293);
            this.tbNoDesde.Name = "tbNoDesde";
            this.tbNoDesde.Size = new System.Drawing.Size(130, 21);
            this.tbNoDesde.TabIndex = 77;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(57, 296);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(66, 13);
            this.label6.TabIndex = 76;
            this.label6.Text = "Desde Nº:";
            // 
            // frmIngresoCajasSinFecha
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SandyBrown;
            this.ClientSize = new System.Drawing.Size(1008, 461);
            this.Controls.Add(this.cbFecha);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.dtFechaHasta);
            this.Controls.Add(this.dtFechaDesde);
            this.Controls.Add(this.tbNoHasta);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.tbNoDesde);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.operacionesDataGridView);
            this.Controls.Add(this.btnIngresar);
            this.Controls.Add(this.cbTraslado);
            this.Controls.Add(this.dtFechaingreso);
            this.Controls.Add(fecha_ingresoLabel);
            this.Controls.Add(this.tbcontenido);
            this.Controls.Add(contenidoLabel);
            this.Controls.Add(this.cajasDataGridView);
            this.Controls.Add(this.groupBox2);
            this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "frmIngresoCajasSinFecha";
            this.Text = "Ingreso de Cajas Generadas Sin Fecha a Depósito";
            this.Load += new System.EventHandler(this.frmIngresoCajasSinFecha_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.archivoDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cajasBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cajasDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.operacionesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.operacionesDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnBuscaCodigo;
        private System.Windows.Forms.TextBox tbCodigo;
        private archivoDataSet archivoDataSet;
        private System.Windows.Forms.BindingSource cajasBindingSource;
        private archivoDataSetTableAdapters.cajasTableAdapter cajasTableAdapter;
        private archivoDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.DataGridView cajasDataGridView;
        private System.Windows.Forms.TextBox tbcontenido;
        private System.Windows.Forms.DateTimePicker dtFechaingreso;
        private System.Windows.Forms.CheckBox cbTraslado;
        private archivoDataSetTableAdapters.operacionesTableAdapter operacionesTableAdapter;
        private System.Windows.Forms.Button btnIngresar;
        private System.Windows.Forms.BindingSource operacionesBindingSource;
        private System.Windows.Forms.DataGridView operacionesDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn2;
        private System.Windows.Forms.CheckBox cbFecha;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker dtFechaHasta;
        private System.Windows.Forms.DateTimePicker dtFechaDesde;
        private System.Windows.Forms.TextBox tbNoHasta;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbNoDesde;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Numdesde;
        private System.Windows.Forms.DataGridViewTextBoxColumn Numhasta;
        private System.Windows.Forms.DataGridViewTextBoxColumn Fechadesde;
        private System.Windows.Forms.DataGridViewTextBoxColumn Fechahasta;
    }
}