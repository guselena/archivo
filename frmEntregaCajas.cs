﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Archivo
{
    public partial class frmEntregaCajas : Form
    {
        frmImprimePlanilla frmplan;


        public frmEntregaCajas()
        {
            InitializeComponent();
        }

        private void Entregacajas_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'archivoDataSet.operaciones' table. You can move, or remove it, as needed.
            this.operacionesTableAdapter.Fill(this.archivoDataSet.operaciones);
            // TODO: This line of code loads data into the 'archivoDataSet.clientes' table. You can move, or remove it, as needed.
            this.clientesTableAdapter.Fill(this.archivoDataSet.clientes);

            for (int i = 0; i <= archivoDataSet.clientes.Rows.Count - 1; i++)
            {
                if (lbNombreEmpresa.FindString(clientesDataGridView.Rows[i].Cells[1].Value.ToString()) == -1)
                    lbNombreEmpresa.Items.Add(clientesDataGridView.Rows[i].Cells[1].Value.ToString());
            }
            txtNomEmpresa.AutoCompleteCustomSource.Clear();
            foreach (object o in lbNombreEmpresa.Items)
                txtNomEmpresa.AutoCompleteCustomSource.Add(o.ToString());

        }

        private void button1_Click(object sender, EventArgs e)
        {
            DataView dv = archivoDataSet.clientes.DefaultView;
            string filter = "Nocliente > '0'";
            dv.RowFilter = filter;
            clientesBindingSource.DataSource = dv;
            if (txtNomEmpresa.Text != "")
                filter = "NombreEmpresa LIKE '*" + txtNomEmpresa.Text + "*'";
            else
                filter = "Nocliente = " + txtNoCliente.Text;
            dv.RowFilter = filter;
            if (clientesDataGridView.RowCount == 0)
            {
                MessageBox.Show("No se encuentra el cliente", "Error", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                return;
            }
/*            int maximo;
            if (archivoDataSet.operaciones.Rows.Count != 0)
                maximo = (int)archivoDataSet.operaciones.Compute("Max(NoOp)", "");
            else
                maximo = 0;*/
            archivoDataSet.operacionesRow filanuevaop = archivoDataSet.operaciones.NewoperacionesRow();
//            filanuevaop.NoOp = maximo + 1;
            string tipostr = ((int)udCantCajas.Value).ToString("D4") + " Entrega de Cajas";  // primeros 4 caracteres: cantidad de cajas entregadas
            filanuevaop.Tipo = tipostr;
            filanuevaop.Fecha = dtFechaentrega.Value;
            filanuevaop.NoCliente = Convert.ToInt32(clientesDataGridView.Rows[0].Cells[0].Value.ToString());
            filanuevaop.Codigo = "";
            filanuevaop.Traslado = false;
            archivoDataSet.operaciones.Rows.Add(filanuevaop);
            operacionesBindingSource.MoveLast();
            this.Validate();
            operacionesBindingSource.EndEdit();
            tableAdapterManager.UpdateAll(this.archivoDataSet);

            if (MessageBox.Show(udCantCajas.Value.ToString()+ " cajas entregadas. Imprimir planilla de entrega?", "Entrega de cajas vacías",
                MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Imprimeplanilla();
            }
        }

        private void Imprimeplanilla()
        {
            DateTime fecha = dtFechaentrega.Value;

            frmplan = new frmImprimePlanilla("entrega", "", clientesDataGridView.Rows[0].Cells[1].Value.ToString(), udCantCajas.Value.ToString(), fecha);
            // en contenido va la cantidad de cajas entregadas
            frmplan.ShowDialog(this);



        }

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            Pen penlinea = new Pen(Color.Black, 0.5F);
            e.Graphics.PageUnit = GraphicsUnit.Millimeter;
            Graphics Canvas = e.Graphics;
            Canvas.PageUnit = GraphicsUnit.Millimeter;
            float dpiCorrectionX = Canvas.DpiX / 100;
            float dpiCorrectionY = Canvas.DpiY / 100;
            Point[] Points = new Point[2];


            Points[0] = new Point(Convert.ToInt32(e.PageBounds.X * dpiCorrectionX), Convert.ToInt32(e.PageBounds.Y * dpiCorrectionY));
            Points[1] = new Point(Convert.ToInt32(e.PageBounds.Width * dpiCorrectionX), Convert.ToInt32(e.PageBounds.Height * dpiCorrectionY));
            Canvas.TransformPoints(System.Drawing.Drawing2D.CoordinateSpace.Page, System.Drawing.Drawing2D.CoordinateSpace.Device, Points);
            Rectangle Bounds = new Rectangle(Points[0].X, Points[0].Y, Points[1].X - 20, Points[1].Y - 20);
            float mizq = Bounds.Left;
            float msup = Bounds.Top;
            float anchopag = Bounds.Width;
            float altopag = Bounds.Height;
            Font letratitulo = new Font("Arial", 23);
            Font letracodigo = new Font("Arial", 12);
            Font letradatobold = new Font("Arial", 12, FontStyle.Bold);
            float largo;


            for (int i = 0; i <= 1; i++)
            {
                msup += i * 143;
                e.Graphics.DrawImage(pctlogo.Image, mizq, msup, 89, 28);


                e.Graphics.DrawString("Entrega de cajas vacías", letratitulo, Brushes.Black, mizq + 91, msup);

 
                e.Graphics.DrawString("Fecha de Entrega: ", letradatobold, Brushes.Black, mizq, msup + 30);
                largo = e.Graphics.MeasureString("Fecha de Entrega: ", letradatobold).Width;
                e.Graphics.DrawString(dtFechaentrega.Value.ToString("d"), letracodigo, Brushes.Black, mizq + largo, msup + 30);
                e.Graphics.DrawString("Cliente: ", letradatobold, Brushes.Black, mizq, msup + 36);
                largo = e.Graphics.MeasureString("Cliente: ", letradatobold).Width;
                e.Graphics.DrawString(clientesDataGridView.Rows[0].Cells[1].Value.ToString(), letracodigo, Brushes.Black, mizq + largo, msup + 36);
                e.Graphics.DrawString("Cantidad de cajas entregadas: " + udCantCajas.Value.ToString(), letradatobold, Brushes.Black, mizq , msup + 42);

                e.Graphics.DrawString("Entregó por GuardaDoc S.A.:", letradatobold, Brushes.Black, mizq, msup + 105);
                e.Graphics.DrawString("Recibió por " + clientesDataGridView.Rows[0].Cells[1].Value.ToString() + ":", letradatobold, Brushes.Black, mizq + 102, msup + 105);
                e.Graphics.DrawString("Firma:", letracodigo, Brushes.Black, mizq + 10, msup + 120);
                e.Graphics.DrawString("Firma:", letracodigo, Brushes.Black, mizq + 112, msup + 120);
                e.Graphics.DrawLine(penlinea, mizq + 24, msup + 124, mizq + 84, msup + 124);
                e.Graphics.DrawLine(penlinea, mizq + 126, msup + 124, mizq + 186, msup + 124);

                e.Graphics.DrawString("Aclaración:", letracodigo, Brushes.Black, mizq, msup + 130);
                e.Graphics.DrawString("Aclaración:", letracodigo, Brushes.Black, mizq + 102, msup + 130);
                e.Graphics.DrawLine(penlinea, mizq + 24, msup + 134, mizq + 84, msup + 134);
                e.Graphics.DrawLine(penlinea, mizq + 126, msup + 134, mizq + 186, msup + 134);
            }

        }

    }
}
