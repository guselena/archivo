﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Archivo
{
    public partial class frmNuevoCliente : Form
    {
        public frmNuevoCliente()
        {
            InitializeComponent();
        }


        private void frmNuevoCliente_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'archivoDataSet.clientes' table. You can move, or remove it, as needed.
            this.clientesTableAdapter.Fill(this.archivoDataSet.clientes);

/*            int maximo;
            if (archivoDataSet.clientes.Rows.Count != 0)
                maximo = (int)archivoDataSet.clientes.Compute("Max(Nocliente)", "");
            else
                maximo = 0;*/
            archivoDataSet.clientesRow filanueva = archivoDataSet.clientes.NewclientesRow();
//            filanueva.Nocliente = maximo + 1;
            archivoDataSet.clientes.Rows.Add(filanueva);
            clientesBindingSource.MoveLast();
//            noclienteTextBox.Text = (maximo + 1).ToString();

            for (int i = 0; i <= archivoDataSet.clientes.Rows.Count - 1; i++)
            {
                if (lbLocalidad.FindString(clientesDataGridView.Rows[i].Cells[3].Value.ToString()) == -1)
                    lbLocalidad.Items.Add(clientesDataGridView.Rows[i].Cells[3].Value.ToString());
                if (lbCP.FindString(clientesDataGridView.Rows[i].Cells[4].Value.ToString()) == -1)
                    lbCP.Items.Add(clientesDataGridView.Rows[i].Cells[4].Value.ToString());
            }
            localidadTextBox.AutoCompleteCustomSource.Clear();
            cPTextBox.AutoCompleteCustomSource.Clear();
            foreach (object o in lbLocalidad.Items)
                localidadTextBox.AutoCompleteCustomSource.Add(o.ToString());
            foreach (object o in lbCP.Items)
                cPTextBox.AutoCompleteCustomSource.Add(o.ToString());


        }



        private void btnGuardar_Click(object sender, EventArgs e)
        {
            iVATextBox.Text = cmbIva.Text;
            this.Validate();
            this.clientesBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.archivoDataSet);
            MessageBox.Show("Datos Guardados con éxito", "Datos del cliente", MessageBoxButtons.OK);
            int maximo;
            if (archivoDataSet.clientes.Rows.Count != 0)
                maximo = (int)archivoDataSet.clientes.Compute("Max(Nocliente)", "");
            else
                maximo = 0;
            archivoDataSet.clientesRow filanueva = archivoDataSet.clientes.NewclientesRow();
//            filanueva.Nocliente = maximo + 1;
            archivoDataSet.clientes.Rows.Add(filanueva);
            clientesBindingSource.MoveLast();
//            noclienteTextBox.Text = (maximo + 1).ToString();

        }

    }
}
