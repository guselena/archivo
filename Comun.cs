﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Win32;
using System.Windows.Forms;

namespace Archivo
{
    class Comun
    {
        public static int cantfilas;   // cantidad de filas en el depósito
        public static int cantpisos;   // cantidad de pisos por cada rack
        public static int cantcajasxpiso;  // cantidad de cajas por piso
        public static int[] cantracks; // cantidad de racks (un valor para cada fila)
        public static int abonoplana, abonoplanb, abonoplanc;
        public static decimal cajaexcedenteA,cajaexcedenteB,cajaexcedenteC;
        public static int maxplana, maxplanb, maxplanc;
        public static int abonohosting,maxpaginas;
        public static decimal tarifascan,tarifadoc,tarifabusqueda,tarifatraslado,preciocaja,tarifatrasladoadicional;
    }
}

