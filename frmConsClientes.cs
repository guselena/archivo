﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Archivo
{
    public partial class frmConsClientes : Form
    {
        public frmConsClientes()
        {
            InitializeComponent();
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'archivoDataSet.clientes' table. You can move, or remove it, as needed.
            this.clientesTableAdapter.Fill(this.archivoDataSet.clientes);

            for (int i = 0; i <= archivoDataSet.clientes.Rows.Count - 1; i++)
            {
                if (lbNombreEmpresa.FindString(clientesDataGridView.Rows[i].Cells[1].Value.ToString()) == -1)
                    lbNombreEmpresa.Items.Add(clientesDataGridView.Rows[i].Cells[1].Value.ToString());
            }
            txtNomEmpresa.AutoCompleteCustomSource.Clear();
            foreach (object o in lbNombreEmpresa.Items)
                txtNomEmpresa.AutoCompleteCustomSource.Add(o.ToString());
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            DataView dv = archivoDataSet.clientes.DefaultView;
            string filter = "Nocliente > '0'";
            dv.RowFilter = filter;
            clientesBindingSource.DataSource = dv;
            if(txtNomEmpresa.Text=="" && txtNoCliente.Text=="")
                return;

            if(txtNomEmpresa.Text !="")
                filter = "NombreEmpresa LIKE '*" + txtNomEmpresa.Text + "*'";
            else
                filter = "Nocliente = " + txtNoCliente.Text;
            dv.RowFilter = filter;
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.clientesBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.archivoDataSet);
            MessageBox.Show("Archivo guardado con éxito.");
        }

    }
}
