﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Archivo
{
    public partial class frmPrincipal : Form
    {
        frmNuevoCliente frmNuevo;
        frmConsClientes frmCons;
        frmIngresoCajas frmIng;
        frmConfig frmConf;
        frmretconsulta frmRet;
        frmreingresar frmReing;
        frmBajaCajas frmBaja;
        frmVerDatos frmVer;
        frmReimpresion frmReimp;
        frmresumen frmres;
        frmimprime_individual frmindiv;
        frmEntregaCajas frmentcajas;
        frmGeneracionCodigo frmGener;
        frmIngresoCajasSinFecha frmSinFecha;
        frmlistado frmlis;
        frmbusqueda frmbus;
        frmdetallesbusqueda frmdet;

        public frmPrincipal()
        {
            InitializeComponent();
        }

        private void frmPrincipal_Load(object sender, EventArgs e)
        {
            actualizalayout();  // actualiza cantidad de filas, racks, pisos o cajas
            actualizatarifas(); // actualiza abono. tarifas, etc.
        }


        private void nuevoClienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmNuevo = new frmNuevoCliente();
            frmNuevo.ShowDialog();
        }

        private void consultaYModificacionDeClientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCons = new frmConsClientes();
            frmCons.ShowDialog();
        }

        private void ingresoDeCajasADepósitoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmIng = new frmIngresoCajas();
            frmIng.ShowDialog();
        }

        private void actualizalayout()
        {
            Comun.cantfilas = Archivo.Properties.Settings.Default.Cantfilas;
            Comun.cantpisos = Archivo.Properties.Settings.Default.Cantpisos;
            Comun.cantracks = new int[Comun.cantfilas];       // Dimensiona un numero por cada fila
            Comun.cantracks[0] = Archivo.Properties.Settings.Default.Cantracks1;
            Comun.cantcajasxpiso = Archivo.Properties.Settings.Default.Cantcajasxpiso;
            for (int i = 0; i <= Comun.cantfilas-1; i++)
            {
                Comun.cantracks[i] = (int)Archivo.Properties.Settings.Default["cantracks" + (i + 1).ToString()];
            }

        }

        private void actualizatarifas()
        {
            Comun.abonoplana = Archivo.Properties.Settings.Default.Abonoplana;
            Comun.abonoplanb = Archivo.Properties.Settings.Default.Abonoplanb;
            Comun.abonoplanc = Archivo.Properties.Settings.Default.Abonoplanc;
            Comun.maxplana = Archivo.Properties.Settings.Default.Maxplana;
            Comun.maxplanb = Archivo.Properties.Settings.Default.Maxplanb;
            Comun.maxplanc = Archivo.Properties.Settings.Default.Maxplanc;
            Comun.cajaexcedenteA = Archivo.Properties.Settings.Default.Cajaexcedentea;
            Comun.cajaexcedenteB = Archivo.Properties.Settings.Default.Cajaexcedenteb;
            Comun.cajaexcedenteC = Archivo.Properties.Settings.Default.Cajaexcedentec;

            Comun.tarifatrasladoadicional = Archivo.Properties.Settings.Default.Trasladoadicional;
            Comun.tarifabusqueda = Archivo.Properties.Settings.Default.Tarifaconsulta;
            Comun.tarifatraslado = Archivo.Properties.Settings.Default.Tarifatraslado;
            Comun.preciocaja = Archivo.Properties.Settings.Default.Preciocaja;
            Comun.abonohosting = Archivo.Properties.Settings.Default.Abonohosting;
            Comun.tarifadoc = Archivo.Properties.Settings.Default.Tarifadocumento;
            Comun.tarifascan = Archivo.Properties.Settings.Default.Tarifascan;
            Comun.maxpaginas = Archivo.Properties.Settings.Default.Maxpaginas;
        }



        private void configuraciónToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmConf = new frmConfig();
            frmConf.ShowDialog();
        }

        private void retiroDeCajasParaConsultaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmRet = new frmretconsulta();
            frmRet.ShowDialog();
        }

        private void consultaDeDatosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmVer= new frmVerDatos();
            frmVer.ShowDialog();
        }

        private void reingresoDeCajasConsultadasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmReing = new frmreingresar();
            frmReing.ShowDialog();
        }

        private void bajaDeCajasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmBaja= new frmBajaCajas();
            frmBaja.ShowDialog();
        }

        private void impresiónDePlanillaDeCajaDepositadaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmReimp = new frmReimpresion();
            frmReimp.ShowDialog();
        }

        private void resumenDeOperacionesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmres = new frmresumen();
            frmres.ShowDialog();
        }

        private void impresiónDeEtiquetasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmindiv = new frmimprime_individual();
            frmindiv.ShowDialog(this);
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmentcajas = new frmEntregaCajas();
            frmentcajas.ShowDialog(this);
        }

        private void generaciónDeCódigoSinFechaDeIngresoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmGener = new frmGeneracionCodigo();
            frmGener.ShowDialog(this);
        }

        private void ingresoDeCajasGeneradasSinFechaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmSinFecha = new frmIngresoCajasSinFecha();
            frmSinFecha.ShowDialog(this);
        }

        private void listadoDeCajasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmlis = new frmlistado();
            frmlis.ShowDialog(this);
        }

        private void búsquedaDeDocumentoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmbus = new frmbusqueda();
            frmbus.ShowDialog(this);
        }

        private void imprimirDetallesDeBúsquedaDeDocumentosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmdet = new frmdetallesbusqueda();
            frmdet.ShowDialog(this);
        }



    }
}
