﻿namespace Archivo
{
    partial class frmVerDatos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label nombre_EmpresaLabel;
            System.Windows.Forms.Label noclienteLabel;
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbContenido3 = new System.Windows.Forms.TextBox();
            this.cbContenido3 = new System.Windows.Forms.CheckBox();
            this.tbContenido2 = new System.Windows.Forms.TextBox();
            this.cbContenido2 = new System.Windows.Forms.CheckBox();
            this.dtFechaCont = new System.Windows.Forms.DateTimePicker();
            this.cbFechaCont = new System.Windows.Forms.CheckBox();
            this.tbnumero = new System.Windows.Forms.TextBox();
            this.cbNumero = new System.Windows.Forms.CheckBox();
            this.rbConsultaNo = new System.Windows.Forms.RadioButton();
            this.rbConsultaSi = new System.Windows.Forms.RadioButton();
            this.cbConsulta = new System.Windows.Forms.CheckBox();
            this.tbContenido = new System.Windows.Forms.TextBox();
            this.cbContenido = new System.Windows.Forms.CheckBox();
            this.btnFiltroParam = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dtFechaHasta = new System.Windows.Forms.DateTimePicker();
            this.dtFechaDesde = new System.Windows.Forms.DateTimePicker();
            this.cbFecha = new System.Windows.Forms.CheckBox();
            this.cbLote = new System.Windows.Forms.CheckBox();
            this.udNolote = new System.Windows.Forms.NumericUpDown();
            this.tbNocliente = new System.Windows.Forms.TextBox();
            this.txtNombreEmpresa = new System.Windows.Forms.TextBox();
            this.lbNombreEmpresa = new System.Windows.Forms.ListBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnBuscaCodigo = new System.Windows.Forms.Button();
            this.tbCodigo = new System.Windows.Forms.TextBox();
            this.cajasDataGridView = new System.Windows.Forms.DataGridView();
            this.Numdesde = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Numhasta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Fechadesde = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Fechahasta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clientesDataGridView = new System.Windows.Forms.DataGridView();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clientesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.archivoDataSet = new Archivo.archivoDataSet();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cajasBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cajasTableAdapter = new Archivo.archivoDataSetTableAdapters.cajasTableAdapter();
            this.tableAdapterManager = new Archivo.archivoDataSetTableAdapters.TableAdapterManager();
            this.clientesTableAdapter = new Archivo.archivoDataSetTableAdapters.clientesTableAdapter();
            nombre_EmpresaLabel = new System.Windows.Forms.Label();
            noclienteLabel = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udNolote)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cajasDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientesDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.archivoDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cajasBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // nombre_EmpresaLabel
            // 
            nombre_EmpresaLabel.AutoSize = true;
            nombre_EmpresaLabel.Location = new System.Drawing.Point(174, 22);
            nombre_EmpresaLabel.Name = "nombre_EmpresaLabel";
            nombre_EmpresaLabel.Size = new System.Drawing.Size(129, 13);
            nombre_EmpresaLabel.TabIndex = 49;
            nombre_EmpresaLabel.Text = "Nombre de Empresa:";
            // 
            // noclienteLabel
            // 
            noclienteLabel.AutoSize = true;
            noclienteLabel.Location = new System.Drawing.Point(14, 22);
            noclienteLabel.Name = "noclienteLabel";
            noclienteLabel.Size = new System.Drawing.Size(88, 13);
            noclienteLabel.TabIndex = 48;
            noclienteLabel.Text = "Nº de Cliente:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tbContenido3);
            this.groupBox1.Controls.Add(this.cbContenido3);
            this.groupBox1.Controls.Add(this.tbContenido2);
            this.groupBox1.Controls.Add(this.cbContenido2);
            this.groupBox1.Controls.Add(this.dtFechaCont);
            this.groupBox1.Controls.Add(this.cbFechaCont);
            this.groupBox1.Controls.Add(this.tbnumero);
            this.groupBox1.Controls.Add(this.cbNumero);
            this.groupBox1.Controls.Add(this.rbConsultaNo);
            this.groupBox1.Controls.Add(this.rbConsultaSi);
            this.groupBox1.Controls.Add(this.cbConsulta);
            this.groupBox1.Controls.Add(this.tbContenido);
            this.groupBox1.Controls.Add(this.cbContenido);
            this.groupBox1.Controls.Add(this.btnFiltroParam);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.dtFechaHasta);
            this.groupBox1.Controls.Add(this.dtFechaDesde);
            this.groupBox1.Controls.Add(this.cbFecha);
            this.groupBox1.Controls.Add(this.cbLote);
            this.groupBox1.Controls.Add(this.udNolote);
            this.groupBox1.Controls.Add(this.tbNocliente);
            this.groupBox1.Controls.Add(this.txtNombreEmpresa);
            this.groupBox1.Controls.Add(nombre_EmpresaLabel);
            this.groupBox1.Controls.Add(noclienteLabel);
            this.groupBox1.Location = new System.Drawing.Point(14, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1159, 198);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Filtro por parámetros";
            // 
            // tbContenido3
            // 
            this.tbContenido3.BackColor = System.Drawing.Color.Wheat;
            this.tbContenido3.Enabled = false;
            this.tbContenido3.Location = new System.Drawing.Point(705, 114);
            this.tbContenido3.Name = "tbContenido3";
            this.tbContenido3.Size = new System.Drawing.Size(253, 21);
            this.tbContenido3.TabIndex = 75;
            // 
            // cbContenido3
            // 
            this.cbContenido3.AutoSize = true;
            this.cbContenido3.Location = new System.Drawing.Point(669, 118);
            this.cbContenido3.Name = "cbContenido3";
            this.cbContenido3.Size = new System.Drawing.Size(40, 17);
            this.cbContenido3.TabIndex = 74;
            this.cbContenido3.Text = "+:";
            this.cbContenido3.UseVisualStyleBackColor = true;
            this.cbContenido3.CheckedChanged += new System.EventHandler(this.cbContenido3_CheckedChanged);
            // 
            // tbContenido2
            // 
            this.tbContenido2.BackColor = System.Drawing.Color.Wheat;
            this.tbContenido2.Enabled = false;
            this.tbContenido2.Location = new System.Drawing.Point(409, 116);
            this.tbContenido2.Name = "tbContenido2";
            this.tbContenido2.Size = new System.Drawing.Size(253, 21);
            this.tbContenido2.TabIndex = 73;
            // 
            // cbContenido2
            // 
            this.cbContenido2.AutoSize = true;
            this.cbContenido2.Location = new System.Drawing.Point(373, 120);
            this.cbContenido2.Name = "cbContenido2";
            this.cbContenido2.Size = new System.Drawing.Size(40, 17);
            this.cbContenido2.TabIndex = 72;
            this.cbContenido2.Text = "+:";
            this.cbContenido2.UseVisualStyleBackColor = true;
            this.cbContenido2.CheckedChanged += new System.EventHandler(this.cbContenido2_CheckedChanged);
            // 
            // dtFechaCont
            // 
            this.dtFechaCont.Enabled = false;
            this.dtFechaCont.Location = new System.Drawing.Point(538, 145);
            this.dtFechaCont.Name = "dtFechaCont";
            this.dtFechaCont.Size = new System.Drawing.Size(233, 21);
            this.dtFechaCont.TabIndex = 71;
            // 
            // cbFechaCont
            // 
            this.cbFechaCont.AutoSize = true;
            this.cbFechaCont.Location = new System.Drawing.Point(377, 150);
            this.cbFechaCont.Name = "cbFechaCont";
            this.cbFechaCont.Size = new System.Drawing.Size(144, 17);
            this.cbFechaCont.TabIndex = 70;
            this.cbFechaCont.Text = "Fecha del contenido:";
            this.cbFechaCont.UseVisualStyleBackColor = true;
            this.cbFechaCont.CheckedChanged += new System.EventHandler(this.cbFechaCont_CheckedChanged);
            // 
            // tbnumero
            // 
            this.tbnumero.BackColor = System.Drawing.Color.Wheat;
            this.tbnumero.Enabled = false;
            this.tbnumero.Location = new System.Drawing.Point(115, 143);
            this.tbnumero.Name = "tbnumero";
            this.tbnumero.Size = new System.Drawing.Size(107, 21);
            this.tbnumero.TabIndex = 69;
            // 
            // cbNumero
            // 
            this.cbNumero.AutoSize = true;
            this.cbNumero.Location = new System.Drawing.Point(17, 146);
            this.cbNumero.Name = "cbNumero";
            this.cbNumero.Size = new System.Drawing.Size(76, 17);
            this.cbNumero.TabIndex = 68;
            this.cbNumero.Text = "Número:";
            this.cbNumero.UseVisualStyleBackColor = true;
            this.cbNumero.CheckedChanged += new System.EventHandler(this.cbNumero_CheckedChanged);
            // 
            // rbConsultaNo
            // 
            this.rbConsultaNo.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbConsultaNo.AutoSize = true;
            this.rbConsultaNo.Enabled = false;
            this.rbConsultaNo.Location = new System.Drawing.Point(151, 169);
            this.rbConsultaNo.Name = "rbConsultaNo";
            this.rbConsultaNo.Size = new System.Drawing.Size(32, 23);
            this.rbConsultaNo.TabIndex = 67;
            this.rbConsultaNo.TabStop = true;
            this.rbConsultaNo.Text = "No";
            this.rbConsultaNo.UseVisualStyleBackColor = true;
            // 
            // rbConsultaSi
            // 
            this.rbConsultaSi.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbConsultaSi.AutoSize = true;
            this.rbConsultaSi.Enabled = false;
            this.rbConsultaSi.Location = new System.Drawing.Point(117, 169);
            this.rbConsultaSi.Name = "rbConsultaSi";
            this.rbConsultaSi.Size = new System.Drawing.Size(28, 23);
            this.rbConsultaSi.TabIndex = 66;
            this.rbConsultaSi.TabStop = true;
            this.rbConsultaSi.Text = "Si";
            this.rbConsultaSi.UseVisualStyleBackColor = true;
            // 
            // cbConsulta
            // 
            this.cbConsulta.AutoSize = true;
            this.cbConsulta.Location = new System.Drawing.Point(17, 175);
            this.cbConsulta.Name = "cbConsulta";
            this.cbConsulta.Size = new System.Drawing.Size(94, 17);
            this.cbConsulta.TabIndex = 65;
            this.cbConsulta.Text = "En Consulta";
            this.cbConsulta.UseVisualStyleBackColor = true;
            this.cbConsulta.CheckedChanged += new System.EventHandler(this.cbConsulta_CheckedChanged);
            // 
            // tbContenido
            // 
            this.tbContenido.BackColor = System.Drawing.Color.Wheat;
            this.tbContenido.Enabled = false;
            this.tbContenido.Location = new System.Drawing.Point(115, 116);
            this.tbContenido.Name = "tbContenido";
            this.tbContenido.Size = new System.Drawing.Size(253, 21);
            this.tbContenido.TabIndex = 64;
            // 
            // cbContenido
            // 
            this.cbContenido.AutoSize = true;
            this.cbContenido.Location = new System.Drawing.Point(17, 116);
            this.cbContenido.Name = "cbContenido";
            this.cbContenido.Size = new System.Drawing.Size(89, 17);
            this.cbContenido.TabIndex = 63;
            this.cbContenido.Text = "Contenido:";
            this.cbContenido.UseVisualStyleBackColor = true;
            this.cbContenido.CheckedChanged += new System.EventHandler(this.cbContenido_CheckedChanged);
            // 
            // btnFiltroParam
            // 
            this.btnFiltroParam.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnFiltroParam.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFiltroParam.Location = new System.Drawing.Point(934, 146);
            this.btnFiltroParam.Name = "btnFiltroParam";
            this.btnFiltroParam.Size = new System.Drawing.Size(218, 42);
            this.btnFiltroParam.TabIndex = 62;
            this.btnFiltroParam.Text = "Filtrar";
            this.btnFiltroParam.UseVisualStyleBackColor = false;
            this.btnFiltroParam.Click += new System.EventHandler(this.btnFiltroParam_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(486, 94);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 61;
            this.label2.Text = "Hasta:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(174, 94);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 60;
            this.label1.Text = "Desde:";
            // 
            // dtFechaHasta
            // 
            this.dtFechaHasta.Enabled = false;
            this.dtFechaHasta.Location = new System.Drawing.Point(538, 89);
            this.dtFechaHasta.Name = "dtFechaHasta";
            this.dtFechaHasta.Size = new System.Drawing.Size(233, 21);
            this.dtFechaHasta.TabIndex = 59;
            // 
            // dtFechaDesde
            // 
            this.dtFechaDesde.Enabled = false;
            this.dtFechaDesde.Location = new System.Drawing.Point(229, 89);
            this.dtFechaDesde.Name = "dtFechaDesde";
            this.dtFechaDesde.Size = new System.Drawing.Size(233, 21);
            this.dtFechaDesde.TabIndex = 58;
            // 
            // cbFecha
            // 
            this.cbFecha.AutoSize = true;
            this.cbFecha.Location = new System.Drawing.Point(17, 92);
            this.cbFecha.Name = "cbFecha";
            this.cbFecha.Size = new System.Drawing.Size(130, 17);
            this.cbFecha.TabIndex = 57;
            this.cbFecha.Text = "Fecha de Ingreso:";
            this.cbFecha.UseVisualStyleBackColor = true;
            this.cbFecha.CheckedChanged += new System.EventHandler(this.cbFecha_CheckedChanged);
            // 
            // cbLote
            // 
            this.cbLote.AutoSize = true;
            this.cbLote.Location = new System.Drawing.Point(17, 57);
            this.cbLote.Name = "cbLote";
            this.cbLote.Size = new System.Drawing.Size(50, 17);
            this.cbLote.TabIndex = 56;
            this.cbLote.Text = "Lote";
            this.cbLote.UseVisualStyleBackColor = true;
            this.cbLote.CheckedChanged += new System.EventHandler(this.cbLote_CheckedChanged);
            // 
            // udNolote
            // 
            this.udNolote.Enabled = false;
            this.udNolote.Location = new System.Drawing.Point(79, 56);
            this.udNolote.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.udNolote.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udNolote.Name = "udNolote";
            this.udNolote.Size = new System.Drawing.Size(78, 21);
            this.udNolote.TabIndex = 54;
            this.udNolote.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // tbNocliente
            // 
            this.tbNocliente.BackColor = System.Drawing.Color.Wheat;
            this.tbNocliente.Location = new System.Drawing.Point(105, 19);
            this.tbNocliente.Name = "tbNocliente";
            this.tbNocliente.Size = new System.Drawing.Size(61, 21);
            this.tbNocliente.TabIndex = 51;
            // 
            // txtNombreEmpresa
            // 
            this.txtNombreEmpresa.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtNombreEmpresa.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtNombreEmpresa.BackColor = System.Drawing.Color.Wheat;
            this.txtNombreEmpresa.Location = new System.Drawing.Point(304, 19);
            this.txtNombreEmpresa.Name = "txtNombreEmpresa";
            this.txtNombreEmpresa.Size = new System.Drawing.Size(508, 21);
            this.txtNombreEmpresa.TabIndex = 50;
            this.txtNombreEmpresa.TextChanged += new System.EventHandler(this.txtNombreEmpresa_TextChanged);
            this.txtNombreEmpresa.Leave += new System.EventHandler(this.txtNombreEmpresa_Leave);
            // 
            // lbNombreEmpresa
            // 
            this.lbNombreEmpresa.FormattingEnabled = true;
            this.lbNombreEmpresa.Location = new System.Drawing.Point(876, 531);
            this.lbNombreEmpresa.Name = "lbNombreEmpresa";
            this.lbNombreEmpresa.Size = new System.Drawing.Size(139, 95);
            this.lbNombreEmpresa.TabIndex = 38;
            this.lbNombreEmpresa.Visible = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnBuscaCodigo);
            this.groupBox2.Controls.Add(this.tbCodigo);
            this.groupBox2.Location = new System.Drawing.Point(14, 216);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1158, 69);
            this.groupBox2.TabIndex = 39;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Buscar Código:";
            // 
            // btnBuscaCodigo
            // 
            this.btnBuscaCodigo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnBuscaCodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBuscaCodigo.Location = new System.Drawing.Point(934, 20);
            this.btnBuscaCodigo.Name = "btnBuscaCodigo";
            this.btnBuscaCodigo.Size = new System.Drawing.Size(218, 41);
            this.btnBuscaCodigo.TabIndex = 63;
            this.btnBuscaCodigo.Text = "Buscar";
            this.btnBuscaCodigo.UseVisualStyleBackColor = false;
            this.btnBuscaCodigo.Click += new System.EventHandler(this.btnBuscaCodigo_Click);
            // 
            // tbCodigo
            // 
            this.tbCodigo.BackColor = System.Drawing.Color.Wheat;
            this.tbCodigo.Location = new System.Drawing.Point(17, 28);
            this.tbCodigo.Name = "tbCodigo";
            this.tbCodigo.Size = new System.Drawing.Size(300, 21);
            this.tbCodigo.TabIndex = 0;
            // 
            // cajasDataGridView
            // 
            this.cajasDataGridView.AllowUserToAddRows = false;
            this.cajasDataGridView.AllowUserToDeleteRows = false;
            this.cajasDataGridView.AutoGenerateColumns = false;
            this.cajasDataGridView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.cajasDataGridView.BackgroundColor = System.Drawing.Color.Wheat;
            this.cajasDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.cajasDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewCheckBoxColumn1,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.Numdesde,
            this.Numhasta,
            this.Fechadesde,
            this.Fechahasta});
            this.cajasDataGridView.DataSource = this.cajasBindingSource;
            this.cajasDataGridView.Location = new System.Drawing.Point(14, 302);
            this.cajasDataGridView.Name = "cajasDataGridView";
            this.cajasDataGridView.Size = new System.Drawing.Size(1158, 202);
            this.cajasDataGridView.TabIndex = 40;
            // 
            // Numdesde
            // 
            this.Numdesde.DataPropertyName = "Numdesde";
            this.Numdesde.HeaderText = "Desde Nº";
            this.Numdesde.Name = "Numdesde";
            this.Numdesde.Width = 65;
            // 
            // Numhasta
            // 
            this.Numhasta.DataPropertyName = "Numhasta";
            this.Numhasta.HeaderText = "Hasta Nº";
            this.Numhasta.Name = "Numhasta";
            this.Numhasta.Width = 80;
            // 
            // Fechadesde
            // 
            this.Fechadesde.DataPropertyName = "Fechadesde";
            this.Fechadesde.HeaderText = "Fecha Desde";
            this.Fechadesde.Name = "Fechadesde";
            // 
            // Fechahasta
            // 
            this.Fechahasta.DataPropertyName = "Fechahasta";
            this.Fechahasta.HeaderText = "Fecha Hasta";
            this.Fechahasta.Name = "Fechahasta";
            // 
            // clientesDataGridView
            // 
            this.clientesDataGridView.AllowUserToAddRows = false;
            this.clientesDataGridView.AllowUserToDeleteRows = false;
            this.clientesDataGridView.AutoGenerateColumns = false;
            this.clientesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.clientesDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn13,
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewTextBoxColumn15,
            this.dataGridViewTextBoxColumn16,
            this.dataGridViewTextBoxColumn17,
            this.dataGridViewTextBoxColumn18,
            this.dataGridViewTextBoxColumn19,
            this.dataGridViewTextBoxColumn20,
            this.dataGridViewTextBoxColumn21,
            this.dataGridViewTextBoxColumn22,
            this.dataGridViewTextBoxColumn23,
            this.dataGridViewTextBoxColumn24,
            this.dataGridViewTextBoxColumn25,
            this.dataGridViewTextBoxColumn26,
            this.dataGridViewTextBoxColumn27,
            this.dataGridViewTextBoxColumn28,
            this.dataGridViewTextBoxColumn29});
            this.clientesDataGridView.DataSource = this.clientesBindingSource;
            this.clientesDataGridView.Location = new System.Drawing.Point(1021, 531);
            this.clientesDataGridView.Name = "clientesDataGridView";
            this.clientesDataGridView.ReadOnly = true;
            this.clientesDataGridView.Size = new System.Drawing.Size(350, 220);
            this.clientesDataGridView.TabIndex = 40;
            this.clientesDataGridView.Visible = false;
            // 
            // btnGuardar
            // 
            this.btnGuardar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnGuardar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardar.Location = new System.Drawing.Point(391, 510);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(218, 41);
            this.btnGuardar.TabIndex = 64;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.UseVisualStyleBackColor = false;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "Nocliente";
            this.dataGridViewTextBoxColumn10.HeaderText = "Nocliente";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "NombreEmpresa";
            this.dataGridViewTextBoxColumn11.HeaderText = "NombreEmpresa";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.DataPropertyName = "Direccion";
            this.dataGridViewTextBoxColumn12.HeaderText = "Direccion";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.DataPropertyName = "Localidad";
            this.dataGridViewTextBoxColumn13.HeaderText = "Localidad";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.DataPropertyName = "CP";
            this.dataGridViewTextBoxColumn14.HeaderText = "CP";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.DataPropertyName = "Telefono";
            this.dataGridViewTextBoxColumn15.HeaderText = "Telefono";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.DataPropertyName = "Nombrecont1";
            this.dataGridViewTextBoxColumn16.HeaderText = "Nombrecont1";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.DataPropertyName = "Telcont1";
            this.dataGridViewTextBoxColumn17.HeaderText = "Telcont1";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.DataPropertyName = "Nombrecont2";
            this.dataGridViewTextBoxColumn18.HeaderText = "Nombrecont2";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn19
            // 
            this.dataGridViewTextBoxColumn19.DataPropertyName = "Telcont2";
            this.dataGridViewTextBoxColumn19.HeaderText = "Telcont2";
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn20
            // 
            this.dataGridViewTextBoxColumn20.DataPropertyName = "Nombrecont3";
            this.dataGridViewTextBoxColumn20.HeaderText = "Nombrecont3";
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            this.dataGridViewTextBoxColumn20.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn21
            // 
            this.dataGridViewTextBoxColumn21.DataPropertyName = "Telcont3";
            this.dataGridViewTextBoxColumn21.HeaderText = "Telcont3";
            this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            this.dataGridViewTextBoxColumn21.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn22
            // 
            this.dataGridViewTextBoxColumn22.DataPropertyName = "Nombrecont4";
            this.dataGridViewTextBoxColumn22.HeaderText = "Nombrecont4";
            this.dataGridViewTextBoxColumn22.Name = "dataGridViewTextBoxColumn22";
            this.dataGridViewTextBoxColumn22.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn23
            // 
            this.dataGridViewTextBoxColumn23.DataPropertyName = "Telcont4";
            this.dataGridViewTextBoxColumn23.HeaderText = "Telcont4";
            this.dataGridViewTextBoxColumn23.Name = "dataGridViewTextBoxColumn23";
            this.dataGridViewTextBoxColumn23.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn24
            // 
            this.dataGridViewTextBoxColumn24.DataPropertyName = "Plan";
            this.dataGridViewTextBoxColumn24.HeaderText = "Plan";
            this.dataGridViewTextBoxColumn24.Name = "dataGridViewTextBoxColumn24";
            this.dataGridViewTextBoxColumn24.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn25
            // 
            this.dataGridViewTextBoxColumn25.DataPropertyName = "DireccionEntrega";
            this.dataGridViewTextBoxColumn25.HeaderText = "DireccionEntrega";
            this.dataGridViewTextBoxColumn25.Name = "dataGridViewTextBoxColumn25";
            this.dataGridViewTextBoxColumn25.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn26
            // 
            this.dataGridViewTextBoxColumn26.DataPropertyName = "CUIT";
            this.dataGridViewTextBoxColumn26.HeaderText = "CUIT";
            this.dataGridViewTextBoxColumn26.Name = "dataGridViewTextBoxColumn26";
            this.dataGridViewTextBoxColumn26.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn27
            // 
            this.dataGridViewTextBoxColumn27.DataPropertyName = "IVA";
            this.dataGridViewTextBoxColumn27.HeaderText = "IVA";
            this.dataGridViewTextBoxColumn27.Name = "dataGridViewTextBoxColumn27";
            this.dataGridViewTextBoxColumn27.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn28
            // 
            this.dataGridViewTextBoxColumn28.DataPropertyName = "usuario";
            this.dataGridViewTextBoxColumn28.HeaderText = "usuario";
            this.dataGridViewTextBoxColumn28.Name = "dataGridViewTextBoxColumn28";
            this.dataGridViewTextBoxColumn28.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn29
            // 
            this.dataGridViewTextBoxColumn29.DataPropertyName = "contrasena";
            this.dataGridViewTextBoxColumn29.HeaderText = "contrasena";
            this.dataGridViewTextBoxColumn29.Name = "dataGridViewTextBoxColumn29";
            this.dataGridViewTextBoxColumn29.ReadOnly = true;
            // 
            // clientesBindingSource
            // 
            this.clientesBindingSource.DataMember = "clientes";
            this.clientesBindingSource.DataSource = this.archivoDataSet;
            // 
            // archivoDataSet
            // 
            this.archivoDataSet.DataSetName = "archivoDataSet";
            this.archivoDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Codigo";
            this.dataGridViewTextBoxColumn1.HeaderText = "Codigo";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 150;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Nocliente";
            this.dataGridViewTextBoxColumn2.HeaderText = "Nocliente";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 70;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Lote";
            this.dataGridViewTextBoxColumn3.HeaderText = "Lote";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Visible = false;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "Fecha ingreso";
            this.dataGridViewTextBoxColumn4.HeaderText = "Fecha ingreso";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewCheckBoxColumn1
            // 
            this.dataGridViewCheckBoxColumn1.DataPropertyName = "Enconsulta";
            this.dataGridViewCheckBoxColumn1.HeaderText = "En consulta";
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            this.dataGridViewCheckBoxColumn1.Width = 70;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn5.DataPropertyName = "Contenido";
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn5.DefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewTextBoxColumn5.HeaderText = "Contenido";
            this.dataGridViewTextBoxColumn5.MinimumWidth = 100;
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Width = 250;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "Ubicacion";
            this.dataGridViewTextBoxColumn6.HeaderText = "Ubicacion";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.Visible = false;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "Nocaja";
            this.dataGridViewTextBoxColumn7.HeaderText = "Nocaja";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.Visible = false;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "Fechaconsulta";
            this.dataGridViewTextBoxColumn8.HeaderText = "Fecha Consulta";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "Codigoint";
            this.dataGridViewTextBoxColumn9.HeaderText = "Codigoint";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.Visible = false;
            // 
            // cajasBindingSource
            // 
            this.cajasBindingSource.DataMember = "cajas";
            this.cajasBindingSource.DataSource = this.archivoDataSet;
            // 
            // cajasTableAdapter
            // 
            this.cajasTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.cajasTableAdapter = this.cajasTableAdapter;
            this.tableAdapterManager.clientesTableAdapter = this.clientesTableAdapter;
            this.tableAdapterManager.detallesoperacionTableAdapter = null;
            this.tableAdapterManager.operacionesTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = Archivo.archivoDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // clientesTableAdapter
            // 
            this.clientesTableAdapter.ClearBeforeFill = true;
            // 
            // frmVerDatos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SandyBrown;
            this.ClientSize = new System.Drawing.Size(1190, 553);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.clientesDataGridView);
            this.Controls.Add(this.cajasDataGridView);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.lbNombreEmpresa);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.KeyPreview = true;
            this.Name = "frmVerDatos";
            this.Text = "Vista de Cajas";
            this.Load += new System.EventHandler(this.frmVerDatos_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmVerDatos_KeyDown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udNolote)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cajasDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientesDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.archivoDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cajasBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tbNocliente;
        private System.Windows.Forms.TextBox txtNombreEmpresa;
        private System.Windows.Forms.ListBox lbNombreEmpresa;
        private System.Windows.Forms.RadioButton rbConsultaNo;
        private System.Windows.Forms.RadioButton rbConsultaSi;
        private System.Windows.Forms.CheckBox cbConsulta;
        private System.Windows.Forms.TextBox tbContenido;
        private System.Windows.Forms.CheckBox cbContenido;
        private System.Windows.Forms.Button btnFiltroParam;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtFechaHasta;
        private System.Windows.Forms.DateTimePicker dtFechaDesde;
        private System.Windows.Forms.CheckBox cbFecha;
        private System.Windows.Forms.CheckBox cbLote;
        private System.Windows.Forms.NumericUpDown udNolote;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnBuscaCodigo;
        private System.Windows.Forms.TextBox tbCodigo;
        private archivoDataSet archivoDataSet;
        private System.Windows.Forms.BindingSource cajasBindingSource;
        private archivoDataSetTableAdapters.cajasTableAdapter cajasTableAdapter;
        private archivoDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private archivoDataSetTableAdapters.clientesTableAdapter clientesTableAdapter;
        private System.Windows.Forms.DataGridView cajasDataGridView;
        private System.Windows.Forms.BindingSource clientesBindingSource;
        private System.Windows.Forms.DataGridView clientesDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn23;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn24;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn25;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn26;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn27;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn28;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn29;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.DateTimePicker dtFechaCont;
        private System.Windows.Forms.CheckBox cbFechaCont;
        private System.Windows.Forms.TextBox tbnumero;
        private System.Windows.Forms.CheckBox cbNumero;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Numdesde;
        private System.Windows.Forms.DataGridViewTextBoxColumn Numhasta;
        private System.Windows.Forms.DataGridViewTextBoxColumn Fechadesde;
        private System.Windows.Forms.DataGridViewTextBoxColumn Fechahasta;
        private System.Windows.Forms.TextBox tbContenido3;
        private System.Windows.Forms.CheckBox cbContenido3;
        private System.Windows.Forms.TextBox tbContenido2;
        private System.Windows.Forms.CheckBox cbContenido2;
    }
}