﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Archivo
{
    public partial class frmReimpresion : Form
    {
        frmImprimeEtiquetas frmetiq;
        frmImprimePlanilla frmplan;

        public frmReimpresion()
        {
            InitializeComponent();
        }

        private void frmReimpresion_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'archivoDataSet.detallesoperacion' table. You can move, or remove it, as needed.
            this.detallesoperacionTableAdapter.Fill(this.archivoDataSet.detallesoperacion);
            // TODO: This line of code loads data into the 'archivoDataSet.operaciones' table. You can move, or remove it, as needed.
            this.operacionesTableAdapter.Fill(this.archivoDataSet.operaciones);
            // TODO: This line of code loads data into the 'archivoDataSet.clientes' table. You can move, or remove it, as needed.
            this.clientesTableAdapter.Fill(this.archivoDataSet.clientes);
            // TODO: This line of code loads data into the 'archivoDataSet.cajas' table. You can move, or remove it, as needed.
            this.cajasTableAdapter.Fill(this.archivoDataSet.cajas);

            for (int i = 0; i <= archivoDataSet.clientes.Rows.Count - 1; i++)
            {
                if (lbNombreEmpresa.FindString(clientesDataGridView.Rows[i].Cells[1].Value.ToString()) == -1)
                    lbNombreEmpresa.Items.Add(clientesDataGridView.Rows[i].Cells[1].Value.ToString());
            }
            tbNombreClienteFiltro.AutoCompleteCustomSource.Clear();
            foreach (object o in lbNombreEmpresa.Items)
                tbNombreClienteFiltro.AutoCompleteCustomSource.Add(o.ToString());


        }

        private void cbNoOp_CheckedChanged(object sender, EventArgs e)
        {
            if (cbNoOp.Checked)
                tbNoOpFiltro.Enabled = true;
            else
                tbNoOpFiltro.Enabled = false;
        }

        private void cbCodigo_CheckedChanged(object sender, EventArgs e)
        {
            if (cbCodigo.Checked)
                tbCodigoFiltro.Enabled = true;
            else
                tbCodigoFiltro.Enabled = false;
        }

        private void cbNoCliente_CheckedChanged(object sender, EventArgs e)
        {
            if (cbNoCliente.Checked)
            {
                tbNoClienteFiltro.Enabled = true;
                cbNombreCliente.Checked = false;
            }
            else
            {
                tbNoClienteFiltro.Enabled = false;
            }
        }

        private void cbNombreCliente_CheckedChanged(object sender, EventArgs e)
        {
            if (cbNombreCliente.Checked)
            {
                tbNombreClienteFiltro.Enabled = true;
                cbNoCliente.Checked = false;
            }
            else
                tbNombreClienteFiltro.Enabled = false;
        }

        private void cbFecha_CheckedChanged(object sender, EventArgs e)
        {
            if (cbFecha.Checked)
            {
                dtFechaDesde.Enabled = true;
                dtFechaHasta.Enabled = true;
            }
            else
            {
                dtFechaDesde.Enabled = false;
                dtFechaHasta.Enabled = false;
            }
        }

        private void cbTipo_CheckedChanged(object sender, EventArgs e)
        {
            if (cbTipo.Checked)
                cmbTipo.Enabled = true;
            else
                cmbTipo.Enabled = false;
        }

        private void btnFiltrar_Click(object sender, EventArgs e)
        {
            DataView dv = archivoDataSet.operaciones.DefaultView;
            string filter = "Nocliente > 0";
            dv.RowFilter = filter;      // Carga todas las operaciones
            operacionesBindingSource.DataSource = dv;
            if (cbNombreCliente.Checked && tbNombreClienteFiltro.Text != "")
            {
                DataView dv2 = archivoDataSet.clientes.DefaultView;
                string filter2 = "Nocliente > 0";
                dv2.RowFilter = filter2;
                clientesBindingSource.DataSource = dv2;   // Carga todos los clientes
                filter2 = "NombreEmpresa LIKE '*" + tbNombreClienteFiltro.Text + "*'";
                dv2.RowFilter = filter2;
                clientesBindingSource.DataSource = dv2;   // Deja solo la fila(s) del cliente
                string nocliente2 = clientesDataGridView.Rows[0].Cells[0].Value.ToString();   // Extrae el Nº de cliente
                filter = "Nocliente = " + nocliente2;   // filtra operaciones
            }
            if (cbNoCliente.Checked && tbNoClienteFiltro.Text != "")
                filter = "Nocliente = " + tbNoClienteFiltro.Text;
            if (cbCodigo.Checked && tbCodigoFiltro.Text != "")
                filter += " AND Codigo = '" + tbCodigoFiltro.Text+"'";
            if (cbNoOp.Checked && tbNoOpFiltro.Text != "")
                filter += " AND NoOp = " + tbNoOpFiltro.Text;
            if (cbFecha.Checked)
                filter += " AND Fecha >= '" + dtFechaDesde.Value.ToShortDateString() + " 00:00:00'  AND Fecha <= '" + dtFechaHasta.Value.ToShortDateString()+" 23:59:59'";
            if(cbTipo.Checked)
                    filter+=" AND Tipo LIKE '*" + cmbTipo.Text + "*'";



            dv.RowFilter = filter;
            operacionesBindingSource.DataSource = dv;

        }

        private void btnimprimir_Click(object sender, EventArgs e)
        {
            string strcodigo=operacionesDataGridView.SelectedRows[0].Cells[4].Value.ToString();
            string comando = Application.StartupPath + "\\Zint.exe ";
            string argumentos = "--notext -o barras.png -d " + strcodigo;
            System.Diagnostics.Process proc = new System.Diagnostics.Process();
            proc.StartInfo.FileName = comando;
            proc.StartInfo.Arguments = argumentos;
            proc.Start();
            proc.WaitForExit();
            System.IO.FileStream fs;
            fs = new System.IO.FileStream(Application.StartupPath + "\\barras.png", System.IO.FileMode.Open, System.IO.FileAccess.Read);
            pctbarras.Image = Image.FromStream(fs);
            fs.Close();

            string tipooperacion;
            tipooperacion = operacionesDataGridView.SelectedRows[0].Cells[1].Value.ToString();

            DateTime fecha = (DateTime)operacionesDataGridView.SelectedRows[0].Cells[2].Value;

            DataView dv = archivoDataSet.clientes.DefaultView;
            string filter = "Nocliente > 0";
            dv.RowFilter = filter;
            clientesBindingSource.DataSource = dv;   // Carga todos los clientes
            filter = "Nocliente = " + operacionesDataGridView.SelectedRows[0].Cells[3].Value.ToString();
            dv.RowFilter = filter;
            clientesBindingSource.DataSource = dv;   // Deja solo la fila(s) del cliente
            string strnombrecliente = clientesDataGridView.Rows[0].Cells[1].Value.ToString();

            DataView dv2 = archivoDataSet.cajas.DefaultView;
            string filter2 = "Nocliente > 0";
            dv2.RowFilter = filter2;
            cajasBindingSource.DataSource = dv2;      // Carga todas las cajas
            filter2 = "Codigo = '" + strcodigo + "'";
            dv2.RowFilter = filter2;
            cajasBindingSource.DataSource = dv2;  // Deja solo la caja con el codigo a imprimir
            string strcontenido;
            if (cajasDataGridView.RowCount != 0)
                strcontenido = cajasDataGridView.Rows[0].Cells[5].Value.ToString();
            else
                strcontenido = "";


            tipooperacion = operacionesDataGridView.SelectedRows[0].Cells[1].Value.ToString();
            if (tipooperacion == "Ingreso")
            {
                frmplan=new frmImprimePlanilla("ingreso",strcodigo,strnombrecliente,strcontenido,fecha);
                frmplan.ShowDialog(this);
                return;
            }
            if (tipooperacion == "Retiro para Consulta")
            {
                frmplan = new frmImprimePlanilla("retiro", strcodigo, strnombrecliente, strcontenido, fecha);
                frmplan.ShowDialog(this);
                return;
            }
            if (tipooperacion == "Reingreso de Caja Consultada")
            {
                frmplan = new frmImprimePlanilla("reingreso", strcodigo, strnombrecliente, strcontenido, fecha);
                frmplan.ShowDialog(this);
                return;
            }
            if (tipooperacion == "Baja")
            {
                frmplan = new frmImprimePlanilla("baja", strcodigo, strnombrecliente, strcontenido, fecha);
                frmplan.ShowDialog(this);
                return;
            }
            if (tipooperacion.Substring(5) == "Entrega de Cajas")
            {
                string cantcajas = tipooperacion.Substring(0, 4);
                int cantcajasint = Convert.ToInt32(cantcajas);

                frmplan = new frmImprimePlanilla("entrega", "", strnombrecliente, cantcajasint.ToString(), fecha);
                frmplan.ShowDialog(this);
                return;
            }
            if (tipooperacion == "Búsqueda de documento")
            {
                DataView dv3 = archivoDataSet.detallesoperacion.DefaultView;
                string filter3 = "NoOp > 0";
                dv3.RowFilter = filter3;
                detallesoperacionBindingSource.DataSource = dv3;
                filter3 = "NoOp = " + operacionesDataGridView.SelectedRows[0].Cells["NoOp"].Value.ToString();
                dv3.RowFilter = filter3;
                string strdescripcion = operacionesDataGridView.SelectedRows[0].Cells["Descdoc"].Value.ToString();
                frmplan = new frmImprimePlanilla("busquedadoc", strcodigo, strnombrecliente, strdescripcion, fecha);
                frmplan.ShowDialog(this);
                return;
            }

            
        }







        private void button1_Click(object sender, EventArgs e)
        {
            frmetiq = new frmImprimeEtiquetas(operacionesDataGridView.SelectedRows[0].Cells[4].Value.ToString());
            frmetiq.ShowDialog(this);
        }



    }
}
