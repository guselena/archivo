﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Archivo
{
    public partial class frmlistado : Form
    {
        int paginasimpresas;
        public frmlistado()
        {
            InitializeComponent();
        }

        private void cajasBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.cajasBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.archivoDataSet);

        }

        private void frmlistado_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'archivoDataSet.clientes' table. You can move, or remove it, as needed.
            this.clientesTableAdapter.Fill(this.archivoDataSet.clientes);
            // TODO: This line of code loads data into the 'archivoDataSet.cajas' table. You can move, or remove it, as needed.
            this.cajasTableAdapter.Fill(this.archivoDataSet.cajas);

            for (int i = 0; i <= archivoDataSet.clientes.Rows.Count - 1; i++)
            {
                if (lbNombreEmpresa.FindString(clientesDataGridView.Rows[i].Cells[1].Value.ToString()) == -1)
                    lbNombreEmpresa.Items.Add(clientesDataGridView.Rows[i].Cells[1].Value.ToString());
            }
            tbNombreClienteFiltro.AutoCompleteCustomSource.Clear();
            foreach (object o in lbNombreEmpresa.Items)
                tbNombreClienteFiltro.AutoCompleteCustomSource.Add(o.ToString());

        }

        private void btnFiltrar_Click(object sender, EventArgs e)
        {
            DataView dv = archivoDataSet.operaciones.DefaultView;
            string filter = "Nocliente > 0";
            dv.RowFilter = filter;      // Carga todas las operaciones
            cajasBindingSource.DataSource = dv;
            if (cbNombreCliente.Checked && tbNombreClienteFiltro.Text != "")
            {
                DataView dv2 = archivoDataSet.clientes.DefaultView;
                string filter2 = "Nocliente > 0";
                dv2.RowFilter = filter2;
                clientesBindingSource.DataSource = dv2;   // Carga todos los clientes
                filter2 = "NombreEmpresa LIKE '*" + tbNombreClienteFiltro.Text + "*'";
                dv2.RowFilter = filter2;
                clientesBindingSource.DataSource = dv2;   // Deja solo la fila(s) del cliente
                string nocliente2 = clientesDataGridView.Rows[0].Cells[0].Value.ToString();   // Extrae el Nº de cliente
                filter = "Nocliente = " + nocliente2;   // filtra operaciones
            }
            if (cbNoCliente.Checked && tbNoClienteFiltro.Text != "")
            {
                DataView dv2 = archivoDataSet.clientes.DefaultView;
                string filter2 = "Nocliente > 0";
                dv2.RowFilter = filter2;
                clientesBindingSource.DataSource = dv2;   // Carga todos los clientes
                filter2 = "Nocliente = " + tbNoClienteFiltro.Text;
                dv2.RowFilter = filter2;
                clientesBindingSource.DataSource = dv2;   // Deja solo la fila(s) del cliente
                filter = "Nocliente = " + tbNoClienteFiltro.Text;
            }

            DataView dv3 = archivoDataSet.cajas.DefaultView;
            string filter3 = "Nocliente > 0";
            if (cbNoCliente.Checked || cbNombreCliente.Checked) // Se filtro por Nº de cliente o por nombre de cliente?
                filter3 = filter;
            if (cbFecha.Checked)
            {
                string fechadesde = dtFechaDesde.Value.ToShortDateString();
                string fechahasta = dtFechaHasta.Value.ToShortDateString();
                filter3 += " AND `Fecha ingreso` >= '" + fechadesde + " 00:00:00' AND `Fecha ingreso` <= '" + fechahasta+" 23:59:59'";
            }

            dv3.RowFilter = filter3;


                cajasBindingSource.DataSource = dv3; // Se filtran cajas para calcular cantidad total;




            if (cbFecha.Checked)
                filter += " AND Fecha >= '" + dtFechaDesde.Value.ToShortDateString() + " 00:00:00'  AND Fecha <= '" + dtFechaHasta.Value.ToShortDateString() + " 23:59:59'";



/*            dv.RowFilter = filter;
            cajasBindingSource.DataSource = dv;*/
            cajasDataGridView.Sort(cajasDataGridView.Columns[0], ListSortDirection.Ascending);

            lbltotalcajas.Text = "Cantidad de cajas: " + cajasDataGridView.RowCount.ToString();


        }

        private void cbNoCliente_CheckedChanged(object sender, EventArgs e)
        {
            if (cbNoCliente.Checked)
            {
                tbNoClienteFiltro.Enabled = true;
                cbNombreCliente.Checked = false;
            }
            else
            {
                tbNoClienteFiltro.Enabled = false;
            }
        }

        private void cbNombreCliente_CheckedChanged(object sender, EventArgs e)
        {
            if (cbNombreCliente.Checked)
            {
                tbNombreClienteFiltro.Enabled = true;
                cbNoCliente.Checked = false;
            }
            else
                tbNombreClienteFiltro.Enabled = false;
        }

        private void cbFecha_CheckedChanged(object sender, EventArgs e)
        {
            if (cbFecha.Checked)
            {
                dtFechaDesde.Enabled = true;
                dtFechaHasta.Enabled = true;
            }
            else
            {
                dtFechaDesde.Enabled = false;
                dtFechaHasta.Enabled = false;
            }
        }

        private void btnimprimir_Click(object sender, EventArgs e)
        {
            btnFiltrar_Click(this, e);
            cajasDataGridView.Sort(cajasDataGridView.Columns["Fechaingreso"],ListSortDirection.Ascending);
            printDialog1.Document = printDocument1;
            if (printDialog1.ShowDialog() == DialogResult.OK)
            {
                paginasimpresas = 0;
                printDocument1.Print();
            }
        }

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            Pen penlinea = new Pen(Color.Black, 0.5F);
            e.Graphics.PageUnit = GraphicsUnit.Millimeter;
            Graphics Canvas = e.Graphics;
            Canvas.PageUnit = GraphicsUnit.Millimeter;
            float dpiCorrectionX = Canvas.DpiX / 100;
            float dpiCorrectionY = Canvas.DpiY / 100;
            Point[] Points = new Point[2];


            Points[0] = new Point(Convert.ToInt32(e.PageBounds.X * dpiCorrectionX), Convert.ToInt32(e.PageBounds.Y * dpiCorrectionY));
            Points[1] = new Point(Convert.ToInt32(e.PageBounds.Width * dpiCorrectionX), Convert.ToInt32(e.PageBounds.Height * dpiCorrectionY));
            Canvas.TransformPoints(System.Drawing.Drawing2D.CoordinateSpace.Page, System.Drawing.Drawing2D.CoordinateSpace.Device, Points);
            Rectangle Bounds = new Rectangle(Points[0].X, Points[0].Y, Points[1].X - 20, Points[1].Y - 20);
            float mizq = Bounds.Left;
            float msup = Bounds.Top;
            float anchopag = Bounds.Width;
            float altopag = Bounds.Height;
            Font letratitulo = new Font("Arial", 23);
            Font letracodigo = new Font("Arial", 12);
            Font letradatobold = new Font("Arial", 12, FontStyle.Bold);


            int cantcajasporpag = 46;        // cantidad de operaciones que entran en una pagina
            int cantcajastotal = cajasDataGridView.RowCount;  // cantidad de operaciones (entran 35 por pagina)
            int cantpaginas = ((int)((cantcajastotal - 1) / cantcajasporpag)) + 1; // calcula la cantidad de paginas requeridas
            int primeraop = paginasimpresas * cantcajasporpag;      // primera pagina: fila 0, segunda: fila 35, etc.
            int cantoperaimprimir = 0;
            if (paginasimpresas + 1 == cantpaginas)   // es la ultima pagina?
                cantoperaimprimir = cantcajastotal - primeraop;  // si
            else
                cantoperaimprimir = cantcajasporpag;                         // no
            e.Graphics.DrawImage(pctlogo.Image, mizq, msup, 89, 28);
            e.Graphics.DrawString("Listado de Cajas", letratitulo, Brushes.Black, mizq + 94, msup);

            float msuptitulo = msup + 11;
            e.Graphics.DrawString(lbltotalcajas.Text, letradatobold, Brushes.Black, mizq + 94, msuptitulo);
            msuptitulo += 5;
            if (cbNoCliente.Checked || cbNombreCliente.Checked)
            {
                e.Graphics.DrawString("Cliente: " + clientesDataGridView.Rows[0].Cells[1].Value.ToString(), letradatobold, Brushes.Black, mizq + 94, msuptitulo);
                msuptitulo += 5;
            }
            if (cbFecha.Checked)
            {
                e.Graphics.DrawString("Fecha: del " + dtFechaDesde.Value.ToShortDateString() + " al " + dtFechaHasta.Value.ToShortDateString(), letradatobold, Brushes.Black, mizq + 94, msuptitulo);
                msuptitulo += 5;
            }
/*            if (cbTipo.Checked && cbTipo.Text != "")
            {
                e.Graphics.DrawString("Tipo de Operación: " + cmbTipo.Text, letradatobold, Brushes.Black, mizq + 94, msuptitulo);
            }*/

            Pen pentabla = new Pen(Brushes.Black, 0.5F);
            Pen pentablainf = new Pen(Brushes.Red, 0.5F);
            /*
            e.Graphics.DrawLine(pentabla,mizq+14,msup+32,mizq+181,msup+32);
            e.Graphics.DrawLine(pentablainf, mizq + 14, msup + 37, mizq + 181, msup + 37);
            */
            e.Graphics.DrawLine(pentabla, mizq + 6, msup + 32, mizq + 189, msup + 32);
            e.Graphics.DrawLine(pentablainf, mizq + 6, msup + 37, mizq + 189, msup + 37);

            e.Graphics.DrawString("Código de Caja", letradatobold, Brushes.Black, mizq + 7, msup + 32);
            e.Graphics.DrawString("Fecha ing.", letradatobold, Brushes.Black, mizq + 58, msup + 32);
            e.Graphics.DrawString("Contenido", letradatobold, Brushes.Black, mizq + 83, msup + 32);


            for (int i = 1; i <= cantoperaimprimir; i++)
            {
                e.Graphics.DrawString(cajasDataGridView.Rows[i + (cantcajasporpag * paginasimpresas) - 1].Cells["Codigo"].Value.ToString(), letracodigo, Brushes.Black, mizq + 7, msup + 33 + 5 * i);
                if (cajasDataGridView.Rows[i + (cantcajasporpag * paginasimpresas) - 1].Cells["Fechaingreso"].Value != DBNull.Value)
                    e.Graphics.DrawString(((DateTime)cajasDataGridView.Rows[i + (cantcajasporpag * paginasimpresas) - 1].Cells["Fechaingreso"].Value).ToShortDateString(), letracodigo, Brushes.Black, mizq + 58, msup + 33 + 5 * i);
                string contstr = cajasDataGridView.Rows[i + (cantcajasporpag * paginasimpresas) - 1].Cells["Contenido"].Value.ToString();
                if(contstr.IndexOf('\r')!=-1)
                    contstr=contstr.Substring(0,contstr.IndexOf('\r'))+"...";
                if(contstr.Length>=50)
                    contstr=contstr.Substring(0,47)+"...";
                e.Graphics.DrawString(contstr, letracodigo, Brushes.Black, mizq + 83, msup + 33 + 5 * i);

            }

            e.Graphics.DrawString("Página " + (paginasimpresas + 1).ToString() + "/" + cantpaginas.ToString(), letradatobold, Brushes.Black, mizq + 90, msup + 275);





            paginasimpresas++;
            if (paginasimpresas == cantpaginas)
                e.HasMorePages = false;
            else
                e.HasMorePages = true;


        }




    }
}
