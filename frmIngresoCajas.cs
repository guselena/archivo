﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Archivo
{
    public partial class frmIngresoCajas : Form
    {
        frmImprimeEtiquetas frmimp;
        frmImprimePlanilla frmplan;


        public frmIngresoCajas()
        {
            InitializeComponent();
        }

        private void IngresoCajas_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'archivoDataSet.operaciones' table. You can move, or remove it, as needed.
            this.operacionesTableAdapter.Fill(this.archivoDataSet.operaciones);
            // TODO: This line of code loads data into the 'archivoDataSet.clientes' table. You can move, or remove it, as needed.
            this.clientesTableAdapter.Fill(this.archivoDataSet.clientes);
            // TODO: This line of code loads data into the 'archivoDataSet.cajas' table. You can move, or remove it, as needed.
            this.cajasTableAdapter.Fill(this.archivoDataSet.cajas);

            for (int i = 0; i <= archivoDataSet.clientes.Rows.Count - 1; i++)
            {
                if (lbNombreEmpresa.FindString(clientesDataGridView.Rows[i].Cells[1].Value.ToString()) == -1)
                    lbNombreEmpresa.Items.Add(clientesDataGridView.Rows[i].Cells[1].Value.ToString());
            }
            txtNombreEmpresa.AutoCompleteCustomSource.Clear();
            foreach (object o in lbNombreEmpresa.Items)
                txtNombreEmpresa.AutoCompleteCustomSource.Add(o.ToString());


        }




        private void actualizacodigo()
        {
            tbcodigo.Text = Convert.ToInt32(this.tbNocliente.Text).ToString("D4") + ((int)udNolote.Value).ToString("D4") + ((int)udNoCaja.Value).ToString("D4")
                + ((int)udNoFila.Value).ToString("D2") + ((int)udNoRack.Value).ToString("D2") + ((int)udNoPiso.Value).ToString("D1")
                + ((int)udPos.Value).ToString("D2");
        }


        private void btnSigLote_Click(object sender, EventArgs e)
        {
            DataView dv = archivoDataSet.cajas.DefaultView;
            string filter = "Nocliente > '0'";  // Carga todos los registros
            dv.RowFilter = filter;
            cajasBindingSource.DataSource = dv;
            filter = "Nocliente =  '" + tbNocliente.Text + "'"; // Carga solo los de este cliente
            dv.RowFilter = filter;
            if (cajasDataGridView.RowCount == 0)
                udNolote.Value = 1;
            else
            {
                cajasDataGridView.Sort(cajasDataGridView.Columns[2], ListSortDirection.Ascending); // Ordena en orden ascendente
                udNolote.Value = Convert.ToDecimal(cajasDataGridView.Rows[cajasDataGridView.RowCount - 1].Cells[2].Value);
            }
        }

        private void btnsigcaja_Click(object sender, EventArgs e)
        {
            DataView dv = archivoDataSet.cajas.DefaultView;
            string filter = "Nocliente > '0'";  // Carga todos los registros
            dv.RowFilter = filter;
            cajasBindingSource.DataSource = dv;
            filter = "Nocliente =  '" + tbNocliente.Text + "' AND Lote = '" + udNolote.Value.ToString()+"'"; // Carga solo los de este lote de este cliente
            dv.RowFilter = filter;
            if (cajasDataGridView.RowCount == 0)
                udNoCaja.Value = 1;
            else
            {
                cajasDataGridView.Sort(cajasDataGridView.Columns[7], ListSortDirection.Ascending); // Ordena en orden ascendente
                udNoCaja.Value = Convert.ToDecimal(cajasDataGridView.Rows[cajasDataGridView.RowCount - 1].Cells[7].Value)+1;
            }
        }

        private void btnSigPos_Click(object sender, EventArgs e)
        {
            DataView dv = archivoDataSet.cajas.DefaultView;
            string filter = "Nocliente>'0'";
            dv.RowFilter = filter;    // Setea el filtro
            cajasBindingSource.DataSource = dv;
            string ubicabuscar;
            int filaactual = (int)udNoFila.Value;
            int rackactual = (int)udNoRack.Value;
            int pisoactual = (int)udNoPiso.Value;
            int posactual = (int)udPos.Value;
            for (int i = filaactual; i <= Comun.cantfilas; i++) // Busca para cada fila
            {
                for (int j = rackactual; j <= Comun.cantracks[i - 1]; j++)  // Busca para cada rack en esa fila
                {
                    for (int k = pisoactual; k <= Comun.cantpisos; k++) // busca para cada piso en ese rack
                    {
                        for (int l = posactual; l <= Comun.cantcajasxpiso; l++)    // Busca para cada lugar en ese piso
                        {
                            filter = "Nocliente> '0'";    // carga todos los registros
                            dv.RowFilter = filter;
                            ubicabuscar = i.ToString("D2") + j.ToString("D2") + k.ToString("D1") + l.ToString("D2"); // Ubicacion a buscar
                            filter = "Ubicacion = '" + ubicabuscar + "'";
                            dv.RowFilter = filter;
                            if (cajasDataGridView.RowCount == 0)   // no esta ocupado el lugar?
                            {
                                udNoFila.Value = Convert.ToDecimal(ubicabuscar.Substring(0, 2));    // carga la fila
                                udNoRack.Value = Convert.ToDecimal(ubicabuscar.Substring(2, 2));   // carga el numero de rack
                                udNoPiso.Value = Convert.ToDecimal(ubicabuscar.Substring(4, 1));   // carga el piso
                                udPos.Value = Convert.ToDecimal(ubicabuscar.Substring(5, 2));  // carga la posicion en el piso
                                filter = "Nocliente> '0'";    // carga todos los registros
                                dv.RowFilter = filter;
                                return;
                            }
                        }
                    }
                }
            }
            // No se encontro ningun hueco
            MessageBox.Show("No existe ningun hueco en el deposito. Por favor ingrese la caja en el ultimo lugar");
        }

        private void btnIngresar_Click(object sender, EventArgs e)
        {
            if (clientesDataGridView.RowCount==0)
            {
                MessageBox.Show("Por favor ingrese el cliente al que asignar la caja depositada");
                return;
            }
            DataView dv = archivoDataSet.cajas.DefaultView;
            string filter = "Nocliente = " + tbNocliente.Text;
            dv.RowFilter = filter;      // Solo las cajas de este cliente
            string plancontratado = clientesDataGridView.CurrentRow.Cells[14].Value.ToString();
            int cantcajas = cajasDataGridView.RowCount;
            if (plancontratado == "1" && cantcajas >= 50)
                MessageBox.Show("El cliente tiene contratado el plan 1,  pero cuenta con " + cantcajas.ToString() +
                    "cajas depositadas.", "Cambio de plan", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            if ((plancontratado == "1"  || plancontratado=="2" )&& cajasDataGridView.RowCount >= 100)
                MessageBox.Show("El cliente tiene contratado el plan "+plancontratado+", pero cuenta con " + cantcajas.ToString() +
                    "cajas depositadas.", "Cambio de plan", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            if ((plancontratado == "1" || plancontratado == "2" || plancontratado=="3" ) && cajasDataGridView.RowCount >= 200)
                MessageBox.Show("El cliente tiene contratado el plan "+plancontratado+", pero cuenta con " + cantcajas.ToString() +
                    "cajas depositadas.", "Cambio de plan", MessageBoxButtons.OK, MessageBoxIcon.Warning);





            filter = "Nocliente>'0'";
            dv.RowFilter = filter;    // Setea el filtro
            cajasBindingSource.DataSource = dv;
            string ubicacionstr = ((int)udNoFila.Value).ToString("D2") + ((int)udNoRack.Value).ToString("D2") + ((int)udNoPiso.Value).ToString("D1") + ((int)udPos.Value).ToString("D2");
            filter = "Ubicacion = '" + ubicacionstr + "'";
            dv.RowFilter = filter;
            if (cajasDataGridView.RowCount != 0)
            {
                MessageBox.Show("Esta ubicacion ya esta ocupada en el depósito. Por favor elija otra ubicación.");
                return;
            }
            filter="Nocliente > 0";
            dv.RowFilter=filter;
            filter = "Nocliente = '" + tbNocliente.Text + "' AND Lote = " + udNolote.Value.ToString() + " AND Nocaja = "+udNoCaja.Value.ToString();
            dv.RowFilter = filter;
            if (cajasDataGridView.RowCount != 0)
            {
                MessageBox.Show("Este Nº de caja ya está utilizado en este lote de este cliente. Por favor elija otro Nº de caja");
                return;
            }
            filter = "Nocliente > 0";
            dv.RowFilter = filter;


            if (udNoFila.Value > Comun.cantfilas)
            {
                MessageBox.Show("La fila Nº " + udNoFila.Value.ToString() + " no existe físicamente en el depósito");
                return;
            }
            if (udNoPiso.Value > Comun.cantpisos)
            {
                MessageBox.Show("El piso Nº " + udNoPiso.Value.ToString() + " no existe físicamente en el depósito");
                return;
            }
            if(udNoRack.Value>Comun.cantracks[(int)udNoFila.Value-1])
            {
                MessageBox.Show("El Rack Nº " + udNoRack.Value.ToString() + " no existe físicamente en la fila Nº "+udNoFila.Value.ToString());
                return;
            }

            actualizacodigo();

/*            int maximo;
            if (archivoDataSet.operaciones.Rows.Count != 0)
                maximo = (int)archivoDataSet.operaciones.Compute("Max(NoOp)", "");
            else
                maximo = 0;*/
            archivoDataSet.operacionesRow filanuevaop = archivoDataSet.operaciones.NewoperacionesRow();
//            filanuevaop.NoOp = maximo + 1;
            filanuevaop.Tipo = "Ingreso";
            filanuevaop.Fecha = dtFechaingreso.Value;
            filanuevaop.NoCliente = Convert.ToInt32(tbNocliente.Text);
            filanuevaop.Codigo = tbcodigo.Text;
            filanuevaop.Traslado = cbTraslado.Checked;

            archivoDataSet.operaciones.Rows.Add(filanuevaop);
            operacionesBindingSource.MoveLast();

            archivoDataSet.cajasRow filanueva = archivoDataSet.cajas.NewcajasRow();
            filanueva.Codigo = tbcodigo.Text;
            filanueva.Codigoint = Convert.ToInt64(tbcodigo.Text);
            filanueva.Nocliente = tbNocliente.Text;
            filanueva.Lote = Convert.ToInt32(udNolote.Value);
            filanueva.Fecha_ingreso = dtFechaingreso.Value;
            filanueva.Enconsulta= false;
            filanueva.Contenido = tbcontenido.Text;
            filanueva.Ubicacion = ((int)udNoFila.Value).ToString("D2") + ((int)udNoRack.Value).ToString("D2") + ((int)udNoPiso.Value).ToString("D1") + ((int)udPos.Value).ToString("D2");
            filanueva.Nocaja = (int)udNoCaja.Value;
            if (tbNoDesde.Text != "" && tbNoHasta.Text != "")
            {
                if (Convert.ToInt32(tbNoDesde.Text) > Convert.ToInt32(tbNoHasta.Text))
                {
                    MessageBox.Show("El número desde debe ser <= al número hasta");
                    return;
                }
                else
                {
                    filanueva.Numdesde = Convert.ToInt32(tbNoDesde.Text);
                    filanueva.Numhasta = Convert.ToInt32(tbNoHasta.Text);
                }
            }
            if (cbFecha.Checked)
            {
                if (dtFechaDesde.Value > dtFechaHasta.Value)
                {
                    MessageBox.Show("La fecha desde debe ser <= a la fecha hasta");
                    return;
                }
                else
                {
                    filanueva.Fechadesde = dtFechaDesde.Value;
                    filanueva.Fechahasta = dtFechaHasta.Value;
                }
            }




            archivoDataSet.cajas.Rows.Add(filanueva);
            this.Validate();
            this.clientesBindingSource.EndEdit();
            this.cajasBindingSource.EndEdit();
            this.operacionesBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.archivoDataSet);
            string comando = Application.StartupPath + "\\Zint.exe ";
            string argumentos = "--notext -o barras.png -d " + tbcodigo.Text;
            System.Diagnostics.Process proc = new System.Diagnostics.Process();
            proc.StartInfo.FileName = comando;
            proc.StartInfo.Arguments = argumentos;
            proc.Start();
            proc.WaitForExit();
            System.IO.FileStream fs;
            fs = new System.IO.FileStream(Application.StartupPath + "\\barras.png", System.IO.FileMode.Open, System.IO.FileAccess.Read);
            pctbarras.Image = Image.FromStream(fs);
            fs.Close();
            if (MessageBox.Show("Caja Ingresada. Código: " + tbcodigo.Text + ". Desea imprimir etiqueta ahora?", "Ingreso de Cajas", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                frmimp = new frmImprimeEtiquetas(tbcodigo.Text);
                frmimp.Show(this);
            }
            if (MessageBox.Show("Desea imprimir planilla ahora?", "Ingreso de Cajas", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Imprimeplanilla();
            }
            // dtFechaingreso.Value = DateTime.Now;
        }

        private void Imprimeetiqueta()
        {
            printDialog1.Document = printDocument1;
            if (printDialog1.ShowDialog() == DialogResult.OK)
                printDocument1.Print();
        }


        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {

            Pen penlinea = new Pen(Color.Black, 1F);
            e.Graphics.PageUnit = GraphicsUnit.Millimeter;
            Graphics Canvas = e.Graphics;
            Canvas.PageUnit = GraphicsUnit.Millimeter;
            float dpiCorrectionX = Canvas.DpiX / 100;
            float dpiCorrectionY = Canvas.DpiY / 100;
            Point[] Points = new Point[2];


            Points[0] = new Point(Convert.ToInt32(e.PageBounds.X * dpiCorrectionX), Convert.ToInt32(e.PageBounds.Y * dpiCorrectionY));
            Points[1] = new Point(Convert.ToInt32(e.PageBounds.Width * dpiCorrectionX), Convert.ToInt32(e.PageBounds.Height * dpiCorrectionY));
            Canvas.TransformPoints(System.Drawing.Drawing2D.CoordinateSpace.Page, System.Drawing.Drawing2D.CoordinateSpace.Device, Points);
            Rectangle Bounds = new Rectangle(Points[0].X, Points[0].Y, Points[1].X - 20, Points[1].Y - 20);
            float mizq = Bounds.Left;
            float msup = Bounds.Top;
            float anchopag = Bounds.Width;
            float altopag = Bounds.Height;
            Font letranumeros = new Font("Arial", 20);

            //e.Graphics.DrawRectangle(penlinea, mizq, msup, 62, 29);
            e.Graphics.DrawImage(pctbarras.Image, 1, 0, 82, 17);
            e.Graphics.DrawString(tbcodigo.Text, letranumeros, Brushes.Black, 3, 18);



            e.HasMorePages = false;


        }

        private void Imprimeplanilla()
        {
            DateTime fecha = dtFechaingreso.Value;

            frmplan=new frmImprimePlanilla("ingreso",tbcodigo.Text,txtNombreEmpresa.Text,tbcontenido.Text,fecha);
            frmplan.ShowDialog(this);
            return;
        }



        private void btnultimapos_Click(object sender, EventArgs e)
        {
            DataView dv = archivoDataSet.cajas.DefaultView;
            string filter = "Nocliente > 0";
            dv.RowFilter = filter;    // Setea el filtro
            cajasBindingSource.DataSource = dv;

            cajasDataGridView.Sort(cajasDataGridView.Columns[6], ListSortDirection.Ascending);  // Ordena por ubicacion
            int ultimaubic;
            if (cajasDataGridView.RowCount != 0)
                ultimaubic = Convert.ToInt32(cajasDataGridView.Rows[cajasDataGridView.RowCount - 1].Cells[6].Value) + 1;  // Obtiene la ultima ubicacion ocupada +1
            else
                ultimaubic = 0101101;
            string ultimaubicstring = ultimaubic.ToString("D7");
            decimal NoFila = Convert.ToDecimal(ultimaubicstring.Substring(0, 2));   // extrae fila
            decimal NoRack = Convert.ToDecimal(ultimaubicstring.Substring(2, 2));   // extrae rack
            decimal NoPiso = Convert.ToDecimal(ultimaubicstring.Substring(4, 1));   // extrae piso
            decimal Pos = Convert.ToDecimal(ultimaubicstring.Substring(5, 2));      // extrae posicion en el piso
            if (Pos >= Comun.cantcajasxpiso+1) // Se lleno un piso?
            {
                Pos = 1; // Si, empezar un nuevo piso de la posicion 1
                if (NoPiso == Comun.cantpisos) // Estamos en el ultimo piso?
                {
                    NoPiso = 1;   // Si, empezar un nuevo Rack en el piso 1
                    if (NoRack == Comun.cantracks[(int)NoFila-1]) // Estamos en el ultimo rack?
                    {
                        NoRack = 1;   // Si, empezar una nueva fila en el Rack 1
                        if (NoFila == Comun.cantfilas) // Estamos en la ultima fila?
                        {
                            MessageBox.Show("La última caja ingresada ocupa el último lugar del depósito. No se pueden ingresar más cajas");
                            return;
                        }
                        else
                        {
                            NoFila++;   // No es la ultima fila, ir a la proxima fila
                        }
                    }
                    else
                    {
                        NoRack++;   // No es el ultimo rack, ir al proximo rack
                    }
                }
                else
                {
                    NoPiso++;   // No es el ultimo piso, ir al proximo piso
                }
            }
            udNoFila.Value = NoFila;
            udNoRack.Value = NoRack;
            udNoPiso.Value = NoPiso;
            udPos.Value = Pos;





            
        }


        private void udNolote_ValueChanged(object sender, EventArgs e)
        {
            actualizacodigo();

        }

        private void udNoCaja_ValueChanged(object sender, EventArgs e)
        {
            actualizacodigo();

        }

        private void udNoFila_ValueChanged(object sender, EventArgs e)
        {
            actualizacodigo();

        }

        private void udNoRack_ValueChanged(object sender, EventArgs e)
        {
            actualizacodigo();

        }

        private void udNoPiso_ValueChanged(object sender, EventArgs e)
        {
            actualizacodigo();

        }

        private void udPos_ValueChanged(object sender, EventArgs e)
        {
            actualizacodigo();

        }

        private void tbNocliente_TextChanged(object sender, EventArgs e)
        {
        }

        private void frmIngresoCajas_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return && tbNocliente.Focused)
            {
                DataView dv = archivoDataSet.clientes.DefaultView;
                string filter = "Nocliente > '0'";      // Carga todos los registros
                dv.RowFilter = filter;
                clientesBindingSource.DataSource = dv;
                filter = "Nocliente = " + tbNocliente.Text; // Busca el Nº de cliente
                if (tbNocliente.Text != "")
                {
                    dv.RowFilter = filter;
                    if (clientesDataGridView.RowCount != 0)
                        txtNombreEmpresa.Text = clientesDataGridView.CurrentRow.Cells[1].Value.ToString(); // carga el nombre de la empresa
                    actualizacodigo();
                }
            }
        }


        private void tbNocliente_Leave(object sender, EventArgs e)
        {
            DataView dv = archivoDataSet.clientes.DefaultView;
            string filter = "Nocliente > '0'";      // Carga todos los registros
            dv.RowFilter = filter;
            clientesBindingSource.DataSource = dv;
            filter = "Nocliente = " + tbNocliente.Text; // Busca el  Nº de cliente
            dv.RowFilter = filter;
            if (clientesDataGridView.RowCount != 0)
                txtNombreEmpresa.Text = clientesDataGridView.CurrentRow.Cells[1].Value.ToString();   // Carga el No de cliente en tbNocliente
            else
                MessageBox.Show("Cliente no encontrado");
        }

        private void txtNombreEmpresa_Leave(object sender, EventArgs e)
        {
            DataView dv = archivoDataSet.clientes.DefaultView;
            string filter = "Nocliente > '0'";      // Carga todos los registros
            dv.RowFilter = filter;
            clientesBindingSource.DataSource = dv;
            filter = "Nombreempresa LIKE '*" + txtNombreEmpresa.Text+ "*'"; // Busca el  Nº de cliente
            dv.RowFilter = filter;
            if (clientesDataGridView.RowCount != 0)
                tbNocliente.Text = clientesDataGridView.CurrentRow.Cells[0].Value.ToString();   // Carga el No de cliente en tbNocliente
            else
                MessageBox.Show("Cliente no encontrado");
        }

        private void cbFecha_CheckedChanged(object sender, EventArgs e)
        {
            if (cbFecha.Checked)
            {
                dtFechaDesde.Enabled = true;
                dtFechaHasta.Enabled = true;
            }
            else
            {
                dtFechaDesde.Enabled = false;
                dtFechaHasta.Enabled = false;
            }
        }



    }
}
