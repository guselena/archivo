﻿namespace Archivo
{
    partial class frmConsClientes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label noclienteLabel;
            System.Windows.Forms.Label nombre_EmpresaLabel;
            System.Windows.Forms.Label direcciónLabel;
            System.Windows.Forms.Label localidadLabel;
            System.Windows.Forms.Label cPLabel;
            System.Windows.Forms.Label telefonoLabel;
            System.Windows.Forms.Label nombrecont1Label;
            System.Windows.Forms.Label telcont1Label;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label label4;
            System.Windows.Forms.Label label5;
            System.Windows.Forms.Label label6;
            System.Windows.Forms.Label label7;
            System.Windows.Forms.Label label8;
            System.Windows.Forms.Label planLabel;
            System.Windows.Forms.Label direcciónEntregaLabel;
            System.Windows.Forms.Label cUITLabel;
            System.Windows.Forms.Label iVALabel;
            System.Windows.Forms.Label emailprincipalLabel;
            System.Windows.Forms.Label emailcont1Label;
            System.Windows.Forms.Label emailcont2Label;
            System.Windows.Forms.Label emailcont3Label;
            System.Windows.Forms.Label emailcont4Label;
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.txtNoCliente = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNomEmpresa = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.noclienteTextBox = new System.Windows.Forms.TextBox();
            this.clientesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.archivoDataSet = new Archivo.archivoDataSet();
            this.nombre_EmpresaTextBox = new System.Windows.Forms.TextBox();
            this.direcciónTextBox = new System.Windows.Forms.TextBox();
            this.localidadTextBox = new System.Windows.Forms.TextBox();
            this.cPTextBox = new System.Windows.Forms.TextBox();
            this.telefonoTextBox = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.emailcont1TextBox = new System.Windows.Forms.TextBox();
            this.telcont1TextBox = new System.Windows.Forms.TextBox();
            this.nombrecont1TextBox = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.emailcont2TextBox = new System.Windows.Forms.TextBox();
            this.telcont2TextBox = new System.Windows.Forms.TextBox();
            this.nombrecont2TextBox = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.emailcont3TextBox = new System.Windows.Forms.TextBox();
            this.telcont3TextBox = new System.Windows.Forms.TextBox();
            this.nombrecont3TextBox = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.emailcont4TextBox = new System.Windows.Forms.TextBox();
            this.telcont4TextBox = new System.Windows.Forms.TextBox();
            this.nombrecont4TextBox = new System.Windows.Forms.TextBox();
            this.lbNombreEmpresa = new System.Windows.Forms.ListBox();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.planTextBox = new System.Windows.Forms.TextBox();
            this.direcciónEntregaTextBox = new System.Windows.Forms.TextBox();
            this.cUITTextBox = new System.Windows.Forms.TextBox();
            this.iVATextBox = new System.Windows.Forms.TextBox();
            this.clientesTableAdapter = new Archivo.archivoDataSetTableAdapters.clientesTableAdapter();
            this.tableAdapterManager = new Archivo.archivoDataSetTableAdapters.TableAdapterManager();
            this.clientesDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.emailprincipalTextBox = new System.Windows.Forms.TextBox();
            this.hostingCheckBox = new System.Windows.Forms.CheckBox();
            this.hosting = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            noclienteLabel = new System.Windows.Forms.Label();
            nombre_EmpresaLabel = new System.Windows.Forms.Label();
            direcciónLabel = new System.Windows.Forms.Label();
            localidadLabel = new System.Windows.Forms.Label();
            cPLabel = new System.Windows.Forms.Label();
            telefonoLabel = new System.Windows.Forms.Label();
            nombrecont1Label = new System.Windows.Forms.Label();
            telcont1Label = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            label5 = new System.Windows.Forms.Label();
            label6 = new System.Windows.Forms.Label();
            label7 = new System.Windows.Forms.Label();
            label8 = new System.Windows.Forms.Label();
            planLabel = new System.Windows.Forms.Label();
            direcciónEntregaLabel = new System.Windows.Forms.Label();
            cUITLabel = new System.Windows.Forms.Label();
            iVALabel = new System.Windows.Forms.Label();
            emailprincipalLabel = new System.Windows.Forms.Label();
            emailcont1Label = new System.Windows.Forms.Label();
            emailcont2Label = new System.Windows.Forms.Label();
            emailcont3Label = new System.Windows.Forms.Label();
            emailcont4Label = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.clientesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.archivoDataSet)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.clientesDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // noclienteLabel
            // 
            noclienteLabel.AutoSize = true;
            noclienteLabel.Location = new System.Drawing.Point(24, 117);
            noclienteLabel.Name = "noclienteLabel";
            noclienteLabel.Size = new System.Drawing.Size(85, 13);
            noclienteLabel.TabIndex = 2;
            noclienteLabel.Text = "N° de cliente:";
            // 
            // nombre_EmpresaLabel
            // 
            nombre_EmpresaLabel.AutoSize = true;
            nombre_EmpresaLabel.Location = new System.Drawing.Point(238, 117);
            nombre_EmpresaLabel.Name = "nombre_EmpresaLabel";
            nombre_EmpresaLabel.Size = new System.Drawing.Size(143, 13);
            nombre_EmpresaLabel.TabIndex = 4;
            nombre_EmpresaLabel.Text = "Nombre de la Empresa:";
            // 
            // direcciónLabel
            // 
            direcciónLabel.AutoSize = true;
            direcciónLabel.Location = new System.Drawing.Point(43, 143);
            direcciónLabel.Name = "direcciónLabel";
            direcciónLabel.Size = new System.Drawing.Size(65, 13);
            direcciónLabel.TabIndex = 6;
            direcciónLabel.Text = "Dirección:";
            // 
            // localidadLabel
            // 
            localidadLabel.AutoSize = true;
            localidadLabel.Location = new System.Drawing.Point(42, 169);
            localidadLabel.Name = "localidadLabel";
            localidadLabel.Size = new System.Drawing.Size(65, 13);
            localidadLabel.TabIndex = 8;
            localidadLabel.Text = "Localidad:";
            // 
            // cPLabel
            // 
            cPLabel.AutoSize = true;
            cPLabel.Location = new System.Drawing.Point(687, 169);
            cPLabel.Name = "cPLabel";
            cPLabel.Size = new System.Drawing.Size(28, 13);
            cPLabel.TabIndex = 10;
            cPLabel.Text = "CP:";
            // 
            // telefonoLabel
            // 
            telefonoLabel.AutoSize = true;
            telefonoLabel.Location = new System.Drawing.Point(47, 195);
            telefonoLabel.Name = "telefonoLabel";
            telefonoLabel.Size = new System.Drawing.Size(61, 13);
            telefonoLabel.TabIndex = 12;
            telefonoLabel.Text = "Telefono:";
            // 
            // nombrecont1Label
            // 
            nombrecont1Label.AutoSize = true;
            nombrecont1Label.Location = new System.Drawing.Point(37, 22);
            nombrecont1Label.Name = "nombrecont1Label";
            nombrecont1Label.Size = new System.Drawing.Size(57, 13);
            nombrecont1Label.TabIndex = 0;
            nombrecont1Label.Text = "Nombre:";
            // 
            // telcont1Label
            // 
            telcont1Label.AutoSize = true;
            telcont1Label.Location = new System.Drawing.Point(31, 48);
            telcont1Label.Name = "telcont1Label";
            telcont1Label.Size = new System.Drawing.Size(61, 13);
            telcont1Label.TabIndex = 2;
            telcont1Label.Text = "Teléfono:";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(31, 48);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(61, 13);
            label3.TabIndex = 2;
            label3.Text = "Teléfono:";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new System.Drawing.Point(37, 22);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(57, 13);
            label4.TabIndex = 0;
            label4.Text = "Nombre:";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Location = new System.Drawing.Point(31, 48);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(61, 13);
            label5.TabIndex = 2;
            label5.Text = "Teléfono:";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Location = new System.Drawing.Point(37, 22);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(57, 13);
            label6.TabIndex = 0;
            label6.Text = "Nombre:";
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Location = new System.Drawing.Point(31, 48);
            label7.Name = "label7";
            label7.Size = new System.Drawing.Size(61, 13);
            label7.TabIndex = 2;
            label7.Text = "Teléfono:";
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Location = new System.Drawing.Point(37, 22);
            label8.Name = "label8";
            label8.Size = new System.Drawing.Size(57, 13);
            label8.TabIndex = 0;
            label8.Text = "Nombre:";
            // 
            // planLabel
            // 
            planLabel.AutoSize = true;
            planLabel.Location = new System.Drawing.Point(614, 248);
            planLabel.Name = "planLabel";
            planLabel.Size = new System.Drawing.Size(104, 13);
            planLabel.TabIndex = 20;
            planLabel.Text = "Plan Contratado:";
            // 
            // direcciónEntregaLabel
            // 
            direcciónEntregaLabel.AutoSize = true;
            direcciónEntregaLabel.Location = new System.Drawing.Point(-3, 221);
            direcciónEntregaLabel.Name = "direcciónEntregaLabel";
            direcciónEntregaLabel.Size = new System.Drawing.Size(113, 13);
            direcciónEntregaLabel.TabIndex = 22;
            direcciónEntregaLabel.Text = "Dirección Entrega:";
            // 
            // cUITLabel
            // 
            cUITLabel.AutoSize = true;
            cUITLabel.Location = new System.Drawing.Point(65, 247);
            cUITLabel.Name = "cUITLabel";
            cUITLabel.Size = new System.Drawing.Size(41, 13);
            cUITLabel.TabIndex = 24;
            cUITLabel.Text = "CUIT:";
            // 
            // iVALabel
            // 
            iVALabel.AutoSize = true;
            iVALabel.Location = new System.Drawing.Point(75, 273);
            iVALabel.Name = "iVALabel";
            iVALabel.Size = new System.Drawing.Size(33, 13);
            iVALabel.TabIndex = 26;
            iVALabel.Text = "IVA:";
            // 
            // emailprincipalLabel
            // 
            emailprincipalLabel.AutoSize = true;
            emailprincipalLabel.Location = new System.Drawing.Point(383, 195);
            emailprincipalLabel.Name = "emailprincipalLabel";
            emailprincipalLabel.Size = new System.Drawing.Size(43, 13);
            emailprincipalLabel.TabIndex = 28;
            emailprincipalLabel.Text = "Email:";
            // 
            // emailcont1Label
            // 
            emailcont1Label.AutoSize = true;
            emailcont1Label.Location = new System.Drawing.Point(367, 48);
            emailcont1Label.Name = "emailcont1Label";
            emailcont1Label.Size = new System.Drawing.Size(43, 13);
            emailcont1Label.TabIndex = 4;
            emailcont1Label.Text = "Email:";
            // 
            // emailcont2Label
            // 
            emailcont2Label.AutoSize = true;
            emailcont2Label.Location = new System.Drawing.Point(369, 48);
            emailcont2Label.Name = "emailcont2Label";
            emailcont2Label.Size = new System.Drawing.Size(43, 13);
            emailcont2Label.TabIndex = 19;
            emailcont2Label.Text = "Email:";
            // 
            // emailcont3Label
            // 
            emailcont3Label.AutoSize = true;
            emailcont3Label.Location = new System.Drawing.Point(369, 48);
            emailcont3Label.Name = "emailcont3Label";
            emailcont3Label.Size = new System.Drawing.Size(43, 13);
            emailcont3Label.TabIndex = 19;
            emailcont3Label.Text = "Email:";
            // 
            // emailcont4Label
            // 
            emailcont4Label.AutoSize = true;
            emailcont4Label.Location = new System.Drawing.Point(369, 48);
            emailcont4Label.Name = "emailcont4Label";
            emailcont4Label.Size = new System.Drawing.Size(43, 13);
            emailcont4Label.TabIndex = 19;
            emailcont4Label.Text = "Email:";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.groupBox1.Controls.Add(this.btnBuscar);
            this.groupBox1.Controls.Add(this.txtNoCliente);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtNomEmpresa);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(15, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(824, 91);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datos a Buscar";
            // 
            // btnBuscar
            // 
            this.btnBuscar.Location = new System.Drawing.Point(13, 55);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(132, 23);
            this.btnBuscar.TabIndex = 4;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // txtNoCliente
            // 
            this.txtNoCliente.Location = new System.Drawing.Point(687, 20);
            this.txtNoCliente.Name = "txtNoCliente";
            this.txtNoCliente.Size = new System.Drawing.Size(116, 21);
            this.txtNoCliente.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(596, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "N° de Cliente:";
            // 
            // txtNomEmpresa
            // 
            this.txtNomEmpresa.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtNomEmpresa.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtNomEmpresa.Location = new System.Drawing.Point(140, 20);
            this.txtNomEmpresa.Name = "txtNomEmpresa";
            this.txtNomEmpresa.Size = new System.Drawing.Size(448, 21);
            this.txtNomEmpresa.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(129, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nombre de Empresa:";
            // 
            // noclienteTextBox
            // 
            this.noclienteTextBox.BackColor = System.Drawing.Color.Wheat;
            this.noclienteTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clientesBindingSource, "Nocliente", true));
            this.noclienteTextBox.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.noclienteTextBox.Location = new System.Drawing.Point(114, 114);
            this.noclienteTextBox.Name = "noclienteTextBox";
            this.noclienteTextBox.Size = new System.Drawing.Size(116, 21);
            this.noclienteTextBox.TabIndex = 3;
            // 
            // clientesBindingSource
            // 
            this.clientesBindingSource.DataMember = "clientes";
            this.clientesBindingSource.DataSource = this.archivoDataSet;
            // 
            // archivoDataSet
            // 
            this.archivoDataSet.DataSetName = "archivoDataSet";
            this.archivoDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // nombre_EmpresaTextBox
            // 
            this.nombre_EmpresaTextBox.BackColor = System.Drawing.Color.Wheat;
            this.nombre_EmpresaTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clientesBindingSource, "NombreEmpresa", true));
            this.nombre_EmpresaTextBox.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nombre_EmpresaTextBox.Location = new System.Drawing.Point(381, 114);
            this.nombre_EmpresaTextBox.Name = "nombre_EmpresaTextBox";
            this.nombre_EmpresaTextBox.Size = new System.Drawing.Size(457, 21);
            this.nombre_EmpresaTextBox.TabIndex = 5;
            // 
            // direcciónTextBox
            // 
            this.direcciónTextBox.BackColor = System.Drawing.Color.Wheat;
            this.direcciónTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clientesBindingSource, "Direccion", true));
            this.direcciónTextBox.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.direcciónTextBox.Location = new System.Drawing.Point(114, 140);
            this.direcciónTextBox.Name = "direcciónTextBox";
            this.direcciónTextBox.Size = new System.Drawing.Size(724, 21);
            this.direcciónTextBox.TabIndex = 7;
            // 
            // localidadTextBox
            // 
            this.localidadTextBox.BackColor = System.Drawing.Color.Wheat;
            this.localidadTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clientesBindingSource, "Localidad", true));
            this.localidadTextBox.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.localidadTextBox.Location = new System.Drawing.Point(114, 166);
            this.localidadTextBox.Name = "localidadTextBox";
            this.localidadTextBox.Size = new System.Drawing.Size(565, 21);
            this.localidadTextBox.TabIndex = 9;
            // 
            // cPTextBox
            // 
            this.cPTextBox.BackColor = System.Drawing.Color.Wheat;
            this.cPTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clientesBindingSource, "CP", true));
            this.cPTextBox.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cPTextBox.Location = new System.Drawing.Point(722, 166);
            this.cPTextBox.Name = "cPTextBox";
            this.cPTextBox.Size = new System.Drawing.Size(116, 21);
            this.cPTextBox.TabIndex = 11;
            // 
            // telefonoTextBox
            // 
            this.telefonoTextBox.BackColor = System.Drawing.Color.Wheat;
            this.telefonoTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clientesBindingSource, "Telefono", true));
            this.telefonoTextBox.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.telefonoTextBox.Location = new System.Drawing.Point(114, 192);
            this.telefonoTextBox.Name = "telefonoTextBox";
            this.telefonoTextBox.Size = new System.Drawing.Size(259, 21);
            this.telefonoTextBox.TabIndex = 13;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(emailcont1Label);
            this.groupBox2.Controls.Add(this.emailcont1TextBox);
            this.groupBox2.Controls.Add(telcont1Label);
            this.groupBox2.Controls.Add(this.telcont1TextBox);
            this.groupBox2.Controls.Add(nombrecont1Label);
            this.groupBox2.Controls.Add(this.nombrecont1TextBox);
            this.groupBox2.Location = new System.Drawing.Point(15, 303);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(827, 81);
            this.groupBox2.TabIndex = 19;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Contacto 1";
            // 
            // emailcont1TextBox
            // 
            this.emailcont1TextBox.BackColor = System.Drawing.Color.Wheat;
            this.emailcont1TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clientesBindingSource, "emailcont1", true));
            this.emailcont1TextBox.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.emailcont1TextBox.Location = new System.Drawing.Point(415, 45);
            this.emailcont1TextBox.Name = "emailcont1TextBox";
            this.emailcont1TextBox.Size = new System.Drawing.Size(387, 21);
            this.emailcont1TextBox.TabIndex = 5;
            // 
            // telcont1TextBox
            // 
            this.telcont1TextBox.BackColor = System.Drawing.Color.Wheat;
            this.telcont1TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clientesBindingSource, "Telcont1", true));
            this.telcont1TextBox.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.telcont1TextBox.Location = new System.Drawing.Point(99, 45);
            this.telcont1TextBox.Name = "telcont1TextBox";
            this.telcont1TextBox.Size = new System.Drawing.Size(259, 21);
            this.telcont1TextBox.TabIndex = 3;
            // 
            // nombrecont1TextBox
            // 
            this.nombrecont1TextBox.BackColor = System.Drawing.Color.Wheat;
            this.nombrecont1TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clientesBindingSource, "Nombrecont1", true));
            this.nombrecont1TextBox.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nombrecont1TextBox.Location = new System.Drawing.Point(99, 19);
            this.nombrecont1TextBox.Name = "nombrecont1TextBox";
            this.nombrecont1TextBox.Size = new System.Drawing.Size(704, 21);
            this.nombrecont1TextBox.TabIndex = 1;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(emailcont2Label);
            this.groupBox3.Controls.Add(this.emailcont2TextBox);
            this.groupBox3.Controls.Add(this.telcont2TextBox);
            this.groupBox3.Controls.Add(this.nombrecont2TextBox);
            this.groupBox3.Controls.Add(label3);
            this.groupBox3.Controls.Add(label4);
            this.groupBox3.Location = new System.Drawing.Point(14, 389);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(827, 80);
            this.groupBox3.TabIndex = 20;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Contacto 2";
            // 
            // emailcont2TextBox
            // 
            this.emailcont2TextBox.BackColor = System.Drawing.Color.Wheat;
            this.emailcont2TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clientesBindingSource, "emailcont2", true));
            this.emailcont2TextBox.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.emailcont2TextBox.Location = new System.Drawing.Point(416, 45);
            this.emailcont2TextBox.Name = "emailcont2TextBox";
            this.emailcont2TextBox.Size = new System.Drawing.Size(387, 21);
            this.emailcont2TextBox.TabIndex = 20;
            // 
            // telcont2TextBox
            // 
            this.telcont2TextBox.BackColor = System.Drawing.Color.Wheat;
            this.telcont2TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clientesBindingSource, "Telcont2", true));
            this.telcont2TextBox.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.telcont2TextBox.Location = new System.Drawing.Point(99, 45);
            this.telcont2TextBox.Name = "telcont2TextBox";
            this.telcont2TextBox.Size = new System.Drawing.Size(259, 21);
            this.telcont2TextBox.TabIndex = 19;
            // 
            // nombrecont2TextBox
            // 
            this.nombrecont2TextBox.BackColor = System.Drawing.Color.Wheat;
            this.nombrecont2TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clientesBindingSource, "Nombrecont2", true));
            this.nombrecont2TextBox.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nombrecont2TextBox.Location = new System.Drawing.Point(99, 19);
            this.nombrecont2TextBox.Name = "nombrecont2TextBox";
            this.nombrecont2TextBox.Size = new System.Drawing.Size(704, 21);
            this.nombrecont2TextBox.TabIndex = 19;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(emailcont3Label);
            this.groupBox4.Controls.Add(this.emailcont3TextBox);
            this.groupBox4.Controls.Add(this.telcont3TextBox);
            this.groupBox4.Controls.Add(this.nombrecont3TextBox);
            this.groupBox4.Controls.Add(label5);
            this.groupBox4.Controls.Add(label6);
            this.groupBox4.Location = new System.Drawing.Point(14, 474);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(827, 79);
            this.groupBox4.TabIndex = 21;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Contacto 3";
            // 
            // emailcont3TextBox
            // 
            this.emailcont3TextBox.BackColor = System.Drawing.Color.Wheat;
            this.emailcont3TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clientesBindingSource, "emailcont3", true));
            this.emailcont3TextBox.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.emailcont3TextBox.Location = new System.Drawing.Point(416, 45);
            this.emailcont3TextBox.Name = "emailcont3TextBox";
            this.emailcont3TextBox.Size = new System.Drawing.Size(387, 21);
            this.emailcont3TextBox.TabIndex = 20;
            // 
            // telcont3TextBox
            // 
            this.telcont3TextBox.BackColor = System.Drawing.Color.Wheat;
            this.telcont3TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clientesBindingSource, "Telcont3", true));
            this.telcont3TextBox.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.telcont3TextBox.Location = new System.Drawing.Point(99, 45);
            this.telcont3TextBox.Name = "telcont3TextBox";
            this.telcont3TextBox.Size = new System.Drawing.Size(259, 21);
            this.telcont3TextBox.TabIndex = 19;
            // 
            // nombrecont3TextBox
            // 
            this.nombrecont3TextBox.BackColor = System.Drawing.Color.Wheat;
            this.nombrecont3TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clientesBindingSource, "Nombrecont3", true));
            this.nombrecont3TextBox.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nombrecont3TextBox.Location = new System.Drawing.Point(99, 19);
            this.nombrecont3TextBox.Name = "nombrecont3TextBox";
            this.nombrecont3TextBox.Size = new System.Drawing.Size(704, 21);
            this.nombrecont3TextBox.TabIndex = 19;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(emailcont4Label);
            this.groupBox5.Controls.Add(this.emailcont4TextBox);
            this.groupBox5.Controls.Add(this.telcont4TextBox);
            this.groupBox5.Controls.Add(this.nombrecont4TextBox);
            this.groupBox5.Controls.Add(label7);
            this.groupBox5.Controls.Add(label8);
            this.groupBox5.Location = new System.Drawing.Point(14, 558);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(828, 84);
            this.groupBox5.TabIndex = 22;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Contacto 4";
            // 
            // emailcont4TextBox
            // 
            this.emailcont4TextBox.BackColor = System.Drawing.Color.Wheat;
            this.emailcont4TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clientesBindingSource, "emailcont4", true));
            this.emailcont4TextBox.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.emailcont4TextBox.Location = new System.Drawing.Point(416, 45);
            this.emailcont4TextBox.Name = "emailcont4TextBox";
            this.emailcont4TextBox.Size = new System.Drawing.Size(387, 21);
            this.emailcont4TextBox.TabIndex = 20;
            // 
            // telcont4TextBox
            // 
            this.telcont4TextBox.BackColor = System.Drawing.Color.Wheat;
            this.telcont4TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clientesBindingSource, "Telcont4", true));
            this.telcont4TextBox.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.telcont4TextBox.Location = new System.Drawing.Point(100, 45);
            this.telcont4TextBox.Name = "telcont4TextBox";
            this.telcont4TextBox.Size = new System.Drawing.Size(259, 21);
            this.telcont4TextBox.TabIndex = 19;
            // 
            // nombrecont4TextBox
            // 
            this.nombrecont4TextBox.BackColor = System.Drawing.Color.Wheat;
            this.nombrecont4TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clientesBindingSource, "Nombrecont4", true));
            this.nombrecont4TextBox.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nombrecont4TextBox.Location = new System.Drawing.Point(100, 19);
            this.nombrecont4TextBox.Name = "nombrecont4TextBox";
            this.nombrecont4TextBox.Size = new System.Drawing.Size(704, 21);
            this.nombrecont4TextBox.TabIndex = 19;
            // 
            // lbNombreEmpresa
            // 
            this.lbNombreEmpresa.FormattingEnabled = true;
            this.lbNombreEmpresa.Location = new System.Drawing.Point(878, 169);
            this.lbNombreEmpresa.Name = "lbNombreEmpresa";
            this.lbNombreEmpresa.Size = new System.Drawing.Size(139, 95);
            this.lbNombreEmpresa.Sorted = true;
            this.lbNombreEmpresa.TabIndex = 22;
            // 
            // btnGuardar
            // 
            this.btnGuardar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnGuardar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardar.Location = new System.Drawing.Point(272, 658);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(332, 45);
            this.btnGuardar.TabIndex = 23;
            this.btnGuardar.Text = "Guardar Datos";
            this.btnGuardar.UseVisualStyleBackColor = false;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // planTextBox
            // 
            this.planTextBox.BackColor = System.Drawing.Color.Wheat;
            this.planTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clientesBindingSource, "Plan", true));
            this.planTextBox.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.planTextBox.Location = new System.Drawing.Point(721, 244);
            this.planTextBox.Name = "planTextBox";
            this.planTextBox.Size = new System.Drawing.Size(116, 21);
            this.planTextBox.TabIndex = 17;
            // 
            // direcciónEntregaTextBox
            // 
            this.direcciónEntregaTextBox.BackColor = System.Drawing.Color.Wheat;
            this.direcciónEntregaTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clientesBindingSource, "DireccionEntrega", true));
            this.direcciónEntregaTextBox.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.direcciónEntregaTextBox.Location = new System.Drawing.Point(114, 218);
            this.direcciónEntregaTextBox.Name = "direcciónEntregaTextBox";
            this.direcciónEntregaTextBox.Size = new System.Drawing.Size(724, 21);
            this.direcciónEntregaTextBox.TabIndex = 15;
            // 
            // cUITTextBox
            // 
            this.cUITTextBox.BackColor = System.Drawing.Color.Wheat;
            this.cUITTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clientesBindingSource, "CUIT", true));
            this.cUITTextBox.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cUITTextBox.Location = new System.Drawing.Point(113, 244);
            this.cUITTextBox.Name = "cUITTextBox";
            this.cUITTextBox.Size = new System.Drawing.Size(166, 21);
            this.cUITTextBox.TabIndex = 16;
            // 
            // iVATextBox
            // 
            this.iVATextBox.BackColor = System.Drawing.Color.Wheat;
            this.iVATextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clientesBindingSource, "IVA", true));
            this.iVATextBox.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.iVATextBox.Location = new System.Drawing.Point(113, 270);
            this.iVATextBox.Name = "iVATextBox";
            this.iVATextBox.Size = new System.Drawing.Size(233, 21);
            this.iVATextBox.TabIndex = 18;
            // 
            // clientesTableAdapter
            // 
            this.clientesTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.cajasTableAdapter = null;
            this.tableAdapterManager.clientesTableAdapter = this.clientesTableAdapter;
            this.tableAdapterManager.operacionesTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = Archivo.archivoDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // clientesDataGridView
            // 
            this.clientesDataGridView.AllowUserToAddRows = false;
            this.clientesDataGridView.AllowUserToDeleteRows = false;
            this.clientesDataGridView.AutoGenerateColumns = false;
            this.clientesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.clientesDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn13,
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewTextBoxColumn15,
            this.dataGridViewTextBoxColumn16,
            this.dataGridViewTextBoxColumn17,
            this.dataGridViewTextBoxColumn18,
            this.dataGridViewTextBoxColumn19,
            this.dataGridViewTextBoxColumn20,
            this.dataGridViewTextBoxColumn21,
            this.dataGridViewTextBoxColumn22,
            this.dataGridViewTextBoxColumn23,
            this.dataGridViewTextBoxColumn24,
            this.dataGridViewTextBoxColumn25,
            this.hosting});
            this.clientesDataGridView.DataSource = this.clientesBindingSource;
            this.clientesDataGridView.Location = new System.Drawing.Point(901, 377);
            this.clientesDataGridView.Name = "clientesDataGridView";
            this.clientesDataGridView.ReadOnly = true;
            this.clientesDataGridView.Size = new System.Drawing.Size(350, 220);
            this.clientesDataGridView.TabIndex = 28;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Nocliente";
            this.dataGridViewTextBoxColumn1.HeaderText = "Nocliente";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "NombreEmpresa";
            this.dataGridViewTextBoxColumn2.HeaderText = "NombreEmpresa";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Direccion";
            this.dataGridViewTextBoxColumn3.HeaderText = "Direccion";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "Localidad";
            this.dataGridViewTextBoxColumn4.HeaderText = "Localidad";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "CP";
            this.dataGridViewTextBoxColumn5.HeaderText = "CP";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "Telefono";
            this.dataGridViewTextBoxColumn6.HeaderText = "Telefono";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "Nombrecont1";
            this.dataGridViewTextBoxColumn7.HeaderText = "Nombrecont1";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "Telcont1";
            this.dataGridViewTextBoxColumn8.HeaderText = "Telcont1";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "Nombrecont2";
            this.dataGridViewTextBoxColumn9.HeaderText = "Nombrecont2";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "Telcont2";
            this.dataGridViewTextBoxColumn10.HeaderText = "Telcont2";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "Nombrecont3";
            this.dataGridViewTextBoxColumn11.HeaderText = "Nombrecont3";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.DataPropertyName = "Telcont3";
            this.dataGridViewTextBoxColumn12.HeaderText = "Telcont3";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.DataPropertyName = "Nombrecont4";
            this.dataGridViewTextBoxColumn13.HeaderText = "Nombrecont4";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.DataPropertyName = "Telcont4";
            this.dataGridViewTextBoxColumn14.HeaderText = "Telcont4";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.DataPropertyName = "Plan";
            this.dataGridViewTextBoxColumn15.HeaderText = "Plan";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.DataPropertyName = "DireccionEntrega";
            this.dataGridViewTextBoxColumn16.HeaderText = "DireccionEntrega";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.DataPropertyName = "CUIT";
            this.dataGridViewTextBoxColumn17.HeaderText = "CUIT";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.DataPropertyName = "IVA";
            this.dataGridViewTextBoxColumn18.HeaderText = "IVA";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn19
            // 
            this.dataGridViewTextBoxColumn19.DataPropertyName = "usuario";
            this.dataGridViewTextBoxColumn19.HeaderText = "usuario";
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn20
            // 
            this.dataGridViewTextBoxColumn20.DataPropertyName = "contrasena";
            this.dataGridViewTextBoxColumn20.HeaderText = "contrasena";
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            this.dataGridViewTextBoxColumn20.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn21
            // 
            this.dataGridViewTextBoxColumn21.DataPropertyName = "emailprincipal";
            this.dataGridViewTextBoxColumn21.HeaderText = "emailprincipal";
            this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            this.dataGridViewTextBoxColumn21.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn22
            // 
            this.dataGridViewTextBoxColumn22.DataPropertyName = "emailcont1";
            this.dataGridViewTextBoxColumn22.HeaderText = "emailcont1";
            this.dataGridViewTextBoxColumn22.Name = "dataGridViewTextBoxColumn22";
            this.dataGridViewTextBoxColumn22.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn23
            // 
            this.dataGridViewTextBoxColumn23.DataPropertyName = "emailcont2";
            this.dataGridViewTextBoxColumn23.HeaderText = "emailcont2";
            this.dataGridViewTextBoxColumn23.Name = "dataGridViewTextBoxColumn23";
            this.dataGridViewTextBoxColumn23.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn24
            // 
            this.dataGridViewTextBoxColumn24.DataPropertyName = "emailcont3";
            this.dataGridViewTextBoxColumn24.HeaderText = "emailcont3";
            this.dataGridViewTextBoxColumn24.Name = "dataGridViewTextBoxColumn24";
            this.dataGridViewTextBoxColumn24.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn25
            // 
            this.dataGridViewTextBoxColumn25.DataPropertyName = "emailcont4";
            this.dataGridViewTextBoxColumn25.HeaderText = "emailcont4";
            this.dataGridViewTextBoxColumn25.Name = "dataGridViewTextBoxColumn25";
            this.dataGridViewTextBoxColumn25.ReadOnly = true;
            // 
            // emailprincipalTextBox
            // 
            this.emailprincipalTextBox.BackColor = System.Drawing.Color.Wheat;
            this.emailprincipalTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clientesBindingSource, "emailprincipal", true));
            this.emailprincipalTextBox.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.emailprincipalTextBox.Location = new System.Drawing.Point(430, 192);
            this.emailprincipalTextBox.Name = "emailprincipalTextBox";
            this.emailprincipalTextBox.Size = new System.Drawing.Size(406, 21);
            this.emailprincipalTextBox.TabIndex = 14;
            // 
            // hostingCheckBox
            // 
            this.hostingCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.clientesBindingSource, "hosting", true));
            this.hostingCheckBox.Location = new System.Drawing.Point(690, 271);
            this.hostingCheckBox.Name = "hostingCheckBox";
            this.hostingCheckBox.Size = new System.Drawing.Size(146, 24);
            this.hostingCheckBox.TabIndex = 31;
            this.hostingCheckBox.Text = "Servicio de hosting";
            this.hostingCheckBox.UseVisualStyleBackColor = true;
            // 
            // hosting
            // 
            this.hosting.DataPropertyName = "hosting";
            this.hosting.HeaderText = "hosting";
            this.hosting.Name = "hosting";
            this.hosting.ReadOnly = true;
            // 
            // frmConsClientes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SandyBrown;
            this.ClientSize = new System.Drawing.Size(1271, 749);
            this.Controls.Add(this.hostingCheckBox);
            this.Controls.Add(emailprincipalLabel);
            this.Controls.Add(this.emailprincipalTextBox);
            this.Controls.Add(this.clientesDataGridView);
            this.Controls.Add(iVALabel);
            this.Controls.Add(this.iVATextBox);
            this.Controls.Add(cUITLabel);
            this.Controls.Add(this.cUITTextBox);
            this.Controls.Add(direcciónEntregaLabel);
            this.Controls.Add(this.direcciónEntregaTextBox);
            this.Controls.Add(planLabel);
            this.Controls.Add(this.planTextBox);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.lbNombreEmpresa);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(telefonoLabel);
            this.Controls.Add(this.telefonoTextBox);
            this.Controls.Add(cPLabel);
            this.Controls.Add(this.cPTextBox);
            this.Controls.Add(localidadLabel);
            this.Controls.Add(this.localidadTextBox);
            this.Controls.Add(direcciónLabel);
            this.Controls.Add(this.direcciónTextBox);
            this.Controls.Add(nombre_EmpresaLabel);
            this.Controls.Add(this.nombre_EmpresaTextBox);
            this.Controls.Add(noclienteLabel);
            this.Controls.Add(this.noclienteTextBox);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "frmConsClientes";
            this.Text = "Consulta y Modificación de Datos de CLientes";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.clientesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.archivoDataSet)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.clientesDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.TextBox txtNoCliente;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtNomEmpresa;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox noclienteTextBox;
        private System.Windows.Forms.TextBox nombre_EmpresaTextBox;
        private System.Windows.Forms.TextBox direcciónTextBox;
        private System.Windows.Forms.TextBox localidadTextBox;
        private System.Windows.Forms.TextBox cPTextBox;
        private System.Windows.Forms.TextBox telefonoTextBox;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox telcont1TextBox;
        private System.Windows.Forms.TextBox nombrecont1TextBox;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox telcont2TextBox;
        private System.Windows.Forms.TextBox nombrecont2TextBox;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox telcont3TextBox;
        private System.Windows.Forms.TextBox nombrecont3TextBox;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox telcont4TextBox;
        private System.Windows.Forms.TextBox nombrecont4TextBox;
        private System.Windows.Forms.ListBox lbNombreEmpresa;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.TextBox planTextBox;
        private System.Windows.Forms.TextBox direcciónEntregaTextBox;
        private System.Windows.Forms.TextBox cUITTextBox;
        private System.Windows.Forms.TextBox iVATextBox;
        private archivoDataSet archivoDataSet;
        private System.Windows.Forms.BindingSource clientesBindingSource;
        private archivoDataSetTableAdapters.clientesTableAdapter clientesTableAdapter;
        private archivoDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.DataGridView clientesDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn23;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn24;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn25;
        private System.Windows.Forms.TextBox emailcont1TextBox;
        private System.Windows.Forms.TextBox emailcont2TextBox;
        private System.Windows.Forms.TextBox emailcont3TextBox;
        private System.Windows.Forms.TextBox emailcont4TextBox;
        private System.Windows.Forms.TextBox emailprincipalTextBox;
        private System.Windows.Forms.CheckBox hostingCheckBox;
        private System.Windows.Forms.DataGridViewCheckBoxColumn hosting;
    }
}