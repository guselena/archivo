﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Archivo
{
    public partial class frmImprimePlanilla : Form
    {
        string tipoimp, cod, nomcliente, cont;
        DateTime fecha;

        
        public frmImprimePlanilla(string tipoimpresion, string codigo, string nombrecliente,string contenido, DateTime fechaop)
        {
            cod = codigo;
            tipoimp = tipoimpresion;
            nomcliente = nombrecliente;
            cont = contenido;
            fecha = fechaop;
            InitializeComponent();
        }

        private void frmImprimePlanilla_Load(object sender, EventArgs e)
        {
            printDocument1.DefaultPageSettings.PaperSize.RawKind = (int)System.Drawing.Printing.PaperKind.A4;
            printDocument1.DefaultPageSettings.Landscape = false;
            printDocument1.DefaultPageSettings.PrinterResolution.Kind = System.Drawing.Printing.PrinterResolutionKind.Low;
            if (tipoimp != "entrega")
            {
                string comando = Application.StartupPath + "\\Zint.exe ";
                string argumentos = "--notext -o barras.png -d " + cod;
                System.Diagnostics.Process proc = new System.Diagnostics.Process();
                proc.StartInfo.FileName = comando;
                proc.StartInfo.Arguments = argumentos;
                proc.Start();
                proc.WaitForExit();
                System.IO.FileStream fs;
                fs = new System.IO.FileStream(Application.StartupPath + "\\barras.png", System.IO.FileMode.Open, System.IO.FileAccess.Read);
                pctbarras.Image = Image.FromStream(fs);
                fs.Close();
            }

            printDialog1.Document = printDocument1;
            
            if (printDialog1.ShowDialog() == DialogResult.OK)
                printDocument1.Print();
            this.Close();
            return;
        }

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            string strfecha= fecha.ToShortDateString();




            Pen penlinea = new Pen(Color.Black, 0.5F);
            e.Graphics.PageUnit = GraphicsUnit.Millimeter;
            Graphics Canvas = e.Graphics;
            Canvas.PageUnit = GraphicsUnit.Millimeter;
            float dpiCorrectionX = Canvas.DpiX / 100;
            float dpiCorrectionY = Canvas.DpiY / 100;
            Point[] Points = new Point[2];


            Points[0] = new Point(Convert.ToInt32(e.PageBounds.X * dpiCorrectionX), Convert.ToInt32(e.PageBounds.Y * dpiCorrectionY));
            Points[1] = new Point(Convert.ToInt32(e.PageBounds.Width * dpiCorrectionX), Convert.ToInt32(e.PageBounds.Height * dpiCorrectionY));
            Canvas.TransformPoints(System.Drawing.Drawing2D.CoordinateSpace.Page, System.Drawing.Drawing2D.CoordinateSpace.Device, Points);
            Rectangle Bounds = new Rectangle(Points[0].X, Points[0].Y, Points[1].X - 20, Points[1].Y - 20);
            float mizq = Bounds.Left;
            float msup = Bounds.Top;
            float anchopag = Bounds.Width;
            float altopag = Bounds.Height;
            Font letratitulo = new Font("Verdana", 16);
            Font letracodigo = new Font("Verdana", 12);
            Font letradatobold = new Font("Verdana", 12, FontStyle.Bold);
            float largo;


            for (int i = 0; i <= 1; i++)
            {
                msup += i * 143;
                e.Graphics.DrawImage(pctlogo.Image, mizq+3, msup+3, 89, 28);

                if(tipoimp=="ingreso")
                    e.Graphics.DrawString("Ingreso de Caja a Depósito".ToUpper(), letratitulo, Brushes.Black, mizq + 100, msup+8);
                if(tipoimp=="retiro")
                    e.Graphics.DrawString("Retiro de Caja para Consulta".ToUpper(), letratitulo, Brushes.Black, mizq + 98, msup+8);
                if(tipoimp=="reingreso")
                    e.Graphics.DrawString("Reingreso de Caja Consultada".ToUpper(), letratitulo, Brushes.Black, mizq + 94, msup+8);
                if (tipoimp == "baja")
                    e.Graphics.DrawString("Baja de Cajas".ToUpper(), letratitulo, Brushes.Black, mizq + 126, msup+8);
                if(tipoimp=="entrega")
                    e.Graphics.DrawString("Entrega de cajas vacías", letratitulo, Brushes.Black, mizq + 110, msup+20);
                if(tipoimp=="busquedadoc")
                    e.Graphics.DrawString("Búsqueda de documento", letratitulo, Brushes.Black, mizq + 110, msup + 8);
                if(tipoimp=="busquedacaja")
                    e.Graphics.DrawString("Búsqueda de caja en depósito", letratitulo, Brushes.Black, mizq + 100, msup + 8);
                if(tipoimp=="reingresodoc")
                    e.Graphics.DrawString("Reingreso de documento a depósito", letratitulo, Brushes.Black, mizq + 100, msup + 8);
                if (tipoimp != "entrega")
                {
                    e.Graphics.DrawImage(pctbarras.Image, mizq + 110, msup + 15, 77, 10);
                    e.Graphics.DrawString(cod, letracodigo, Brushes.Black, mizq + 122, msup + 26);
                }

                Font letraform = new Font("Verdana", 8);

                string codigoform = "";
                if (tipoimp == "entrega")
                    codigoform = "F21";
                if (tipoimp == "ingreso")
                    codigoform = "F22";
                if (tipoimp == "retiro")
                    codigoform = "F23";
                if (tipoimp == "reingreso")
                    codigoform = "F24";
                if (tipoimp == "busquedadoc")
                    codigoform = "F25";
                if (codigoform != "")
                {
                    e.Graphics.DrawString(codigoform, letracodigo, Brushes.Black, mizq + mizq + 185, msup);
                }



                largo = 0;
                if(tipoimp=="ingreso")
                {
                    e.Graphics.DrawString("Fecha de Ingreso: ", letradatobold, Brushes.Black, mizq+3, msup + 34);
                    largo = e.Graphics.MeasureString("Fecha de Ingreso: ", letradatobold).Width;
                }
                if (tipoimp == "retiro")
                {
                    e.Graphics.DrawString("Fecha de Retiro: ", letradatobold, Brushes.Black, mizq+3, msup + 34);
                    largo = e.Graphics.MeasureString("Fecha de Retiro: ", letradatobold).Width;
                }
                if (tipoimp == "reingreso" || tipoimp=="reingresodoc")
                {
                    e.Graphics.DrawString("Fecha de Reingreso: ", letradatobold, Brushes.Black, mizq+3, msup + 34);
                    largo = e.Graphics.MeasureString("Fecha de Reingreso: ", letradatobold).Width;
                }
                if (tipoimp == "baja")
                {
                    e.Graphics.DrawString("Fecha de Baja: ", letradatobold, Brushes.Black, mizq+3, msup + 34);
                    largo = e.Graphics.MeasureString("Fecha de Baja: ", letradatobold).Width;
                }
                if (tipoimp == "entrega")
                {
                    e.Graphics.DrawString("Fecha de Entrega: ", letradatobold, Brushes.Black, mizq+3, msup + 34);
                    largo = e.Graphics.MeasureString("Fecha de Entrega: ", letradatobold).Width;
                }
                if (tipoimp == "busquedadoc" || tipoimp=="busquedacaja")
                {
                    e.Graphics.DrawString("Fecha: ", letradatobold, Brushes.Black, mizq + 3, msup + 34);
                    largo = e.Graphics.MeasureString("Fecha: ", letradatobold).Width;
                }



                if(fecha != Convert.ToDateTime("1/1/1911"))  // Si la fecha es 1/1/1911 (ingreso de cajas sin fecha) no imprime nada
                    e.Graphics.DrawString(strfecha, letracodigo, Brushes.Black, mizq + largo+3, msup + 34);
                e.Graphics.DrawString("Cliente: ", letradatobold, Brushes.Black, mizq+3, msup + 42);
                largo = e.Graphics.MeasureString("Cliente: ", letradatobold).Width;
                e.Graphics.DrawString(nomcliente, letracodigo, Brushes.Black, mizq + largo+3, msup + 42);

                if (tipoimp == "entrega")
                {
                    e.Graphics.DrawString("Cantidad de cajas entregadas: " + cont, letradatobold, Brushes.Black, mizq+3, msup + 50);
                }
                else
                {
                    e.Graphics.DrawString("Contenido: ", letradatobold, Brushes.Black, mizq+3, msup + 50);
                    Imprimemultiline(e.Graphics, cont, mizq+3, msup + 56, anchopag - mizq-6, letracodigo);
                }

                string nomclientecorto = nomcliente;
                if (nomclientecorto.Length > 25)
                    nomclientecorto = nomclientecorto.Substring(0, 25) + "...";

                if (tipoimp == "ingreso" || tipoimp=="reingreso" || tipoimp=="reingresodoc")
                {
                    e.Graphics.DrawString("Entregó por " + nomclientecorto + ":", letradatobold, Brushes.Black, mizq+10, msup + 105);
                    e.Graphics.DrawString("Recibió por GuardaDoc S.A.:", letradatobold, Brushes.Black, mizq + 112, msup + 105);
                }
                if (tipoimp == "retiro" || tipoimp=="baja" || tipoimp=="entrega" || tipoimp=="busquedacaja")
                {
                    e.Graphics.DrawString("Entregó por GuardaDoc S.A.:", letradatobold, Brushes.Black, mizq+10, msup + 105);
                    e.Graphics.DrawString("Recibió por " + nomclientecorto + ":", letradatobold, Brushes.Black, mizq + 112, msup + 105);
                }
                if (tipoimp == "busquedadoc")
                {
                    if (i == 0)
                    {
                        e.Graphics.DrawString("Entregó por GuardaDoc S.A.:", letradatobold, Brushes.Black, mizq + 10, msup + 105);
                        e.Graphics.DrawString("Recibió por " + nomclientecorto + ":", letradatobold, Brushes.Black, mizq + 112, msup + 105);
                    }
                    else
                    {
                        e.Graphics.DrawString("Recibió por GuardaDoc S.A.:", letradatobold, Brushes.Black, mizq + 10, msup + 105);
                        e.Graphics.DrawString("Entregó por " + nomclientecorto + ":", letradatobold, Brushes.Black, mizq + 112, msup + 105);
                    }
                }



                e.Graphics.DrawString("Firma:", letracodigo, Brushes.Black, mizq + 20, msup + 120);
                e.Graphics.DrawString("Firma:", letracodigo, Brushes.Black, mizq + 122, msup + 120);
                e.Graphics.DrawLine(penlinea, mizq + 40, msup + 124, mizq + 84, msup + 124);
                e.Graphics.DrawLine(penlinea, mizq + 142, msup + 124, mizq + 186, msup + 124);

                e.Graphics.DrawString("Aclaración:", letracodigo, Brushes.Black, mizq+10, msup + 130);
                e.Graphics.DrawString("Aclaración:", letracodigo, Brushes.Black, mizq + 112, msup + 130);
                e.Graphics.DrawLine(penlinea, mizq + 40, msup + 134, mizq + 84, msup + 134);
                e.Graphics.DrawLine(penlinea, mizq + 142, msup + 134, mizq + 186, msup + 134);


            }
        }

        
        
        
        int Imprimemultiline(Graphics g, string texto, float x, float y, float ancho, Font f)
        {
            // go line by line and draw each string
            int alturatotal = 0;
            int alturalinea;
            int arranque = 0;
            int index = texto.IndexOf("\n", arranque);
            int proximaposicion = (int)y;
            Rectangle rtLayout;
            // just use the default string format
            StringFormat sf = new StringFormat();
            while (index != -1)     // Mientras no sea la ultima linea
            {   // carga la proxima linea
                string proximalinea = texto.Substring(arranque, index - arranque);
                // calcula el alto de la linea para el ancho indicado
                alturalinea = (int)(g.MeasureString(proximalinea, f, (int)ancho).Height);

                //                alturalinea=(int)(g.MeasureString(proximalinea,f,ancho).Height);
                // produce un nuevo rectangulo para imprimir la linea
                rtLayout = new Rectangle((int)x, proximaposicion, (int)ancho, alturalinea);
                // Dibuja la linea
                g.DrawString(proximalinea, f, Brushes.Black, rtLayout, sf);
                // avanza a la proxima posicion
//                if (proximalinea[proximalinea.Length - 1] == '\r')
//                    alturatotal += 2 * alturalinea;
//                else
                    alturatotal += alturalinea;
//                proximaposicion += (int)(alturalinea + 3);
                proximaposicion += (int)(alturalinea + 1);
                arranque = index + 1;
                // busca el proximo index
                index = texto.IndexOf("\n", arranque);
            } // Dibuja la ultima linea
            string ultimalinea = texto.Substring(arranque);
            alturalinea = (int)(g.MeasureString(ultimalinea, f, (int)ancho).Height);
            rtLayout = new Rectangle((int)x, proximaposicion, (int)ancho, alturalinea);
            g.DrawString(ultimalinea, f, Brushes.Black, rtLayout, sf);
            alturatotal += alturalinea;
            return alturatotal; // finished printing
        }

    }
}
