﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Archivo
{
    public partial class frmbusqueda : Form
    {
        frmImprimePlanilla frmplan;
        public frmbusqueda()
        {
            InitializeComponent();
        }


        private void frmbusqueda_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'archivoDataSet.detallesoperacion' table. You can move, or remove it, as needed.
            this.detallesoperacionTableAdapter.Fill(this.archivoDataSet.detallesoperacion);
            // TODO: This line of code loads data into the 'archivoDataSet.operaciones' table. You can move, or remove it, as needed.
            this.operacionesTableAdapter.Fill(this.archivoDataSet.operaciones);
            // TODO: This line of code loads data into the 'archivoDataSet.clientes' table. You can move, or remove it, as needed.
            this.clientesTableAdapter.Fill(this.archivoDataSet.clientes);
            // TODO: This line of code loads data into the 'archivoDataSet.cajas' table. You can move, or remove it, as needed.
            this.cajasTableAdapter.Fill(this.archivoDataSet.cajas);

        }


        private void btnRetirar_Click(object sender, EventArgs e)
        {
/*            int maximo;
            if (archivoDataSet.operaciones.Rows.Count != 0)
                maximo = (int)archivoDataSet.operaciones.Compute("Max(NoOp)", "");
            else
                maximo = 0;*/
            archivoDataSet.operacionesRow filanuevaop = archivoDataSet.operaciones.NewoperacionesRow();
//            filanuevaop.NoOp = maximo + 1;
            if (rbdocint.Checked)
                filanuevaop.Tipo = "Búsqueda de documento";
            else if (rbbusqcaja.Checked)
                filanuevaop.Tipo = "Búsqueda de caja en depósito";
            else
                filanuevaop.Tipo = "Reingreso de documento";
            filanuevaop.Fecha = dtFechaconsulta.Value;
            filanuevaop.NoCliente = Convert.ToInt32(tbNoCliente.Text);
            filanuevaop.Codigo = tbCodigo.Text;
            filanuevaop.Traslado = cbTraslado.Checked;
            filanuevaop.Urgente = cbUrgente.Checked;

            if (filanuevaop.Tipo=="Búsqueda de documento" || filanuevaop.Tipo=="Reingreso de documento")
            {
                filanuevaop.Descdoc = tbDescripcion.Text;
                filanuevaop.Paginas = Convert.ToInt32(udPaginas.Value);
            }

            archivoDataSet.operaciones.Rows.Add(filanuevaop);
            operacionesBindingSource.MoveLast();

            this.Validate();
            operacionesBindingSource.EndEdit();
            tableAdapterManager.UpdateAll(this.archivoDataSet);
            if (MessageBox.Show("Evento ingresado. Imprimir planilla?", "Búsqueda en depósito",
                MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Imprimeplanilla();
            }
        }


        private void Imprimeplanilla()
        {
            if(rbdocint.Checked)
                frmplan = new frmImprimePlanilla("busquedadoc", tbCodigo.Text, tbNombreCiente.Text, tbDescripcion.Text, dtFechaconsulta.Value);
            else if(rbbusqcaja.Checked)
                frmplan = new frmImprimePlanilla("busquedacaja", tbCodigo.Text, tbNombreCiente.Text, tbContenido.Text, dtFechaconsulta.Value);
            else
                frmplan = new frmImprimePlanilla("reingresodoc", tbCodigo.Text, tbNombreCiente.Text, tbDescripcion.Text, dtFechaconsulta.Value);
            frmplan.Show(this);


        }


        private void frmretconsulta_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return && tbCodigo.Focused)
            {
                btnRetirar.Enabled = false;
                if (tbCodigo.Text.Length == 19)
                {
                    DataView dv = archivoDataSet.cajas.DefaultView;
                    string filter = "Nocliente > 0";
                    dv.RowFilter = filter;
                    cajasBindingSource.DataSource = dv;
                    filter = "Codigo = " + tbCodigo.Text;
                    dv.RowFilter = filter;
                    cajasBindingSource.DataSource = dv;
                    if (cajasDataGridView.RowCount == 0) // Si no encuentra el codigo
                    {
                        MessageBox.Show("La caja con el código " + tbCodigo.Text + " no se encuentra en la base de datos. ");
                        return;
                    }
                    if (Convert.ToBoolean(cajasDataGridView.Rows[0].Cells[4].Value.ToString()) == true)  // Si a está en consulta
                    {
                        MessageBox.Show("La caja con el código " + tbCodigo.Text + " figura en la base de datos como en poder del cliente para consultar."
                                        + "Por favor ingrésela nuevamente a depósito o verifique el código ingresado.");
                        return;
                    }
                    DataView dv2 = archivoDataSet.clientes.DefaultView;
                    int nocliente = Convert.ToInt32(tbCodigo.Text.Substring(0, 4));
                    filter = "Nocliente > 0";     // Carga todos los registros
                    dv2.RowFilter = filter;
                    filter = "Nocliente = " + nocliente.ToString();      // Busca el cliente
                    dv2.RowFilter = filter;
                    clientesBindingSource.DataSource = dv2;
                    if (clientesDataGridView.RowCount == 0)
                    {
                        MessageBox.Show("El cliente Nº " + nocliente.ToString() + " no existe en la base de datos.");
                        return;
                    }
                    else
                    {
                        tbNoCliente.Text = nocliente.ToString();
                        tbNombreCiente.Text = clientesDataGridView.Rows[0].Cells[1].Value.ToString();
                        tbLote.Text = Convert.ToInt32(tbCodigo.Text.Substring(4, 4)).ToString();
                        tbNocaja.Text = Convert.ToInt32(tbCodigo.Text.Substring(8, 4)).ToString();
                        tbFila.Text = Convert.ToInt32(tbCodigo.Text.Substring(12, 2)).ToString();
                        tbRack.Text = Convert.ToInt32(tbCodigo.Text.Substring(14, 2)).ToString();
                        tbPiso.Text = Convert.ToInt32(tbCodigo.Text.Substring(16, 1)).ToString();
                        tbPosicion.Text = Convert.ToInt32(tbCodigo.Text.Substring(17, 2)).ToString();
                        tbFechaIngreso.Text = cajasDataGridView.Rows[0].Cells[3].Value.ToString();
                        // dtFechaconsulta.Value = DateTime.Now;
                        btnRetirar.Enabled = true;
                        tbContenido.Text = contenidoTextBox.Text;
                    }



                }
                else
                {
                    tbNoCliente.Text = "";
                    tbNombreCiente.Text = "";
                    tbLote.Text = "";
                    tbNocaja.Text = "";
                    tbFila.Text = "";
                    tbRack.Text = "";
                    tbPiso.Text = "";
                    tbPosicion.Text = "";
                    tbFechaIngreso.Text = "";
                    tbContenido.Text = "";
                }
            }
        }





    }

}
